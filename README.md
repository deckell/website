[![pipeline status](https://gitlab.com/tripetto/website/badges/master/pipeline.svg)](https://gitlab.com/tripetto/website/commits/master)

Tripetto corporate website published at [tripetto.com](https://tripetto.com). Generated using [Jekyll](https://jekyllrb.com/).

## Prerequisites
Make sure [Ruby](https://www.ruby-lang.org/en/) is installed. Run the following command to install the required gems.

```bash
gem install jekyll bundler
```

## Local build
To test the website on your local machine run the following command inside the repository:

```bash
bundle install
bundle exec jekyll serve
```

## Deployment build
For deployment run:

```bash
bundle install
bundle exec jekyll build
```

Make sure you set the following environment variable: `JEKYLL_ENV=production`

The website is generated in the folder `./public`.
