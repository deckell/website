---
base: ../
---

<section class="features-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-lg-8 shape-before">
        <h1>Features</h1>
        <p><strong>Tripetto comes in two versions.</strong> Both are separate, full-blown Tripetto platforms with practically identical feature sets. For different users.</p>
      </div>
    </div>
  </div>
</section>
