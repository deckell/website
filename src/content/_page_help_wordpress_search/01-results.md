---
base: ../../../
---

<section class="help-list">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2><span id="search-process">Loading</span> articles <span id="search-query-container" style="display: none;">about '<strong id="search-query"></strong>'</span></h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-11 col-xl-9">
            <ul id="search-results" class="help-list"></ul>
        </div>
    </div>
  </div>
</section>
<script>
window.localStorage.setItem('tripetto_help_area', 'wordpress');
window.data = {
    {% assign ref = 0 %}
    {% assign articles = site.articles_help | where_exp: "item", "item.areas contains 'wordpress'" %}
    {% for article in articles %}
        {% if added %},{% endif %}
        {% assign added = false %}
        {% assign data_category = site.data.help-categories[article.category_id] %}
        "{{ ref }}": {
            "id": "{{ ref }}",
            "title": "{{ article.article_title | xml_escape }}",
            "time": "{{ article.time }}",
            "time_video": "{{ article.time_video }}",
            "category_pill": "{{ data_category.pill }}",
            "category_id": "{{ article.category_id }}",
            "url": "{{ article.permalink | replace_first: "/", page.base }}",
            "content": {{ article.content | strip_html | replace_regex: "[\s/\n]+"," " | strip | jsonify }}
        }
        {% assign ref = ref | plus: 1 %}
        {% assign added = true %}
    {% endfor %}
};
</script>
<script src="{{ page.base }}js/lunr-2.3.1.js?v={{ site.cache_version }}"></script>
<script src="{{ page.base }}js/search.js?v={{ site.cache_version }}"></script>
