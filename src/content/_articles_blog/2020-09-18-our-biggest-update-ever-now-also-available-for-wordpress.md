---
layout: blog-article
base: ../../
permalink: /blog/our-biggest-update-ever-now-also-available-for-wordpress/
title: Our biggest update ever, now also available for WordPress - Tripetto Blog
description: Our biggest update ever is now available in the WordPress plugin too! Including new features, improvements and new license options.
article_title: Our biggest update ever, now also available for WordPress 🥳
author: mark
time: 3
category: product-release
category_name: Product release
tags: [product-release, product, release, showcase]
---
<p>Our <a href="{{ page.base }}blog/our-biggest-update-ever-has-arrived/" target="_blank">big update</a> from a few weeks ago got tremendous positive feedback from our studio users. And now the wait for our WordPressers is also over: the full update is now available in the WP plugin, too! On top of that we're also introducing often requested license plans, like multisite licences and even a lifetime deal!</p>

<h2>The full platform update</h2>
<p>The big update of July 23<sup>th</sup> 2020 really changed the way our studio users built their forms. Of course the plan was to also get all of these features into the WordPress plugin. Even though the update is huge, we can actually be quite short about it: all new and improved features that we described in the <a href="{{ page.base }}blog/our-biggest-update-ever-has-arrived/" target="_blank">update blog</a> are now fully available for WordPress. End of discussion! 😎</p>
<p>Okay then, a quick summary:</p>
<ul>
  <li><strong><a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" target="_blank">Form faces</a></strong> - Show all your forms in an autoscroll, chat or classic form face;</li>
  <li><strong><a href="{{ page.base }}help/articles/how-to-add-a-welcome-message/" target="_blank">Welcome message</a></strong> - Show a welcome message before the form starts;</li>
  <li><strong><a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/" target="_blank">Closing message(s)</a></strong> - Show flexible closing messages, based on given answers;</li>
  <li><strong><a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/" target="_blank">Redirect</a></strong> - Redirect to a URL upon form completion;</li>
  <li><strong><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">Translations</a></strong> - Edit/translate all labels inside your form;</li>
  <li><strong><a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">Styling</a></strong> - Style your forms way better, with customizable colors, fonts, backgrounds, buttons, inputs, etc.;</li>
  <li><strong><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">Extended logic</a></strong> - Way more flexible logic conditions, behaviors and endings;</li>
  <li><strong><a href="{{ page.base }}help/articles/how-tripetto-prevents-spam-entries-from-your-forms/" target="_blank">Unique built-in SPAM protection</a></strong> - Tripetto forms have unique SPAM protection built in, so no need for CAPTCHAs or traffic light pictures;</li>
  <li><strong>New question blocks</strong> - Now use the date (and time) and telephone question block;</li>
  <li><strong>New action block</strong> - Now use the raise error block to prevent a form from submitting;</li>
  <li><strong><a href="{{ page.base }}changelog/" target="_blank">And much more...</a></strong></li>
</ul>
<figure>
  <img src="{{ page.base }}images/blog/20200916/wordpress-customizations.gif" alt="Screenshot of WordPress plugin of Tripetto" />
  <figcaption>A quick showcase of the new form faces and improved styling in the WordPress plugin.</figcaption>
</figure>

<h3>Your existing forms and entries</h3>
<p>All this is now available after you update your plugin installation. All your existing forms should work out-of-the-box after the update, but we recommend that you test your forms to be sure everything is as it should be. Form entries are maintained of course!</p>

<h3>Additional for WordPress</h3>
<p>We also improved the following WordPress specific stuff on top of the platform update:</p>
<ul>
  <li><strong><a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/" target="_blank">Shareable link</a></strong> - You can now instantly use a shareable link within your WP site to the form; without embedding it. Your WordPress instance is now a full-blown survey tool!</li>
  <li><strong><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site/" target="_blank">Shortcode editor</a></strong> - Prefer the shortcode, so you can control where and how the form is shown? You can now generate the shortcode with the right parameters in the form builder, so you don't have to code the parameters yourself.</li>
</ul>
<hr/>

<h2>About premium features</h2>
<p>There are some changes in the features for the free and premium version of the plugin:</p>
<ul>
  <li>Styling is now always and completely free for all your forms!</li>
  <li>The one free form with all premium features that you got before is not applicable anymore for new installations. If you were using Tripetto before this big update, we are very grateful for that. That's why <strong>you will keep your designated free premium form</strong>! 🙏</li>
</ul>
<hr/>

<h2>New WordPress licenses</h2>
<p>Before the update we only offered a single-site recurring license. But our users requested other license plans frequently, including support for multisite installations. So we now added support for <strong>multisite</strong> installations, divided into a 5-sites license and an unlimited sites license. And we even have a <strong>lifetime</strong> deal now for each license type.</p>
<p>From now on this is the range of our <a href="{{ page.base }}pricing/wordpress/" target="_blank">premium licenses</a>:</p>
<ul>
  <li><strong>Single-site license</strong> - Usable for one WordPress installation:
    <ul>
      <li><strong>Monthly</strong> - Recurring, billed monthly;</li>
      <li><strong>Annually</strong> - Recurring, billed annually;</li>
      <li><strong>Lifetime</strong> - Billed once.</li>
    </ul>
  </li>
  <li><strong>5-Sites license</strong> - Usable for five WordPress installations:
    <ul>
      <li><strong>Monthly</strong> - Recurring, billed monthly;</li>
      <li><strong>Annually</strong> - Recurring, billed annually;</li>
      <li><strong>Lifetime</strong> - Billed once.</li>
    </ul>
  </li>
  <li><strong>Unlimited sites license</strong> - Usable for unlimited WordPress installations:
    <ul>
      <li><strong>Monthly</strong> - Recurring, billed monthly;</li>
      <li><strong>Annually</strong> - Recurring, billed annually;</li>
      <li><strong>Lifetime</strong> - Billed once.</li>
    </ul>
  </li>
</ul>
<p>We hope you can pick the right one for you easily! Please see our <a href="{{ page.base }}pricing/wordpress/" target="_blank"><strong>WordPress pricing page</strong></a> for the license prices.</p>
<hr/>

<h2>Also updated: our website</h2>
<p>You also might have noticed our website has gotten a total make-over (just a little bit 😅). Because the recent updates have made the studio and plugin finally as good as identical feature-wise, it was the excellent opportunity for us to improve the story and feeling around Tripetto.</p>
<p>That resulted in a more human and (hopefully) better understandable story of the possibilities of Tripetto. Of course the <a href="{{ page.base }}help/wordpress/" target="_blank"><strong>help center</strong></a> remains an important part of the website, with even more help articles. New are the <a href="{{ page.base }}tutorials/" target="_blank"><strong>tutorials</strong></a> that give a quick overview of all features in Tripetto. And don't forget our new <a href="{{ page.base }}examples/" target="_blank"><strong>examples</strong></a> that give an instant experience of all the new form faces!</p>
<hr/>

<h2>What's next?</h2>
<p>We'll keep working on Tripetto. We have some additional important and frequently heard feature requests to tackle, as you can see in our <a href="{{ page.base }}roadmap/" target="_blank">roadmap</a> 🤐.</p>
