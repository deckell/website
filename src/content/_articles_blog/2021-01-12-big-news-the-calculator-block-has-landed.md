---
layout: blog-article
base: ../../
permalink: /blog/big-news-the-calculator-block-has-landed/
title: Big news! The calculator block has landed - Tripetto Blog
description: The calculator block is here! More powerful, more flexible and more intelligent than you can imagine!
article_title: Big news! The calculator block has landed 💯
author: jurgen
time: 4
category: feature-spotlight
category_name: Feature spotlight
tags: [feature-spotlight, feature, editor, blocks]
---
<p>By far the most requested feature by our users was to be able to make calculations inside their forms. And now it's here: the calculator block! More powerful, more flexible and more intelligent than you can imagine! Now available for everybody on all our platforms.</p>

<h2>A calculator in forms?</h2>
<p>Yes, a calculator in your forms! Tripetto aims to build forms and surveys that are as smart as possible. With the <strong>introduction of the <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">calculator block</a></strong> that smartness rises to a whole new level, as you now can use given answers of your respondents to perform all kinds of calculations. Think of simple <strong>additions</strong>, <strong>subtractions</strong>, <strong>multiplications</strong> and <strong>divisions</strong>, but also more advanced actions like <strong>multistep formulas</strong>, <strong>mathematical functions</strong> and <strong> constants</strong>. All in realtime, while filling out your form!</p>
<p>The calculator block fits into your forms like any other block, making it usable on any given position in your form. Without any limits on the amount of calculator blocks in your form of course. And even inside logic branches, so you can use conditional logic in combination with calculators. The possibilities really are endless 🤯!</p>

<h2>Examples</h2>
<p>We have made some examples of the possibilities we could think of, but we're sure you have even cooler ideas with the calculator:</p>
<h3>Quizzes</h3>
<p>Quizzes that give you a score based on your answers. <a href="{{ page.base }}examples/trivia-quiz/" target="_blank">Live example over here</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-block-calculator/demo-quiz.gif" alt="Screenshot of a quiz in Tripetto" />
  <figcaption>Calculate a quiz score.</figcaption>
</figure>

<h3>Order and quote forms</h3>
<p>Order/quote forms that can calculate prices based on your product selections, prices and amounts. <a href="{{ page.base }}examples/order-form/" target="_blank">Live example over here</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-block-calculator/demo-shopping.gif" alt="Screenshot of a order form in Tripetto" />
  <figcaption>Calculate prices and discounts.</figcaption>
</figure>

<h3>Form wizards with formulas</h3>
<p>Forms that feel like wizards with formulas that can calculate outcomes based on your input, for example for health checks. <a href="{{ page.base }}examples/body-mass-index-bmi-wizard/" target="_blank">Live example over here</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-block-calculator/demo-bmi.gif" alt="Screenshot of a BMI wizard in Tripetto" />
  <figcaption>Calculate a BMI with a formula.</figcaption>
</figure>

<hr/>

<h2>More information</h2>
<p>We made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<hr/>

<h2>Get started with the calculator</h2>
<p>Just start playing with the calculator block in our form builder and you'll discover how easy, yet powerful, the calculator block is! And of course our Help center is here to help you.</p>

<h3>Help center</h3>
<p>👉 <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a></p>

<h2>Now available everywhere</h2>
<p>The calculator block is now available across <a href="{{ page.base }}launch/">all our platforms</a>, so in the studio at tripetto.app and the WordPress plugin for our WordPress users. And even in our SDK for developers!</p>
<p>We also added some smaller (but handy) extensions to our logic features to make your forms even smarter. You can see all our updates in our <a href="{{ page.base }}changelog/" target="_blank">changelog</a>.</p>

<h2>What's next?</h2>
<p>If you don't mind, we will take a little drink on this big milestone for Tripetto 🍾.</p>
<p>And then: up to the next additions and improvements! We will keep collecting all your feedback to determine what we will work on next. And you can keep track of that in <a href="{{ page.base }}roadmap/" target="_blank">our roadmap</a>. So please <a href="{{ page.base }}subscribe/" target="_blank">stay tuned</a>!</p>
