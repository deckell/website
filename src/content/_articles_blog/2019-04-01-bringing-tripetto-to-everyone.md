---
layout: blog-article
base: ../../
permalink: /blog/bringing-tripetto-to-everyone/
title: Bringing Tripetto to everyone - Tripetto Blog
description: We’re launching the Tripetto App for all to create cleverly flowing forms on a self-organizing storyboard and share or embed them.
article_title: Bringing Tripetto to everyone
article_folder: 20190401
author: martijn
time: 4
category: product-release
category_name: Product release
tags: [product-release, product, release, studio]
---
<p>Enter the Tripetto Studio.</p>

<p>We’re launching the Tripetto <a href="https://tripetto.com/studio/" target="_blank">Studio</a> for all to create and deploy powerful online interactions with your audience. Build cleverly flowing forms and surveys on a self-organizing storyboard and share these with a link or embedded in a website or application. Or use Tripetto for easy composing only and then bypass it completely by self-hosting what you build and collect.</p>
<p>The Tripetto Studio is free for now. No account required to <a href="https://tripetto.app/" target="_blank">give it a try</a>.</p>

<h2>What came before</h2>
<p>We started Tripetto a couple of years ago. We have since pivoted occasionally and soft-launched various earlier versions of the software, but the core premise remains unchanged. We elaborated on it in <a href="{{ page.base }}blog/the-journey-of-tripetto/" target="_blank">The Journey of Tripetto</a> (which is actually about the <a href="https://tripetto.com/developers/" target="_blank">SDK</a> we launched for developers and used ourselves to build the studio for everyone).</p>
<p>Tripetto ultimately serves to change conventional forms to vivid and much more engaging conversations with your audience. But it couldn’t be just another SurveyMonkey, Typeform or the likes. It had to be substantially different to shift the form and survey paradigm meaningfully — and to stand a chance against notorious incumbents, quite frankly 😬.</p>
<figure class="inline">
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/editor.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Storyboard with drag-and-drop and layout guidance</figcaption>
</figure>
<ul>
  <li>Firstly, conversations require sound logic. Tripetto’s clever <strong>self-organizing storyboard</strong> triggers and simplifies the <a href="{{ page.base }}blog/forms-dont-need-to-suck/" target="_blank">use of advanced logic flows</a> (i.e. jumps, skips, branches, pipes, loops). Also, it constantly visualizes these flows in a readable and directly editable overview.</li>
  <li>Secondly, all of Tripetto had to be <strong>platform agnostic</strong> and work identically in all browsers — <strong>mouse, pen and touch. </strong>That put us at odds with default browser scrolling behavior and touch handling, especially for the 2D storyboard. Spoiler alert: We cracked that!</li>
  <li>Additionally, Tripetto must be <strong>developer-friendly</strong>. Enthusiasts should be able to independently run and extend it with custom functionality. Also, they should decide where and how to (self-)host stuff — Hello GDPR! We <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/" target="_blank">wrote about this</a> before. And so we cut things up into a handy open-source SDK for a Tripetto community to walk with and for ourselves to build on top of.</li>
</ul>

<blockquote><p>There’s obviously so much more to Tripetto than the above, but that’s all TL;DR anyway. Suffice it to say that ambitious concepts take time to translate into stable software <em>🤯</em>.</p></blockquote>

<h2>Fast forward to today</h2>
<p>With today’s launch of the <a href="https://tripetto.app/" target="_blank">studio</a> and release of the <a href="{{ page.base }}blog/a-christmas-present-from-tripetto/" target="_blank">SDK</a> last December we now have two distinct versions of Tripetto; the studio for everyone and the SDK for developers. How these relate to each other and to upcoming Tripetto releases is best explained with an illustration we use internally (and yes — it’s laughably graphic, but we feel no shame 😂).</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/roadmap.png" alt="Screenshot of the roadmap of Tripetto" />
  <figcaption>Early draft of Tripetto roadmap</figcaption>
</figure>
<p>The idea is that we’re layering stuff and serving different users at different levels on one single foundational core technology. Enhancements flow up and down the metaphoric <em>building</em>. We’re also <a href="https://docs.tripetto.com/" target="_blank">documenting</a> and <a href="https://docs.tripetto.com/examples/" target="_blank">open-sourcing</a> as much as we responsibly can, as soon as we can for the <a href="https://spectrum.chat/tripetto" target="_blank">community</a>.</p>
<p>The different levels in the Tripetto structure shown in the illustration above serve the following purposes:</p>
<ul>
  <li><strong>Level 3</strong> — Not available yet. It will hold very specific functionality on top of everything at the underlying levels.</li>
  <li><strong>Levels 2 and 1</strong> — A.k.a. the <a href="https://tripetto.app/" target="_blank">studio</a> for the <em>general public</em>. Launched early 2019 and free for now. Levels 1 and 2 were originally intended as separate products for developers and end-users respectively. But feedback from earlier releases pointed us towards a combined offering. The Publish screen in the studio shows why and how. By the way, for this release we chose to go for a conversational UX for respondents that’s very reminiscent of the avant-garde approach by Typeform, and SurveyMonkey’s spins on it. But thanks to the platform’s versatility we’ll be able to introduce additional distinctive flavors for our users to choose from.</li>
  <li><strong>Basement </strong>—A.k.a. the <a href="https://tripetto.com/developers/" target="_blank">SDK</a> for <em>developers</em>. Launched v1.0 end of 2018. It’s mostly free. We built the studio with it, and the exact same kit, <a href="https://docs.tripetto.com/" target="_blank">documentation</a>, <a href="https://docs.tripetto.com/examples/" target="_blank">examples</a> and <a href="{{ page.base }}blog/implementing-tripetto-forms-using-react/" target="_blank">tutorials</a> are available to developers as a set of Javascript libraries through <a href="https://www.npmjs.com/package/tripetto" target="_blank">npm</a> and code on <a href="https://gitlab.com/tripetto" target="_blank">GitLab</a>. Developers can integrate Tripetto tech straight into their own applications just like that. A paid <a href="https://tripetto.com/developers/" target="_blank">license</a> is only required for integrating the editor (storyboard). All other tech and resources in the kit are free no matter what.</li>
  <li><strong>Foundation</strong> — The granular magic is in here. Surgically crafted in-house completely and cut up into separate technologies for separate core purposes, working all together. Among 50+ packages are a sequencer for lightning fast UI rendering, a positioning matrix organizing the storyboard and a touch handler to take over browser scroll and touch behavior.</li>
</ul>

<hr />
<p>Please note that Tripetto is a work in progress. We are bootstrapping our efforts and work really hard to make the SDK and studio better every day. Feel free to check all of Tripetto out and let us know what you think. We most surely welcome your <a href="https://gitlab.com/tripetto/app/issues" target="_blank">input</a>. Also, <a href="https://tripetto.com/subscribe/" target="_blank">stay tuned</a> for upcoming releases.</p>
<p><strong>Many thanks in advance for your interest and feedback!</strong></p>
