---
layout: blog-article
base: ../../
permalink: /blog/hidden-fields-in-tripetto/
title: Hidden fields in Tripetto - Tripetto Blog
description: Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.
article_title: Hidden fields in Tripetto
article_folder: 20190726
author: mark
time: 3
category: feature-spotlight
category_name: Feature spotlight
tags: [feature-spotlight, feature, editor, blocks]
---
<p>Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.</p>

<h2>What are hidden fields?</h2>
<p>The <strong>hidden field</strong> block is a new action block for Tripetto. Action blocks don’t have a visual representation in the form like regular blocks (text input, dropdown, checkbox, etc.) do. They have only one job: Triggering actions.</p>
<p>In this case storing information in a hidden field. That information is then saved with the rest of the form data when the form is submitted. But it can also be used to create logic in your form and make certain decisions based on the information in the hidden field. And they are straightforward to configure using the Tripetto editor. No need to code anything.</p>

<h2>What can I do with hidden fields?</h2>
<p>Here are some use cases for hidden fields:</p>
<ul>
  <li>Personalize forms with respondents information you already know. For example, show the name or email address of a person in a form.</li>
  <li>Detect where respondents come from using referrer information. For example, clicks from the <a href="https://www.producthunt.com/posts/tripetto-2-0" target="_blank">Product Hunt website</a> always contain the URL query string addition <code>?ref=producthunt</code>. You can utilize that.</li>
  <li>Feed information from your CRM tool to a form.</li>
  <li>Send personalized forms from mailings (for example with MailChimp).</li>
</ul>

<h2>Nice, now show me how!</h2>
<p>Ok, let's say you are running a Product Hunt campaign just like we did a <a href="https://www.producthunt.com/posts/tripetto" target="_blank">couple</a> <a href="https://www.producthunt.com/posts/tripetto-2-0" target="_blank">of</a> <a href="https://www.producthunt.com/posts/tripetto-for-wordpress" target="_blank">times</a>. And let's say you use a Tripetto form on that campaign page to ask hunters for feedback. Wouldn't it be nice to greet those hunters when they open your form? Maybe kindly ask them to upvote your product. Let's build that in Tripetto!</p>
<p>To do so we add a hidden field block to the form. Then we click on the block to edit its properties. We set a name for the block (<i>Are you a hunter?</i>) and then set the type of field to <i>Query string</i>. Next, we enable the parameter feature and enter <code>ref</code> in the parameter field. We now have a hidden field that will lookup for a <code>ref</code> query string parameter and stores value of that parameter for further processing.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/hidden-field.png" alt="Screenshot of a hidden field in Tripetto" />
  <figcaption>Setting up our hidden field to detect Product Hunters is easy!</figcaption>
</figure>

<h2>Now add some logic!</h2>
<p>Now we have the information we need in the hidden field. Let's add some logic to display a personalized message to the hunter and get the magic going. We'll add a new branch with the condition that checks if the value of the hidden field is <code>producthunt</code>. You will end up with something like <a href="https://tripetto.app/template/3IMW1OTOEH" target="_blank">this</a>:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logic.png" alt="Screenshot of a hidden field in Tripetto" />
  <figcaption>Our hidden field with the conditional branch. Sweet!</figcaption>
</figure>

<h2>Show me the result</h2>
<p>Now you can see the result by visiting the form with and without the query string parameter:</p>
<ul>
  <li>Hunters will see this: <a href="https://tripetto.app/collect/QNDGRZEFSO?ref=producthunt" target="_blank">https://tripetto.app/collect/QNDGRZEFSO?ref=producthunt</a></li>
  <li>Regular visitors will see this: <a href="https://tripetto.app/collect/QNDGRZEFSO" target="_blank">https://tripetto.app/collect/QNDGRZEFSO</a></li>
</ul>

<h2>Conclusion</h2>
<p>Hidden fields can add that little bit of magic to your forms to make them more personal, engaging and interactive. You can use them in the <a href="https://tripetto.app/" target="_blank">Tripetto studio</a> where you can build forms for free in our online SaaS offering. If you are a WordPress user, simply install the <a href="https://wordpress.org/plugins/tripetto" target="_blank">Tripetto WordPress plugin</a> in your WordPress admin (then you got all the functionality of Tripetto completely integrated into your WordPress instance). Or take it even further and implement Tripetto in your own application using the <a href="https://tripetto.com/developers/" target="_blank">SDK</a>.</p>
<p>It’s really up to you. Enjoy!</p>
