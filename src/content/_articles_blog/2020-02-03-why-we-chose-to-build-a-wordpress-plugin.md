---
layout: blog-article
base: ../../
permalink: /blog/why-we-chose-to-build-a-wordpress-plugin/
title: Why we chose to build a WordPress plugin - Tripetto Blog
description: The Tripetto WordPress plugin wasn't part of our initial plans. Then why did we decide to go for WordPress anyway? Let's go back in time and find out.
article_title: Why we chose to build a WordPress plugin
article_folder: 20200203
author: jurgen
time: 4
category: product-story
category_name: Product story
tags: [product-story]
---
<p>Besides our free form studio, we offer a WordPress plugin that incorporates all the same functionality fully inside WordPress. But that plugin wasn't part of our initial plans. Then why did we decide to go for WordPress anyway? Let's go back in time and find out.</p>

<h2>Release of the studio</h2>
<p>In the spring of 2019 we released our first end user product: <a href="{{ page.base }}studio/" target="_blank"><strong>the studio</strong></a>. It's a web app to create smart forms and surveys, which you can share with a link or embed in your website. As our user base in the studio grew, we were also asked more frequently whether it was possible to use Tripetto forms inside WordPress websites. The short answer is: 'Yes, forms made in the studio can be embedded in WordPress websites', but the repeated questions made us think a little deeper about it 🤔.</p>

<h2>Doing some WordPress research</h2>
<p>We must admit, we weren't WordPress experts at the time, but with the knowledge and experiences of Maarten, our new Platform Lead, we got ourselves a kickstart in the world of WordPress. It positively surprised us how dedicated and helpful the community was.</p>
<p>Next up, we had to do some research into our future competition: form plugins. We quickly learned there were tons of form plugins with a few clear market leaders, but we also saw some overall shortcomings in all those plugins:
</p>
<ul>
  <li><h3>Old-fashioned form builders</h3>To be honest: Almost all form builders pushed us back to the zero's 🙊. It especially surprised us that one of the most popular form plugins – Contact Form 7 – has its users build forms with coding tags; without some sort of graphical form builder interface at all.</li>
  <li><h3>Not the smartest and nicest forms</h3>Most plugins create a form in your website that's not really inviting your visitors to start filling out your form. In most cases it's just a list of question fields, without any interaction. A <a href="{{ page.base }}blog/why-conversational-forms-still-matter/" target="_blank">conversational approach</a>, which will boost your completion rates, seems far away for WordPress forms...</li>
  <li><h3>No full control over your data</h3>Some plugins are just wrappers of third-party form solutions, using their external back offices. For example, Typeform does have a plugin, but it requires both a Typeform account and storage of your form data on their servers.</li>
</ul>
<p>It just so happens that all of these downsides play a big role in our Tripetto philosophy; we offer a powerful form builder UI to create attractive conversational and logic-driven experiences, and we want our users to be able to store form data wherever they want.</p>
<p>We knew we had some catching up to do in other areas (e.g. missing question types), but our overall feeling was that it could be an opportunity for us to build the full Tripetto functionality into our own WordPress form plugin; as a stand-alone solution that runs completely independent of our other offerings and infrastructure inside users’ WordPress environment.</p>

<h2>Let's do it!</h2>
<p>With the experiences of building the studio and researching other form plugins, we set some core principles for ourselves, that could differentiate us from our competitors:
</p>
<ul>
  <li>It must be a dedicated stand-alone WordPress plugin and not just a wrapper of our form studio, and no Tripetto account should be required;</li>
  <li>Our <a href="{{ page.base }}blog/why-our-visual-form-builder-works-like-this/" target="_blank">unique form builder</a> has to be integrated flawless;</li>
  <li>The forms have to be <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site/" target="_blank">easily embeddable</a> inside WordPress sites;</li>
  <li>The forms have to be <a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/" target="_blank">smart, logic driven</a> conversational experiences;</li>
  <li>The form data should be stored inside the WordPress Admin exclusively, so you have full control over your data (no external storage on Tripetto servers at all);</li>
  <li>Updates in the core of Tripetto technology should also flow to the WordPress plugin automatically to maintain feature parity between our form studio and the plugin.</li>
</ul>
<p>Because Tripetto's core technologies, like the form builder and the engine for running the forms, are bundled in a <a href="{{ page.base }}developers/" target="_blank">SDK (Software Development Kit)</a>, we were able to develop the <a href="{{ page.base }}wordpress/" target="_blank">WordPress plugin</a> pretty easily and quickly. It all resulted in <a href="{{ page.base }}blog/tripetto-for-wordpress/" target="_blank">the release</a> of the <a href="{{ site.url_wordpress_plugin }}" target="_blank">WordPress plugin</a> in the summer of 2019, just months after the launch of our free studio.</p>
<div>
  <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto - A full WordPress plugin for smart forms and surveys<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Easily create smart forms and surveys. From a simple contact form to an advanced survey with lots of logic.</span>
      <span class="url">wordpress.org</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-wordpress.png" alt="WordPress logo" />
    </div>
  </a>
</div>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/interface.png" alt="Screenshot of the WordPress plugin of Tripetto" />
  <figcaption>Editor and live preview side-by-side in WP Admin</figcaption>
</figure>

<h2>A half year later</h2>
<p>So here we are now, about half a year later. The plugin has been well-received by the WordPressers that have used it. We also received lots of feedback that showed us the strength of the WordPress community by helping each other in a positive way.</p>
<p>But we also learned it's hard to get a foot in the door with a few big players on the market. Still, we are confident we can make a difference, especially because the WordPress plugin will always benefit from all updates and improvements we develop in the core of Tripetto. And we have some cool <a href="{{ page.base }}roadmap/" target="_blank">updates coming up soon</a>, so please stay tuned!</p>

