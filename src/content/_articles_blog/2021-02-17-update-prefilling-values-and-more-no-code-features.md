---
layout: blog-article
base: ../../
permalink: /blog/update-prefilling-values-and-more-no-code-features/
title: Update 🚀 Prefilling values and more no-code features - Tripetto Blog
description: We added some new blocks that our no-coders will enjoy, namely the custom variable block and the set value block. These will definitely help you to create even more advanced no-code solutions.
article_title: Update 🚀 Prefilling values and more no-code features
author: jurgen
time: 3
category: feature-spotlight
category_name: Feature spotlight
tags: [feature-spotlight, feature, editor, blocks]
---
<p>Tripetto is of course perfect to create all kinds of forms and surveys. But we're also developing the platform to be capable of more advanced use cases, primarily aimed for the rising no-code community. Today we introduce some important features that will help our no-coders.</p>

<h2>Hi no-coders 👋</h2>
<p>Forms... you can’t live with them. You can’t live without them. Yet, we all need them one way or another. Especially no-coders can hardly work without them when building their own cool, innovative no-code tools, so why not try to make forms as smart, easy and beautiful as possible?</p>
<p>Tripetto was already highly suited for creating stunning, mind-blowingly intelligent forms, but with the introduction of several new blocks it’s now really ready for lift-off in the no-code community! 🚀</p>

<h2>Introducing new features</h2>
<p>A few weeks ago, we <a href="{{ page.base }}blog/big-news-the-calculator-block-has-landed/" target="_blank">launched the calculator block</a>. That was one of the most important steps for no-code usage, but today we’re introducing additional blocks to make it all the more powerful:</p>
<ul>
  <li><strong>Set value block</strong> - Set, change, clear and lock/unlock values in your form;</li>
  <li><strong>Custom variable block</strong> - Store a certain value in a variable and use that throughout your form.</li>
</ul>

<h3>NEW! The set value block</h3>
<p>The most common use case for the set value block is to prefill questions in your form. You can now prefill them with static values, but also with query string values, other block values, calculator values, custom variable values and so on...</p>
<p>On top of that it's also capable of locking form blocks, so given answers can’t be edited anymore. Handy to prevent cheating on a quiz form, right?!</p>
<p>👉 <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">How to use the set value block</a><br/>
👉 <a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-the-right-values/">How to prefill question blocks with the right values</a><br/></p>

<h3>NEW! The custom variable block</h3>
<p>The variable block is a real no-code block! It can store all kinds of variable values that you can use throughout the form. Just like you would in your code, but then in a completely no-code fashion!</p>
<p>It can, for example, help you to make certain decisions in your form, or just as the holder of the outcome of a certain branch.</p>
<p>👉 <a href="{{ page.base }}help/articles/how-to-use-the-custom-variable-block/">How to use the custom variable block</a></p>

<h3>Other improvements</h3>
<p>We also improved some smaller stuff in this update. The most important ones are:</p>
<ul>
  <li><strong>Instant scores and calculations</strong> - Add <a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">instant scores and calculations</a> inside each question block.</li>
  <li><strong>Suggestions</strong> - Help your respondents to quickly select an item, by adding <a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/#suggestions-feature">lists of suggestions</a> to your text input;</li>
  <li><strong>Import choices</strong> - A handy improvement in the form builder: add unlimited amounts of choices at once with the new import function.</li>
</ul>
<p>You can see all our updates and bugfixes in our <a href="{{ page.base }}changelog/" target="_blank">changelog</a>.</p>
<hr/>

<h2>Start no-coding in Tripetto</h2>
<p>Check out these new no-code blocks and play around with them in our form builder. We call these blocks <strong>action blocks</strong>, and this is the full list:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">Calculator</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-custom-variable-block/">Custom variable</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-force-stop-block/">Force stop</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">Hidden field</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-raise-error-block/">Raise error</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/">Send email</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">Set value</a>.</li>
</ul>
<p>And of course, our <a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/">very advanced logic features</a> also help you to build your own amazing no-code applications!</p>
<hr/>

<h2>Now available everywhere</h2>
<p>All no-code blocks are available across <a href="{{ page.base }}launch/">all our platforms</a>, so in the  <a href="{{ page.base }}studio/">studio at tripetto.app</a> and the  <a href="{{ page.base }}wordpress/">WordPress plugin</a> for our WordPress users. And even in our  <a href="{{ page.base }}developers/">SDK</a> for developers!</p>

<h2>What's next?</h2>
<p>We will keep collecting all your feedback to determine what we will work on next. And you can keep track of that in <a href="{{ page.base }}roadmap/" target="_blank">our roadmap</a>. So please <a href="{{ page.base }}subscribe/" target="_blank">stay tuned</a>!</p>
