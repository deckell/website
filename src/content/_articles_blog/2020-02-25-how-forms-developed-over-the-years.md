---
layout: blog-article
base: ../../
permalink: /blog/how-forms-developed-over-the-years/
title: How forms developed over the years - Tripetto Blog
description: Forms have been here since the start of the internet, but just like the internet they have evolved a lot since then. What can we learn from that journey to determine the future of forms?
article_title: How forms developed over the years
article_folder: 20200225
author: jurgen
time: 4
category: background-story
category_name: Background story
tags: [background-story]
---
<p>Forms have been here since the start of the internet, but just like the internet they have evolved a lot since then. What can we learn from that journey to determine the future of forms?</p>

<h2>Stage 1 - How it all began</h2>
<p>My first internet experience must have been around 20 years ago. I was 12, you had to dial in via the telephone line (including the sounds and angry parents when they needed to make a call 😅), and Netscape was the one and only browser.</p>
<figure class="inline inline-right">
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/netscape.gif" alt="Screenshot of Netscape" />
  <figcaption>Example of a form in Netscape - Source <a href="http://speed.eik.bme.hu/help/html/Special_Edition-Using_CGI/ch6.htm" target="_blank">Michael Erwin</a></figcaption>
</figure>
<p>Websites from that era didn't have much style and interaction in them. The main reason for that was the fact that each byte counted. The connection was rather slow, and people paid per byte used. So, the heavier you'd make your website, the more unusable it became.</p>
<p>This affected forms. And although there weren't very many forms back then, the forms you did come across were a big mess: unordered, unclear, unstyled. Basically just unusable...</p>

<h2>Stage 2 - Better internet, better forms</h2>
<p>As the years went by, the internet changed. Suddenly your computer was always connected, the connection got faster, and you could choose from multiple browsers that all supported way more styling and interaction.</p>
<p>This also affected forms. They became a bigger part of the internet experience and therefore got more attention from the designers. Forms became more usable, with an understandable structure and styling that was in favor of the usability.</p>
<p>Personally, I saw this evolution happen in my own usage of websites, but also during my 'Multimedia Design' studies and later in my first job as web developer: forms were now an important part of the design process and functionality of each website.</p>

<h2>Stage 3 - The mobile revolution</h2>
<p>Does June 29<sup>th</sup>, 2007 ring a bell? Probably not, but I can tell you it's a big turning point in the development of the internet.</p>
<p>It's the day Steve Jobs introduced three new Apple products: an iPod, a phone and a breakthrough internet communications device. I still get goosebumps when I watch that presentation. And especially the moment it turns out to be just one device... 🤯</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/x7qPAY9JqE4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Steve Jobs introducing <s>three new Apple products</s> the iPhone.</figcaption>
</figure>
<p>I must admit I did not attach much value to the 'internet communicator' part at that moment. Turns out I'm not as genius as Steve Jobs, because we now know the iPhone (and every other smartphone) has changed the way we use the internet. Nowadays the smartphone is used even more than desktop, as you can see in the graph below.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/stats-desktop-mobile.png" alt="Graph of device usage" />
  <figcaption>Usage of desktop vs. mobile - Source <a href="https://gs.statcounter.com/platform-market-share/desktop-mobile/worldwide/" target="_blank">StatCounter Global Stats</a></figcaption>
</figure>
<p>Of course, that development also had an impact on forms. Until then forms were designed for display on large screens and for usage with mouse and keyboard. But now it gets more and more important that forms show well on smaller screens and work flawlessly with touch controls.</p>
<p>This changed the mindset of designers and developers, forcing them to design 'mobile friendly' websites and forms. Nowadays we even predominantly design 'mobile first'.</p>

<h2>Stage 4 - What's next?</h2>
<p>So, here we are now: forms evolved from a bunch of input fields to the most beautiful set of interactional elements that are usable on every device that we use 24/7. What's next?</p>
<p>Some internet trends for 2020 and beyond are personalization, human interaction and the usage of chatbots. Users no longer want to feel like they are talking to a computer. They want to interact in a human way. This results in the upcoming usage of chatbots, that make each interaction with a company feel like a real conversation, although in lots of cases it's still a computer you're talking to. By using personalization, the bot can use each input a user gives, resulting in a human interaction experience.</p>
<p>These trends will also become more noticeable in the forms we use. Forms have already started to look and feel different than traditional forms, and this trend will persist. They will become ever smarter and only ask the right questions based on the user's input, and thereby feel like a real conversation. This will even <a href="{{ page.base }}higher-completion-rates-with-logic/" target="_blank">boost the completion rates of forms</a>.</p>
<p>So, beware: while you think you're talking to a person, you might just end up talking to a computer. But then one that actually understands you 🤫.</p>
