---
layout: blog-article
base: ../../
permalink: /blog/add-some-kanye-genius-to-your-survey/
title: Add some Kanye genius to your survey - Tripetto Blog
description: How to integrate an API in your survey with Tripetto.
article_title: Add some Kanye genius to your survey (tutorial on integrating an API)
article_folder: 20190416
article_image: header.png
article_image_caption: Tripetto editor and collector side-by-side
author: martin
time: 4
category: coding-tutorial
category_name: Coding tutorial
tags: [coding-tutorial, editor, collector, blocks, feature]
---
<p>How to integrate an API in your survey with Tripetto.</p>

<h2>Extend your survey with custom question types</h2>
<p>When designing a survey you usually have a number of basic question types to choose from like Number, Dropdown or Star Rating. Some vendors make more complex question types available like <a href="https://help.surveymonkey.com/articles/en_US/kb/Matrix-Question" target="_blank">Matrix</a> or <a href="https://www.typeform.com/help/payment-field-stripe-integration/" target="_blank">Payment</a>.</p>
<p>With <a href="https://tripetto.com/" target="_blank">Tripetto</a> you can build your own question types. This brings endless opportunities to enrich your survey and collect more user data. In this article I’ll demonstrate how easy it is to create a question type that connects to an external API. This example isn’t a question actually, but it shows a random quote from Kanye West any where you need his genius.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/quote.png" alt="Screenshot of a Kanye West quote in Tripetto" />
  <figcaption>Kanye West quote from <a href="https://api.kanye.rest/" target="_blank">https://api.kanye.rest</a></figcaption>
</figure>

<h2>Concept</h2>
<p>Tripetto distinguishes itself with the way it visually presents the flow of a survey in the <a href="https://docs.tripetto.com/guide/builder/" target="_blank">Editor</a>. When building a custom block (Tripetto uses blocks as a synonym for question types) you have to define how your custom block exposes itself in the editor and which properties you can edit.</p>
<p>Another part of the custom block is how it represents itself in the <a href="https://docs.tripetto.com/guide/runner/" target="_blank">Collector</a>. The collector is where your respondents file your survey. Tripetto gives you absolute freedom how to display your survey, but there are some <a href="https://docs.tripetto.com/examples/" target="_blank">examples</a> to give you a head start.</p>
<p>This example is based on the <a href="https://example-react-conversational.tripetto.com/" target="_blank">Conversational UX demo with React</a>.</p>

<h2>Editor</h2>
<p>To create a custom block in the editor you have to implement the <code>NodeBlock</code> abstract class, prefixed with the <code>@tripetto</code> decorator to supply (meta) information about the block.</p>

```typescript
import { NodeBlock, Slots, slots, tripetto } from "tripetto";
import * as ICON from "./kanye.svg";

@tripetto({
    type: "node",
    identifier: "kanye",
    label: "Kanye Quote",
    icon: ICON
})
export class Kanye extends NodeBlock {
    @slots
    defineSlot(): void {
        this.slots.static({
            type: Slots.String,
            reference: "kanye-quote",
            label: "Kanye quote"
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name(false, false, "Name");
        this.editor.visibility();
    }
}
```

<p>Slots are a fundamental part of a custom block, because they define how response data is stored. In this example I’ve chosen for a single <a href="https://docs.tripetto.com/guide/blocks/#slots" target="_blank">static</a> slot of type <code>Slots.String</code> where I can store the random quote that was presented to the respondent.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/editor.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Properties of the Kanye block in the Tripetto editor</figcaption>
</figure>

<h2>Collector</h2>
<p>The collector is where the real <i>API magic</i> happens! In the editor we’ve described the way our custom block can be managed. In the collector we’ll describe how it looks and how it works. Since this example is based on React and Bootstrap, this is where these frameworks come together with Tripetto.</p>

```tsx
import * as React from "react";
import * as Tripetto from "tripetto-collector";
import * as Superagent from "superagent";
import { IBlockRenderer } from "collector/helpers/interfaces/renderer";
import { IBlockHelper } from "collector/helpers/interfaces/helper";
import "./kanye.scss";

const DEFAULT_QUOTE = "I feel like I'm too busy writing history to read it.";
@Tripetto.block({
  type: "node",
  identifier: "kanye"
})

export class KanyeBlock extends Tripetto.NodeBlock implements IBlockRenderer {
  readonly quoteSlot = Tripetto.assert(this.valueOf<string>("kanye-quote"));

  constructor(node: Tripetto.Node, context: Tripetto.Context) {
    super(node, context);
    Superagent
      .get("https://api.kanye.rest")
      .then((response: Superagent.Response) => {
        this.quoteSlot.value = response.body.quote || DEFAULT_QUOTE;
      });
  }

  render(h: IBlockHelper): React.ReactNode {
    return (
      <div tabIndex={h.tabIndex} className="kanye">
        <blockquote>
          <p className="quotation">
            {this.quoteSlot.value || "..."}</p>
          <footer>
            — Kanye West
          </footer>
        </blockquote>
        <div className={h.isActive ? "active" : ""}>
          <button className="btn btn-lg btn-primary" onClick={() => h.next()}>
            Continue
            <i className="fas fa-level-down-alt fa-fw fa-rotate-90 ml-2" />
          </button>
        </div>
      </div>
    );
  }
}
```

<p>I’ve used Superagent to retrieve the quote from the API. React is used for updating the quote inside the paragraph tag. You can see that the slot is updated with the quote as soon it is retrieved. This way the quote is added to the response data and therefore stored when the respondent completes the survey. Add some Bootstrap styling and this results in a beautiful quote from Kanye West inside your survey 🕶️.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/collector.png" alt="Screenshot of a collector in Tripetto" />
  <figcaption>Kanye West quote in action in Tripetto collector</figcaption>
</figure>

<h2>Try it yourself</h2>
<p>You can <em>experience</em> the Kanye West block on <a href="https://tripetto.gitlab.io/lab/kanye-west/" target="_blank">GitLab Pages</a>.</p>
<p>You can run this example on your local machine by downloading the source code from <a href="https://gitlab.com/tripetto/lab/kanye-west" target="_blank">https://gitlab.com/tripetto/lab/kanye-west</a>. Run <code>npm install</code> and <code>npm test</code> from your command prompt and you’re up and running on <a href="http://localhost:9000" target="_blank">http://localhost:9000</a>.</p>

<h2>Final thoughts</h2>
<p>The Kanye West quote block is obviously meant for demo purposes (unless you are a real Kanye West fan and you want to share his wisdom with your target audience 😎). Imagine what you can do with custom blocks in your survey or even in your application. <a href="https://tripetto.com" target="_blank">Tripetto</a> can be fully integrated in your own software and its blocks are fully extendable as seen in this example.</p>
<p>Let me know what custom blocks you expect to create for your surveys.</p>
<p><strong>Enjoy Tripetto!</strong></p>
<p>(And very special thanks to <a href="https://twitter.com/ajzbc" target="_blank"><strong>@ajzbc</strong></a> for his Kanye Rest API!)</p>
