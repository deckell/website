---
layout: blog-article
base: ../../
permalink: /blog/why-conversational-forms-still-matter/
title: Why Conversational Forms still matter - Tripetto Blog
description: Conversational forms offer upsides, but they’re difficult to do well — at least, it takes more effort than a traditional form to get right.
article_title: Why Conversational Forms still matter
article_folder: 20190702
author: owen
time: 4
category: background-story
category_name: Background story
tags: [background-story, collector, conversational]
---
<p>How to build conversational exchanges right.</p>

<p>Conversational interfaces were the hot new thing in 2017, and everyone was trying to implement them as quickly as possible. The problem, as many early adopters found out, is that it’s really difficult to do a conversational form <em>well</em> — at least, it takes more effort than a traditional form to get right.</p>
<p>As the hype wave came and went, conversational forms have become less common, but they’re still a powerful way to gather information from your users without it feeling like a chore to fill out a long, laborious form. If used right, conversational forms can help increase survey conversion and make it easier for your users to understand what’s going on.</p>
<p>Projects like <a href="https://space10-community.github.io/conversational-form/landingpage/" target="_blank">SPACE10’s conversational form</a> make it simple to drop in a library and transform your traditional form. And with the right tools, you can build your dream survey without complex coding challenges to get it right.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/space10.png" alt="Screenshot of Space10" />
</figure>
<p>Here’s some general advice for building a great conversational form experience, which will help you stand out from the crowd, as you’re building your unique form-based robots.</p>

<h2>Make it friendly</h2>
<p>Building a conversational form experience is a little different from the traditional form experience. Rather than simple text boxes named “first name” and “email address,” you now need to provide a script that makes the conversation flow as the user would expect.</p>
<p>Your ‘form’ should introduce itself, and explain to the user why you need the information before launching into asking for information. This helps them orient themselves before you begin barraging them with questions, and allows an opportunity to set expectations at the beginning.</p>
<p>A great first conversation might go something like this:</p>
<blockquote>
  <p><strong>Bot</strong>: Hello! We’re looking for feedback on our new product, which you can give us here. Ready to get started?</p>
  <p><strong>User</strong>: Let’s do it!</p>
  <p><strong>Bot</strong>: Perfect. What should I call you? Just your first name is fine.</p>
</blockquote>

<h2>Keep it short</h2>
<p>As you’re writing out your script, it’s important to avoid being too verbose. It can be tempting to be too talkative, particularly if you haven’t created a conversational experience before. The absolute best experiences feel natural, without being overly wordy or heavy on the friendliness.</p>
<p>A good rule of thumb here is to limit any questions or descriptions to no more than two sentences, because chat bubbles quickly feel too wordy when there’s much more inside of them.</p>
<p>If you need to add more text or a description, you can offer an extra button once you’ve asked the question that allows the user to get more context before answering.</p>

<h2>Don’t leave users hanging</h2>
<p>Speaking of buttons, it’s important to never leave your users wondering where they should go next. Sometimes they might not understand the question, or might simply not want to answer it at all.</p>
<p>To avoid leaving them hanging or wondering what to do, use simple buttons to offer more information or a way to skip ahead.</p>
<p>Traditional forms often offer further information hidden behind a tooltip or even a separate page, and conversational forms lend themselves to these scenarios really well. Once you’ve asked the user for information, a button that offers a description can send them useful links or even just a sentence about the question to help them along.</p>
<p>Consider all of the ways that users might get hung up — is this question relevant to them? Could we route them to somewhere else, smarter, with logic-flows. Or how do we explain that these are mandatory questions? Many of these questions will be answered as you test your flow, but it’s important to be sure there are no dead-ends!</p>
<p>A good example of a powerful tool for building smart flowing forms and surveys is <a href="https://tripetto.com" target="_blank">Tripetto</a>, which allows drag-and-drop form building on any device (they offer a version <a href="https://tripetto.com/developers/" target="_blank">for developers</a>, too).</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The Tripetto storyboard and form preview, side-by-side</figcaption>
</figure>

<h2>Think about error states</h2>
<p>When you’re building a form, error states are right next to the text box the user just entered information into. In a conversational flow, your bot needs to respond to them and explain what went wrong.</p>
<p>The easiest way to avoid issues with error states is to be clear up-front, but people are unpredictable, and sometimes they’ll enter <em>weird</em> answers into any text box. Think about the constraints you want to impose and how you’ll respond when it’s not right.</p>
<p>Rather than saying “Enter only numbers” when they paste in a bunch of symbols, say something clear, but still human, such as “Sorry, you can’t enter symbols for your phone number. Try again with only numbers!”</p>
<p>There’s likely an array of possible error states, so it’s important to consider everything from what happens if they push enter without even responding to what happens when the wrong data is entered or even if the user keeps trying without changing an invalid answer.</p>

<h2>Get building</h2>
<p>Conversational forms are a great experience for your users, but it’s important to implement them right. With tools like Tripetto’s visual editor, you can wrap your head around the complex flows involved and stick to building a killer experience for your users.</p>
<p><strong>Just remember the golden rule as you’re building them: Keep it human!</strong></p>
