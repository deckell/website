---
layout: blog-article
base: ../../
permalink: /blog/a-christmas-present-from-tripetto/
title: 🎄 A Christmas present from Tripetto - Tripetto Blog
description: Version 1.0.0 of the Tripetto SDK is out. Check our release notes.
article_title: 🎄 A Christmas present from Tripetto
article_folder: 20181221
author: mark
time: 3
category: product-release
category_name: Product release
tags: [product-release, product, release, sdk, coding-tutorial]
---
<p>Version 1.0.0 of the Tripetto SDK is out.</p>

<h2>Shipping version 1.0.0</h2>
<p>A lot of work’s gone into the initial development of our full-fledged SDK 🤯. And probably as much time was spent on countless refinements, especially in response to the valuable feedback since our <a href="https://www.producthunt.com/posts/tripetto" target="_blank">Product Hunt launch</a>. It made sense to do all of that before stabilizing. But that’s done now and version 1.0.0 is <a href="https://www.npmjs.com/package/tripetto" target="_blank">here</a>.</p>
<p>We owe a very special thanks to the community for their support and sensible feedback. We’ll make sure to keep listening and improving Tripetto. And we’re really eager to hear much more from you again. Many thanks in advance.</p>
<p>By the way, from now on we’ll be sticking to <a href="https://semver.org/" target="_blank">semantic versioning</a> for future releases.</p>

<h2>New features</h2>
<p>Among the extensive list of improvements are the following interesting features:</p>
<ul>
  <li><strong>Storyline</strong><br />This is a new way to render building blocks to a collector UI. The storyline structure contains all key moments of a collector session and the blocks that need to be rendered. It allows for non-typical UX’s (<a href="https://example-react-conversational.tripetto.com/" target="_blank">like this one!</a>) and the following show modes.</li>
  <li><strong>Show mode</strong><br />This one’s powerful. Thanks to the mentioned storyline feature you may set the show mode of your collector implementation to <em class="in">paginated</em>, <em class="in">continuous</em>, or <em class="in">progressive</em>. Paginated makes for a more traditional approach, rendering (groups of) questions per page and showing pages one after the other. The continuous mode renders questions one after the other on a single page and always also shows all previous questions top to bottom. The progressive mode works the same as continuous, but also renders all possibly upcoming questions as far as conditions allow their previewing.</li>
  <li><strong>Markdown support</strong><br />Almost any text in a form can now be formatted. This obviously goes for main texts and questions, but also for placeholders, help texts etc.</li>
  <li><strong>Pipe variables in text</strong><br />Any respondent’s text input can be piped to other questions and fields to offer more flexibility and cleverness to your forms. Press the <code>@</code> sign right where you need this to unlock all available pipeable items. By the way, we’ll shortly drastically improve here with a more user-friendly control for this.</li>
  <li><strong>Export API</strong><br />With this feature you may export a completed form’s response data from the collector to a field set or CSV very easily.</li>
  <li><strong>Import API</strong><br />And with this one you may import a completed form’s response data back into a collector. That’s handy for data review or redaction.</li>
  <li><strong>Improved building blocks on npm</strong><br />Our updated building blocks now also include the UI-less part of the collector implementation, such as validation and conditions. So, if you use our building blocks you now only have to focus on your UI.</li>
  <li><strong>Compliance with JSON style guide</strong><br />The JSON form definitions are now all camelCase instead of PascalCase. This will break compatibility with form definitions made with previous versions of Tripetto but aligns much nicer with the relevant coding guidelines.</li>
</ul>

<h2>Upgraded examples</h2>
<p>Our <a href="https://docs.tripetto.com/examples/#runner" target="_blank">collector examples</a> now show, for demo purposes, the visual editor for form creation (on the left) and the collector for response gathering (on the right) side by side. We’ve updated the following demo collector implementations:</p>
<ul>
  <li><a href="https://example-react-bootstrap.tripetto.com/" target="_blank">React + Bootstrap</a></li>
  <li><a href="https://example-react-material-ui.tripetto.com/" target="_blank">React + Material UI</a></li>
  <li><a href="https://example-angular-bootstrap.tripetto.com/" target="_blank">Angular + Bootstrap</a></li>
  <li><a href="https://example-angular-material.tripetto.com/" target="_blank">Angular + Angular Material</a></li>
</ul>
<h3>Check this!</h3>
<p>We’ve also added a whole new demo with a <a href="https://example-react-conversational.tripetto.com/" target="_blank"><strong>conversational UX, built with React + Bootstrap</strong></a>. And last but not least, we’ve updated all of our <a href="https://docs.tripetto.com/examples/#blocks" target="_blank">building block examples</a>, too. These are used across our demos.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/collectors.png" alt="Screenshot of collectors in Tripetto" />
</figure>

<h2>Updated documentation</h2>
<p>The <a href="https://docs.tripetto.com" target="_blank">documentation website</a> has been updated with all the latest changes and demos. Shortly, it will be expanded with an SDK section and the complete function reference for the Tripetto components.</p>

<h2>Extended license model</h2>
<p>The <a href="https://tripetto.com/developers/" target="_blank">license model</a> has been updated as well. You still only pay if you are integrating the editor into business applications. The use of the stand-alone editor, the collector and the building blocks is still completely free. And we still offer a free version of the integrated editor for non-commercial personal or academic use and evaluation purposes.</p>
<p>But in addition to the internal and external business licenses for integrating the editor into commercial applications, we now offer an agency license. We added this especially for businesses that want to develop their own data collection applications and integrate the Tripetto editor into that.</p>
<hr />

<p>That’s it for now. Please have a go with all of this and <a href="https://tripetto.com/contact/" target="_blank">let us know</a> what you think. Thanks again!</p>
