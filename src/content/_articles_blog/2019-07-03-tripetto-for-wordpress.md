---
layout: blog-article
base: ../../
permalink: /blog/tripetto-for-wordpress/
title: Tripetto for WordPress - Tripetto Blog
description: Build and deploy conversational forms and surveys for your WP websites.
article_title: Tripetto for WordPress
article_folder: 20190703
author: jurgen
time: 3
category: product-release
category_name: Product release
tags: [product-release, product, release, wordpress]
---
<p>Build and deploy conversational forms and surveys for your WP websites.</p>

<p>We’re introducing the official Tripetto plugin for WordPress! Now you can unleash all the power of Tripetto inside your WordPress websites. Build and publish smooth conversational forms and surveys. Customize their look and feel to match the style of your WordPress project. Directly from your WP Admin.</p>
<div>
  <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto - A full WordPress plugin for smart forms and surveys<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Easily create smart forms and surveys. From a simple contact form to an advanced survey with lots of logic.</span>
      <span class="url">wordpress.org</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-wordpress.png" alt="WordPress logo" />
    </div>
  </a>
</div>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/EBy0qqxCjmA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>A full WordPress plugin for conversational forms and surveys</figcaption>
</figure>

<p>By the way, the plugin is not a wrapper for embedding externally created Tripetto forms in WordPress front-ends. Instead, it’s a full-featured plugin that puts everything you need inside your WordPress Admin: a visual editor for building engaging conversational experiences, their easy deployment with shortcodes, and response collection and storage within your WP instance (<a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/" target="_blank">Hello GDPR!</a>). So no external dependencies, and you don’t even need a Tripetto account.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/interface.png" alt="Screenshot of the WordPress plugin of Tripetto" />
  <figcaption>Editor and live preview side-by-side in WP Admin</figcaption>
</figure>
<hr />

<h2>How to install</h2>
<p>You have two options to start using the Tripetto plugin for WordPress:</p>
<ul>
  <li><strong>Install directly from your WordPress Admin</strong><br/>Sign in to your WP Admin and click <em>Plugins</em> and then <em>Add New</em>. Search for 'Tripetto' and click <em>Install Now</em>.</li>
  <li><strong>Download the plugin from the <a href="{{ site.url_wordpress_plugin }}" target="_blank">WordPress Plugins directory</a></strong><br/>Then sign in to your WP Admin and click <em>Plugins</em> and then <em>Add New</em>. Next, click <em>Upload Plugin</em> and select the downloaded ZIP-file. Finally click <em>Install Now</em>.
</li>
</ul>
<hr />

<h2>How to use</h2>
<p>After installing and activating the plugin a new menu option <em>Tripetto</em> appears in your WP Admin menu. Go there to open the plugin’s dashboard.</p>
<h3>Create a form or survey</h3>
<p>To start building, click <em>Create New Form</em> to create an empty form and start the visual editor. Use it to create your form on a self-organizing storyboard. Your actions are stored automatically and a live preview of your creation is displayed to the right of the storyboard. When you’re done editing, click the <em>Back</em> button in the top left corner to return to your form and survey overview.</p>
<h3>Deploy to your front-end</h3>
<p>To publish a form or survey on a page, copy the shortcode (displayed in the form and survey overview). Then jump to the desired page for your form. Add a block and paste your shortcode. After that, publish the page and you are ready to start collecting data!</p>
<h3>Handle response</h3>
<p>When someone fills out a form the data is stored in the database inside your WordPress instance. You can view all submitted entries or download them as a CSV file for further analysis or processing. Optionally, you can set a notification to automatically receive an e-mail message when a new form entry is submitted. Also, you can enable one of the integrations.</p>
<h3>Integrate</h3>
<p>Currently, we support the following additional integrations:</p>
<ul>
  <li><strong>Slack notification</strong> — Receive a slack message when a new entry is available;</li>
  <li><a href="https://zapier.com/" target="_blank"><strong>Zapier</strong></a><strong> integration</strong> — Enable further data processing and connect with 2.000+ services and applications.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/automate.png" alt="Screenshot of automations in Tripetto" />
  <figcaption>Configure notifications, hooks and integrations</figcaption>
</figure>
<hr />

<h2>Source and support</h2>
<p>The WordPress plugin is completely open source. We use Gitlab for our repositories. The plugin code lives <a href="https://gitlab.com/tripetto/wordpress" target="_blank">here</a>. Feel free to review it, propose changes or submit new features.</p>
<p>Let us know when you run into problems using the plugin. We are more than happy to help. You can submit issues <a href="https://gitlab.com/tripetto/wordpress/issues" target="_blank">here</a>. Or <a href="{{ page.base }}contact/" target="_blank">contact</a> us directly.</p>
<div>
  <a href="https://gitlab.com/tripetto/wordpress" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / WordPress<i class="fas fa-external-link-alt"></i></span>
      <span class="description">WordPress plugin to create forms and surveys using Tripetto.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-gitlab.png" alt="Gitlab logo" />
    </div>
  </a>
</div>
