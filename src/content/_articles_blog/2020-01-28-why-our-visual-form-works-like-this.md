---
layout: blog-article
base: ../../
permalink: /blog/why-our-visual-form-builder-works-like-this/
title: Why our visual form builder works like this - Tripetto Blog
description: As you may have noticed, our form builder is not like all the other form builders out there. Of course we got a good reason for that and it's all about creating smart forms with the right logic.
article_title: Why our visual form builder works like this
article_folder: 20200128
author: jurgen
time: 4
category: product-story
category_name: Product story
tags: [product-story]
---
<p>As you may have noticed, our form builder is not like most – if not all – other form builders out there. Of course, we have a good reason for that and it's all about creating smart forms with the right logic. In this article we'd like to explain what makes our form builder different than others.</p>

<h2>Our journey</h2>
<p>The development of the form builder of Tripetto was quite a journey. Tripetto originated from another company we founded back in 1999 that mostly sells sophisticated survey and panel solutions for large healthcare-related organizations.</p>
<p>One of the most important experiences from those projects, was that most people don’t like to fill out surveys at all — let alone lengthy ones (we certainly don’t 😂). So, we realized that surveys need to be as compact and engaging as possible and should flow much more like real conversations, asking only the relevant questions to the respondent. To do that, you need decent flow logic — often referred to as skip logic or display logic. The magic sauce that makes a survey smart. That’s why most survey tools, just like ours, offer that functionality.</p>
<div>
  <a href="{{ page.base }}blog/the-journey-of-tripetto/" target="_blank" class="blocklink">
    <div>
      <span class="title">The journey of Tripetto - Tripetto<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Read our blog article about the journey to the first release of Tripetto.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>

<h2>The struggle</h2>
<p>But there’s a remarkable similarity between most survey solutions: All those form builders tend to work generally the same! You enter questions in a flat list — often organized on pages — and during or after that, you use a separate logic manager to specify the rules for the display or skipping of the different survey elements.</p>
<p>The disadvantages of this approach are fairly clear, particularly for the end-user who needs to design the form:</p>
<ol>
  <li>There is no good overview of the structure of your form or survey because of the lack of visualization of flow, requiring you to manage the entire model in your head;</li>
  <li>Collaboration is hard because it is not easy for other people to understand the structure of your survey at a glance, making them go through the nitty-gritty settings all the time;</li>
  <li>You become more of a programmer creating spaghetti-like code by defining logic, further steepening the learning curve;</li>
  <li>As soon as the logic rules are defined, it often becomes problematic to easily apply structural changes to your survey without the risk of bringing down your careful composition;</li>
  <li>The ability to use logic in the first place is often a separate, expensive ‘pro’ feature, requiring you to pay before you can use it.</li>
</ol>
<blockquote class="big"><p>Bottomline:<br/>We think it is just way too hard to use flow logic in forms and surveys, so we need to do something about it!</p></blockquote>

<h2>The idea</h2>
<p>So, we thought the whole design process for smart forms had to be much easier, maybe even fun. But how? We ended up with the idea of a <strong>visual form builder</strong>, embracing flow logic at the core of it. No endless lists of questions, but something like a map visualizing the flows of your form.</p>

<h2>The solution</h2>
<p>The visual form builder lets you think of the flows in your form as an integral, logical part of your form designing process, instead of adding some logic to a list of questions. This approach results in smart forms, that are also easily understandable and instantly editable; all in the same view.</p>

<h3>Logic at the core</h3>
<p>The builder supports <a href="{{ page.base }}higher-completion-rates-with-logic/" target="_blank">multiple types of logic</a> that you can use to make your forms smart. The most important are:
</p>
<ul>
  <li>Branch logic;</li>
  <li>Jump logic;</li>
  <li>Pipe logic;</li>
  <li>Display logic.</li>
</ul>
<p>We've made a <a href="{{ page.base }}higher-completion-rates-with-logic/" target="_blank">dedicated overview page</a> to explain all types of logic, including examples, help articles and tutorial videos. Those will help you to make your forms smart!</p>
<div>
  <a href="{{ page.base }}higher-completion-rates-with-logic/" target="_blank" class="blocklink">
    <div>
      <span class="title">Boost completion rates by using the right logic - Tripetto<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Apply logic to your form and survey flows visually on the go to make them smarter and only ask your audience the right questions for optimal response.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>

<h3>Creating flows</h3>
<p>With the idea of logic at the core of the form builder, we use a visual map representing the flows inside your forms, based on possible answers your respondents give. Each possible flow is presented as a new track, or as we call it: <strong>a branch</strong>.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/form-builder.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Creating questions and logic in our visual form builder</figcaption>
</figure>

<h3>Editing flows</h3>
<p>Creating the right logic is one. But changing it can be difficult with traditional form builders. In our visual form builder, you can drag-and-drop each cluster/block/branch to the desired position, while always preserving the whole logic that's behind it. This makes our builder into a storyboard on which you can playfully work out the best logic flows for your forms and surveys as you go.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/drag-drop.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Drag-and-drop with layout guidance</figcaption>
</figure>

<h3>Higher completion rates</h3>
<p>In the end smart forms with the right logic help achieving better completion rates, as your respondents don't have to fill out endless forms, but only answer the applicable questions that are meant for them.</p>
<p>Just let us help you create such smart forms with our visual form builder. You can try it for free in our <a href="{{ site.url_app }}" target="_blank">studio</a>, or when you're a WordPresser use our <a href="{{ site.url_wordpress_plugin }}" target="_blank">WordPress plugin</a>.</p>
