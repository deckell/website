---
layout: blog-article
base: ../../
permalink: /blog/implementing-tripetto-forms-using-react/
title: Implementing Tripetto using React - Tripetto Blog
description: Tripetto brings a new way of creating and deploying forms and surveys in websites and applications.
article_title: Implementing Tripetto using React
article_folder: 20171109
author: mark
time: 7
category: coding-tutorial
category_name: Coding tutorial
tags: [coding-tutorial, products, editor, collector, sdk, release]
---
<p>Tripetto brings a new way of creating and deploying forms and surveys in websites and applications.</p>

<p>You use its intuitive <a href="https://www.npmjs.com/package/tripetto" target="_blank"><strong>graphical editor</strong></a> to build and edit smart forms with logic and conditional flows in 2D on a self-organizing storyboard. In any modern browser. Mouse, touch or pen.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/editor.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The Tripetto editor in action!</figcaption>
</figure>
<p>And you then deploy these smart forms in websites and applications using the supplementary <a href="https://www.npmjs.com/package/tripetto-runner-foundation" target="_blank"><strong>collector library</strong></a>. Anything you build in the editor, the collector will simply run. You just focus on the visuals of the embedded form.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/editor-collector.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The editor on the left and the React collector with DevTools open on the right</figcaption>
</figure>
<p>We’ll explain here how to build such a collector implementation using <a href="https://reactjs.org/" target="_blank">React</a> that’s capable of running Tripetto smart forms inside a website or application.</p>
<hr />

<h2>The basics</h2>
<p>Before we dive into the real power-coding, let’s briefly touch on the following basics behind Tripetto.</p>
<h3>Editor</h3>
<p>The editor is the graphical 2D form designer. It runs in any modern browser. Forms you build in the editor are stored in JSON files. We call these files <a href="https://docs.tripetto.com/guide/builder/#definitions" target="_blank"><strong>form definitions</strong></a>. A single form definition contains the complete structure of a form. The definition file is parsed by the collector.</p>
<h3>Collector</h3>
<p>The collector basically transforms a form definition into an executable program. Once correctly implemented in your website or application, the collector autonomously parses any form definition you build or alter in the editor to an actual working form. You only have to worry about the visuals. We don’t impose any particular UI for the collector.</p>
<h3>Blocks</h3>
<p>You compose forms in the editor with the available <strong>building blocks</strong>. These essentially are the different element types (e.g. <a href="https://www.npmjs.com/package/tripetto-block-text" target="_blank">text input</a>, <a href="https://www.npmjs.com/package/tripetto-block-checkbox" target="_blank">checkbox</a>, <a href="https://www.npmjs.com/package/tripetto-block-dropdown" target="_blank">dropdown</a> etc.) you typically use in a form. You decide which blocks to use in the editor and collector. And you may also develop your own.</p>
<blockquote><p>For this tutorial we are going to use some of our default blocks. But if you want to learn more about creating blocks, take a look <a href="https://docs.tripetto.com/guide/blocks/" target="_blank">here</a>.</p></blockquote>
<p><strong>That’s it for now. Let’s start coding!</strong></p>

<h2>Part A — Setting up your project</h2>
<p>First of all, we’re assuming you have <a href="https://nodejs.org/" target="_blank">Node.js</a> installed. If you don’t, please go ahead and take care of that first. After that, do the following.</p>
<p><strong>1. Create</strong> a new folder for your project. Then create two subfolders in there. One for our source and one for some static files:</p>

```
mkdir your-project
cd your-project
mkdir src
mkdir static
```

<p><strong>2. Initialize</strong> your <code>package.json</code> file:</p>

```
npm init -y
```

<p><strong>3. Install</strong> the required packages:</p>

```
npm install tripetto-collector react react-dom @types/react @types/react-dom @types/superagent typescript ts-loader webpack webpack-cli webpack-dev-server superagent --save-dev
```

<p><strong>4. Create</strong> a <code>tsconfig.json</code> file in the root of your project folder with the following content:</p>

```json
{
  "compilerOptions": {
    "target": "ES5",
    "module": "commonjs",
    "baseUrl": "./src",
    "jsx": "react",
    "strict": true,
    "experimentalDecorators": true
  },
  "include": ["./src/**/*.ts", "./src/**/*.tsx"]
}
```

<p><strong>6. Setup</strong> some test/build tasks by inserting a <code>script</code> section in your<code>package.json</code> file:</p>

```json
"scripts": {
  "test": "webpack-dev-server --mode development",
  "make": "webpack --mode production"
}
```

<p><strong>7. Create</strong> the application entry point by creating the <code>./src/app.tsx</code> file with the following content:</p>

```tsx
import * as React from "react";
import * as ReactDOM from "react-dom";

class Collector extends React.Component {
  render() {
    return (
      <div>
        Hello world!
      </div>
    );
  }
}

ReactDOM.render(
  <Collector />,
  document.getElementById("app")
);
```

<p><strong>8. Create</strong> a static HTML page <code>./static/index.html</code> for your project with the following content:</p>

```html
<!DOCTYPE html>
<html>
<body>
    <div id="app"></div>
    <script src="bundle.js"></script>
</body>
</html>
```

<h3>That’s it for now. Let’s check what we’ve got!</h3>
<p>If you’ve correctly followed the steps above you should now have this file tree:</p>

```
your-project/
  src/
    app.tsx
  static/
    index.html
  package.json
  tsconfig.json
  webpack.config.js
```

<p>This should now be an actual working application. We can run it by executing the following command:</p>

```
npm test
```

<p>This will start the <em class="kq">webpack-dev-server</em> which invokes webpack, compiles your application and makes it available at <code><a href="http://localhost:9000" target="_blank" rel="noopener">http://localhost:9000</a></code>. You should be able to open it with your browser and it should welcome you with the iconic <code>Hello world!</code>. <strong>Yeah!</strong></p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/hello-world.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The editor on the left and the React collector with DevTools open on the right</figcaption>
</figure>
<hr />

<h2>Part B — Coding the collector</h2>
<p>We are going to create a React component for the Tripetto collector. It will be capable of running Tripetto forms and can be used anywhere in your React application. It accepts a form definition as a prop and we use it to implement three blocks (text input, dropdown and checkbox).</p>
<p>Take the steps below to get this going.</p>
<p><strong>1. Create</strong> a <code>./src/helper.tsx</code> file with the following content to implement the collector:</p>

```tsx
import * as Tripetto from "tripetto-collector";
import * as React from "react";

export interface IBlock extends Tripetto.NodeBlock {
  render: () => React.ReactNode;
}

export class CollectorHelper extends Tripetto.Collector<IBlock> {
  render(): React.ReactNode {
    const storyline = this.storyline;

    if (!storyline) {
      return;
    }

    return (
      <>
        {storyline.map((moment: Tripetto.Moment<IBlock>) =>
          moment.nodes.map((node: Tripetto.IObservableNode<IBlock>) =>
            node.block ? (
              <div key={node.key}>{node.block.render()}</div>
            ) : (
              <div key={node.key}>
                {Tripetto.castToBoolean(node.props.nameVisible, true) && (
                  <h3>{node.props.name || "..."}</h3>
                )}
                {node.props.description && <p>{node.props.description}</p>}
              </div>
            )
          )
        )}

        <button
          type="button"
          disabled={storyline.isAtStart}
          onClick={() => storyline.stepBackward()}
        >
          Back
        </button>

        <button
          type="button"
          disabled={
            storyline.isFailed ||
            (storyline.isAtFinish && !storyline.isFinishable)
          }
          onClick={() => storyline.stepForward()}
        >
          {storyline.isAtFinish ? "Complete" : "Next"}
        </button>
      </>
    );
  }
}
```

<p>This class simply extends the base collector class of Tripetto. It implements a simple render function that renders the blocks and some buttons. It also implements an interface called <code>IBlock</code> that defines the contract for the block implementation. In this case each block should implement a method <code>render</code>. In other words, this class <code>CollectorHelper</code> is able to use all the blocks that have implemented a proper <code>render</code> method.</p>
<p><strong>2. Create</strong> a<code>./src/component.tsx</code> file with the following content to implement our React component:</p>

```tsx
import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { CollectorHelper } from "./helper";

export class Collector extends React.PureComponent<{
  definition: Tripetto.IDefinition | string;
  onFinish?: (instance: Tripetto.Instance) => void;
}> {
  collector = new CollectorHelper(this.props.definition);

  /** Render the collector. */
  render(): React.ReactNode {
    return (
      <>
        <h1>{this.collector.name}</h1>
        {this.collector.render()}
      </>
    );
  }

  componentDidMount(): void {
    this.collector.onChange = () => {
      // Since the collector has the actual state, we need to update the component.
      // We are good React citizens. We only do this when necessary!
      this.forceUpdate();
    };

    this.collector.onFinish = (instance: Tripetto.Instance) => {
      if (this.props.onFinish) {
        this.props.onFinish(instance);
      }
    };
  }
}
```

<p>The React component shown above is actually quite simple. It creates a new instance of the collector helper (implemented in step 1) and invokes its render function when the component needs to be rendered. Since the state of the form is kept in the collector instance, the collector should be able to request an update of the component. So the rest of the code inside the component does just that. We also implement an <code>onFinish</code> prop that can be used to bind a function that is invoked when the collector is finished.</p>
<hr />

<h2>Part C — Implementing blocks</h2>
<p>This tutorial covers the implementation of three simple blocks:</p>
<ul>
  <li><a href="https://www.npmjs.com/package/tripetto-block-checkbox" target="_blank"><strong>Checkbox</strong></a><strong>;</strong></li>
  <li><a href="https://www.npmjs.com/package/tripetto-block-dropdown" target="_blank"><strong>Dropdown</strong></a><strong>;</strong></li>
  <li><a href="https://www.npmjs.com/package/tripetto-block-text" target="_blank"><strong>Text input</strong></a><strong>.</strong></li>
</ul>
<p><strong>1. Add</strong> these blocks to the project first. To do so, fire up your terminal/command line and enter:</p>

```
npm install tripetto-block-checkbox tripetto-block-dropdown tripetto-block-text --save-dev
```

<p><strong>2. Add</strong> a special <a href="https://docs.tripetto.com/guide/builder/#cli-configuration" target="_blank">configuration section</a> to our <code>package.json</code> file:</p>

```json
"tripetto": {
  "blocks": [
    "tripetto-block-checkbox",
    "tripetto-block-dropdown",
    "tripetto-block-text"
  ],
  "noFurtherLoading": true
}
```

<p>This section instructs the editor to only load the three blocks we just added. Your <code>package.json</code> should now look like this:</p>

```json
{
  "name": "your-project",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "webpack-dev-server --mode development",
    "make": "webpack --mode production"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "@types/react": "^16.7.18",
    "@types/react-dom": "^16.0.11",
    "@types/superagent": "^3.8.5",
    "react": "^16.7.0",
    "react-dom": "^16.7.0",
    "superagent": "^4.1.0",
    "tripetto-block-checkbox": "^2.0.0",
    "tripetto-block-dropdown": "^2.0.0",
    "tripetto-block-text": "^2.0.0",
    "tripetto-collector": "^1.0.0",
    "ts-loader": "^5.3.2",
    "typescript": "^3.2.2",
    "webpack": "^4.28.2",
    "webpack-cli": "^3.1.2",
    "webpack-dev-server": "^3.1.14"
  },
  "tripetto": {
    "blocks": [
      "tripetto-block-checkbox",
      "tripetto-block-dropdown",
      "tripetto-block-text"
    ],
    "noFurtherLoading": true
  }
}
```

<p><strong>3. Implement</strong> the blocks in the collector component by adding the following files:</p>
<ul>
  <li><code>./src/blocks/checkbox.tsx</code></li>
  <li><code>./src/blocks/dropdown.tsx</code></li>
  <li><code>./src/blocks/text.tsx</code></li>
</ul>
<p>As you can see, a block simply implements a render function. These functions are automatically invoked by the collector library. Each block needs to register itself by calling the <code>@tripetto.block</code> decorator at the top of the class. The source codes for the three blocks we want to implement look as follows.</p>

```tsx
import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Checkbox } from "tripetto-block-checkbox/collector";
import { IBlock } from "../helper";

@Tripetto.block({
  type: "node",
  identifier: "tripetto-block-checkbox"
})
export class CheckboxBlock extends Checkbox implements IBlock {
  render(): React.ReactNode {
    return (
      <label>
        <input
          key={this.key()}
          type="checkbox"
          defaultChecked={this.checkboxSlot.value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            this.checkboxSlot.value = e.target.checked;
          }}
        />
        {this.node.name}
      </label>
    );
  }
}
```

```tsx
import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Dropdown, IDropdownOption } from "tripetto-block-dropdown/collector";
import { IBlock } from "../helper";

@Tripetto.block({
  type: "node",
  identifier: "tripetto-block-dropdown"
})
export class DropdownBlock extends Dropdown implements IBlock {
  render(): React.ReactNode {
    return (
      <div>
        <label>
          {this.node.name || "..."}
          {this.required && <span>*</span>}
        </label>
        {this.node.description && <p>{this.node.description}</p>}
        <select
          key={this.key()}
          defaultValue={this.value}
          onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
            this.value = e.target.value;
          }}
        >
          {this.node.placeholder && (
            <option value="">{this.node.placeholder}</option>
          )}
          {this.props.options.map(
            (option: IDropdownOption) =>
              option.name && (
                <option key={option.id} value={option.id}>
                  {option.name}
                </option>
              )
          )}
        </select>
      </div>
    );
  }
}
```

```tsx
import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Text } from "tripetto-block-text/collector";
import { IBlock } from "../helper";

@Tripetto.block({
  type: "node",
  identifier: "tripetto-block-text"
})
export class TextBlock extends Text implements IBlock {
  render(): React.ReactNode {
    return (
      <div>
        <label>
          {this.node.name || "..."}
          {this.required && <span>*</span>}
        </label>
        {this.node.description && <p>{this.node.description}</p>}
        <input
          key={this.key()}
          type="text"
          required={this.required}
          defaultValue={this.textSlot.value}
          placeholder={this.node.placeholder}
          maxLength={this.maxLength}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            this.textSlot.value = e.target.value;
          }}
        />
      </div>
    );
  }
}
```
<hr />

<h2>Part D — Firing things up</h2>
<p>We are almost done. Follow these final steps to get airborne.</p>
<p><strong>1. Import</strong> your blocks in your <code>app.tsx</code> to make sure the blocks are registered:</p>

```typescript
import "./blocks/checkbox";
import "./blocks/dropdown";
import "./blocks/text";
```

<p><strong>2. Add</strong> some code to load a form definition and feed it to the collector. Open your <code>app.tsx</code> and replace the code with:</p>

```tsx
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as Superagent from "superagent";
import { Collector } from "./component";
import "./blocks/checkbox";
import "./blocks/dropdown";
import "./blocks/text";

// Fetch our form and render the collector component.
Superagent.get("form.json").end((error: {}, response: Superagent.Response) => {
  if (response.ok) {
    ReactDOM.render(
      <Collector definition={response.text} />,
      document.getElementById("app")
    );
  } else {
    alert("Bummer! Cannot load form definition. Does the file exists?");
  }
});
```

<p>We use <a href="https://github.com/visionmedia/superagent" target="_blank">superagent</a> here to fetch the form definition stored in <code>./static/form.json</code> and feed it to the collector component.</p>
<p><strong>3. Install</strong> the Tripetto editor using <a href="https://www.npmjs.com/" target="_blank">npm</a> (detailed instructions <a href="https://docs.tripetto.com/guide/builder/#cli-installation" target="_blank">here</a>) to create a form definition:</p>

```
npm install tripetto -g
```

<p>This command installs the editor globally and allows you to start it from your command line just by entering <code>tripetto</code>. And it allows you to start creating a form by executing the following command from your project folder (make sure you execute this command from the root <code>./</code> of your project folder):</p>

```
tripetto ./static/form.json
```

<p>This will start the editor at <code><a href="http://localhost:3333" target="_blank" rel="noopener">http://localhost:3333</a></code> so you can create your form with it. When you’re done, hit the <strong>Save </strong>button to store the form definition. Verify that the file <code>./static/form.json</code> is present.</p>
<p><strong>4. Start</strong> your collector by executing <code>npm test</code>. Open <code><a href="http://localhost:9000" target="_blank" rel="noopener">http://localhost:9000</a></code> in your browser. You should then see the form running.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/collector.png" alt="Screenshot of collector in Tripetto" />
</figure>
<hr />

<h2>Part E — Processing collected data</h2>
<p>Now that your form is running properly, you probably want to do something with data you collect through it. That's where our <code>onFinish</code> prop comes into play.</p>
<p>Open <code>app.tsx</code> and add the prop as shown below. This will output the collected data to your console when the form is completed. We use the export API for this job.</p>

```tsx
<Collector
  definition={response.text}
  onFinish={(instance: Tripetto.Instance) =>
    console.dir(Tripetto.Export.fields(instance))}
/>
```

<p>Don't forget to import the required symbols. In this case:</p>

```typescript
import * as Tripetto from "tripetto-collector";
```
<hr />

<h2>Browse the full example</h2>
<div>
  <a href="https://gitlab.com/tripetto/examples/react" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / Examples / React example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using React.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-react.png" alt="React logo" />
    </div>
  </a>
</div>

<h3>What else?</h3>
<p>This tutorial is just a basic example of what you can do with Tripetto. It shows you how to implement a collector and some simple building blocks. But Tripetto can do more than that. For example, you can write your own conditional blocks that allow Tripetto to branch on certain conditions — making ever smarter forms. Dive into our <a href="https://docs.tripetto.com" target="_blank">documentation</a> to learn more!</p>

<h3>Documentation</h3>
<p>You can find all Tripetto docs <a href="https://docs.tripetto.com" target="_blank">here</a>. The detailed collector documentation can be found <a href="https://docs.tripetto.com/guide/runner/" target="_blank">here</a>. If you want to develop your own building blocks, have a look around <a href="https://docs.tripetto.com/guide/blocks/" target="_blank">here</a>.</p>

<h3>Other examples</h3>
<p>We also have <a href="https://gitlab.com/tripetto/examples" target="_blank">examples</a> for <a href="https://example-react-material-ui.tripetto.com/" target="_blank">React with Material UI</a>, <a href="https://example-angular-bootstrap.tripetto.com/" target="_blank">Angular</a>, <a href="https://example-angular-material.tripetto.com/" target="_blank">Angular Material</a> and more:</p>
<div>
  <a href="https://gitlab.com/tripetto/examples/react-material-ui" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / Examples / React Material-UI example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using React with Material-UI including pause/resume functionality.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-materialui.png" alt="Material-UI logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://gitlab.com/tripetto/examples/react-conversational" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / Examples / Conversational example using React<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for using Tripetto to build a conversational collector (using React).</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-conversational.png" alt="Conversational logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://gitlab.com/tripetto/examples/angular" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / Examples / Angular example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using Angular including pause/resume functionality.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-angular.png" alt="Angular logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://gitlab.com/tripetto/examples/angular-material" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / Examples / Angular Material example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using Angular Material including pause/resume functionality.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-angular.png" alt="Angular logo" />
    </div>
  </a>
</div>

<h3>Community</h3>
<p>We hope more people will start developing building blocks and collectors for Tripetto. If you have created something yourself and want to share it with the community, add your implementation to our list and create a PR:</p>
<div>
  <a href="https://github.com/tripetto/community" target="_blank" class="blocklink">
    <div>
      <span class="title">tripetto/community<i class="fas fa-external-link-alt"></i></span>
      <span class="description">List of community driven collectors and building blocks for Tripetto.</span>
      <span class="url">github.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
