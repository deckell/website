---
layout: blog-article
base: ../../
permalink: /blog/our-biggest-update-ever-has-arrived/
title: Our biggest update ever has arrived 🎉 - Tripetto Blog
description: We released an amazing update of the Tripetto studio with lots of new features, improvements and bugfixes! And we’re excited to tell you all about it!
article_title: Our biggest update ever has arrived 🎉
author: mark
time: 9
category: product-release
category_name: Product release
tags: [product-release, product, release, showcase]
---
<p>Finally, our biggest update ever has arrived! We admit it took a bit longer than expected, but we think it was worth the wait. It's really packed with new features, improvements and fixes. And we're excited to tell you all about it!</p>

<blockquote>
  <p><b>Update 18 Sept. 2020</b> - At September 18<sup>th</sup> 2020 we also released the updated version of the WordPress plugin that we refer to in this article. <a href="{{ page.base }}blog/our-biggest-update-ever-now-also-available-for-wordpress/" target="_blank">Please read the blog about that WordPress release over here.</a></p>
</blockquote>

<blockquote>
  <p><b>TLDR</b> - We released an amazing update with lots of new features, improvements and bugfixes! Now available in our free <a href="{{ site.url_app }}" target="_blank">studio at tripetto.app</a>, so please go check it out. All updates will also come to the WordPress plugin and SDK soon.</p>
</blockquote>

<h2>Boy, what a journey 😅</h2>
<p>We started developing this update with the goal to add one amazing new feature. But as we were developing that feature and we had to deep dive into the cores of Tripetto, we saw more and more opportunities to improve so much more at once. Not only some bugfixes here and there, but also some very important features that were requested often by our users and at the top of our wishlist for a long time.</p>
<p>And so, this update has turned out not to be just one new feature, but a big bundle of new features, improvements and bugfixes that take the whole Tripetto platform to the next level. That's exactly the reason it took a little bit longer to release this update (ahum 🙄). But from now on we'll be updating more frequently. That's a promise!</p>
<hr />

<h3>Available for free now!</h3>
<p><strong>All updates are available for free now in our <a href="{{ site.url_app }}" target="_blank">studio at tripetto.app</a>, so have a look around and try for yourself.</strong></p>
<p>By the way, all updates will of course also be released in the WordPress plugin and SDK. We're working very hard to release those really soon.</p>
<p>🎁 So let's have a look what's in the update!</p>
<hr />

<h2>Form faces</h2>
<p>The biggest new feature is what we call the <strong>form faces</strong>. If you've used Tripetto before the update, you'll know how all Tripetto forms worked: scrolling through the form, asking questions one by one. We still think this is a very convenient way to engage with your respondents through your forms, resulting in <a href="{{ page.base }}higher-completion-rates-with-logic/" target="_blank">higher completion rates</a>.</p>
<p>But we also got some signals from our users that they wanted to use the smartness of Tripetto, but with a different form experience for their respondents. It turned out not all use cases benefit from the experience we offered. Some use cases would benefit from a simpler interface, whereas others would benefit from an even more conversational experience.</p>
<p>And so, we decided to give <i>you</i> the choice: How would <i>you</i> like to expose each of your forms? And from now on we give you three choices on that:</p>
<ul>
  <li>
    <h3>Autoscroll form face</h3>Presenting your form in a scrolling interface and experience, scrolling through the questions one by one. This is the initial form face of all Tripetto forms, but it's rebuilt from the ground up with lots of enhancements and improvements.
    <figure>
      <img src="{{ page.base }}images/help/styling-autoscroll/autoscroll-demo.gif" alt="Screenshot of an autoscoll form face in Tripetto" />
      <figcaption>Example of the autoscroll form face.</figcaption>
    </figure>
  </li>
  <li>
    <h3>Chat form face</h3>Presenting your form in a chat interface and experience, making it feel like a real conversation with question bubbles and answer bubbles. And you can even create your own avatar!
    <figure>
      <img src="{{ page.base }}images/help/styling-chat/chat-demo.gif" alt="Screenshot of a chat form face in Tripetto" />
      <figcaption>Example of the chat form face.</figcaption>
    </figure>
  </li>
  <li>
    <h3>Classic form face</h3>Presenting your form in a more classic interface and experience, with the ability to show multiple questions at a time.
    <figure>
      <img src="{{ page.base }}images/help/styling-classic/classic-demo.gif" alt="Screenshot of a classic form face in Tripetto" />
      <figcaption>Example of the classic form face.</figcaption>
    </figure>
  </li>
</ul>
<h3>Just try all form faces</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/customize/00-switch.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Switch your form face in the form builder.</figcaption>
</figure>
<p>You can just switch these form faces inside the form builder at any time. You'll see the live preview update instantly to the chosen form face and you can test your whole form behavior in the preview right away.</p>
<p>And the best part is you don't have to worry about your form's question blocks, structure, logic and styling: everything just works in every form face! 🤯</p>
<p><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" target="_blank">How to switch between form faces</a></span></p>
<hr />

<h2>Other new features</h2>
<p>But that's not all. Here's a list of all other new features that we've packed in this update:</p>
<ul>
  <li>
    <h3>Welcome message</h3>
    <figure class="inline-right">
      <img src="{{ page.base }}images/help/editor-start/welcome-demo.png" alt="Screenshot of a welcome message in Tripetto" />
      <figcaption>Example of a welcome message.</figcaption>
    </figure>
    Make a real good first impression by adding a beautiful welcome message. This way you can introduce yourself and/or the form and invite respondents to start the form.<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-add-a-welcome-message/" target="_blank">How to add a welcome message</a></span>
  </li>
  <li><h3>Flexible closing messages</h3>Thank your respondents in a personal way with flexible closing messages, based on the input of each respondent. That way you can customize the outcome for each respondent.<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/" target="_blank">How to add one or multiple closing messages</a></span></li>
  <li><h3>Redirect at form completion</h3>Instead of a closing message, redirect your respondent directly to another page, for example your own landing page to sell your products.<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/" target="_blank">How to redirect to a URL at form completion</a></span></li>
  <li><h3>Full translations</h3>The ability to translate all labels inside the form was an often-heard request. You can now translate all labels inside the form; such as buttons, messages, errors, etc.<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">How to fully translate your forms</a></span></li>
  <li><h3>New question blocks</h3>We added the following often requested question blocks:
    <ul>
      <li>Date (and time), including ranges and the ability to perform logic on answered date values;<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-use-the-date-and-time-block/" target="_blank">How to use the date (and time) block</a></span></li>
      <li>Telephone number;<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-use-the-phone-number-block/" target="_blank">How to use the phone number block</a></span></li>
      <li>More new question blocks coming up!</li>
    </ul>
  </li>
  <li><h3>New action block</h3>We added an action block called 'Raise error'. You can use it to prevent a form from submitting; for example, when a respondent doesn't fit your requirements.<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-use-the-raise-error-block/" target="_blank">How to use the raise error block</a></span></li>
  <li>
    <h3>New branch conditions for logic</h3>
    <figure class="inline-right">
      <img src="{{ page.base }}images/help/editor-branch-conditions/05-add.gif" alt="Screenshot of the form builder in Tripetto" />
      <figcaption>Branch conditions in the form builder.</figcaption>
    </figure>
    Create even smarter forms with the new possibilities to create branch conditions. You can now create conditions based on:
    <ul>
      <li>Basic conditions: Basic conditions based on the value of a certain question block and/or action block, like a check if your respondent selected a certain option;</li>
      <li>Evaluate conditions: Advanced conditions based on the value of a certain question block and/or action block, like a check if your respondent entered a date between a certain range of start and end dates;</li>
      <li>Regular expression conditions: Conditions based on a regular expression, like a check if your respondent entered a certain format of membership number;</li>
      <li>Device conditions: Conditions based on the device of the respondent, like a check if your respondent is using a mobile phone;</li>
      <li>Password match: Protect (parts of) your form by the new password match check.</li>
    </ul>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">How to use different types of branch conditions for your logic</a></span><br/>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-verify-passwords-inside-your-form/" target="_blank">How to verify passwords inside your form</a></span><br/>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">Discover the power of branches for your logic</a></span>
  </li>
  <li>
    <h3>New branch behavior for logic</h3>
    <figure class="inline-right">
      <img src="{{ page.base }}images/help/editor-branch-behavior/00-add.gif" alt="Screenshot of the form builder in Tripetto" />
      <figcaption>Branch behavior in the form builder.</figcaption>
    </figure>
    Smart forms are what it's all about, so new branch behaviors will help you with that. You can now choose from these branch behaviors:
    <ul>
      <li>For the first condition match: Follow the branch if <i>at least one</i> of the conditions matches and do so for the first applicable match;</li>
      <li>When all conditions match: Follow the branch if <i>all</i> the conditions match;</li>
      <li>For each condition match (iteration): Follow the branch for <i>each</i> condition match. This will create an iteration of the branch.</li>
    </ul>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/" target="_blank">How to use different types of branch behavior for your logic</a></span><br/>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">Discover the power of branches for your logic</a></span>
  </li>
</ul>
<hr />

<h2>Improvements</h2>
<p>Some big, some small, but lots of improvements to existing features have been made in this update:</p>
<ul>
  <li><h3>Improved styling</h3>
    <figure class="inline-right">
      <img src="{{ page.base }}images/help/styling/demo-whatsapp.gif" alt="Screenshot of styling in Tripetto" />
      <figcaption>From basic to Whatsapp styling in a few seconds.</figcaption>
    </figure>
    The improved styling really takes your form to the next level, as you can now fully style your form to meet your brand's style guides, or just create the most beautiful looking form ever. Styling has also been rebuilt from the ground up for more control:
    <ul>
      <li>Coloring: Full flexible color choices;</li>
      <li>Font: Select a predefined font or even use all <a href="https://fonts.google.com/" target="_blank">Google Fonts</a> instantly;</li>
      <li>Font size: Set the font size and the form will grow/shrink with it;</li>
      <li>Backgrounds: Set the background color or a background image;</li>
      <li>Inputs: Style your form input controls;</li>
      <li>Buttons: Style your form buttons;</li>
      <li>Avatar: Create your own avatar in the chat form face.</li>
    </ul>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">How to style your forms</a></span><br/><br/>
    <i>N.B. We did our best to maintain the styling of your present forms. Despite our efforts, it could be that you have to (partly) improve or update your styling.</i>
  </li>
  <li><h3>Improved Autoscroll form face</h3>This is the original form runner in Tripetto and we improved the following to it:
      <figure class="inline-right">
        <img src="{{ page.base }}images/help/styling-autoscroll/autoscroll-demo-2.gif" alt="Screenshot of a Tripetto form" />
        <figcaption>Demo of a horizontal scroll direction.</figcaption>
      </figure>
    <ul>
      <li>Scroll direction: You can now determine if the form should scroll vertically (top-down) or horizontally (left-right). This really makes it a whole different experience at once;</li>
      <li>Alignment: You can now determine if the form should align at the top, middle or bottom of the screen;</li>
      <li>Hide previous/upcoming questions: You can now determine if previous and/or upcoming questions should be partly visible, or totally hidden;</li>
      <li>Direct complete: You can now choose to show the Complete button directly after the last question, instead of an extra OK button first.</li>
    </ul>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/" target="_blank">Discover additional options for autoscroll form face</a></span>
  </li>
  <li><h3>Improved question blocks</h3>We improved some stuff in the following question blocks:
    <ul>
      <li>Rating: You can now determine the amount of rating steps/scale, so you're no longer stuck to a number of only 5 stars for ratings;</li>
      <li>Text single line: You can now use the autocomplete functionality of the browser to prefill a value;</li>
      <li>Multiple choice/checkboxes: For the options of multiple choice and checkboxes you can now define 'exclusive options'. When an option is marked as exclusive, the other options can no longer be selected by the respondent once the ‘exclusive’ option is selected.</li>
    </ul>
  </li>
  <li><h3>Improved send email block</h3>We extended the send email block with a few handy features:
    <ul>
      <li>Sender: You can now set a sender email address. This will be used as a 'reply-to' address for the sent email;</li>
      <li>Form data: You can now include all form data directly inside the sent email;</li>
      <li>Exportability: The data of the send email block is no longer saved to the dataset by default. And if you do want to save the data, you can choose which of the email data you want to be saved.</li>
    </ul>
    <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank">How to use the send email block</a></span>
  </li>
  <li><h3>Improved form embedding</h3>Embedding your forms in your own website sometimes resulted in some conflict issues with your own scripts and styles. We've completely remastered the way the form technically gets embedded, so these conflicts belong to the past. We also improved the automated height calculation of embedded forms, so your forms will grow with your content.<br/>
  <span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/" target="_blank">How to embed a form from the studio into your website</a></span><br/><br/>
  <i>N.B. Your current embed codes keep working automatically, but you may update your embed code by copy-pasting the new embed code from the Share screen in the studio.</i>
  </li>
  <li><h3>Improved data control</h3>You can now determine for each question block individually if the responses to that block should be saved to the dataset. This gives you more control over the saved data, so you don't save unnecessary information (<a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/" target="_blank">Hi GDPR</a>).<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" target="_blank">How to determine what data fields get saved</a></span></li>
  <li><h3>Improved spam protection</h3>Because of the way Tripetto works, spammers already had a hard time trying to spam form entries. But we took spam protection to the next level without annoying your respondents with it (no CAPTCHAs, no need to select the photos with bridges, cars or other stuff).<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-tripetto-prevents-spam-entries-from-your-forms/" target="_blank">How Tripetto prevents spam entries from your forms</a></span></li>
  <li><h3>Improved sharing previews</h3>When you're sharing a Tripetto link on socials, you'll see a sharing preview. You can now set the title and description that your audience sees in that sharing preview to improve your click rate.<br/><span class="help-center"><span><i class="fas fa-life-ring"></i> Help Center</span> <a href="{{ page.base }}help/articles/how-to-optimize-your-form-for-better-sharing/" target="_blank">How to optimize your form for better sharing</a></span></li>
</ul>
<hr />

<h2>Bug fixes</h2>
<p>Of course, we always try to fix bugs as soon as possible, but in this update we smashed a few that we had collected along the way:</p>
<ul>
  <li>Fixed a bug in the validation of empty number fields;</li>
  <li>Fixed a bug while filling out number fields in Firefox browser;</li>
  <li>Fixed the unwanted behavior of the first radio button getting selected for required radio button questions;</li>
  <li>Fixed a bug on Android devices having difficulty with showing the keyboard while filling out the form;</li>
  <li>Fixed the unwanted ability to create 'infinite loops' inside your form, resulting in a freezing form.</li>
</ul>
<hr />

<h2>That's it!</h2>
<p>That's a pretty big update if you ask us 🤐 Hopefully it enables you to create even more stunning form experiences that boost your completion rates.</p>
<p>We also extended the <a href="{{ page.base }}help/" target="_blank">Help Center</a> with articles on all new features. And we added search boxes to the help sections of the <a href="{{ page.base }}help/studio/" target="_blank">studio</a> and <a href="{{ page.base }}help/wordpress/" target="_blank">WordPress plugin</a> to quickly find what you're looking for.</p>
<p>Please let us know if you have any other questions, remarks or requests. Or if you just want to show us your awesome creations 😎</p>
