---
layout: blog-article
base: ../../
permalink: /blog/how-we-added-our-first-premium-feature-with-our-own-no-code-form-solutions/
title: How we added a premium feature with our own no-code form solutions - Tripetto Blog
description: Since this week you can remove Tripetto branding from your forms in the studio. And we built it with our own no-code form solutions!
article_title: How we added our first premium feature with our own no-code form solutions
author: jurgen
time: 4
category: showcase
category_name: Showcase
tags: [showcase, background-story]
---
<p>Since this week you can <a href="{{ page.base }}help/articles/how-to-remove-branding-in-studio-forms/" target="_blank">remove Tripetto branding</a> from your forms in the studio. That's great news of course, but we also are enthusiastic about how we built this within just a few hours without any coding. Using our own form solutions! 🤯</p>

<h2>Introducing the removal of Tripetto branding</h2>
<p>As you may know, <a href="{{ page.base }}blog/our-biggest-update-ever-has-arrived/" target="_blank">we launched a very big update to the Tripetto Studio</a> last week. The reactions were overwhelming, so thanks for all your kind messages 🙏</p>
<p>In the days that followed we received several requests if it was possible to remove the Tripetto branding from the forms. Our first answer was '<i>No, not yet</i>', but it got us thinking...</p>

<h3>Why we show our branding</h3>
<p>Everything in the <a href="{{ site.url_app }}" target="_blank">studio at tripetto.app</a> is free for now, as we focus on growing our user base. In exchange for the free usage, we show our name at some subtle places inside each form. This has a high value for us in our growing ambitions, as that's the best commercial spot we can have. Hopefully people that see your Tripetto forms want to know more about it by simply clicking on our name.</p>
<p>A possible downside on always showing our branding is that professionals often cannot use our forms, as they don't want a third party name on their forms. But of course we also want them to be able to use Tripetto, so we went searching for a quick solution to remove our branding.</p>

<h2>No-code solution, using our own forms</h2>
<p>Luckily we already prepared the removal of branding inside our forms. The missing link was the possibility from within the studio to purchase, pay and enable it. But with limited development time, we weren't able to build a whole payment system and let that take care of the whole process.</p>
<p>So we went searching for an easier solution. And we didn't have to look very far:</p>
<p><strong>We can just use our own Tripetto forms inside our own Tripetto Studio to sell our own first Tripetto premium feature! 🤯</strong></p>

<h2>Showcase</h2>
<p>This is how the result looks. Please keep reading to learn how we used the benefits of Tripetto forms to make it a no-code solution.</p>
<figure>
  <img src="{{ page.base }}images/help/branding/00-branding-studio.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The result of the Tripetto form inside the Tripetto Studio.</figcaption>
</figure>

<h3>🛒 Embed the form inside our studio</h3>
<p>Tripetto forms can be seamlessly <strong><a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/" target="_blank">embedded</a></strong> inside websites, apps and other software. We created a button inside our Tripetto Studio and that button just opens up the embedded form to request for branding removal, right inside the studio.</p>

<h3>🎨 Style the form</h3>
<p>Tripetto forms can be <strong><a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">styled</a></strong> exactly the way you like/need. We styled our form in such a way it fits perfectly inside our studio interface. And we gave it a personal feeling by using the <strong><a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" target="_blank">chat form face</a></strong>. If you wouldn't know better, you'd think you were talking to a real person inside the studio itself!</p>

<h3>📣 Let the studio talk to the form</h3>
<p>Tripetto forms can be <strong><a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/" target="_blank">fed with data</a></strong> to use in the form. We send the account and form data to the embedded request form and read that data inside the form with <strong><a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/" target="_blank">hidden fields</a></strong>. The user does not have to supply this data; the form is so smart it already knows all required data to quickly finish the request.</p>

<h3>🙋‍♀️ Let the user agree pricing and terms</h3>
<p>Tripetto forms of course can <strong><a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/" target="_blank">gather inputs</a></strong> of users. We primarly use the Yes-No question block to let the user agree on pricing, the process and our terms.</p>

<h3>📫 Send a confirmation email to the user</h3>
<p>Tripetto forms can <strong><a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank">send emails</a></strong> to an email address that's entered inside the form, including the form data. In this case we use the email address of the Tripetto account to send a confirmation mail to the user.</p>

<h3>🔔 Send the request data to Tripetto's Slack</h3>
<p>Tripetto forms can <strong><a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/" target="_blank">send notifications to Slack</a></strong> for every new entry, including the form data. We receive every new request in a centralized Slack channel, where we also keep track on the progress of the follow-up. In this way the whole team always knows the up-to-date status of each request.</p>

<h2>All inside a Tripetto form without a single line of code!</h2>
<p>In this showcase we used a Tripetto form inside our own studio to initiate the request process. A nice example of how Tripetto forms can help you to create no-code solutions within just a few hours.</p>
<p>You could even extend this by automating more follow-up processes with <strong><a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/" target="_blank">webhook integrations</a></strong>. You could for example connect your form to your payment processor (like PayPal) and let automatically create a new payment request for each new entry. With thousands of apps to connect to, the possibilities really are almost endless.</p>

<p>So, what are you going to automate with Tripetto, without any coding? And hey, while you're doing that, don't forget you can <strong><a href="{{ page.base }}help/articles/how-to-remove-branding-in-studio-forms/" target="_blank">remove the Tripetto branding</a></strong> inside your forms! 😉</p>
