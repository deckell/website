---
layout: blog-article
base: ../../
permalink: /blog/introducing-picture-choice-and-scale-blocks/
title: Introducing picture choice and scale blocks - Tripetto Blog
description: We are happy to announce two brand new question blocks, namely the picture choice block and the scale block. Plus a handy new feature in the rating block.
article_title: Introducing picture choice and scale blocks
author: jurgen
time: 3
category: feature-spotlight
category_name: Feature spotlight
tags: [feature-spotlight, feature, editor, blocks]
---
<p>We are happy to announce two brand new question blocks, namely the picture choice block and the scale block. Plus a handy new feature in the rating block.</p>
<p>All available in the studio at tripetto.app and the WordPress plugin for our WordPressers.</p>

<h2>NEW! The picture choice block</h2>
<p>One of the most requested features was the possibility to use images as answer options. We now fully support that with the introduction of the new <a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/" target="_blank"><strong>Picture choice</strong></a> block. The usage of the picture choice block really lights up your forms and surveys. Making it even more fun and easy to fill out by your respondents.</p>
<p>You can use images or directly insert any emoji inside each option. And picture choice questions are usable for single and multiple selection questions!</p>
<p>Of course the picture choice block also is fully customizable with styling and translations, so it fits perfectly inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/picture-choice.gif" alt="Screenshot of picture choice block in Tripetto" />
  <figcaption>Demonstration of two picture choice blocks with images and emojis.</figcaption>
</figure>

<h2>NEW! The scale block</h2>
<p>Another important block that we release now is the <a href="{{ page.base }}help/articles/how-to-use-the-scale-block/" target="_blank"><strong>Scale</strong></a> block. This enables your respondents to select a value from a certain scale you give them.</p>
<p>That scale can be a <strong>numeric scale</strong>. Ideal for a Net Promoter Score question from 0 to 10, but you can even use it to collect negative values like between -5 and +5.</p>
<p>But the scale can also be a <strong>text scale</strong>, for example from Very bad to Very good with some options in between.</p>
<p>And again, the scale block of course is fully customizable with styling and translations.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/scale.gif" alt="Screenshot of picture choice block in Tripetto" />
  <figcaption>Demonstration of two scale blocks with a numeric scale and a text scale.</figcaption>
</figure>

<h2>IMPROVED! The rating block</h2>
<p>We already had the <a href="{{ page.base }}help/articles/how-to-use-the-rating-block/" target="_blank"><strong>Rating</strong></a> block, but that was limited to a star shape. We now added more shapes to choose from, so you can now present your rating in stars, hearts, thumbs, or persons. Just choose which shape suits your question the best.</p>
<p>You miss any shape? Please let us know and we will see if we can add that, too.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/rating.gif" alt="Screenshot of picture choice block in Tripetto" />
  <figcaption>Demonstration of three rating blocks with different shapes and steps.</figcaption>
</figure>
<hr/>

<h2>Now available everywhere</h2>
<p>All the above is now available across all our platforms, so for the studio at tripetto.app and the WordPress plugin for our WordPress friends.</p>
<p>And along these new blocks we did numerous smaller and bigger improvements over the last few weeks. You can see all our updates in our <a href="{{ page.base }}changelog/" target="_blank">changelog</a>.</p>

<h2>What's next?</h2>
<p>At the top of our <a href="{{ page.base }}roadmap/" target="_blank">roadmap</a> is now the <strong>Calculator block</strong>. We promise this will be a very flexible and powerful block to perform all kinds of calculations inside your forms. The calculator is under development as we speak and we do our best to introduce it as soon as possible. So please <a href="{{ page.base }}subscribe/" target="_blank">stay tuned</a>!</p>
