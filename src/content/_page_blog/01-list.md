---
base: ../
---

<section class="blog-list">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 col-xl-8">
        <ul>
          {% for article in site.articles_blog reversed %}
          <li>
            <div onclick="window.location='{{ article.url }}';">
              <a href="{{ article.url }}"><h2>{{ article.article_title }}</h2></a>
              <p>{{ article.description }}</p>
              <small class="pills">
                <span class="pill-splitter pill-splitter-after">{{ article.category_name }}</span>{% if article.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ article.time }} Min.</span>{% endif %}{% if article.time_video > 0 %}<span><i class="fas fa-video"></i>{{ article.time_video }} Min.</span>{% endif %}
              </small>
              {% include blog-article-info.html date=article.date author=article.author role=article.role avatar=article.avatar time=article.time time_video=article.time_video %}
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>

