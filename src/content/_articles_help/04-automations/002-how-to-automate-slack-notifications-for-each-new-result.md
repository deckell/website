---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automate-slack-notifications-for-each-new-result/
title: How to automate Slack notifications for each new result - Tripetto Help Center
description: You can get automated Slack notifications when someone completes a form. Here is how to enable this feature.
article_title: How to automate Slack notifications for each new result
article_id: slack
article_folder: automate-slack
author: jurgen
time: 3
category_id: automations
subcategory: automations_notifications
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-automate-slack-notifications-in-the-studio/
- /help/articles/how-to-automate-slack-notifications-in-the-wordpress-plugin/
---
<p>You can get automated Slack notifications when someone completes a form. Here is how to enable this feature.</p>

<h2 id="when-to-use">When to use</h2>
<p>It can be handy to get notified when a respondent completes a form, for example to have a look at its response. Tripetto offers multiple ways to get notified, one of which is a <strong>Slack notification</strong>. You can very easily enable this for each form you'd like to.</p>

<h2 id="how-to-use">How to use</h2>
<p>You must take some steps in Slack to prepare the Slack notification.</p>

<h3>Create Incoming Webhook in Slack</h3>
<p>Before Slack can receive messages from external sources (like in this case Tripetto) you have to create and Incoming Webhook in your Slack App. How to set this up, gets very well explained in <a href="https://api.slack.com/messaging/webhooks" target="_blank">this article</a> at the Slack website.</p>
<div>
  <a href="https://api.slack.com/messaging/webhooks" target="_blank" class="blocklink">
    <div>
      <span class="title">Sending messages using Incoming Webhooks - Slack<i class="fas fa-external-link-alt"></i></span>
      <span class="description">We're going to walk through a really quick 4-step process that will have you posting messages using Incoming Webhooks in a few minutes.</span>
      <span class="url">api.slack.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-slack.png" alt="Slack logo" />
    </div>
  </a>
</div>
<p>You only have to follow steps 1 to 3, so don't worry about step 4.</p>
<p>At the end of step 3 you will get a webhook URL from Slack that you'll need to use in Tripetto.</p>

<h3 id="enable">Enable Slack notifications</h3>
<p>Now back to Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code>. The Automate pane will show up on the right side of the form builder.</p>
<p>The second feature of this screen is <code>Slack notification</code>. After enabling the feature <code>Send a Slack message when someone completes your form</code> you can enter the webhook URL you got from your Slack App. This will result in a simple Slack message in the designated Slack channel when someone completes the form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-slack.png" alt="Screenshot of Slack notification in Tripetto" />
  <figcaption>Enable Slack notification.</figcaption>
</figure>

<h3 id="response-data">Include response data</h3>
<p>It's also possible to include all response data of the completed form directly inside the Slack message. To do so, just enable the checkbox <code>Include response data in the message</code> and all given answers will be shown in the Slack message automatically.</p>

<blockquote>
  <h2>How we use Slack notifications at the office</h2>
  <p>We think Slack notifications are especially handy for teams. At Tripetto we use Slack notifications for example in all of our website forms. It enables us to keep track of all submitted forms, including a centralized archive of all submitted form data.</p>
  <p>We've also made the internal agreement to mark each Slack notification with a <code><i class="fas fa-eye"></i></code> icon when you've read the message, so we all know who saw the message.</p>
  <p>And we use the Slack threads for each message to discuss potential follow-ups.</p>
</blockquote>
