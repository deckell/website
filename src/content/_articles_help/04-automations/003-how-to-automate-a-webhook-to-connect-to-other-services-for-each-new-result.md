---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/
title: How to automate a webhook to connect to other services for each new result - Tripetto Help Center
description: You can connect your response data to all kinds of other online services and configure automatic follow-up actions. Without a single line of code. Fully no-code!
article_title: How to automate a webhook to connect to other services for each new result
article_id: webhook
article_folder: automate-webhook
author: mark
time: 4
category_id: automations
subcategory: automations_webhook
areas: [studio, wordpress]
common_content_core: true
redirect_from:
- /help/articles/how-to-automate-a-webhook-for-each-new-result-using-zapier/
- /help/articles/how-to-automate-a-webhook-using-Zapier-in-the-studio/
- /help/articles/how-to-automate-a-webhook-using-Zapier-in-the-wordpress-plugin/
---
<p>You can connect your response data to all kinds of other online services and configure automatic follow-up actions. Without a single line of code. Fully no-code!</p>

<div class="article-content-core">
<h2 id="when-to-use">When to use</h2>
<p>Tripetto helps you to collect response data, by letting your respondents fill out your form(s). That's nice and valuable, but in some cases you want to do more with that data, like automatically use the data in other software services to add follow-up actions to it. Our <strong>webhook</strong> will help you with this to make your life a whole lot easier!</p>
<h3 id="examples">Examples</h3>
<p>There are lots of possible connections you could think of, but these are some popular services that help you with everyday tasks:</p>
</div>

<ul class="tiles tiles-two">
{% include tile.html url='/help/articles/how-to-connect-to-other-services-with-integromat/' type='Integromat' title='Tripetto to Google Sheets' description='Add a new row to a Google Sheet with all Tripetto response data.' webhook-service='google-sheets' webhook-service-name='Google Sheets' palette-top='light' palette-bottom='light' %}
{% include tile.html url='/help/articles/how-to-connect-to-other-services-with-integromat/' type='Integromat' title='Tripetto to MailChimp' description='Add a new subscriber to a MailChimp audience based on Tripetto response data.' webhook-service='mailchimp' webhook-service-name='MailChimp' palette-top='light' palette-bottom='light' %}
{% include tile.html url='/help/articles/how-to-connect-to-other-services-with-integromat/' type='Integromat' title='Tripetto to Zendesk' description='Create a new Zendesk support ticket based on Tripetto response data.' webhook-service='zendesk' webhook-service-name='Zendesk' palette-top='light' palette-bottom='light' %}
{% include tile.html url='/help/articles/how-to-connect-to-other-services-with-integromat/' type='Integromat' title='Tripetto to Airtable + PayPal' description='Add a new record in an Airtable database with all Tripetto response data and create a payment in PayPal.' webhook-service='airtable' webhook-service-name='Airtable' webhook-chain-service='paypal' webhook-chain-service-name='PayPal' palette-top='light' palette-bottom='light' %}
</ul>

<div class="article-content-core">
<h3 id="no-code">No-code</h3>
<p>Now you're probably thinking: I can't code, this is way to difficult for me...</p>
<p>But that's where the <strong>webhooks</strong> show up! Those help you to automatically send your data to other services, without coding. <strong>Fully no-code!</strong></p>
<p>A webhook is an endpoint that you can trigger by sending data to its URL. The webhook URL will then receive the response data from your Tripetto form and then connect that data to other services.</p>

<h2 id="how-to-use">How to use</h2>
<p>Setting up a webhook takes a few steps, which we will describe globally in this article. Based on your choice for a certain automation tool (see step 1), we have other articles with detailed instructions for each automation tool.</p>

<h3 id="automation-tools">Step 1 - Prepare automation tool</h3>
<p>Let's begin with choosing a tool that is going to help us: an <strong>automation tool</strong>. Basically that's the glue between your Tripetto form and other online services. The automation tool receives your data via a webhook URL and sends that data to the third party service(s) that you want to connect with. Without a single line of code.</p>
<p>There are many automation tools available out there. We will help you with setting up a Tripetto webhook in some popular automation tools, namely:</p>
<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/integromat.svg" alt="Logo Integromat" />Integromat</h4>
<p>Integromat offers a dedicated Tripetto app that makes the webhook process a little easier. <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-integromat/">Click here for instructions how to connect with Integromat</a>.</p>
<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/pabbly.png" alt="Logo Pabbly" />Pabbly Connect</h4>
<p>Pabbly Connect offers a dedicated Tripetto app that makes the webhook process a little easier. <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Click here for instructions how to connect with Pabbly Connect</a>.</p>
<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/zapier.svg" alt="Logo Zapier" />Zapier</h4>
<p>Zapier does not offer a dedicated Tripetto app, but you can still connect your Tripetto forms by using their Webhook app. <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Click here for instructions how to connect with Zapier</a>.</p>
<blockquote>
  <h4>Choose your automation tool wisely</h4>
  <p>Integromat, Pabbly Connect and Zapier are some examples, but there are lots of other automation tools that you can use. It depends on your own needs, wishes and budget which tool fits you best. That's why we advise to have a close look at the features and pricings before you select a certain automation tool.</p>
</blockquote>

<h3 id="webhook">Step 2 - Connect webhook</h3>
<p>After you have setup your automation tool you receive a unique webhook URL from them. You need that URL to be pasted into Tripetto.</p>
<p>At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code>. The Automate pane will show up on the right side of the form builder.</p>
<p>The third option of this screen is <code>Webhook</code>. After enabling the feature <code>Notify an external service when someone completes your form</code> you can copy-paste the webhook URL you got from your automation tool.</p>
<p>Now your Tripetto form is connected to your automation tool. Your automation tool can now receive response data from your Tripetto form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto.png" alt="Screenshot of the webhook in Tripetto" />
  <figcaption>Enter the webhook URL in Tripetto (the entered URL is just an example).</figcaption>
</figure>

<h3 id="test">Step 3 - Receive response data</h3>
<p>Next step is to test if your automation tool is indeed receiving the response data from your Tripetto form. It depends on your automation tool how that's done exactly.</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-integromat/">Click here for instructions to connect with Integromat</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Click here for instructions to connect with Pabbly Connect</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Click here for instructions to connect with Zapier</a>.</li>
</ul>
<p>To simply test the connection, you can use the <code>Test</code> button in Tripetto to send a test message. This will not contain your real response data from your form. That's why we advise to also send a real completed form response, so you can see your data gets received by your automation tool.</p>

<h3 id="services">Step 4 - Add services</h3>
<p>Now back to your automation tool to add the connections with other services that you want. It depends on your automation tool and the service(s) you're connecting to how that process works exactly.</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-integromat/">Click here for instructions to connect with Integromat</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Click here for instructions to connect with Pabbly Connect</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Click here for instructions to connect with Zapier</a>.</li>
</ul>

<h3 id="services">Step 5 - Activate magic</h3>
<p>If you have tested your connection, you can activate your automation process. From now on, each newly completed Tripetto form will send its data to your webhook and the webhook will execute the magic into other services. Again, it depends on your automation tool and the service(s) you're connecting to how you can activate your automation process.</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-integromat/">Click here for instructions to connect with Integromat</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Click here for instructions to connect with Pabbly Connect</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Click here for instructions to connect with Zapier</a>.</li>
</ul>

</div>
<script src="{{ page.base }}js/tiles.js?v={{ site.cache_version }}"></script>
