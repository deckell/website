---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automate-email-notifications-for-each-new-result/
title: How to automate email notifications for each new result - Tripetto Help Center
description: You can get automated email notifications when someone completes a form. Here is how to enable this feature.
article_title: How to automate email notifications for each new result
article_id: email
article_folder: automate-email
author: jurgen
time: 1
category_id: automations
subcategory: automations_notifications
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-automate-email-notifications-in-the-studio/
- /help/articles/how-to-automate-email-notifications-in-the-wordpress-plugin/
---
<p>You can get automated email notifications when someone completes a form. Here is how to enable this feature.</p>

<h2 id="when-to-use">When to use</h2>
<p>It can be handy to get notified when a respondent completes a form, for example to have a look at its response. Tripetto offers multiple ways to get notified, of which one is an <strong>email notification</strong>. You can very easily enable this for each form you'd like to.</p>
<blockquote>
  <h4>Having email troubles in WordPress?</h4>
  <p>If you're using our WordPress plugin, the sending of emails is handled by your own WordPress installation. <a href="{{ page.base }}help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/" target="_blank">Click here for troubleshooting if your WordPress installation is not sending emails.</a></p>
</blockquote>

<h2 id="how-to-use">How to use</h2>
<p>At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code>. The Automate pane will show up on the right side of the form builder.</p>
<p>The first feature of this screen is <code>Email notification</code>. After enabling the feature <code>Send an email when someone completes your form</code> you can enter the email address of the recipient. This will result in a simple email message to the entered recipient when someone completes the form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-email.png" alt="Screenshot of email notification in Tripetto" />
  <figcaption>Enable email notification.</figcaption>
</figure>

<h3 id="response-data">Include response data</h3>
<p>It's also possible to include all response data of the completed form directly inside the email message. To do so, just enable the checkbox <code>Include response data in the message</code> and all given answers will be shown in the email message automatically.</p>
