---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-connect-to-other-services-with-integromat/
title: How to connect to other services with Integromat - Tripetto Help Center
description: Integromat is our preferred automation partner that helps you to connect your Tripetto response data to other online services. This article describes how to enable our webhook for this and how to configure it with Integromat.
article_title: How to connect to other services with Integromat
article_id: webhook
article_folder: automate-webhook-integromat
author: jurgen
time: 5
category_id: automations
subcategory: automations_webhook
areas: [studio, wordpress]
common_content_core: true
---
<p>Integromat is our preferred automation partner that helps you to connect your Tripetto response data to other online services. This article describes how to enable our webhook for this and how to configure it with Integromat.</p>

<div class="article-content-core">
<h2 id="when-to-use">When to use</h2>
<p>By connecting Tripetto to other services you can do all kinds of actions with your response data, like pushing it to a spreadsheet editor (Microsoft Excel/Google Sheets) or a database, or trigger other follow-up actions. The possibilities are endless!</p>
<p>Integromat is our preferred partner to help you with this. Follow the instructions in this article if you want to use Integromat to connect your Tripetto response data with other software services.</p>
<blockquote>There are alternatives to use for Integromat, for example <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Pabbly Connect</a> and <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Zapier</a>. Of course it's up to you to use your favorite automation tool.</blockquote>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to connect Tripetto with Integromat. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">global article about how webhooks work</a>.</p>

<h3 id="step-1">Step 1 - Prepare automation tool</h3>
<p>Integromat makes it easy for you to connect with Tripetto. They offer a dedicated <a href="https://www.integromat.com/en/integrations/tripetto?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program" target="_blank">Tripetto integration, which you can find over here</a>.</p>
<div>
  <a href="https://www.integromat.com/en/integrations/tripetto?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto on Integromat<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Create your customized scenario with Integromat. Automate repetitive tasks involved in using Tripetto and make your work easier.</span>
      <span class="url">integromat.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/integromat.svg" alt="Integromat logo" />
    </div>
  </a>
</div>
<h4>In Integromat:</h4>
<p>In Integromat simply create a new scenario and add the Tripetto module to it. Select the trigger <code>Watch responses</code> to let Integromat watch for new Tripetto responses.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-trigger.png" alt="Screenshot of Integromat" />
  <figcaption>Select the <code>Watch responses</code> trigger of Tripetto.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Connect webhook</h3>
<p>After you selected the <code>Watch responses</code> trigger, you can add a webhook connection to it. We advise to create a new webhook connection for each Tripetto form.</p>
<h4>In Integromat:</h4>
<p>Click <code>Add</code> to create a new webhook. You can give the webhook a name, so you can recognize to which Tripetto form that webhook is connected.</p>
<p>Now, underneath the selected webhook, you see a URL. That's your webhook URL that you need in your Tripetto form. Click <code>Copy address to clipboard</code>, as we will need that URL in Tripetto now.</p>
<p>Click <code>OK</code> to save your settings.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-webhook.png" alt="Screenshot of Integromat" />
  <figcaption>The result of your webhook connection with the webhook URL.</figcaption>
</figure>

<h4>In Tripetto:</h4>
<p>Now switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code>. The Automate pane will show up on the right side of the form builder.</p>
<p>The third feature of this screen is <code>Webhook</code>. After enabling the feature <code>Notify an external service when someone completes your form</code> you can paste the webhook URL you got from your new Integromat scenario.</p>
<p>Important: leave the option <code>Send raw response data to webhook</code> disabled. Don't select that option, as it's for experts only!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-tripetto.png" alt="Screenshot of the webhook in Tripetto" />
  <figcaption>Copy-paste your webhook URL in Tripetto.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Receive response data</h3>
<p>Now your Tripetto form is connected to Integromat. Next step is to get the structure of your form to Integromat, so you can use that structure and data in your follow-up services.</p>
<h4>In Integromat:</h4>
<p>To do so, first switch back to Integromat and click the big <code>Run once</code> button. Integromat is now temporarily opened to receive a response from your Tripetto form.</p>
<h4>In Tripetto:</h4>
<p>Now, switch back to Tripetto and submit an entry to your Tripetto form. The best way to do so, is by just completing your form once.</p>
<blockquote>It's recommended to enter some proper test data inside your test response, by which you can recognize each field in your form later on. You can use the <code>Test</code> button in Tripetto to test the connection, but that will not send proper data to your webhook.</blockquote>
<h4>In Integromat:</h4>
<p>After that, you can switch to Integromat again. You'll see the Tripetto module has an update now. When you click that update bubble you'll see your submitted response data. Great, you're connected!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-data.png" alt="Screenshot of Integromat" />
  <figcaption>Your response data has been received by Integromat.</figcaption>
</figure>
<p>Your connection between Tripetto and Integromat is all set! Now it's up to you what follow-up actions you want to attach to it.</p>

<h3 id="step-4">Step 4 - Add services</h3>
<p>Now it's time to connect the services you want.</p>
<h4>In Integromat:</h4>
<p>At the right side of the Tripetto module you can add one or more modules of other services that you want to connect. Simply add a module and search for the wanted service. It depends on the service how you connect to it and what options are available.</p>
<p>If the service you want to connect supports the input of response fields, you can map which Tripetto field must be entered in each input field. That way you can determine where your Tripetto response data goes to in the connected service.</p>
<p>You can test your scenario all the time by clicking the <code>Run once</code> button in Integromat. Make sure you submit a new Tripetto response each time you want to test it.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-scenario.png" alt="Screenshot of Integromat" />
  <figcaption>An example of a scenario that's connected to a Google Sheets file.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Activate magic</h3>
<p>If you're done editing your scenario, you can activate it.</p>
<h4>In Integromat:</h4>
<p>Switch the schedule button at the bottom from <code><i class="fas fa-toggle-off"></i> Off</code> to <code><i class="fas fa-toggle-on"></i> On</code>. We advise to schedule the scenario <code>Immediately</code>, so the scenario handles each Tripetto response immediately when it is submitted.</p>
<hr />

<h2 id="possibilities">Discover the possibilities!</h2>
<p>Integromat supports lots of great services to connect to.</p>

<h3 id="examples">Examples</h3>
<p>We listed some common used scenarios that you can start with right away!</p>
</div>
<ul class="tiles tiles-two">
{% include tile.html url='https://www.integromat.com/en/integrations/tripetto/google-sheets?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program' target=true type='Integromat' title='Tripetto to Google Sheets' description='Add a new row to a Google Sheet with all Tripetto response data.' webhook-service='google-sheets' webhook-service-name='Google Sheets' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.integromat.com/en/integrations/tripetto/microsoft-to-do?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program' target=true type='Integromat' title='Tripetto to Microsoft To Do' description='Add a new task to a list in Microsoft To Do based on Tripetto response data.' webhook-service='microsoft-to-do' webhook-service-name='Microsoft To Do' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.integromat.com/en/integrations/tripetto/mailchimp?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program' target=true type='Integromat' title='Tripetto to MailChimp' description='Add a new subscriber to a MailChimp audience based on Tripetto response data.' webhook-service='mailchimp' webhook-service-name='MailChimp' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.integromat.com/en/integrations/tripetto/shopify?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program' target=true type='Integromat' title='Tripetto to Shopify' description='Create a new customer and create a new order in Shopify based on Tripetto response data.' webhook-service='shopify' webhook-service-name='Shopify' webhook-chain-service='shopify' webhook-chain-service-name='Shopify' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.integromat.com/en/integrations/tripetto/zendesk?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program' target=true type='Integromat' title='Tripetto to Zendesk' description='Create a new Zendesk support ticket based on Tripetto response data.' webhook-service='zendesk' webhook-service-name='Zendesk' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.integromat.com/en/integrations/tripetto/airtable?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program' target=true type='Integromat' title='Tripetto to Airtable + PayPal' description='Add a new record to an Airtable database with all Tripetto response data and create a payment in PayPal.' webhook-service='airtable' webhook-service-name='Airtable' webhook-chain-service='paypal' webhook-chain-service-name='PayPal' palette-top='light' palette-bottom='light' %}
</ul>
<div class="article-content-core">

<h3 id="templates">Templates</h3>
<p>We prepared some templates that you can start with right away in Integromat. Templates help you to quickly setup a scenario.</p>
<div>
  <a href="https://www.integromat.com/en/templates/tripetto?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program" target="_blank" class="blocklink">
    <div>
      <span class="title">Templates for Tripetto on Integromat<i class="fas fa-external-link-alt"></i></span>
      <span class="description">You can choose from our templates for Tripetto. You can use them as they are or customize them to suit your needs.</span>
      <span class="url">integromat.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/integromat.svg" alt="Integromat logo" />
    </div>
  </a>
</div>

<h3 id="all">All Integromat modules</h3>
<p>Take a look at the endless possibilities of services in Integromat in their <a href="https://www.integromat.com/en/integrations?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program" target="_blank">apps & services overview</a>. Just make sure you configure the Tripetto module as described in the article above and then connect it with the service(s) you want.</p>
<div>
  <a href="https://www.integromat.com/en/integrations?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program" target="_blank" class="blocklink">
    <div>
      <span class="title">Apps & Services - Integromat<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Explorer all apps and services you can connect to with Integromat.</span>
      <span class="url">integromat.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/integromat.svg" alt="Integromat logo" />
    </div>
  </a>
</div>

</div>
<script src="{{ page.base }}js/tiles.js?v={{ site.cache_version }}"></script>
