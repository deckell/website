---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-connect-to-other-services-with-pabbly-connect/
title: How to connect to other services with Pabbly Connect - Tripetto Help Center
description: Pabbly offers an automation tool that helps you to connect your Tripetto response data to other online services. This article describes how to enable our webhook for this and how to configure it with Pabbly Connect.
article_title: How to connect to other services with Pabbly Connect
article_id: webhook
article_folder: automate-webhook-pabbly
author: jurgen
time: 5
category_id: automations
subcategory: automations_webhook
areas: [studio, wordpress]
common_content_core: true
---
<p>Pabbly offers an automation tool that helps you to connect your Tripetto response data to other online services: Pabbly Connect. This article describes how to enable our webhook for this and how to configure it with Pabbly Connect.</p>

<div class="article-content-core">
<h2 id="when-to-use">When to use</h2>
<p>By connecting Tripetto to other services you can do all kinds of actions with your response data, like pushing it to a spreadsheet editor (Microsoft Excel/Google Sheets) or a database, or trigger other follow-up actions. The possibilities are endless!</p>
<p>Pabbly Connect is a tool that can help you with this. Follow the instructions in this article if you want to use Pabbly to connect your Tripetto response data with other software services.</p>
<blockquote>There are alternatives to use for Pabbly, for example <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-integromat/">Integromat</a> and <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Zapier</a>. Of course it's up to you to use your favorite automation tool.</blockquote>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to connect Tripetto with Pabbly. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">global article about how webhooks work</a>.</p>

<h3 id="step-1">Step 1 - Prepare automation tool</h3>
<p>Pabbly makes it easy for you to connect with Tripetto. They offer a dedicated <a href="https://www.pabbly.com/connect/integrations/Tripetto/" target="_blank">Tripetto integration, which you can find over here</a>.</p>
<div>
  <a href="https://www.pabbly.com/connect/integrations/Tripetto/" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto on Pabbly Connect<i class="fas fa-external-link-alt"></i></span>
      <span class="description">With Pabbly Connect, you can create automated workflows and transfer the data between your favorite apps and services without any manual efforts.</span>
      <span class="url">pabbly.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/pabbly.png" alt="Pabbly logo" />
    </div>
  </a>
</div>
<h4>In Pabbly Connect:</h4>
<p>In Pabbly Connect simply create a new Workflow. You'll immediately see the first block called <code>Trigger: When this happens ...</code>. This is where we are going to collect the responses from Tripetto.</p>
<p>Under <code>Choose App</code> search for <code>Tripetto</code> and click the corresponding app button in the search results. The Tripetto app will be added to your workflow.</p>
<p>Next step, select the <code>Trigger Event</code> that receives new responses: from the dropdown, select <code>New Form Response</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/trigger.png" alt="Screenshot of Pabbly Connect" />
  <figcaption>The settings of the Tripetto trigger block.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Connect webhook</h3>
<p>Now we're going to connect your Tripetto form and your webhook in Pabbly Connect.</p>
<h4>In Pabbly Connect:</h4>
<p>Pabbly Connect will show the webhook URL, which you need in Tripetto. So, copy the webhook URL and switch to Tripetto.</p>
<h4>In Tripetto:</h4>
<p>At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code>. The Automate pane will show up on the right side of the form builder.</p>
<p>The third feature of this screen is <code>Webhook</code>. After enabling the feature <code>Notify an external service when someone completes your form</code> you can paste the webhook URL you got from your new Pabbly Connect workflow.</p>
<p>Important: leave the option <code>Send raw response data to webhook</code> disabled. Don't select that option, as it's for experts only!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto.png" alt="Screenshot of Tripetto" />
  <figcaption>Copy-paste your webhook URL in Tripetto.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Receive response data</h3>
<p>Next up, Pabbly Connect wants to test if the workflow is receiving data. To test this, we're going to submit a test response in our Tripetto form.</p>
<h4>In Pabbly Connect:</h4>
<p>First let Pabbly Connect know you're going to submit some test data, by clicking <code>Capture Webhook Response</code>. Pabbly now enables your workflow temporarily to receive data. Leave the <code>Simple Response</code> toggle enabled.</p>
<h4>In Tripetto:</h4>
<p>Now, switch back to Tripetto and submit an entry to your Tripetto form. The best way to do so, is by just completing your form once.</p>
<blockquote>It's recommended to enter some proper test data inside your test response, by which you can recognize each field in your form later on. You can use the <code>Test</code> button in Tripetto to test the connection, but that will not send proper data to your webhook.</blockquote>
<h4>In Pabbly Connect:</h4>
<p>After you completed your form, return to Pabbly Connect and you will see Pabbly has found some data. You'll recognize your test data immediately. Sweet, you are connected!</p>
<p>You have now prepared Pabbly Connect to receive Tripetto response data. The next step is to add your wanted service(s) to your workflow.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/test-data.png" alt="Screenshot of Pabbly Connect" />
  <figcaption>Pabbly Connect has received some test data.</figcaption>
</figure>

<h3 id="step-4">Step 4 - Add services</h3>
<p>In Pabbly Connect you can now add the follow-up services you want to trigger.</p>
<h4>In Pabbly Connect:</h4>
<p>For each service you add another step and follow the instructions of Pabbly. How to connect to other services differs for each service, so we cannot give an instruction on that.</p>
<p>In most cases Pabbly Connect will show data fields of the connected service in which you can enter Tripetto response data. You can select the corresponding data fields that you tested with in step 3. In that way you can connect certain questions in your Tripetto form to the desired end location of another service.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/google-sheets.png" alt="Screenshot of Pabbly Connect" />
  <figcaption>An example of the settings of a Google Sheets file.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Activate magic</h3>
<p>If you're done editing your workflow, you can activate it.
</p>
<h4>In Pabbly Connect:</h4>
<p>To really let the workflow receive response data, you'll need to enable it. Use the big switch at the right top corner to switch if from <code><i class="fas fa-toggle-off"></i> Off</code> to <code><i class="fas fa-toggle-on"></i> On</code>.</p>
<hr />

<h2 id="possibilities">Discover the possibilities!</h2>
<p>Pabbly supports lots of great services to connect to.</p>

<h3 id="examples">Examples</h3>
<p>We listed some common used scenarios that you can start with right away!</p>
</div>
<ul class="tiles tiles-two">
{% include tile.html url='https://www.pabbly.com/connect/integrations/google-sheets' target=true type='Pabbly Connect' title='Tripetto to Google Sheets' description='Add a new row to a Google Sheet with all Tripetto response data.' webhook-service='google-sheets' webhook-service-name='Google Sheets' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.pabbly.com/connect/integrations/activecampaign' target=true type='Pabbly Connect' title='Tripetto to ActiveCampaign' description='Add a new contact to ActiveCampaign based on Tripetto response data.' webhook-service='activecampaign' webhook-service-name='ActiveCampaign' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.pabbly.com/connect/integrations/mailchimp' target=true type='Pabbly Connect' title='Tripetto to MailChimp' description='Add a new subscriber to a MailChimp audience based on Tripetto response data.' webhook-service='mailchimp' webhook-service-name='MailChimp' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.pabbly.com/connect/integrations/woocommerce' target=true type='Pabbly Connect' title='Tripetto to Woocommerce' description='Create a new customer and create a new order in Woocommerce based on Tripetto response data.' webhook-service='woocommerce' webhook-service-name='Woocommerce' webhook-chain-service='woocommerce' webhook-chain-service-name='Woocommerce' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.pabbly.com/connect/integrations/zendesk' target=true type='Pabbly Connect' title='Tripetto to Zendesk' description='Create a new Zendesk support ticket based on Tripetto response data.' webhook-service='zendesk' webhook-service-name='Zendesk' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.pabbly.com/connect/integrations/airtable' target=true type='Pabbly Connect' title='Tripetto to Airtable + Stripe' description='Add a new record to an Airtable database with all Tripetto response data and create a transaction in Stripe.' webhook-service='airtable' webhook-service-name='Airtable' webhook-chain-service='stripe' webhook-chain-service-name='Stripe' palette-top='light' palette-bottom='light' %}
</ul>
<div class="article-content-core">

<h3 id="all">All Pabbly Connect integrations</h3>
<p>Take a look at the endless possibilities of services in Pabbly in their <a href="https://www.pabbly.com/connect/integrations/" target="_blank">integrations overview</a>. Just make sure you configure the Tripetto trigger as described in this article and then integrate it with the service(s) you want.</p>
<div>
  <a href="https://www.pabbly.com/connect/integrations/" target="_blank" class="blocklink">
    <div>
      <span class="title">500+ Pabbly Connect Integrations - Pabbly<i class="fas fa-external-link-alt"></i></span>
      <span class="description">All popular apps for - CRM, Marketing, E-Commerce, Helpdesk, Payments, Web forms, Collaboration and more.</span>
      <span class="url">pabbly.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/pabbly.png" alt="Pabbly logo" />
    </div>
  </a>
</div>
</div>
<script src="{{ page.base }}js/tiles.js?v={{ site.cache_version }}"></script>
