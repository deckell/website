---
layout: help-article
base: ../../../
permalink: /help/articles/want-to-help-translating-tripetto/
title: Want to help translating Tripetto? - Tripetto Help Center
description: Tripetto is fully prepared to work in all languages. All we need is some good translations. You want to help us with that?
article_title: Want to help translating Tripetto?
article_folder: translations
author: martijn
time: 2
category_id: customization
subcategory: customization_labels
areas: [studio, wordpress]
---
<p>Tripetto is fully prepared to work in all languages. All we need is some good translations. You want to help us with that?</p>

<h2 id="start">Get started</h2>
<p>If you want to help us to translate Tripetto, you're more than welcome! 🙏</p>
<p>Before you start, please <a href="{{ page.base }}contact/" target="_blank">contact us</a>, so we can help you to get going. <a href="javascript:$crisp.push(['do', 'chat:open']);">Using the chat</a> will be the fastest and easiest way to do so. Thanks in advance!</p>

<h2 id="instructions">Instructions</h2>
<p>All instructions, translation files (.pot) and translation progresses are collected in our <a href="https://gitlab.com/tripetto/translations" target="_blank">Gitlab Translations repository</a>.</p>
<div>
  <a href="https://gitlab.com/tripetto/translations" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / Translations<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Overview of the translations available for Tripetto and instructions to create new translations or update existing ones.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-gitlab.png" alt="Gitlab logo" />
    </div>
  </a>
</div>

<h3 id="runners">Translate form runners</h3>
<p>The easiest way to help us and our community is by translating our form runners. That contains all labels that are used inside the forms that users create and your respondents get to see. You already can translate all labels inside your forms (<a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">see instructions</a>), but as long as a language is not predefined by us, you will have to translate this again for each form.</p>
<p>If you help us to translate the runners, we can add those translations to the predefined languages, so all our users can use that form language at once.</p>

<h3 id="components">Translate all other components</h3>
<p>The runner translations have effect on the forms that respondents get to see, but Tripetto of course is much bigger than that. Also the whole builder, all question blocks and our different platforms are 100% translatable. That way you can also build, style, share and do everything else in your own language.</p>
<p>If you help us to translate all Tripetto components, we can offer our products in more foreign languages to our users, making it easier to use for more people.</p>
<hr/>

<div>
  <a href="https://gitlab.com/tripetto/translations" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto / Translations<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Overview of the translations available for Tripetto and instructions to create new translations or update existing ones.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-gitlab.png" alt="Gitlab logo" />
    </div>
  </a>
</div>
