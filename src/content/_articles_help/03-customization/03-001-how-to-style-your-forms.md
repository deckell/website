---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-style-your-forms/
title: How to style your forms - Tripetto Help Center
description: You can style all your forms completely to your needs, for example to meet your company's brand.
article_title: How to style your forms
article_id: styling
article_folder: styling
author: jurgen
time: 5
category_id: customization
subcategory: customization_styling
areas: [studio, wordpress]
---
<p>You can style all your forms completely to your needs, for example to meet your company's brand. You can start simple with just one base color, or modify each element's appearance in your form.</p>

<h2 id="before" data-anchor="Tips & tricks">Before we start styling</h2>
<p>Let's start with a few things that could be handy while styling your forms.</p>

<h3 id="preview">Use live preview</h3>
<p>The <a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/" target="_blank">live preview</a> in the form builder is your biggest friend while styling. It will always update what you're doing, so everything you do to style your forms, will immediately be visible in the live preview.</p>

<h3 id="coloring">About coloring</h3>
<p>For all color fields you can style, there are a few ways to enter your colors:</p>
<ul>
  <li><strong>Color name</strong> - This is the easiest way to add some color. Just type the name of one of the 140 predefined colors that work in all modern browsers. For the full list, <a href="https://htmlcolorcodes.com/color-names/" target="_blank">have a look at this page</a>;</li>
  <li><strong>HEX color code</strong> - Need a custom color that's not on the color names list? Just enter any HEX color code, prefixed with a <code>#</code>. Have a look over here to select the right color from a <a href="https://htmlcolorcodes.com/color-picker/" target="_blank">color picker</a> or get some inspiration from a <a href="https://htmlcolorcodes.com/color-chart/" target="_blank">color chart</a>;</li>
  <li><strong>RGB color code</strong> - Rather use the RGB notation instead of HEX? No problem, just enter your RGB color in the right format <code>rgb()</code>.</li>
</ul>

<h3 id="contrast">About color contrast</h3>
<p>Always make sure your form stays well readable while applying colors to it. The form will automatically try to keep the right amount of contrast, but you can always overrule that with your own color settings.</p>
<hr />

<h2 id="styling" data-anchor="Styling options">Let's start styling 👨‍🎨👩‍🎨</h2>
<p>At the top menu bar of the form builder click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>.</p>

<h3 id="basic-styling">Basic styling</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-color.png" alt="Screenshot of the styling in Tripetto" />
  <figcaption>Example of basic color styling (HEX color).</figcaption>
</figure>
<p>You can start off with selecting just one simple base color. That color already will have a major effect on the styling of your form, as you can see in the live preview.</p>
<p>It depends on the selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" target="_blank">form face</a> what the effect of this base color is.</p>

<h3 id="font">Font</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-font.png" alt="Screenshot of the styling in Tripetto" />
  <figcaption>Example of a Google Font styling.</figcaption>
</figure>
<p>Next step that has a major impact on your styling is the font style selection. To enable font styling, activate the <code>Font</code> option in the Styling pane.</p>
<p>You can simply select one of the predefined fonts that work in all browsers, but if you really want to show off you also can use a Google Font or your own font.</p>
<h4>Google Font</h4>
<p>All Google Fonts are directly available to use in your form. In the font selection, choose <code>Google Fonts or URL</code>.</p>
<p>Now open <a href="https://fonts.google.com/" target="_blank">Google Fonts</a> and search for a font that you'd like to use in your form. Then switch back to the form builder and enter the name of the Google Font of your choosing. Keep in mind Google Fonts are case sensitive.</p>
<div>
  <a href="https://fonts.google.com/" target="_blank" class="blocklink">
    <div>
      <span class="title">Google Fonts<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Making the web more beautiful, fast, and open through great typography.</span>
      <span class="url">fonts.google.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-google-fonts.png" alt="Google Fonts logo" />
    </div>
  </a>
</div>

<h4>Custom font from URL</h4>
<p>Want to use your own custom font? In the font selection, choose <code>Google Fonts or URL</code> and enter the URL to your font file.</p>
<p>To make your font usable in the form you need to add the font name as a suffix to the font URL, like this: <code>#FONT_NAME</code>. For example: <code>https://tripetto.com/roboto.woff#Roboto</code>.</p>
<p>If your font name contains spaces, please replace each space with <code>%20</code> to ensure the right encoding. For example: <code>https://tripetto.com/roboto-regular.woff#Roboto%20Regular</code>.</p>

<h4>Font size</h4>
<p>After you selected the right font, you can set the font size. The font size will also have its effect on other form elements, like the size of input fieds, buttons and icons.</p>

<h3 id="background">Background</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-background.png" alt="Screenshot of the styling in Tripetto" />
  <figcaption>Example of a background image styling.</figcaption>
</figure>
<p>To style the background with a color and/or a background image, activate the <code>Background</code> option in the Styling pane.</p>
<p>You can now supply a background color as a color name, HEX code or RGB code.</p>
<h4>Background image</h4>
<p>You can also add an image as the background of your form. You can <a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" target="_blank">enter the URL to an image</a> and then determine how the image must be positioned over the screen size.</p>

<h3 id="inputs">Inputs</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-inputs.png" alt="Screenshot of the styling in Tripetto" />
  <figcaption>Example of input styling.</figcaption>
</figure>
<p>Input fields of course are an important part of your conversational forms. To enable input styling, activate the <code>Inputs</code> option in the Styling pane.</p>

<h4>Input fields</h4>
<p>Input fields are common form elements, like text fields, checkboxes and radio buttons. Those are all stylable by multiple colors, border size and roundness. Just play with them and see the result in the live preview.</p>

<h4>Input buttons</h4>
<p>Some question types, like Multiple choice, Rating and yes/no are presented with input buttons. You can style the following states of those buttons:</p>
<ul>
  <li><strong>Selection color</strong> - The color of buttons used in Multiple choice and Rating blocks;</li>
  <li><strong>Agree color</strong> - The color of <code>Yes</code> buttons used in yes/no blocks;</li>
  <li><strong>Decline color</strong> - The color of <code>No</code> buttons used in yes/no blocks;</li>
</ul>

<h3 id="buttons">Buttons</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-buttons.png" alt="Screenshot of the styling in Tripetto" />
  <figcaption>Example of button styling.</figcaption>
</figure>
<p>Buttons are used in several places, like start buttons, navigation buttons and complete buttons. To enable button styling, activate the <code>Buttons</code> option in the Styling pane.</p>
<p>Buttons are stylable by color and style. With the button style you can determine if the buttons should be filled with the desired color, or shown as a button with only border coloring. The roundness sets the border radius of your buttons.</p>

<h4>Complete button</h4>
<p>The <code>Complete</code> button of course is an important one, so you can set a custom color for that button.</p>
<hr />

<h2 id="example">Example: Whatsapp styling</h2>
<p>To show you how powerful styling is we made a demo on replicating the Whatsapp styling in our chat form face with only a few styling options 🤯</p>
 <figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-whatsapp.gif" alt="Screenshot of the result of styling in Tripetto" />
  <figcaption>From basic styling to Whatsapp styling in the chat form face.</figcaption>
</figure>
<hr />

<h2 id="form-faces">Custom styling per form face</h2>
<p>This is how you can style your forms, but each form face also has its own additional settings and styling options to improve the finishing touch. Click the help articles below to see these options for each form face.</p>
<ul>
  <li><a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/" target="_blank">Additional options for the autoscroll form face</a>;</li>
  <li><a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/" target="_blank">Additional options for the chat form face</a>;</li>
  <li><a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/" target="_blank">Additional options for the classic form face</a>.</li>
</ul>
