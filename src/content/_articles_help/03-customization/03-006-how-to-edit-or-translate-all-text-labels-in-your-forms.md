---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/
title: How to edit or translate all text labels in your forms - Tripetto Help Center
description: All text labels inside your Tripetto forms can be edited to meet your needs. It even has some advanced translation possibilities built in to let your forms fully match the language of your respondents.
article_title: How to edit or translate all text labels in your forms
article_id: translations
article_folder: translations
author: mark
time: 5
category_id: customization
subcategory: customization_labels
areas: [studio, wordpress]
---
<p>All text labels inside your Tripetto forms can be edited to meet your needs. It even has some advanced translation possibilities built in to let your forms fully match the language of your respondents.</p>

<blockquote>
  <h4>Want to help translating?</h4>
  <p>In the article you're reading now we demonstrate how you can edit/translate the labels inside your form. Do you miss a predefined form language and have some time to translate it for us? We would love that! 🥰</p>
  <p><a href="{{ page.base }}help/articles/want-to-help-translating-tripetto/" target="_blank">See how to contribute to our translations.</a> Thanks in advance!</p>
</blockquote>

<h2 id="open">Open Translations</h2>
<p>Let's see how to set the right language and translations in your form. At the top menu bar of the form builder click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Translations</code>.</p>

<h2 id="form-language">Determine your form language</h2>
<p>The first step is to determine in what language your form is made. You can select the right language from the <code>Form language</code> dropdown. You can even choose from a detailed list of dialects and regions.</p>
<p>The localization system will now try to see if there's a predefined translation for the selected language (see below). If there is no predefined translation, the labels will fall back to English. At this moment this is the list of predefined languages:</p>
<ul>
  <li><code>English</code> (default);</li>
  <li><code>Dutch</code> (Tripetto is developed in Amsterdam, so this was a good starting point for us 😉).</li>
</ul>
<p>If you selected a form language, all your respondents will get to see the labels in that language.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-form-language.gif" alt="Screenshot of languages in Tripetto" />
  <figcaption>Switching the form language to Dutch.</figcaption>
</figure>

<h3>No form language set</h3>
<p>It's not necessary to select a static form language. If you don't select a form language, the browser of the respondent will determine what's the best language to use. So let's say you didn't supply a certain language to your form, respondents that use their browser in Dutch language will see the Dutch labels.</p>
<p>But also if you don't save a certain form language, you can still edit all labels for all languages. In that case you can select the language you want to edit in the <code>Translations</code> block. This will also try to load any predefined labels (currently only English and Dutch), or fall back to English if it's an unknown language. You can then fully translate those labels for respondents that use that language in their browser.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-labels.gif" alt="Screenshot of languages in Tripetto" />
  <figcaption>Switching the language to translate those labels.</figcaption>
</figure>

<blockquote>
  <p>The translations only affect the form labels. Your form blocks are not translatable (yet). If you want your total form in multiple languages, the best way is to create a form per language.</p>
</blockquote>

<h2 id="edit-translate">Edit/Translate messages and labels</h2>
<p>Now you see all labels and messages inside your form that can be edited and/or translated. You can overwrite each label or only the desired ones. If you're using a predefined language, you'll see all labels already are translated and you can still overwrite each label. If you're using a not-predefined language it's a bit more work, because you'd have to translate each label the way you'd like.</p>

<h3>Live preview</h3>
<p>The <a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/" target="_blank">live preview of your form</a> will use your labels and messages immediately.</p>

<h3>What's the '<i>%1</i>' in some labels?</h3>
<p>For some labels you'll see a code <code>%1</code> in it. In that case the label has a variable in it that the form can use. An example of that is the error message that can show the number of questions that your respondent has to validate. To retain such variables inside your labels, please make sure you use them in your own labels also, for example: <code>%1 items require your attention!</code>.</p>

<h3>Optional: Plural rules</h3>
<p>For some languages there are several ways of plural rules available. This has its effect on the way the form handles the messages that have a single/plural appearance. Select the desired plural rule setting from the <code>Plural rules</code> dropdown and the messages that need to be translated will update with the right labels.</p>

<h3>Optional: Locale settings</h3>
<p>To fully match your language settings you can even select how to format data that's shown inside your forms, like dates and numbers. You can choose from these locale settings:</p>
<ul>
  <li><code>Automatic</code> - This will use the locale settings of the browser of the respondent, so your data can be formatted differently on different browser settings of your respondents;</li>
  <li><code>Manual</code> - This will open up the list of all available locales. You can select the locale you'd like to use and now all respondents will see this formatting inside your form. </li>
</ul>
<p>Keep in mind this only affects the formatting of the data inside the form; not the format of the saved data in your entries (<a href="{{ page.base }}help/articles/how-to-use-the-number-block/" target="_blank">although that's also possible for numbers</a>).</p>
<hr />

<h2 id="examples">Examples</h2>
<p>Let's show you some examples of the language settings in different use cases:</p>

<h3 id="example-1">1 - Not using a static form language</h3>
<p>If you don't select a form language, the form will determine the language to use on its own, based on the browser language of each respondent. You can translate labels of all desired languages and those will be used for respondents that use that language.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-example1.png" alt="Screenshot of languages in Tripetto" />
  <figcaption>The settings for no static form language.</figcaption>
</figure>

<h3 id="example-2">2 - Using the default Dutch translations</h3>
<p>Let's say your form is in Dutch and you want to use the default Dutch translations. Then all you have to do is select <code>Dutch</code> at the <code>Form language</code> dropdown. All labels will automatically be translated to Dutch. No further action needed!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-example2.png" alt="Screenshot of languages in Tripetto" />
  <figcaption>The settings for the default Dutch labels.</figcaption>
</figure>

<h3 id="example-3">3 - Overwriting the default English translations</h3>
<p>Now let's have a look in case you'd like to overwrite a specific label in a language that's predefined, in this case English. The first step is to select <code>English</code> at the <code>Form language</code> dropdown. You can now simply translate each individual label.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-example3.png" alt="Screenshot of languages in Tripetto" />
  <figcaption>The settings for your custom English labels, with the first label customized.</figcaption>
</figure>

<h3 id="example-4">4 - Adding your own Spanish (Latin America) translations</h3>
<p>Lastly let's have a look at a language that's not predefined, in this case Spanish (Latin America). The first step is to select <code>Spanish (Latin America)</code> at the <code>Form language</code> dropdown.</p>
<p>As this language hasn't got any predefined labels, all labels will fall back to English and you can translate each of them. And this language even has multiple plural rules that you can choose from.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-example4.png" alt="Screenshot of languages in Tripetto" />
  <figcaption>The settings for your own Spanish (Latin America) labels.</figcaption>
</figure>
