---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-remove-branding-in-studio-forms/
title: How to remove Tripetto branding in studio forms - Tripetto Help Center
description: Forms you create in the studio show some Tripetto branding, but you can hide that by requesting for branding removal.
article_title: How to remove Tripetto branding in studio forms
article_folder: branding
author: martijn
time: 3
category_id: customization
subcategory: customization_styling
areas: [studio]
---
<p>Forms you create in the studio show some Tripetto branding, but you can hide that by requesting for branding removal.</p>

<h2 id="about" data-anchor="About Tripetto branding">Why we show our branding</h2>
<p>Everything in the <a href="{{ site.url_app }}" target="_blank">studio at tripetto.app</a> is free for now, as we focus on growing our user base. In exchange for the free usage, we show our name at some subtle places inside each form. This has a high value for us in our growing ambitions, as that's the best commercial spot we can have. Hopefully people that see your Tripetto forms want to know more about it by simply clicking on our name.</p>
<p>But of course we also understand and respect it when you don't want to show our branding to your users, for example when you use a form with a professional purpose. In that case you can simply hide it.</p>

<h3 id="wordaround">Temporary workaround</h3>
<p>Currently the only way to hide the Tripetto branding is with some manual actions from the Tripetto team. That's the reason you have to request the branding removal using a request form. You can only open this form from within the studio.</p>
<p>Of course the plan is there will be paid features inside the Tripetto Studio and most probably one of the first paid features will be the automated removal of the branding, without any manual processes. But as we're not at that point yet with the studio, we wanted to make it possible already by providing this temporary workaround.</p>
<p>We also wrote a blog post with some more background information on our branding and how we solved this with our own no-code solutions.</p>
<div>
  <a href="{{ page.base }}blog/how-we-added-our-first-premium-feature-with-our-own-no-code-form-solutions/" target="_blank" class="blocklink">
    <div>
      <span class="title">How we added our first premium feature with our own no-code form solutions - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Since this week you can remove Tripetto branding from your forms in the studio. And we built it with our own no-code form solutions!</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>

<h2 id="remove">Remove Tripetto branding</h2>
<p>You can remove Tripetto branding per form. After you opened the desired form, click <code>Customize</code> at the top menu bar of the form builder <i class="fas fa-arrow-right"></i> Click <code>Remove Tripetto branding</code>. The request form will open up at the right of your screen. Fill out this request form and we will process your request as soon as possible.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-branding-studio.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Request Tripetto branding removal from studio.</figcaption>
</figure>

<h3 id="pricing">Pricing</h3>
<p>Because of the manual steps we have to take, the price of Tripetto branding removal is a <strong>one time fee of $89 per form</strong>.</p>

<h3 id="how-it-works">How it works</h3>
<p>The total process works as follows:</p>
<ol>
  <li><strong>Request</strong> - Submit your request using our request form from the studio.</li>
  <li><strong>Payment</strong> - We will send you (or an alternative payer you provide) a PayPal payment link. You can pay with all payment options that PayPal offers, including all creditcards. You don't need a PayPal account, you can pay directly as a guest.</li>
  <li><strong>Activation</strong> - After we received your payment we will manually activate the 'Remove Tripetto branding' option for the desired form. We will contact you when this is done. This will not yet disable the branding inside your form.</li>
  <li><strong>Disable branding</strong> - When you go to the Style screen of the form inside the studio, you will see you then can toggle the <code>Hide all the Tripetto branding</code> option. By enabling this option, the branding will be removed right away!</li>
</ol>
<hr/>

<h2>Also read our blog</h2>
<p>We also wrote a blog post with some more background information on our branding and how we solved this with our own no-code solutions.</p>
<div>
  <a href="{{ page.base }}blog/how-we-added-our-first-premium-feature-with-our-own-no-code-form-solutions/" target="_blank" class="blocklink">
    <div>
      <span class="title">How we added our first premium feature with our own no-code form solutions - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Since this week you can remove Tripetto branding from your forms in the studio. And we built it with our own no-code form solutions!</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
