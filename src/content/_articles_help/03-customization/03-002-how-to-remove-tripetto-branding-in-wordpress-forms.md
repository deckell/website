---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-remove-tripetto-branding-in-wordpress-forms/
title: How to remove Tripetto branding in WordPress forms - Tripetto Help Center
description: Forms you create in the WordPress plugin show some Tripetto branding in the free version, but you can hide that in the premium version of the WordPress plugin.
article_title: How to remove Tripetto branding in WordPress forms
article_folder: branding
author: martijn
time: 1
category_id: customization
subcategory: customization_styling
areas: [wordpress]
---
<p>Forms you create in the WordPress plugin show some Tripetto branding in the free version, but you can hide that in the <a href="{{ page.base }}pricing/wordpress/" target="_blank">premium version</a> of the WordPress plugin.</p>

<h2 id="about" data-anchor="About Tripetto branding">Why we show our branding</h2>
<p>In the free version of the plugin we show subtle Tripetto branding on some places in your form. This has a high value for us in our growing ambitions, as that's the best commercial spot we can have. Hopefully people that see your Tripetto forms want to know more about it by simply clicking on our name.</p>
<p>But of course we also understand and respect it when you don't want to show our branding to your users, for example when you use a form with a professional purpose. In that case you can simply hide it.</p>

<h2 id="remove">Remove Tripetto branding</h2>
<p>Removing the Tripetto branding is only possible in the <a href="{{ page.base }}pricing/wordpress/" target="_blank">premium version of our plugin</a>. Please see these <a href="{{ page.base }}help/articles/how-to-upgrade-the-wordpress-plugin-to-premium/" target="_blank">instructions on how to upgrade to premium</a>.</p>
<p>In the premium plugin open your form in the form builder. At the top menu bar of the form builder click <code>Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>. The style pane will show up on the right side of the form builder.</p>
<p>In the style pane you can now enable the option <code>Hide all the Tripetto branding</code>. Now refresh your form and you will see it no longer shows the Tripetto branding.</p>

<blockquote>
  <h4>Branding still showing?</h4>
  <p>It could be your browser uses a cached version of your form. In that case please perform a 'hard refresh' to the page your form is embedded in:</p>
  <p><i class="fab fa-windows"></i> Windows: hold <code>CTRL</code> key + Click <code>Refresh</code> button in browser<br/><i class="fab fa-apple"></i> MacOS: hold <code>Apple</code> key or <code>CMD</code> key + Click <code>Refresh</code> button in browser</p>
  <p>This should load the latest version of the form, without the branding.</p>
</blockquote>
