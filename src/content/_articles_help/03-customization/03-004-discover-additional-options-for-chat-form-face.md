---
layout: help-article
base: ../../../
permalink: /help/articles/discover-additional-options-for-chat-form-face/
title: Discover additional options for chat form face - Tripetto Help Center
description: The chat form face has some additional settings and specific styling options and translatable messages.
article_title: Discover additional options for chat form face
article_id: form_face
article_folder: styling-chat
author: jurgen
time: 3
category_id: customization
subcategory: customization_faces
areas: [studio, wordpress]
---
<p>The chat form face has some additional settings and specific styling options and translatable messages. These extra settings help you to push the finishing touch to the next level.</p>

<h2 id="customization">Customizations of chat form face</h2>
<p>To see the additional options for the chat form face, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>.</p>

<h3 id="appearance">Appearance</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-appearance.png" alt="Screenshot of a chat form face in Tripetto" />
  <figcaption>Example of the chat appearance.</figcaption>
</figure>
<p>The appearance determines how the form gets displayed in the start position. There are two options:</p>
<ul>
  <li><strong>Inline</strong> - The form is shown right away;</li>
  <li><strong>Button</strong> - The form needs to be opened by clicking a Chat button (only supported when embedding the chat).</li>
</ul>

<h3 id="options">Options</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-options.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of the chat options.</figcaption>
</figure>
<p>The following options are available for the chat form face:</p>
<ul>
  <li><strong>Automatically gain focus</strong> - Determine if the first question should be focussed when opening the form. This only applies when you're embedding the form;</li>
  <li><strong>Display/hide scrollbar</strong> - Determine if the form shows a scrollbar so respondents can scroll through the form (only if the appearance is set to <code>Button</code>);</li>
  <li><strong>Use full width</strong> - Determine if the chat window may use the full width or has a maximum width;</li>
  <li><strong>Automatically open chat window</strong> - Determine if the chat window should popup automatically (only if the appearance is set to <code>Button</code>).</li>
</ul>

<h3 id="styling">Styling</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-styling.png" alt="Screenshot of chat avatars in Tripetto" />
  <figcaption>Example of the chat avatar styling.</figcaption>
</figure>
<p>All common styling options can be found in the <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">help article about styling your forms</a>.</p>
<p>The following additional styling options are available for the chat form face:</p>
<ul>
  <li><strong>Avatar</strong> - Style the avatar representing the interviewer. The following types are available:
    <ul>
      <li>Illustration - Select <code>Illustration</code> from the <code>Avatar Type</code> dropdown.<br/>You can compose your own avatar by playing with all options and see the result in the live preview;</li>
      <li>Custom image - Select <code>Custom image</code> from the <code>Avatar Type</code> dropdown.<br/>You can <a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" target="_blank">supply the URL to your own image</a>.</li>
    </ul>
  </li>
  <li><strong>Questions</strong> - Style the bubbles that represent your questions;</li>
  <li><strong>Answers</strong> - Style the bubbles that represent the answers of the respondent;</li>
  <li><strong>Chat button</strong> - Style the chat button (only if the appearance is set to <code>Button</code>).</li>
</ul>
<hr />

<h2 id="translations">Translations of chat form face</h2>
<p>If you have enabled translations of form labels and messages, you should check the labels, as the chat form face has some additional messages that you'd have to translate.</p>
<p>To see the additional labels for the chat form face, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Translations</code>.</p>
<p>More information on translations can be found in the <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">help article about editing/translating your forms</a>.</p>
