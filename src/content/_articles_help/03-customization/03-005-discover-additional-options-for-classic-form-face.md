---
layout: help-article
base: ../../../
permalink: /help/articles/discover-additional-options-for-classic-form-face/
title: Discover additional options for classic form face - Tripetto Help Center
description: The classic form face has some additional settings and specific styling options and translatable messages.
article_title: Discover additional options for classic form face
article_id: form_face
article_folder: styling-classic
author: jurgen
time: 3
category_id: customization
subcategory: customization_faces
areas: [studio, wordpress]
---
<p>The classic form face has some additional settings and specific styling options and translatable messages. These extra settings help you to push the finishing touch to the next level.</p>

<h2 id="customization">Customizations of classic form face</h2>
<p>To see the additional options for the classic form face, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>.</p>

<h3 id="appearance">Appearance</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-appearance.png" alt="Screenshot of a classic form face in Tripetto" />
  <figcaption>Example of the classic appearance.</figcaption>
</figure>
<p>The appearance determines how the form evolves while your respondents fill it out. There are three options:</p>
<ul>
  <li><strong>Progressive</strong> - Present the whole form at once. The form will automatically show the appropriate question blocks based on the answers;</li>
  <li><strong>Continuous</strong> - Keep answered blocks in view while working through the form using the <code>Next</code> and <code>Back</code> buttons;</li>
  <li><strong>Paginated</strong> - Use pagination for the form and let the users navigate through the form using the <code>Next</code> and <code>Back</code> buttons.</li>
</ul>

<h3 id="options">Options</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-options.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of the classic options.</figcaption>
</figure>
<p>The following options are available for the classic form face:</p>
<ul>
  <li><strong>Automatically gain focus</strong> - Determine if the first question should be focussed when opening the form. This only applies when you're embedding the form;</li>
  <li><strong>Display/hide enumerators</strong> - Determine if the form shows numbers with the questions;</li>
  <li><strong>Display/hide page indicators</strong> - Determine if the form shows page indicator buttons, so respondents can navigate through the form (only if the form mode is set to <code>Paginated</code>);</li>
  <li><strong>Display/hide progressbar</strong> - Determine if the form shows a progressbar indicating the progress of the respondent while filling out the form (only if the form mode is set to <code>Paginated</code>).</li>
</ul>

<h3 id="styling">Styling</h3>
<p>No additional styling is available for the classic form face. All common styling options can be found in the <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">help article about styling your forms</a>.</p>
<hr />

<h2 id="translations">Translations of classic form face</h2>
<p>If you have enabled translations of form labels and messages, you should check the labels, as the classic form face has some additional messages that you can translate.</p>
<p>To see the additional labels for the classic form face, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Translations</code>.</p>
<p>More information on translations can be found in the <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">help article about editing/translating your forms</a>.</p>
