---
layout: help-article
base: ../../../
permalink: /help/articles/discover-additional-options-for-autoscroll-form-face/
title: Discover additional options for autoscroll form face - Tripetto Help Center
description: The autoscroll form face has some additional settings and specific styling options and translatable messages.
article_title: Discover additional options for autoscroll form face
article_id: form_face
article_folder: styling-autoscroll
author: jurgen
time: 3
category_id: customization
subcategory: customization_faces
areas: [studio, wordpress]
---
<p>The autoscroll form face has some additional settings and specific styling options and translatable messages. These extra settings help you to push the finishing touch to the next level.</p>

<h2 id="customization">Customizations of autoscroll form face</h2>
<p>To see the additional options for the autoscroll form face, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>.</p>

<h3 id="appearance">Appearance</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-appearance.png" alt="Screenshot of an autoscroll form face in Tripetto" />
  <figcaption>Example of the autoscroll appearance.</figcaption>
</figure>
<p>The following appearance settings are available for the autoscroll form face:</p>
<ul>
  <li><strong>Scroll direction</strong> - Determine if the form should scroll in a vertical (top to bottom) or a horizontal (left to right) direction;</li>
  <li><strong>Vertical alignment</strong> - Determine how the question blocks should be vertically aligned within the available height of the form: top, middle, or bottom;</li>
  <li><strong>Visibility</strong> - Determine if the form shows the previous and upcoming question while filling out the active question.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/autoscroll-demo-2.gif" alt="Screenshot of a horizontal autoscroll form face Tripetto" />
  <figcaption>Demo of a horizontal scroll direction.</figcaption>
</figure>

<h3 id="options">Options</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-options.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of the autoscroll options.</figcaption>
</figure>
<p>The following options are available for the autoscroll form face:</p>
<ul>
  <li><strong>Display/hide navigation bar</strong> - Determine if a navigation bar is shown at the bottom of the form;</li>
  <li><strong>Display/hide progressbar</strong> - Determine if the navigation bar (if it's shown) shows a progressbar indicating the progress of the respondent while filling out the form;</li>
  <li><strong>Display/hide enumerators</strong> - Determine if the form shows numbers with the questions;</li>
  <li><strong>Automatically gain focus</strong> - Determine if the first question should be focussed when opening the form. This only applies when you're embedding the form;</li>
  <li><strong>Display separate submit button</strong> - Determine if the form shows a separate submit button at the end (an extra step for your respondents), or the form recognizes the end automatically and shows the submit button immediately at the last question;</li>
  <li><strong>Display/hide back button</strong> - Determine if the form shows a back button at top of each question;</li>
  <li><strong>Display/hide scrollbar</strong> - Determine if the form shows a scrollbar so respondents can scroll through the form.</li>
</ul>

<h3 id="styling">Styling</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-styling.png" alt="Screenshot of styling in Tripetto" />
  <figcaption>Example of the navigation bar styling.</figcaption>
</figure>
<p>All common styling options can be found in the <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">help article about styling your forms</a>.</p>
<p>The following additional styling options are available for the autoscroll form face:</p>
<ul>
  <li><strong>Navigation bar</strong> - Style the navigation bar at the bottom of the form (if the navigation bar is shown). You can style the progressbar (if enabled) separately.</li>
</ul>
<hr />

<h2 id="translations">Translations of autoscroll form face</h2>
<p>If you have enabled translations of form labels and messages, you should check the labels, as the autoscroll form face has some additional messages that you'd have to translate.</p>
<p>To see the additional labels for the autoscroll form face, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Translations</code>.</p>
<p>More information on translations can be found in the <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">help article about editing/translating your forms</a>.</p>
