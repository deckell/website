---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-dropdown-block/
title: How to use the dropdown block - Tripetto Help Center
description: Learn everything you need to know to use the dropdown block in your forms.
article_title: How to use the dropdown block
article_folder: editor-block-dropdown
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-dropdown-block/
---
<p>Learn everything you need to know to use the dropdown block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the dropdown block to let your respondents select one item from a dropdown list with choices you give them.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/dropdown.gif" alt="Screenshot of a dropdown block in Tripetto" />
  <figcaption>Demonstration of a dropdown block.</figcaption>
</figure>
<blockquote>
  <h4>Looking for a dropdown with text search?</h4>
  <p>Are you looking for a dropdown question type in which the respondent also can type to get suggestions? Please have a look at the <a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/">text single line block</a> and activate the <code>Suggestions</code> feature over there.</p>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Date</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the dropdown block has the following advanced options:</p>
<ul>
  <li>
    <h4>Score</h4>
    <p>You can attach scores to the choices to perform instant calculations. By enabling the <code>Score</code> feature you can enter the desired score value per choice (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>

<h3 id="choices">Choices</h3>
<p>For the dropdown block you can enter the list of choices you want to show to your respondents:</p>
<ul>
  <li>To <strong>add choices one by one </strong> click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list;</li>
  <li>To <strong>import a list of choices at once</strong>, click the <code><i class="fas fa-download"></i></code> icon at the top of the list. You can now supply a list with one option label per text line and click <code>Import</code> to add them to your list of choices.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/import.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Importing choices in the dropdown block.</figcaption>
</figure>
<p>From your list of choices you can open each choice to get to the settings of that single choice. Over there you can use these features:</p>
<ul>
  <li>
    <h4>Name</h4>
    <p>This is the name of the choice.</p>
  </li>
  <li>
    <h4>Identifier</h4>
    <p>By enabling the <code>Identifier</code> feature you can set an identifier for that choice to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about identifiers</a>).</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>By enabling the <code>Score</code> feature you can enter the desired score value for that choice (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The dropdown block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match one of the options.</li>
  <li>Nothing selected.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
