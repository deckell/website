---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-number-block/
title: How to use the number block - Tripetto Help Center
description: Learn everything you need to know to use the number block in your forms.
article_title: How to use the number block
article_folder: editor-block-number
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-number-block/
---
<p>Learn everything you need to know to use the number block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the number block to let your respondents enter a number, including automated checks for valid number inputs.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/number.gif" alt="Screenshot of a number block in Tripetto" />
  <figcaption>Demonstration of a number block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Number</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the number block has the following advanced options:</p>
<ul>
  <li>
    <h4>Format</h4>
    <p>By enabling the <code>Format</code> feature, you can determine how many decimal places your number input may contain.</p>
  </li>
  <li>
    <h4>Signs</h4>
    <p>By enabling the <code>Signs</code> feature, you can set what signs to use as decimal sign and thousands separator. Please note that in your forms these signs can be overwritten by the respondent's locale settings. In your dataset the selected signs always will be used.</p>
  </li>
  <li>
    <h4>Limit values</h4>
    <p>By enabling the <code>Limit values</code> feature, you can enter a minimum and maximum value that respondents can enter.</p>
  </li>
  <li>
    <h4>Prefix</h4>
    <p>By enabling the <code>Prefix</code> feature, you can enter a label that's shown in front of the entered number value. Optionally you can specify a different prefix for plural values.</p>
  </li>
  <li>
    <h4>Suffix</h4>
    <p>By enabling the <code>Suffix</code> feature, you can enter a label that's shown at the end of the entered number value. Optionally you can specify a different suffix for plural values.</p>
  </li>
  <li>
    <h4>Prefill</h4>
    <p>By enabling the <code>Prefill</code> feature, you can set a fixed initial value of the number block (<a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-the-right-values/" target="_blank">more information about prefilling</a>).</p>
  </li>
  <li>
    <h4>Calculator</h4>
    <p>By enabling the <code>Calculator</code> feature, you can instantly use <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">all calculator features</a> inside the number block (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-number.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the number block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The number block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Number is equal to <code>your filter</code>;</li>
  <li>Number is not equal to <code>your filter</code>;</li>
  <li>Number is lower than<code>your filter</code>;</li>
  <li>Number is higher than <code>your filter</code>;</li>
  <li>Number is between <code>your filters</code>;</li>
  <li>Number is not between <code>your filters</code>;</li>
  <li>Number is empty;</li>
  <li>Number is not empty.</li>
</ul>
<h3 id="calculation-conditions">Calculation conditions</h3>
<ul>
  <li>Calculation is equal to <code>your filter</code>;</li>
  <li>Calculation is not equal to <code>your filter</code>;</li>
  <li>Calculation is lower than<code>your filter</code>;</li>
  <li>Calculation is higher than <code>your filter</code>;</li>
  <li>Calculation is between <code>your filters</code>;</li>
  <li>Calculation is not between <code>your filters</code>;</li>
  <li>Calculation is valid;</li>
  <li>Calculation is not valid.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
