---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-scale-block/
title: How to use the scale block - Tripetto Help Center
description: Learn everything you need to know to use the scale block in your forms.
article_title: How to use the scale block
article_folder: editor-block-scale
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-scale-block/
---
<p>Learn everything you need to know to use the scale block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the scale block to let your respondents select an opinion on the scale you provide them, for example for a Net Promoter Score (NPS) on the scale from 0 to 10. But you can also use texts scales instead of numeric scales.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/scale.gif" alt="Screenshot of scales in Tripetto" />
  <figcaption>Demonstration of two scale blocks with a numeric scale and a text scale.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Scale</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the scale block has the following advanced options:</p>
<ul>
  <li>
    <h4>Mode</h4>
    <p>The <code>Mode</code> feature is always activated. You can select if your scale is a <code>Numeric scale</code> (for example from 0 to 10) or a <code>Text scale</code> (for example from bad to good). Depending on this setting, one of the following features gets activated:</p>
    <ul>
      <li>If you selected a numeric scale, you can determine the <code>From</code> and <code>To</code> values. You can even use negative scales over here;</li>
      <li>If you selected a text scale, you can compose the list of possible options you want to show. You can add options one by one, or import a list of options from a text file at once.</li>
    </ul>
  </li>
  <li>
    <h4>Labels</h4>
    <p>By enabling the <code>Labels</code> feature, you can add some labels that are shown on the left, center and/or right of the scale.</p>
  </li>
  <li>
    <h4>Width</h4>
    <p>By enabling the <code>Width</code> feature, you can determine if the width of the scale should be fixed or the scale can grow with the size of the screen of the respondent.</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>You can attach scores to the choices to perform instant calculations. By enabling the <code>Score</code> feature you can enter the desired score value per choice (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>). This feature is only available if your scale mode is set to text scale.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-scale.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the scale block.</figcaption>
</figure>

<h3 id="text-scale-features">Text scale features</h3>
<p>If your scale mode is set to text scale, you can open each scale option to get to the settings of that single option. Over there you can use these features:</p>
<ul>
  <li>
    <h4>Name</h4>
    <p>This is the name of the scale option.</p>
  </li>
  <li>
    <h4>Identifier</h4>
    <p>By enabling the <code>Identifier</code> feature you can set an identifier for that scale option to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about identifiers</a>).</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>By enabling the <code>Score</code> feature you can enter the desired score value for that scale option (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>
<hr />

<h2 id="logic-numeric">Logic (numeric scales)</h2>
<p>Logic is important to make your forms smart and conversational. The scale block with a numeric scale can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match the scale value selected.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value is equal to <code>your filter</code>;</li>
  <li>Value is not equal to <code>your filter</code>;</li>
  <li>Value is lower than<code>your filter</code>;</li>
  <li>Value is higher than <code>your filter</code>;</li>
  <li>Value is between <code>your filters</code>;</li>
  <li>Value is not between <code>your filters</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>

<h2 id="logic-text">Logic (text scales)</h2>
<p>The scale block with a text scale can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match one of the options;</li>
  <li>Unanswered.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
