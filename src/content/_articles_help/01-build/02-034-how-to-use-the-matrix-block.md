---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-matrix-block/
title: How to use the matrix block - Tripetto Help Center
description: Learn everything you need to know to use the matrix block in your forms.
article_title: How to use the matrix block
article_folder: editor-block-matrix
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-matrix-block/
---
<p>Learn everything you need to know to use the matrix block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the matrix block to let your respondents choose from a set of options (columns) for multiple questions/statements (rows).</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/matrix.gif" alt="Screenshot of a matrix block in Tripetto" />
  <figcaption>Demonstration of a matrix block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Matrix</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="rows">Rows</h3>
<p>For the matrix block you can enter the list of rows you want to show to your respondents:</p>
<ul>
  <li>To <strong>add rows one by one </strong> click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list;</li>
  <li>To <strong>import a list of rows at once</strong>, click the <code><i class="fas fa-download"></i></code> icon at the top of the list. You can now supply a list with one row label per text line and click <code>Import</code> to add them to your list of rows.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/import.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Importing rows in the matrix block.</figcaption>
</figure>
<p>From your list of rows you can open each row to get to the settings of that single row. Over there you can use these features:</p>
<ul>
  <li>
    <h4>Row</h4>
    <p>This is the main text of the row.</p>
  </li>
  <li>
    <h4>Description</h4>
    <p>By enabling the <code>Description</code> feature, you can extend the row with a description, for example to show an extra explanation. This description is shown below the row label.</p>
  </li>
  <li>
    <h4>Required</h4>
    <p>You can make the whole matrix required by enabling the <code>Required</code> feature on the whole block, but you can also set the requirement per row. By enabling the <code>Required</code> feature per row, you can mark specific rows to be required. In that way not all rows are required, but only the ones you mark.</p>
  </li>
  <li>
    <h4>Identifier</h4>
    <p>By enabling the <code>Identifier</code> feature you can set an identifier for that row to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about identifiers</a>).</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>You can attach scores to the columns to perform instant calculations. By enabling the <code>Score</code> feature you can enter the desired score value per column (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-matrix.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the rows of the matrix block.</figcaption>
</figure>
<hr />

<h3 id="columns">Columns</h3>
<p>For the matrix block you can enter the list of columns that your respondents can choose from:</p>
<ul>
  <li>To <strong>add columns one by one </strong> click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list;</li>
  <li>To <strong>import a list of columns at once</strong>, click the <code><i class="fas fa-download"></i></code> icon at the top of the list. You can now supply a list with one column label per text line and click <code>Import</code> to add them to your list of columns.</li>
</ul>
<p>From your list of columns you can open each column to get to the settings of that single column. Over there you can use these features:</p>
<ul>
  <li>
    <h4>Column</h4>
    <p>This is the main text of the column;</p>
  </li>
  <li>
    <h4>Identifier</h4>
    <p>By enabling the <code>Identifier</code> feature you can set an identifier for that column to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about identifiers</a>);</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>By enabling the <code>Score</code> feature you can enter the desired score value for that column (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The matrix block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Answer state (per row).</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
