---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-paragraph-block/
title: How to use the paragraph block - Tripetto Help Center
description: Learn everything you need to know to use the paragraph block in your forms.
 to let it behave exactly as you need it.
article_title: How to use the paragraph block
article_folder: editor-block-paragraph
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-paragraph-block/
---
<p>Learn everything you need to know to use the paragraph block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the paragraph block to show a static block of text without any input controls for your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/paragraph.png" alt="Screenshot of a paragraph in Tripetto" />
  <figcaption>Demonstration of a paragraph block with an image.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Paragraph</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the paragraph block has the following advanced options:</p>
<ul>
  <li>
    <h4>Caption</h4>
    <p>By enabling the <code>Caption</code> feature, you can add a subtitle to the paragraph.</p>
  </li>
  <li>
    <h4>Image</h4>
    <p>By enabling the <code>Image</code> feature, you can add an image from a URL to the paragraph (<a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" target="_blank">more information about using images inside your form</a>). You can additionally set the width and position of the image.</p>
  </li>
  <li>
    <h4>Video</h4>
    <p>By enabling the <code>Video</code> feature, you can embed a video from YouTube or Vimeo to the paragraph (<a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" target="_blank">more information about using videos inside your form</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-paragraph.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Extra options in the paragraph block.</figcaption>
</figure>
<hr />

{% include help-article-blocks.html %}
