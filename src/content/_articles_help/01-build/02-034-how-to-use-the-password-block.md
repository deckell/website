---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-password-block/
title: How to use the password block - Tripetto Help Center
description: Learn everything you need to know to use the password block in your forms.
article_title: How to use the password block
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-password-block/
---
<p>Learn everything you need to know to use the password block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the password block to let your respondent enter a value that's not visible in the form. Optionally you can use this block to <a href="{{ page.base }}help/articles/how-to-verify-passwords-inside-your-form/" target="_blank">protect parts of your form</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/password.gif" alt="Screenshot of a password block in Tripetto" />
  <figcaption>Demonstration of a password block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Password</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="exportability">Exportability</h3>
<p>Unlike <a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" target="_blank">other question blocks</a> the entered values of password blocks aren't saved to the dataset of the form entries by default. In that way the passwords that your respondents enter, aren't saved unnecessary.</p>
<p>If you wish to include the entered passwords in your dataset anyway, you can overrule the default setting by enabling the <code>Exportability</code> feature and activate the <code>Include in the dataset</code> setting. Entered passwords will now be saved and readable inside your dataset.</p>
<blockquote>
  <h4>Warning</h4>
  <p>Please make sure you are always careful with using the dataset if you choose to save passwords!</p>
</blockquote>
<hr />

<h2 id="verification">Secure password verification</h2>
<p>You can use the password block to protect (parts of) your form by using the secure password match check. <a href="{{ page.base }}help/articles/how-to-verify-passwords-inside-your-form/" target="_blank">More information on that can be found in this article</a>.</p>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The password block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Password matches <code>your password</code> (<a href="{{ page.base }}help/articles/how-to-verify-passwords-inside-your-form/" target="_blank">more information about password verification</a>);</li>
  <li>Password does not match <code>your password</code> (<a href="{{ page.base }}help/articles/how-to-verify-passwords-inside-your-form/" target="_blank">more information about password verification</a>).</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
