---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-rating-block/
title: How to use the rating block - Tripetto Help Center
description: Learn everything you need to know to use the rating block in your forms.
article_title: How to use the rating block
article_folder: editor-block-rating
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-rating-block/
---
<p>Learn everything you need to know to use the rating block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the rating block to let your respondents select a rating on the scale you provide them. The scale can have various shapes (like stars, hearts, thumbs) and various steps.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/rating.gif" alt="Screenshot of rating blocks in Tripetto" />
  <figcaption>Demonstration of three rating blocks with different shapes and steps.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Rating</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the rating block has the following advanced options:</p>
<ul>
  <li>
    <h4>Shape</h4>
    <p>By enabling the <code>Shape</code> feature, you can select the shape of the rating elements. You can choose from the list of shapes we provide.</p>
  </li>
  <li>
    <h4>Steps</h4>
    <p>By enabling the <code>Steps</code> feature, you can determine the scale of your rating question. For example from 1 to 5, or from 1 to 10.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-rating.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the rating block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The rating block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match the rating value selected.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value is equal to <code>your filter</code>;</li>
  <li>Value is not equal to <code>your filter</code>;</li>
  <li>Value is lower than<code>your filter</code>;</li>
  <li>Value is higher than <code>your filter</code>;</li>
  <li>Value is between <code>your filters</code>;</li>
  <li>Value is not between <code>your filters</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
