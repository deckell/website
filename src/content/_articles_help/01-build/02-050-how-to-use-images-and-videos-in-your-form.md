---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-images-and-videos-in-your-form/
title: How to use images and videos in your form - Tripetto Help Center
description: You can use external hosted images and videos on several places inside your form.
article_title: How to use images and videos in your form
article_folder: editor-images
author: jurgen
time: 2
category_id: build
subcategory: build_images_videos
areas: [studio, wordpress]
---
<p>You can use external hosted images and videos on several places inside your form.</p>

<h2 id="usage">How to use</h2>
<p>Multimedia items like images and videos of course brighten up your form, or even can be essential for your form's purpose. There are several places inside your form where you can use external hosted images and videos.</p>
<p>At the places where you can use images and/or videos, you can enable the <code>Image</code> and <code>Video</code> features (<a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">see this article about enabling block features</a>).</p>

<h3 id="images">How to use images</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-image.png" alt="Screenshot of Giphy" />
  <figcaption>Example of a Giphy image URL.</figcaption>
</figure>
<p>You can use images that are hosted somewhere else. This can be on your own site or an image hosting site (for example <a href="https://imgur.com/" target="_blank">Imgur</a>). Always make sure the image you host somewhere is publicly accessible. For example if you use a cloud platform like Google Photos, by default your photos will be not publicly accessible and you have to set the photo as publicly available manually before you can use it.</p>
<p>There's also lots of free to use image databases that you can use (for example <a href="https://unsplash.com/" target="_blank">Unsplash</a> and <a href="https://giphy.com/" target="_blank">Giphy</a>).</p>
<p>When your image is online available, you can use it in your form. Please make sure you enter the direct URL to the image, by right clicking the image and then select <code>Copy image address</code>. Now paste that URL in the <code>Image</code> field in the form builder. The image will now be shown directly inside your form.</p>
<blockquote>
  <p>Always make sure it's allowed to use the image and/or take care of the appropiate copyright notice.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-image-url.gif" alt="Screenshot of Unsplash" />
  <figcaption>Get the image URL by right-clicking the image.<br/><a href="https://unsplash.com/photos/5rKw0ho9qTw" target="_blank">Photo credits Unsplash</a>.</figcaption>
</figure>


<h3 id="videos">How to use videos</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-video.png" alt="Screenshot of YouTube" />
  <figcaption>Example of a YouTube embed.</figcaption>
</figure>
<p>You can use videos that are published on YouTube or Vimeo.</p>
<p>You can copy the link to your video directly in the <code>Video</code> field in the form builder. The video will now be shown directly inside your form.</p>

<h2 id="places">Where to use</h2>
<p>You can use images and/or videos at the following places in your form:</p>
<ul>
  <li><strong>Welcome message</strong> - You can place an image and video inside <a href="{{ page.base }}help/articles/how-to-add-a-welcome-message/" target="_blank">the welcome message</a>;</li>
  <li><strong>Closing messages</strong> - You can place an image and video inside <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/" target="_blank">all closing messages</a>;</li>
  <li><strong>Question blocks</strong> - You can place an image inside some question blocks, like the Paragraph, Multiple choice, Rating and Yes/No;</li>
  <li><strong>Background image</strong> - You can add a background image to <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">the styling of your form</a>;</li>
  <li><strong>Chat Avatar</strong> - You can use a custom image to show as <a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/" target="_blank">the avatar in the chat form face</a>.</li>
</ul>
