---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-picture-choice-block/
title: How to use the picture choice block - Tripetto Help Center
description: Learn everything you need to know to use the picture choice block in your forms.
article_title: How to use the picture choice block
article_folder: editor-block-picture-choice
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-picture-choice-block/
---
<p>Learn everything you need to know to use the picture choice block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the picture choice block to let your respondents select one or multiple items from a set of picture choices you give them. Each picture choice can contain an image or an emoji.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/picture-choice.gif" alt="Screenshot of picture choices in Tripetto" />
  <figcaption>Demonstration of two picture choice blocks with images and emojis.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Picture choice</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the picture choice block has the following advanced options:</p>
<ul>
  <li>
    <h4>Multiple select</h4>
    <p>By default the picture choice block behaves a single selection, so respondents can only select one option from the list of choices.</p>
    <p>By enabling the <code>Multiple select</code> feature, you can choose how the block must behave. Enable <code>Allow the selection of multiple images</code> to let respondents select one or more option(s). Disable this option to restrict the selection to only one answer.</p>
  </li>
  <li>
    <h4>Size</h4>
    <p>By default the choices are shown as medium sized buttons.</p>
    <p>By enabling the <code>Size</code> feature, you can choose what size the buttons should have: <code>Small</code>, <code>Medium</code> or <code>Large</code>.</p>
  </li>
  <li>
    <h4>Labels</h4>
    <p>By default each choice will be marked as <code>Selected</code> or <code>Not selected</code> in your dataset (your results). By enabling the <code>Labels</code> feature you can overwrite these labels in your dataset with your own values for each choice individually (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about labels</a>). This feature is only available if your question allows the selection of multiple answers.</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>You can attach scores to the choices to perform instant calculations. By enabling the <code>Score</code> feature you can enter the desired score value per choice (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-picture-choice.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the picture choice block.</figcaption>
</figure>

<h3 id="choices">Choices</h3>
<p>For the picture choice block you can enter the list of choices you want to show to your respondents:</p>
<ul>
  <li>To <strong>add choices one by one </strong> click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list;</li>
  <li>To <strong>import a list of choices at once</strong>, click the <code><i class="fas fa-download"></i></code> icon at the top of the list. You can now supply a list with one option label per text line and click <code>Import</code> to add them to your list of choices.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/import.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Importing choices in the picture choice block.</figcaption>
</figure>
<p>From your list of choices you can open each choice to get to the settings of that single choice. Over there you can use these features:</p>
<ul>
  <li>
    <h4>Name</h4>
    <p>This is the name of the choice. You can select if you'd like to show this name in the button.</p>
  </li>
  <li>
    <h4>Image</h4>
    <p>A picture choice always contains an image or emoji to show in the form.</p>
    <ul>
      <li>By enabling <code>Use an image</code> you can enter a URL to the image you'd like to show. More information about <a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" target="_blank">using images can be found in this article</a>;</li>
      <li>By enabling <code>Use an emoji</code> you can enter an emoji icon you'd like to show. All emojis are available. You can easily search and select emojis using these keyboard shortcut:<br/><i class="fab fa-windows"></i> Windows: <code>Windows key</code> + <code>.</code><br/><i class="fab fa-apple"></i> MacOS: <code>CTRL</code> + <code>CMD</code> + <code>space</code></li>
    </ul>
  </li>
  <li>
    <h4>Description</h4>
    <p>By enabling the <code>Description</code> feature, you can extend the button name with a description for example to show an extra explanation. This description is shown inside the button below the name.</p>
  </li>
  <li>
    <h4>URL</h4>
    <p>You can even turn a choice into a real button that opens a URL. By enabling the <code>URL</code> feature, you can enter a URL that will be opened in a new tab/window when a respondent selects the option.</p>
    <p>Important notice: a choice that opens a URL can NOT be selected as an answer by your respondents.</p>
  </li>
  <li>
    <h4>Moniker</h4>
    <p>The moniker is a feature you can use when you're implementing <a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/" target="_blank">a repeated follow-up</a> for multiple selected options. Inside the block(s) of the repeated follow-up, you can use the label of the selected choice, so you can clarify which selected choice the follow-up question is about. This is part of piping logic, as described in <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">this article</a>.</p>
    <p>But sometimes the label you used in the button isn't usable in the context of your follow-up. A simple example of such a scenario is when you entered the label in the button with a capital first letter, but you want to mention the label in the middle of a sentence in the follow-up (without a capital letter). By enabling the <code>Moniker</code> feature, you can enter a deviant label to use in the follow-up.</p>
  </li>
  <li>
    <h4>Exclusivity</h4>
    <p>By enabling the <code>Exclusivity</code> feature you can make that choice exclusive, so if a respondent selects that choice, all other choices will be unselected, making the selected choice exclusive. This feature is only available if your question allows the selection of multiple answers.</p>
  </li>
  <li>
    <h4>Labels</h4>
    <p>By default each choice will be marked as <code>Selected</code> or <code>Not selected</code> in your dataset (your results). By enabling the <code>Labels</code> feature you can overwrite these labels in your dataset with your own values for each choice (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about labels</a>). This feature is only available if your question allows the selection of multiple answers.</p>
  </li>
  <li>
    <h4>Identifier</h4>
    <p>By enabling the <code>Identifier</code> feature you can set an identifier for that choice to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about identifiers</a>).</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>By enabling the <code>Score</code> feature you can enter the desired score value for that choice (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-choice.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the choices inside the picture choice block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The picture choice block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match one of the options;</li>
  <li>No image selected.</li>
</ul>
<h3 id="evaluate-conditions-block">Evaluate conditions</h3>
<p>Conditions for whole block:</p>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<p>Conditions for each option:</p>
<ul>
  <li>Option is true;</li>
  <li>Option is false;</li>
  <li>Option equals <code>your filter</code>;</li>
  <li>Option not equals <code>your filter</code>;</li>
  <li>Option is empty;</li>
  <li>Option is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
