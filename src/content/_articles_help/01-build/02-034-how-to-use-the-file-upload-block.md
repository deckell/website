---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-file-upload-block/
title: How to use the file upload block - Tripetto Help Center
description: Learn everything you need to know to use the file upload block in your forms.
article_title: How to use the file upload block
article_folder: editor-block-file-upload
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-file-upload-block/
---
<p>Learn everything you need to know to use the file upload block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the file upload block to let your respondents upload a file or image to their responses.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/file-upload.gif" alt="Screenshot of a file upload block in Tripetto" />
  <figcaption>Demonstration of a file upload block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>File upload</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> option.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the file upload block has the following advanced options:</p>
<ul>
  <li>
    <h4>File size</h4>
    <p>By enabling the <code>File size</code> feature, you can determine the maximum file size (in Mb) that respondents can upload.</p>
  </li>
  <li>
    <h4>File type</h4>
    <p>By enabling the <code>File type</code> feature, you can determine what file type(s) respondents can select. You can supply multiple file types, separated with a comma (<code>,</code>). Make sure you start each file type with the dot (for example <code>.pdf</code>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-file-upload.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced settings of the file upload block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The file upload block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>File uploaded;</li>
  <li>No file uploaded.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
