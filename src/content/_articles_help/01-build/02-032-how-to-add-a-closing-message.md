---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-a-closing-message/
title: How to add a closing message - Tripetto Help Center
description: Learn how to alter the common closing message that's shown after form completion.
article_id: closing
article_title: How to add a closing message
article_folder: editor-end
author: jurgen
time: 4
category_id: build
subcategory: build_messages
areas: [studio, wordpress]
---
<p>Learn how to alter the common closing message that's shown after form completion.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the closing message to show your own message after your respondents have completed your form. For example to thank them or present them the result of the form they filled out.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.gif" alt="Screenshot of a closing message in Tripetto" />
  <figcaption>Demonstration of a closing message.<br/><a href="https://unsplash.com/photos/IPx7J1n_xUc" target="_blank">Photo credits Unsplash</a>.</figcaption>
</figure>
<blockquote>
  <h4>Flexible closing messages</h4>
  <p>Closing messages are way more flexible than just one closing message for all your respondents. In this article we will show the common closing message, but you can also add unlimited flexible closing messages, based on the given answers of your respondents.</p>
  <p><a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/" target="_blank">Please see this article for more information about flexible closing messages.</a></p>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>To enable the closing message, open your form inside the form builder. Each form always has at least one ending of the form, represented by the red bubble with a <code><i class="fas fa-sign-out-alt"></i></code> icon at the bottom of the form. By clicking that bubble the pane to add/edit the closing message opens up.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-add.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The closing message can be set from the red bubble.</figcaption>
</figure>

<h3 id="features">Features</h3>
<p>The closing message consists of some features that you can enable, just the way you like/need it. You can see those features on the left of the closing message pane. The following features are available:</p>
<ul>
  <li><strong>Text</strong> - Enable the <code>Text</code> feature to supply a title to the closing message.</li>
  <li><strong>Description</strong> - Enable the <code>Description</code> feature to supply a description to the closing message.</li>
  <li><strong>Image</strong> - Enable the <code>Image</code> feature to add an image to the closing message. The image must be hosted somewhere else so you can supply the URL to the image;</li>
  <li><strong>Video</strong> - Enable the <code>Video</code> feature to add a video to the closing message. We support YouTube and Vimeo videos. All you have to do is copy-paste the URL of the video from YouTube/Vimeo and the video will be embedded;</li>
  <li><strong>Button</strong> - Enable the <code>Button</code> feature to add a button to the closing message, with the following options:
    <ul>
      <li>Label - The text label shown inside the button;</li>
      <li>URL - The URL to open, including <code>https://</code>;</li>
      <li>Open in - Determine if the URL should be opened in the same browser window (form gets closed) or a new browser window (form stays opened in original browser window).</li>
    </ul>
  </li>
  <li><strong>Repeatability</strong> - Enable the <code>Repeatability</code> feature to determine if a form that is completed can be started over again immediately after completion;</li>
  <li><strong>Redirect</strong> - Enable the <code>Redirect</code> feature to automatically redirect your respondents to an external URL on completion. All of the above features will be disabled when the redirect is enabled. More information on redirecting after completion can be found in <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/" target="_blank">this article</a>.</li>
</ul>

<h3 id="form-faces">Form faces</h3>
<p>Tripetto forms can be presented in different ways, that we call <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" target="_blank">form faces</a>. It depends on the selected form face how the closing message gets presented and behaves.</p>

<h3 id="piping" data-anchor="Piping logic">Recall values using piping logic</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-identifier.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Add the identification number of the entry in the redirect URL.</figcaption>
</figure>
<p>Of course <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">piping logic</a> is available to show given answers from the form in the closing message.</p>
<p>These piping values are also available in the URL fields (<code>Button</code> and <code>Redirect</code>), using the <code>@</code> sign. You can use these piping values to send certain data to a URL. This even enables you to <a href="{{ page.base }}help/articles/how-to-share-response-data-between-forms/" target="_blank">share response data between different Tripetto forms</a>.</p>
<p>On top of that, there is an extra piping value available in the closing message: the <code>Identification number</code>. This is the unique identification number of each entry by which you can identify/track a certain entry on the URL that's opened.</p>
<hr />
