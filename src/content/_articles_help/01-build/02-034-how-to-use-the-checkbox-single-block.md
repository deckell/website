---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-checkbox-single-block/
title: How to use the checkbox (single) block - Tripetto Help Center
description: Learn everything you need to know to use the checkbox (single) block in your forms.
article_title: How to use the checkbox (single) block
article_folder: editor-block-checkbox
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-checkbox-single-block/
---
<p>Learn everything you need to know to use the checkbox (single) block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the checkbox (single) block to let your respondents check/uncheck one checkbox, for example to agree your terms.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/checkbox.gif" alt="Screenshot of a single checkbox block in Tripetto" />
  <figcaption>Demonstration of a single checkbox block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Checkbox (single)</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the checkbox block has the following advanced options:</p>
<ul>
  <li>
    <h4>Placeholder</h4>
    <p>By default the name of the block will be used as the label for the checkbox. By enabling the <code>Placeholder</code> feature you can show the name of the block on top of the checkbox and show the placeholder as the label for the checkbox.</p>
  </li>
  <li>
    <h4>Labels</h4>
    <p>By default the checkbox will be marked as <code>Checked</code> or <code>Not checked</code> in your dataset (your results). By enabling the <code>Labels</code> feature you can overwrite these labels in your dataset with your own values (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about labels</a>).</p>
  </li>
  <li>
    <h4>Score</h4>
    <p>You can attach scores to the checkbox states to perform instant calculations. By enabling the <code>Score</code> feature you can enter the desired score values (<a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" target="_blank">more information about instant scores</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/options.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Additional features of the checkbox (single) block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The checkbox (single) block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Checkbox is checked;</li>
  <li>Checkbox is not checked.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value is true;</li>
  <li>Value is false;</li>
  <li>Value equals <code>your filter</code>;</li>
  <li>Value not equals <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, you can select another question block that you want to compare the given answer with (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</p>
<hr />

{% include help-article-blocks.html %}
