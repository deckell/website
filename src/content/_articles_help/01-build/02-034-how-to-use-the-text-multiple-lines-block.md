---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-text-multiple-lines-block/
title: How to use the text (multiple lines) block - Tripetto Help Center
description: Learn everything you need to know to use the text (multiple lines) block in your forms.
article_title: How to use the text (multiple lines) block
article_folder: editor-block-text-multiple
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-text-multiple-lines-block/
---
<p>Learn everything you need to know to use the text (multiple lines) block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the text (multiple lines) block to let your respondents enter a text answer that can contain line breaks. Usually used for long text answers, like explanations.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/text-multiple.gif" alt="Screenshot of text inputs in Tripetto" />
  <figcaption>Demonstration of a text multiple lines block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Text (multiple lines)</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the text (multiple lines) block has the following advanced options:</p>
<ul>
  <li>
    <h4>Transform</h4>
    <p>By enabling the <code>Transform</code> feature, you can determine how the answered text should be transformed. For example you can capitalize certain parts of the answer, or convert all characters to lowercase/uppercase.</p>
  </li>
  <li>
    <h4>Prefill</h4>
    <p>By enabling the <code>Prefill</code> feature, you can set a fixed initial value of the text block (<a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-the-right-values/" target="_blank">more information about prefilling</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-text-multiple.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced settings of the text multiple lines block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The text (multiple lines) block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
