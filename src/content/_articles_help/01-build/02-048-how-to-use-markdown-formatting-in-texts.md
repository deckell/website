---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-markdown-formatting-in-texts/
title: How to use markdown formatting in texts - Tripetto Help Center
description: You can use markdown formatting in text features to create highlighted texts and hyperlinks.
article_title: How to use markdown formatting in texts
author: martijn
time: 2
category_id: build
subcategory: build_formatting
areas: [studio, wordpress]
---
<p>Markdown helps you to highlight important parts of your texts. And you can use it to create hyperlinks inside your text features.</p>

<h2 id="text-formatting">Text formatting</h2>
<p>Use the following formats in your text features to apply the desired text formatting:</p>
<ul>
  <li>Bold text: <code>**</code><b>bold text</b><code>**</code> (double asteriks);</li>
  <li>Italic text: <code>*</code><i>italic text</i><code>*</code> (single asteriks);</li>
  <li>Underline text: <code>_</code><u>underline text</u><code>_</code> (single underscore);</li>
  <li>Strikethrough text: <code>~</code><s>strikethrough text</s><code>~</code> (single tilde).</li>
</ul>
<p>Bear in mind you need to open the formatting at the desired starting point and end the formatting at the desired ending point by using the same markdown sign.</p>

<h2 id="hyperlink">Hyperlink</h2>
<p>You can also create hyperlinks inside your text features. Use the following format to do so: <code>[Link text](link URL)</code>.</p>

<blockquote>
  <h3 id="example">Full example</h3>
  <h4><i class="far fa-keyboard"></i>Your text input</h4>
  <p><code>The **quick** brown *fox* jumps _over_ the ~lazy~ dog. Just like [this](https://en.wikipedia.org/wiki/The_quick_brown_fox_jumps_over_the_lazy_dog).</code></p>
  <h4><i class="fas fa-indent"></i><b>Result in your form</b></h4>
  <p>The <b>quick</b> brown <i>fox</i> jumps <u>over</u> the <s>lazy</s> dog. Just like <a href="https://en.wikipedia.org/wiki/The_quick_brown_fox_jumps_over_the_lazy_dog" target="_blank">this</a>.</p>
</blockquote>
<p>Please have a look at <a href="{{ page.base }}help/articles/how-to-insert-hyperlinks/" target="_blank">this article for more information about hyperlinks and buttons in your form</a>.</p>

<h2 id="piping">Given answers, using piping logic</h2>
<p>Not really part of markdown formatting, but it is usable in our text features: show given answers of your respondent inside a form, using <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">piping logic</a>.</p>
<p>You can show given answers by typing the <code>@</code> sign at the desired place in a text feature. A menu will appear with all question blocks in your form that you can use for piping logic. After selecting the right question block, Tripetto will take care of showing the respondents' answer on that position.</p>
