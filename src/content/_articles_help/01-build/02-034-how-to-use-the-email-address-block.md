---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-email-address-block/
title: How to use the email address block - Tripetto Help Center
description: Learn everything you need to know to use the email address block in your forms.
article_title: How to use the email address block
author: jurgen
time: 2
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-email-address-block/
---
<p>Learn everything you need to know to use the email address block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the email address block to let your respondents enter an email address, including automated checks for valid addresses.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/email-address.gif" alt="Screenshot of an email address block in Tripetto" />
  <figcaption>Demonstration of an email address block, including real-time validation.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Email address</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The email address block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Email address matches <code>your filter</code>;</li>
  <li>Email address does not match <code>your filter</code>;</li>
  <li>Email address matches domain <code>your filter</code>;</li>
  <li>Email address does not match domain <code>your filter</code>;</li>
  <li>Email address is empty;</li>
  <li>Email address is not empty.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
