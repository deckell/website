---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-text-single-line-block/
title: How to use the text (single line) block - Tripetto Help Center
description: Learn everything you need to know to use the text (single line) block in your forms.
article_title: How to use the text (single line) block
article_folder: editor-block-text-single
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-text-single-line-block/
---
<p>Learn everything you need to know to use the text (single line) block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the text (single line) block to let your respondents enter a text answer that consists of one line. Mostly used for short text answers, like personal data.</p>
<p>You also can extend the text input with <a href="#suggestions-feature" class="anchor">suggestions</a> to let the respondent select from a list of suggestions you give them, while they are typing.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/text-single.gif" alt="Screenshot of text inputs in Tripetto" />
  <figcaption>Demonstration of a text single line block. The second question shows the usage of suggestions.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Text (single line)</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the text (single line) block has the following advanced options:</p>
<ul>
  <li>
    <h4>Character count</h4>
    <p>By enabling the <code>Character count</code> feature, you can control the amount of characters your respondents can enter. You can set the <code>Minimum</code> and <code>Maximum</code> character count.</p>
  </li>
  <li>
    <h4>Autocomplete</h4>
    <p>By enabling the <code>Autocomplete</code> feature, you can use the autocomplete functionality of the respondent's browser to help the respondent to fill out the form as quickly as possible. You can select which autocomplete value you'd like to insert, like the name or address of the respondent.</p>
  </li>
  <li>
    <h4>Suggestions</h4>
    <p>By enabling the <code>Suggestions</code> feature, you can extend your text input with suggestions that popup while the respondent types in your text input (<a href="#suggestions-feature" class="anchor">more information</a>).</p>
  </li>
  <li>
    <h4>Transform</h4>
    <p>By enabling the <code>Transform</code> feature, you can determine how the answered text should be transformed. For example you can capitalize certain parts of the answer, or convert all characters to lowercase/uppercase.</p>
  </li>
  <li>
    <h4>Prefill</h4>
    <p>By enabling the <code>Prefill</code> feature, you can set a fixed initial value of the text block (<a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-the-right-values/" target="_blank">more information about prefilling</a>).</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-text-single.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced settings of the text single line block.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-autocomplete.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Autocomplete options of the text single line block.</figcaption>
</figure>

<h3 id="suggestions-feature">Suggestions feature</h3>
<p>You can extend your text input with suggestions that popup while the respondent types in your text input. The respondent can quickly select an item from your suggestions list. This way your text input can behave as a combination between a text input and a dropdown box.</p>
<p>To activate this, enable the <code>Suggestions</code> feature. Now you can add/import your list of suggestions that you want your respondents to choose from.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-suggestions.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>List of suggestions in the text single line block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The text (single line) block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
