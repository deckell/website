---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-date-and-time-block/
title: How to use the date (and time) block - Tripetto Help Center
description: Learn everything you need to know to use the date (and time) block in your forms.
article_title: How to use the date (and time) block
article_folder: editor-block-date
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-date-block/
---
<p>Learn everything you need to know to use the date (and time) block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the date block to let your respondents select a date and optionally a time, for example to make an appointment.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/date.gif" alt="Screenshot of a date block Tripetto" />
  <figcaption>Demonstration of a date selection block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Date</code>. You can now customize this block to your needs with the following features.</p>

<h3 id="basic-features">Basic features</h3>
<p>Each question block has basic features to present the block the way you need, for example <code>Name</code>, <code>Description</code> and <code>Help text</code>. And most of the question blocks have common options, like the <code>Required</code> and <code>Exportability</code> options.</p>
<p>More information about these basic features can be found in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" target="_blank">the help article about our form builder</a>.</p>

<h3 id="additional-features">Additional features</h3>
<p>On top of those basic features, the date block has the following advanced options:</p>
<ul>
  <li>
    <h4>Time</h4>
    <p>By enabling the <code>Time</code> feature, you can let your respondents also enter a time with the date.</p>
  </li>
  <li>
    <h4>Range</h4>
    <p>By enabling the <code>Range</code> feature, you can let the block ask for two dates, namely a <code>From</code> date and a <code>To</code> date. The form will automatically show two date fields if this feature is enabled.</p>
    <p><i>If enabled, you can also enter two separate <code>Placeholders</code> for the date fields in the form.</i></p>
  </li>
  <li>
    <h4>Limit values</h4>
    <p>By enabling the <code>Limit values</code> feature, you can determine in between what dates the answered date(s) have to be. You can select the minimum and maximum date (including an optional time).</p>
    <p>Next to static date limits you can also determine if the date that your respondent selects has to be <code>In the future</code> and/or <code>In the past</code>. This will be checked at the moment the respondent fills out the form.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-date-settings.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced options of the date block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The date block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Date (and time) is equal to <code>your filter</code>;</li>
  <li>Date (and time) is not equal to <code>your filter</code>;</li>
  <li>Date (and time) is before <code>your filter</code>;</li>
  <li>Date (and time) is after <code>your filter</code>;</li>
  <li>Date (and time) is between <code>your filters</code>;</li>
  <li>Date (and time) is not between <code>your filters</code>;</li>
  <li>Date (and time) is empty;</li>
  <li>Date (and time) is not empty.</li>
</ul>
<p>If the <code>Range</code> is enabled, you can use these conditions for the <code>From</code> date and the <code>To</code> date separately.</p>

<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Current date/time - Compare with the current date (and time) of the moment of form usage by a respondent;</li>
  <li>Fixed date/time - Compare with a fixed date (and time) that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
