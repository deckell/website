---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-build-your-forms-in-the-form-builder/
title: How to build your forms in the form builder - Tripetto Help Center
description: Learn how to use our unique form builder to create your forms.
article_title: How to build your forms in the form builder
article_id: build
author: martijn
time: 4
time_video: 3
category_id: build
subcategory: build_basics
areas: [studio, wordpress]
popular: true
---
<p>Let's have a quick look on the principles of our form builder, how to add the desired content (like question blocks, welcome and closing messages) and how to add some smartness to it all.</p>

<h2 id="video">Video tutorial</h2>
<p>We have made a video tutorial on the basics of the form builder. This shows you how to add and edit question blocks.</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/KRFAR4Sw2sk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on adding and editing question blocks.</figcaption>
</figure>
<hr />

<h2 id="principles" data-anchor="Principles">Principles of the form builder</h2>
<p>As you may have noticed, our form builder is a bit different from traditional form builders. Let us explain some principles of our form builder.</p>
<blockquote>
<h4>Tip</h4>
<p>The form builder can be used with some smart gestures. Please have a look at this article to learn how to control the form builder: <a href="{{ page.base }}help/articles/learn-the-basics-of-the-form-builder/">Learn the basics of the form builder</a>.</p>
</blockquote>

<h3>Intended for smartness</h3>
<p>The form builder is intended to build smart forms. Smart forms need logic and that's what the form builder helps you with: it shows a visual presentation of the flows in your forms on a storyboard.
  <ul>
    <li>The top-down direction shows the order of the form blocks inside the form;</li>
    <li>The left-right direction shows decisions inside the form that can be taken depending on the input of your respondents. We call those decicions <strong>branches</strong>.</li>
  </ul>
</p>

<h3 id="form-blocks">Form blocks</h3>
<p>The actual content of your form gets determined by the <strong>form blocks</strong> you add. We offer question blocks and action blocks. Each form block contains its own content, depending on the block type, question type and enabled block features.</p>

<h4>Question blocks</h4>
<p>For each question block you can select the <strong>question type</strong>. The question type takes care of the input control that is associated with each block. Examples of question types are text (single and multiple line), multiple choice, picture choice, checkboxes, radio buttons, date, matrix, file upload, etc. The chosen question type also determines which block features are available.</p>

<h4>Action blocks</h4>
<p>Action blocks, just like question blocks, can be used anywhere in the form, but instead of providing a certain input, it performs a certain action. Examples of actions are performing a calculation, sending an email or prefilling a value in a question.</p>

<h4>Block features</h4>
<p>On the left of the block pane, you'll see a list of <strong>block features</strong>. These are switches you can toggle to add content and settings to each form block.</p>
<p>Depending on the type of block and the selected question type, the list of block features will update. To help you with this we made detailed help articles for each form block, that you can find over here:</p>
{% include help-article-blocks.html %}

<h4>Common block features</h4>
<p>Most of these block features are available for all question types, for example:</p>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-features/00-features.gif" alt="Screenshot of features in Tripetto" />
  <figcaption>Enabling the needed features of a block.</figcaption>
</figure>
<ul>
  <li><code>Text</code> - The main text of the form block;</li>
  <li><code>Description</code> - An optional description of the form block;</li>
  <li><code>Help text</code> - An optional help text of the form block;</li>
  <li><code>Required</code> - An option to determine if the question is required to fill out by the respondents;</li>
  <li><code>Visibility</code> - An option to determine if the block is visible in the form;</li>
  <li><code>Alias</code> - An option to set an alias to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information on alias over here</a>);</li>
  <li><code>Exportability</code> - An option to determine if the data is saved (<a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" target="_blank">more information on the exportability over here</a>).</li>
</ul>

<h3 id="clusters">Clusters</h3>
<p>All form blocks are placed within <strong>clusters</strong>. Clusters are the containers for the questions you're asking/actions you're performing. You can use clusters to add logic and create cuts inside your form.</p>

<h3 id="branches">Branches for logic</h3>
<p>To create the necessary logic for smart forms, you use so called <strong>branches</strong>. By adding the desired <strong><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a></strong> and <strong><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/" target="_blank">branch behavior</a></strong> you determine what condition(s) must be matched to enter a certain branch in the form. Branches are pretty powerful and you can <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">read all about it in this article</a>.</p>
<p>Underneath each branch you can fill in the follow-up, containing the form blocks and/or logic that respondents get to see when they have entered a certain branch in the form.</p>
<p>At the end of each branch you can select how the form should proceed after this branch is completed by your respondents. You can choose from different types of <strong><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/" target="_blank">branch endings</a></strong>.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-conditions/00-basic.png" alt="Screenshot of branches in Tripetto" />
  <figcaption>Example of a branch.</figcaption>
</figure>

<h3 id="start-end">Start and End</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-start/00-add.gif" alt="Screenshot of a welcome message in Tripetto" />
  <figcaption>Add a welcome message.</figcaption>
</figure>
<p>When you start a new form, all you see is a green bubble with a <code><i class="fas fa-sign-in-alt"></i></code> icon and a red bubble with a <code><i class="fas fa-sign-out-alt"></i></code> icon. Those two bubbles indicate the <strong>starting point (green)</strong> and <strong>ending point (red)</strong> of your form. In between those, we're going to create our form.</p>
<p>You can add a <a href="{{ page.base }}help/articles/how-to-add-a-welcome-message/" target="_blank">welcome message</a> and a <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/" target="_blank">closing message</a> (or <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/" target="_blank">even multiple closing messages, based on your respondent's answers</a>). Click the links to find out how.</p>
<hr />

<h2>More in-depth tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" alt="YouTube logo" />
    </div>
  </a>
</div>
