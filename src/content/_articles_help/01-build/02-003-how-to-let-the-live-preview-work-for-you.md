---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-let-the-live-preview-work-for-you/
title: How to let the live preview work for you - Tripetto Help Center
description: Learn how to let the live preview help you to build your forms.
article_title: How to let the live preview work for you
article_id: preview
author: martijn
time: 3
category_id: build
subcategory: build_basics
areas: [studio, wordpress]
---
<p>While building your form, you can immediately see what you're working on in the <strong>live preview</strong>. Let us take a closer look at how the preview can help you while building your forms.</p>

<h2 id="preview">Live preview</h2>
<p>The preview is always shown on the right side of the form builder, so you can see your form structure and the form itself side-by-side.</p>

<h3 id="live">Truly live</h3>
<p>The live preview is truly live! Every little change you do in the form builder gets updated in the preview right away. From a simple text change to changing a question type, to rearranging the order of questions. All changes have an immediate effect on the live preview.</p>
<p>Even changes in the <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" target="_blank">form face</a>, <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">styling</a> and <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">translations</a> get executed right away, so you can play with all these customizations very easily and quickly.</p>

<h2 id="modes">Logic modes</h2>
<p>At the top of the preview pane you can switch between two modes that determine if the logic should be applied to the preview form:</p>
<ul>
  <li><h3 id="without-logic">All without logic</h3>Click <code>All without logic</code> - All question blocks will be shown, without executing any logic you might have built. You can use this mode best if you're still designing the contours of your form, so you can quickly edit texts, question types and the order of blocks;</li>
  <li><h3 id="with-logic">Test with logic</h3>Click <code>Test with logic</code> - The form will execute in the way your respondents will use your form, including the logic you have built. Click the <code><i class="fas fa-sync"></i></code> icon at the top of the preview to start the test form from the beginning.</li>
</ul>
<p>Please make sure that if you want to test your logic, the preview mode is set to <code>Test with logic</code>.</p>

<h2 id="edit">Quick edit</h2>
<p>From the preview, you can quickly jump to a specific block in the form builder to start editing that block, so you don't have to search inside your form structure to edit a specific question block.</p>
<p>To do so, make sure the preview mode is set to <code>Edit</code> mode and then click the title of the desired block inside the preview to open it in the form builder. You can now edit that block immediately and of course see those changes in the preview right away.</p>

<h2 id="device" data-anchor="Device previews">Switch device preview</h2>
<p>Of course, all forms are designed in a responsive way for optimal usage on all modern devices. In the top bar, you can switch the preview to different device sizes, to see how the form looks at each device type:</p>
<ul>
  <li><strong>Mobile</strong> - Click the <code><i class="fas fa-mobile-alt"></i></code> icon;</li>
  <li><strong>Tablet</strong> - Click the <code><i class="fas fa-tablet-alt"></i></code> icon;</li>
  <li><strong>Desktop</strong> - Click the <code><i class="fas fa-desktop"></i></code> icon;</li>
</ul>
<hr />

<h2>More in-depth tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" alt="YouTube logo" />
    </div>
  </a>
</div>
