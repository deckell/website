---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/
title: How to update the WordPress plugin to the latest release - Tripetto Help Center
description: Learn how to update the WordPress plugin when a new version is released.
article_title: How to update the WordPress plugin to the latest release
author: mark
time: 1
category_id: basics
areas: [wordpress]
---
<p>We regularly update the WordPress plugin with new features, improvements and bugfixes. Let us show you how to stay up-to-date with the latest release.</p>

<h2 id="check">Check available updates</h2>
<p>In case one or more plugin updates are available inside your WP Admin, you'll see a badge in the Plugins section. Navigate to your installed plugins to see which plugins can be updated. Click <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Installed Plugins</code>.</p>

<h2 id="update">Get plugin update</h2>
<p>If the Tripetto plugin has an update waiting for you, it will show an update notification in the list of installed plugins. Updating is simple, by clicking <code>Update now</code> inside this update notification.</p>
<p>The update will be executed right away and a confirmation will be shown after completing the update.</p>
