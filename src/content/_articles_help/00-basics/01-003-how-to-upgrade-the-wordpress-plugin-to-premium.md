---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-upgrade-the-wordpress-plugin-to-premium/
title: How to upgrade the WordPress plugin to premium - Tripetto Help Center
description: Learn how to upgrade the WordPress plugin from the free version to the premium version.
article_title: How to upgrade the WordPress plugin to premium
article_folder: wordpress-upgrade
author: jurgen
time: 3
category_id: basics
areas: [wordpress]
---
<p>The Tripetto WordPress plugin is a freemium plugin. This means you can use the free version of our plugin with some limitations. <a href="{{ page.base }}pricing/wordpress/" target="_blank">Upgrading to a premium license</a> gives you access to all premium features. Let's see show how you can upgrade.</p>

<h2>Licenses</h2>
<p>We offer various license to meet your needs with our plugin:</p>
<ul>
  <li><strong>Single-site license</strong> - Usable for one WordPress installation:
    <ul>
      <li><strong>Monthly</strong> - Recurring, billed monthly;</li>
      <li><strong>Annually</strong> - Recurring, billed annually;</li>
      <li><strong>Lifetime</strong> - Billed once.</li>
    </ul>
  </li>
  <li><strong>5-Sites license</strong> - Usable for five WordPress installations:
    <ul>
      <li><strong>Monthly</strong> - Recurring, billed monthly;</li>
      <li><strong>Annually</strong> - Recurring, billed annually;</li>
      <li><strong>Lifetime</strong> - Billed once.</li>
    </ul>
  </li>
  <li><strong>Unlimited sites license</strong> - Usable for unlimited WordPress installations:
    <ul>
      <li><strong>Monthly</strong> - Recurring, billed monthly;</li>
      <li><strong>Annually</strong> - Recurring, billed annually;</li>
      <li><strong>Lifetime</strong> - Billed once.</li>
    </ul>
  </li>
</ul>
<p><a href="{{ page.base }}pricing/wordpress/" target="_blank">More information about our premium upgrades and pricing can be found over here.</a></p>

<h2 id="purchase">Purchase a premium license</h2>
<p>All of the above licenses can be purchased in two ways:</p>

<h3 id="wp_admin">From inside your WP Admin</h3>
<p>Inside your WP Admin navigate to the Tripetto plugin menu<i class="fas fa-arrow-right"></i>Click <code>Upgrade</code><i class="fas fa-arrow-right"></i>Select your preferred plan<i class="fas fa-arrow-right"></i>Follow the steps for checkout.</p>

<h3 id="website">From our website</h3>
<p>On <a href="{{ page.base }}pricing/wordpress/" target="_blank">our pricing page</a> select the right license for you <i class="fas fa-arrow-right"></i>Follow the steps for checkout.</p>

<h2 id="activate">Download, install and activate</h2>
<p>After checkout you'll receive two things to proceed:</p>
<ul>
  <li>A new .zip file with the premium plugin version, which you can download after the checkout. You don't have to unpack the file;</li>
  <li>Your personal license key, which will be sent by email.</li>
</ul>
<p>Now navigate to <code>Plugins</code> inside your WP Admin<i class="fas fa-arrow-right"></i>Click <code>Add New</code><i class="fas fa-arrow-right"></i>Click <code>Upload Plugin</code><i class="fas fa-arrow-right"></i>Select your downloaded file<i class="fas fa-arrow-right"></i>Click <code>Install Now</code><i class="fas fa-arrow-right"></i>Click <code>Activate</code><i class="fas fa-arrow-right"></i>Enter your personal license key.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-upload.png" alt="Screenshot of WordPress plugin upload" />
  <figcaption>Upload the premium plugin.</figcaption>
</figure>

<blockquote>
  <h3 id="data">What happens with your data?</h3>
  <p>The free version of the plugin will be deactivated automatically during the upgrade process.</p>
  <p>Your existing forms and entries in the free version will become availabe in the premium version after completing the upgrade process.</p>
</blockquote>
