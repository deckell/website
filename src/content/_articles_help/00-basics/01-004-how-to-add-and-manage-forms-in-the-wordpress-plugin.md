---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-and-manage-forms-in-the-wordpress-plugin/
title: How to add and manage forms in the WordPress plugin - Tripetto Help Center
description: Learn how to add and manage forms in the WordPress plugin using the forms list.
article_title: How to add and manage forms in the WordPress plugin
article_folder: wordpress-manage-forms
author: jurgen
time: 3
time_video: 0
category_id: basics
areas: [wordpress]
---
<p>You can create an unlimited amount of forms in the WordPress plugin, but how do you keep these forms organized? Let's find out.</p>

<h2 id="create">Create new form</h2>
<p>After you <a href="{{ page.base }}help/articles/how-to-install-the-wordpress-plugin/" target="_blank">installed the WordPress plugin</a>, you can start a new form in a few ways:</p>
<ul>
  <li>Open the Tripetto plugin menu from the WP Admin menu<i class="fas fa-arrow-right"></i>Click <code>Add New</code>;</li>
  <li>Open the Tripetto Dashboard inside the plugin<i class="fas fa-arrow-right"></i>Click <code>Create New Form</code>;</li>
  <li>Open the list of your Tripetto forms inside the plugin<i class="fas fa-arrow-right"></i>Click <code>Create New Form</code>.</li>
</ul>
<p>You can create an unlimited amount of forms in the WordPress plugin.</p>

<h2 id="manage">Manage your forms</h2>
<p>To manage your forms go to <code>All Forms</code>. This shows a simple list of all Tripetto forms in your WP Admin, including the amount of entries and and the shortcode for each form.</p>

<h3 id="actions">Form actions</h3>
<p>By moving your cursor over each row in the list, you can execute the following actions.</p>
<p>Underneath the name of each form in the list:</p>
<ul>
  <li><code>Edit</code> - Edit the form in the form builder;</li>
  <li><code>Run</code> - Open the form with the <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/" target="_blank">shareable link</a>;</li>
  <li><code>Results</code> - Open the list of entries to see and <a href="{{ page.base }}help/articles/how-to-get-your-results-from-the-wordpress-plugin/" target="_blank">download the results</a>;</li>
  <li><code>Duplicate</code> - Create an exact duplicate of the form structure in one click;</li>
  <li><code>Delete</code> - Delete the form, after confirmation. To prevent data loss, you can only delete forms that have no entries left in it. So please first <a href="{{ page.base }}help/articles/how-to-get-your-results-from-the-wordpress-plugin/" target="_blank">delete all the results</a> of the form, before deleting the form itself.</li>
</ul>
<p>Underneath the shortcode of each form in the list:</p>
<ul>
  <li><code>Copy to clipboard</code> - Copy the full shortcode to your clipboard, so you can use it anywhere in your WP site;</li>
  <li><code>Customize</code> - Open the <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site/" target="_blank">shortcode editor</a> to modify the parameters of your shortcode.</li>
</ul>

<h3 id="form-name">Form names</h3>
<p>To keep track of your forms, it's recommended to give each form a good name, so you can find it back later on. To set the name of the form use the following options:</p>
<ul>
  <li>Open the form in the form builder<i class="fas fa-arrow-right"></i>Click the name of the form in the top left corner (<code>Unnamed form</code> by default)<i class="fas fa-arrow-right"></i>Name your form;</li>
  <li>Open the form in the form builder<i class="fas fa-arrow-right"></i>Click <code>Customize</code><i class="fas fa-arrow-right"></i><code>Properties</code><i class="fas fa-arrow-right"></i>Name your form.</li>
</ul>
