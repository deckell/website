---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-form-templates-in-the-studio/
title: How to use form templates in the studio - Tripetto Help Center
description: Learn how to share and use form templates in the studio.
article_title: How to use form templates in the studio
article_folder: studio-templates
author: martin
time: 2
time_video: 0
category_id: basics
areas: [studio]
---
<p>In the studio you can share your form stuctures with other users, so you can collaborate on the building process. This works with form templates and we'd like to show you how this works.</p>

<h2 id="share">Share as template</h2>
<p>To share a form structure with someone else, click the <code><i class="fas fa-bars"></i></code> icon on the top left<i class="fas fa-arrow-right"></i>Click <code>Share as template</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-menu.png" alt="Screenshot of template link in Tripetto" />
  <figcaption>Start sharing a template.</figcaption>
</figure>

<p>A new pane will show up on the right side of the form builder. Over there you'll find the URL to share this form as a template. You can send this template link to the desired recipient(s), for example via email or Slack.</p>

<h2 id="use">Use a template</h2>
<p>When you received a template link, just click it and the studio will open the form structure of the source form right away. You can save this template as a form in your own account by clicking <code>Use this template</code> in the top menu bar. The form will then be saved to your account in your default workspace/collection.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-use.png" alt="Screenshot of template usage in Tripetto" />
  <figcaption>Start using a template.</figcaption>
</figure>

<blockquote>
  <p>Bear in mind that a template is only an exact duplicate of a source form. There's no connection between the source form and the new form you saved in your own account using the template link.</p>
</blockquote>
<hr />

<h2 id="examples" data-anchor="Examples">Use our example templates</h2>
<p>We created some <a href="{{ page.base }}examples/" target="_blank">examples of templates</a> you can use to kickstart your form building process. They show various implementations of logic and styling that you can use right away. You can even use any of these example in the studio right away by clicking the template link.</p>
<div>
  <a href="{{ page.base }}examples/" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto form examples<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Test our example templates and use them as a template right away.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
