---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-create-an-account-in-the-studio/
title: How to create an account in the studio - Tripetto Help Center
description: Learn how to create an account in the studio and log in with our magic link.
article_title: How to create an account in the studio
article_folder: studio-create-account
author: martijn
time: 2
time_video: 0
category_id: basics
areas: [studio]
---
<p>You can start creating in the studio right away, even without an account. But sooner or later, you'll come to a point you need an account to be able to run your form. And that's where some little magic comes in.</p>

<h2 id="account">Your account</h2>
<p>Let's say, you're working on your first form in the studio, without ever having thought about creating an account. At some point, your form is ready and you want to share it with your audience. That's the point you'll need to create your account.</p>

<h3 id="create">Create</h3>
<p>Creating an account is very simple. All you need is an email address; no useless personal information, no passwords; just an email address.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-create.png" alt="Screenshot of the studio of Tripetto" />
  <figcaption>Enter your email address on the right to create your account.</figcaption>
</figure>

<p>After you submitted your email address you'll receive a magic link in your Inbox. By clicking that magic link (which is usable for 15 minutes), you'll get logged in automatically.</p>
<p><strong>And that's it!</strong> Your account is created and ready for use!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-email.png" alt="Screenshot of email" />
  <figcaption>Click the magic link in your email to log in.</figcaption>
</figure>
<p>Now that you're logged in, the form you were building is saved immediately inside your new account. You don't lose anything of the form you were working on.</p>
<p>And from now on, Tripetto keeps you logged in automatically.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-loggedin.png" alt="Screenshot of the studio of Tripetto" />
  <figcaption>Your form is saved immediately inside your new account after your clicked the magic link.</figcaption>
</figure>

<h3 id="logout">Logout</h3>
<p>To log out from the studio you click the <code><i class="fas fa-user-lock"></i></code> icon<i class="fas fa-arrow-right"></i>Click <code>Sign out</code>.</p>

<h3 id="login">Login</h3>
<p>To log back in you just enter the email address associated with your account again. A new magic link will come your way.</p>
<hr />

<h2 id="troubleshooting">Troubleshooting</h2>
<p>Having trouble with the magic link? Let's do some troubleshooting.</p>

<h3 id="not-receiving">Magic link not received</h3>
<p>It can happen you don't see the magic link coming into your email inbox. Please have a look if these actions help:</p>
<ul>
  <li>Check your SPAM folder. Maybe our mail accidently got marked as SPAM;</li>
  <li>Whitelist our sending email address: <code>no-reply@tripetto.com</code>. Whitelisting this address could prevent our mails getting rejected. It depends on your email provider/software how to achieve whitelisting. After you've whitelisted us, please try again to receive a new magic link.</li>
</ul>

<h3 id="not-working">Magic link not working</h3>
<p>If you received the magic link, but it's not working when you click on it, a few things can be happening:</p>
<ul>
  <li>The magic link has expired. Each link is valid for 15 minutes. Request a new magic link in the studio again and use that within 15 minutes;</li>
  <li>The magic link has already been used. Each link can be used 1 time. Request a new magic link in the studio again and use that to login;</li>
  <li>In some exceptional occasions the magic link does not work while you're using it for the first time and within the timeframe. Then probably your email server and/or email client is crawling links inside email messages for security reasons. Without you knowing it, the magic link then has been used by the crawler and becomes unavailable.<br/>In such cases try to bypass your email crawler by receiving your email in a different email client (for example a webmail), or trying to login with a different email address.</li>
</ul>

<h3>Still not working?</h3>
<p>Don't hesitate to <a href="{{ page.base }}contact/" target="_blank">contact us</a> if you have any problems with the magic link. <a href="javascript:$crisp.push(['do', 'chat:open']);">Using the chat</a> will be the fastest and easiest way to do so. We're happy to help you!</p>
