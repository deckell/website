---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-install-the-wordpress-plugin/
title: How to install the WordPress plugin - Tripetto Help Center
description: Learn how to get the WordPress plugin running inside your WP Admin.
article_title: How to install the WordPress plugin
article_folder: wordpress-install
author: mark
time: 2
category_id: basics
areas: [wordpress]
---
<p>The installation of the WordPress plugin follows the standard workflow for plugin installation. Not familiar with this process? We're happy to help you.</p>

<h2 id="activate">Download, install and activate</h2>
<p>We offer two ways to get the plugin running inside your WP Admin.</p>

<h3 id="wp_admin" data-anchor="From WP Admin">From inside your WP Admin</h3>
<p>Inside your WP Admin navigate to <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Add New</code><i class="fas fa-arrow-right"></i>Search by keyword <code>Tripetto</code><i class="fas fa-arrow-right"></i>Click <code>Install Now</code> within the search result<i class="fas fa-arrow-right"></i>Click <code>Activate</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-plugins.png" alt="Screenshot of WordPress plugins" />
  <figcaption>Install plugin from inside your WP Admin.</figcaption>
</figure>

<h3 id="wp_repository" data-anchor="From WP repository">From the WordPress.org plugin repository</h3>
<p>Download the plugin via the <a href="{{ site.url_wordpress_plugin }}" target="_blank">WordPress.org plugin repository</a>. You'll receive a .zip file, which you'll need in the next steps. You don't have to unpack the file.</p>
<p>Now use the downloaded file to install and activate it. Inside your WP Admin navigate to <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Add New</code><i class="fas fa-arrow-right"></i>Click <code>Upload Plugin</code><i class="fas fa-arrow-right"></i>Select your downloaded file<i class="fas fa-arrow-right"></i>Click <code>Install Now</code><i class="fas fa-arrow-right"></i>Click <code>Activate</code>.</p>
<div>
  <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto - WordPress.org plugin repository<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A full WordPress form plugin for smart forms and surveys. Use Tripetto to do it all just a bit better, without a single line of code. All inside your own WP Admin. No third-party account needed, not even a Tripetto account. GDPR proof.</span>
      <span class="url">wordpress.org</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-wordpress.svg" alt="WordPress logo" />
    </div>
  </a>
</div>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-upload.png" alt="Screenshot of WordPress plugin upload" />
  <figcaption>Upload plugin from the WordPress.org plugin repository.</figcaption>
</figure>

<h2 id="start">Start using the plugin</h2>
<p>After activating the Tripetto plugin is shown in the WP Admin menu on the left. Navigate to the <code>Tripetto</code> section in this menu.</p>
<p>Click the <code>Add New</code> button to start building your first form. Read <a href="{{ page.base }}help/articles/how-to-add-and-manage-forms-in-the-wordpress-plugin/" target="_blank">this article</a> for more information on starting your first form.</p>
