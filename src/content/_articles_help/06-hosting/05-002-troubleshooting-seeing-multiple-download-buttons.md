---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-seeing-multiple-download-buttons/
title: Troubleshooting - Seeing multiple download buttons - Tripetto Help Center
description: In some situations you can get to see multiple downloads buttons in your results. Let us explain what this means.
article_title: Troubleshooting - Seeing multiple download buttons
author: jurgen
time: 3
category_id: hosting
subcategory: hosting_results
areas: [studio, wordpress]
redirect_from:
- /help/articles/troubleshooting-seeing-multiple-download-buttons-in-the-studio/
---
<p>In some situations you can get to see multiple downloads buttons in your list of results. Let us explain what this means.</p>

<blockquote>
  <h4>Important notice</h4>
  <p>Due to an improvement in our anti-spam system deployed on September 30<sup>th</sup>, 2020, you may see multiple downloadable exports while you didn't change your form recently. It is a one-off drawback affecting a limited number of accounts and has a technical reason.</p>
  <p>Please see the article below for an explanation on how to merge your exports into one file.</p>
</blockquote>

<h2 id="about">Why does this happen?</h2>
<p>In general, this happens when you have gained results with different data fieldsets. The structure and question blocks of your form determine the data fieldset that is required to store all given answers of your respondents.</p>
<p>As long as your data fieldset doesn't change, all results will be available in one single download file. But if you already gathered some responses and then make a change in the form structure and/or question blocks, the data fieldset might change due to those form changes. For example if you add or delete question blocks. Or if you change the question type from a single selection field to a multiple selection field.</p>
<p>Such form changes also have their effect on the fieldset, causing a conflict in the column layout of different responses. That's the reason newly gathered responses can't be combined with the earlier gathered responses.</p>
<p>Tripetto will bundle all responses with the same data fieldset together in a download. So if you see multiple download buttons, this means each download file will have its own data fieldset, with all results that are gathered for that particular fieldset.</p>

<h2 id="merge">Merge multiple exports</h2>
<p>If you're in a situation where you have multiple CSV files, you may want to merge them into one file so you can accumulate all results and then process them the way you want. We can't really help you to automate this, but by taking the following steps you can manually merge all data:</p>
<ol>
  <li>Download all exports and open them in a spreadsheet editor like Microsoft Excel or Google Sheets;</li>
  <li>Convert all comma-separated values (CSV) to columns. How to do that, depends on the spreadsheet editor you are using (also see <a href="{{ page.base }}help/articles/how-to-use-your-csv-file-in-office-excel-or-google-sheets/" target="_blank">this article</a>). This will result in different column layouts for the different files;</li>
  <li>Now investigate the differences and for each difference determine what version you want to use in your final data fieldset;</li>
  <li>You can create a new file to assembly the final data fieldset, resulting in the column headers you eventually want to use;</li>
  <li>Then manually copy and paste the data from the different exports to the new file.</li>
  <li>Save the new file.</li>
</ol>
