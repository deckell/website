---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-take-control-over-your-data-from-the-studio/
title: How to take control over your data from the studio - Tripetto Help Center
description: Data control is important to us (and maybe to you too). With Tripetto you can take full control over your data, so no data will be stored at our end. It's up to you!
article_title: How to take control over your data from the studio
article_id: data-studio
article_folder: studio-embed
author: mark
time: 4
category_id: hosting
subcategory: hosting_data_control
areas: [studio]
---
<p>Data control is important to us (and maybe to you too). With Tripetto you can take full control over your data, so no data will be stored at our end. It's up to you!</p>

<h2 id="about">The importance of data control</h2>
<p>We wrote about <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/" target="_blank">the importance of data control earlier in our blog</a>. That blog article mainly focusses on our Developer Kit which you can use to fully integrate all Tripetto components.</p>
<p>But also when you're using our studio, you can take advantage of Tripetto's unique data control options. In that way you can manage and build your forms in the studio, but as soon as something has to be done with data, you can overrule the studio.</p>
<p>Let's assume you've created a form in the form builder at tripetto.app and you're now ready to share it with your audience. But in this case you don't want any data to be stored at Tripetto and take control over that data.</p>

<h2 id="embed">Embed the form</h2>
<p>You can only take control over your data when you <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/" target="_blank">embed the form</a> into your own website/project. To do so we start by setting the share method to embed. At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there choose for method <code>Embed in a website or application</code> and you will see the embed code at the bottom of the Share pane. At this point all data will still be stored at Tripetto's endpoints.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-embed.png" alt="Screenshot of embedding in Tripetto" />
  <figcaption>Choose for embedding your form.</figcaption>
</figure>

<h2 id="take-control">Now take over your data</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-options.png" alt="Screenshot of hosting options in Tripetto" />
  <figcaption>Take control over the hosting options.</figcaption>
</figure>
<p>At the moment you select to embed the form, a few extra options pop up regarding <code>Hosting options</code>. Over there you can choose what data you'd like to cut loose from the Tripetto infrastructure. We'll cover all options one by one down here.</p>

<h3 id="self-host-definition">Self-host the form definition</h3>
<p>The form definition is a JSON object containing the structure of your form, including all question blocks, content and logic, but also your styling and label translations.</p>
<p>By default, the form definition of embedded forms gets fetched from your Tripetto account. In that way, you're sure your embedded form always uses the latest version of your form definition. Changes you make in the form definition after you've embedded the form are automatically updated inside your embedded form.</p>
<p>By enabling the setting <code>Self-host the form definition</code>, you can detach the form definition from your Tripetto account and include the form definition inside the embed code. When you toggle this setting, you'll see the embed code changing. Keep in mind that changes you make to your form after you've embedded the self-hosted form definition will not be parsed to your form implementation automatically. You'll have to update the JSON object inside your embed code manually.</p>

<h3 id="self-host-data">Self-host collected data</h3>
<p>By default, all data you collect with your form, will be stored in your Tripetto account. You can <a href="{{ page.base }}help/articles/how-to-get-your-results-from-the-studio/" target="_blank">manage your responses inside the studio and export them to CSV</a>.</p>
<p>By enabling the setting <code>Self-host collected data</code> you can prevent the data from being stored in your Tripetto account. When you toggle this setting you'll see the embed code changing. An <code>onSubmit</code> event will be inserted inside the embed code. Inside that event you can code the desired processing of your response data.</p>
<p>The response data is stored inside the variable <code>instance</code> that's parsed inside the <code>onSubmit</code> event. We've built in some functions to use the response data:</p>
```typescript
onSubmit: function(instance) {
  // TODO: Handle the results
  // For example retrieve the results as a CSV-file:
  var csv = TripettoRunner.Export.CSV(instance);

  // Or retrieve the individual fields:
  var fields = TripettoRunner.Export.fields(instance);

  // Or retrieve the NVPs:
  var NVPs = TripettoRunner.Export.NVPs(instance);
}
```

<h3 id="custom-data-actions">Add custom data actions</h3>
<p>To help you perform custom actions after the form submission, you can use the <code>onComplete</code> event in the embed code. Inside that event you can code the desired custom actions after form completion, for example to send data to Google Analytics.</p>
<p>The response data is stored inside the variable <code>instance</code> that's parsed inside the <code>onComplete</code> event. The <code>onComplete</code> event also parses the server <code>id</code> of the submission.</p>
```typescript
onComplete: function(instance, id) {
  // TODO: Code the custom actions
  // For example log the server id:
  console.log("This submission is saved with id: " + id);

  // Or retrieve the NVPs to use the response data:
  var NVPs = TripettoRunner.Export.NVPs(instance);
}
```

<h3 id="pause-resume">Allow pausing and resuming</h3>
<p>Tripetto forms have a built in pause and resume functionality, as you can read <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/" target="_blank">over here</a>. You can also use this functionality when you embed your form, by simply enabling the setting <code>Allow pausing and resuming</code>. The embed code will update.</p>
<p>It depends on how you host your collected data (see paragraph 'Self-host collected data' up here) what to do next to make this work:</p>
<ul>
  <li>In case collected data is stored at Tripetto: This way you can just sail along with the default process of Tripetto's <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/" target="_blank">pause and resume functionality in shareable links</a>. Please be aware that your respondents receive a resume link to the shareable link at tripetto.app, so not to your embedded form;</li>
  <li>In case collected data is self-hosted: As Tripetto cannot access the data in the form, you have to process the pause and resume requests yourself with the following functions in the embed code:
  </li>
</ul>
```typescript
snapshot: undefined /* Feed snapshot data here */,
onPause: function(snapshot) {
  // TODO: Store the snapshot data somewhere
}
```

<h3 id="save-restore">Save and restore uncompleted forms</h3>
<p>Another option to help your respondents take off where they stopped the form is the save and restore functionality, as you can read <a href="{{ page.base }}help/articles/let-your-form-save-and-restore-uncompleted-entries/" target="_blank">over here</a>. When you toggle the setting <code>Save and restore uncompleted forms</code>, you'll see the embed code changing.</p>
<p>If enabled, this option will save a snapshot of the uncompleted form in the local storage of the browser of the respondent. This is especially handy when you've embedded your form on multiple pages within your site, so people can browse through your website and continue with the form where they left off.</p>
<hr />

<h2>More information</h2>
<p>For more information on self-hosting forms, you can have a look at the <a href="{{ site.url_docs }}/guide/runner/" target="_blank">docs-website of our SDK</a>. The SDK contains more than just self-hosting forms, but it can help you to understand the concept and used techniques.</p>
<div>
  <a href="{{ site.url_docs }}/guide/runner/" target="_blank" class="blocklink">
    <div>
      <span class="title">Runner - Guide - Tripetto Documentation<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Use the Tripetto SDK Documentation to implement the visual form builder inside your own projects. Or customize the form runner to your own needs. This documentation is part of the SDK of Tripetto.</span>
      <span class="url">docs.tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
