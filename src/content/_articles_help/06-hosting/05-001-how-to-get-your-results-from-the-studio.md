---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-get-your-results-from-the-studio/
title: How to get your results from the studio - Tripetto Help Center
description: Learn how you can manage and export your results to CSV to use in Excel or Google Sheets.
article_title: How to get your results from the studio
article_id: download-studio
article_folder: studio-results
author: jurgen
time: 2
category_id: hosting
subcategory: hosting_results
areas: [studio]
---
<p>Learn how you can manage and export your results to CSV so you can use your data for example in Excel or Google Sheets.</p>

<h2 id="manage">Manage your results</h2>
<p>Inside the studio you can see and manage all results of each form. At the top menu bar of the form builder click <code><i class="fas fa-download"></i> Results</code>. The Result pane will show up on the right side of the form builder.</p>

<h3 id="view">View results</h3>
<p>This will show a list of all results you received for the particular form. You can click each result to view the data of the result right away.</p>
<p>If you have one or multiple File upload blocks in your form, the uploaded files from your respondents are available as individual download links in the result view.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/view.png" alt="Screenshot of results in Tripetto" />
  <figcaption>View list of entries and the details of each response.</figcaption>
</figure>

<h3 id="delete">Delete results</h3>
<p>From the list of results, you can delete one or multiple result(s) that you don't longer need. By clicking the <code><i class="far fa-trash-alt"></i></code> icon one (without selecting a result row) the result list will become multiselectable, so you can select the rows you want to delete. After the selection, click <code><i class="far fa-trash-alt"></i> Delete</code> to permanently delete those results (after confirmation).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/delete.png" alt="Screenshot of results in Tripetto" />
  <figcaption>Select multiple responses to delete them.</figcaption>
</figure>

<h2 id="export">Export your results</h2>
<p>Exporting your results is very easy. From the top of the Results pane, just click <code><i class="fas fa-download"></i> Download</code><i class="fas fa-arrow-right"></i>Select right download version (also see chapter 'Seeing multiple download buttons?'). The download of your results will start immediately and will be downloaded as a CSV file to your Downloads folder.</p>
<p>If you have one or multiple File upload blocks in your form, the uploaded files from your respondents are available as individual download links in the CSV export.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/download.png" alt="Screenshot of results in Tripetto" />
  <figcaption>Click the download button to export your results to CSV.</figcaption>
</figure>

<blockquote>
  <h3 id="multiple">Seeing multiple download buttons?</h3>
  <p>It can be possible you see multiple download buttons, grouped on date. Most probably this means you have results of different form definitions. This can occur when you change the structure of your form, resulting in a changed fieldset of your form data. Read <a href="{{ page.base }}help/articles/troubleshooting-seeing-multiple-download-buttons/" target="_blank">this article</a> for more information on that.</p>
</blockquote>

<h2 id="use">Use your results</h2>
<p>If you have downloaded the CSV file you can do what you'd like with the data. For example, open the CSV file in a spreadsheet editor, like Microsoft Excel or Google Sheets. In there, you can convert the comma-separated values (CSV) to columns. More information on how to do that can be found in <a href="{{ page.base }}help/articles/how-to-use-your-csv-file-in-office-excel-or-google-sheets/" target="_blank">this article</a>.</p>
<p>After that, you can process your data, for example, by calculating averages or displaying the data in graphs. How to do that, depends on the spreadsheet editor you are using.</p>
<p>Another option to use your results is by connecting your form with a webhook to automate all kinds of actions. More information on how to do that can be found in <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/" target="_blank">this article</a>.</p>

<h2 id="notifications">Get notifications by email and Slack</h2>
<p>You can also get notifications of new responses by email and/or in Slack. You can include all form data directly in those notifications. Please see the following articles for more information:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/" target="_blank">How to get email notifications for each new result</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/" target="_blank">How to get Slack notifications for each new result</a>.</li>
</ul>
