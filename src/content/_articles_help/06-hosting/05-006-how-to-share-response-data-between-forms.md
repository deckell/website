---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-share-response-data-between-forms/
title: How to share response data between forms - Tripetto Help Center
description: With some smart usage of the features of Tripetto you can connect multiple Tripetto forms to each other and share response data between them.
article_title: How to share response data between forms
article_folder: connected-forms
author: jurgen
time: 4
category_id: hosting
subcategory: hosting_data_exchange
areas: [studio, wordpress]
---
<p>With some smart usage of the features of Tripetto you can connect multiple Tripetto forms to each other and share response data between them.</p>

<h2 id="concept">The concept</h2>
<p>Imagine you've got two separate forms. In the first form you've collected some data, for example the name of your respondent. Now you want to use that name from the first form in the second form, so your respondent doesn't have to enter this again.</p>

<h3 id="preparation">Preparation</h3>
<p>First make sure you have created both forms, so you know the URL of your second form that you want to redirect to.</p>

<h3 id="form-1">Form 1 - Send data with a redirect URL</h3>
<p>In the first form (that already contains the name question), we have to make sure the data is sent to the second form. To do so, we enable <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/" target="_blank">the redirect at the end of the form</a> and insert the URL of the second form to redirect to.</p>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-redirect.gif" alt="Screenshot of redirect in Tripetto" />
  <figcaption>Set up the query string in the redirect URL.</figcaption>
</figure>
<p>Now we're going to extend that URL with <a href="https://en.wikipedia.org/wiki/Query_string" target="_blank">a query string</a> to pass the desired data to. You can determine the name of the parameter yourself. In this case we will use <code>firstname</code> as parameter name. Now we attach the answer of the name question to it, by typing the <code>@</code> sign and select the right question (<a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">more information on piping logic over here</a>).</p>
<blockquote>
  <h4>About query strings</h4>
  <p>A query string is a somewhat technical tool to send data in a URL. Some background information on how to supply a query string:</p>
  <ul>
    <li>You can add multiple parameters to a query string;</li>
    <li>Each parameter consists of a name and a value, separated by a <code> = </code> sign, for example <code>name=value</code>;</li>
    <li>The first parameter is preceded by a <code> ? </code> sign;</li>
    <li>Any following parameters are preceded by a <code> & </code> sign;</li>
  </ul>
  <p>A full example could be something like this: <code>https://yoursite.com/?name=abc&city=klm&country=xyz</code>.</p>
</blockquote>

<h3 id="form-2">Form 2 - Receive data with a hidden field block</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-hidden-field.gif" alt="Screenshot of hidden field in Tripetto" />
  <figcaption>Set up the hidden field.</figcaption>
</figure>
<p>Now over to the second form. Over there we need to receive the data that is sent by the first form through the query string.</p>
<p>To do so, we add a <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/" target="_blank">hidden field block</a> to the second form and set that up to read the query string. And in this case we want to read the value of a certain parameter inside the query string, so we enable the feature <code>Parameter</code> at the left and then enter the name of our parameter, in this case <code>firstname</code>.</p>
<p>Now the hidden field block will be filled automatically at the moment the form is opened. In our case that moment is when the first form redirects to the second form, including the query string.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-forms.gif" alt="Screenshot of hidden field in Tripetto" />
  <figcaption>Example of the first name shared between two forms.</figcaption>
</figure>
<hr />

<h2 id="connect">Connect responses to each other</h2>
<p>The responses of your two forms remain separated from each other. But you can do a little trick to be able to manually connect two responses of the same person from different forms. That uses the same concept as above, but with a different parameter.</p>
<p>Each form entry gets a unique identification number when it's saved. In the first form you can use that <code>Identification number</code> as piping value in the redirect URL. That sends the unique identification number of the response of that first form to the second form.</p>
<p>By saving that identification number in a hidden field in the entry of the second form, you have saved a reference to the corresponding entry of the first form.</p>
<p>In the CSV exports you make of both forms, you can now connect the two separate entries to each other, as the identification number of the first form is also saved to the entry of the second form.</p>
