---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-your-csv-file-in-office-excel-or-google-sheets/
title: How to use your CSV file in Office Excel or Google Sheets - Tripetto Help Center
description: Learn how you can use the downloaded CSV file of your results in Office Excel or Google Sheets.
article_title: How to use your CSV file in Office Excel or Google Sheets
article_id: download-csv
article_folder: csv
author: jurgen
time: 2
category_id: hosting
subcategory: hosting_results
areas: [studio, wordpress]
popular: true
redirect_from:
- /help/articles/how-to-use-csv-from-the-studio-in-office-excel-or-google-sheets/
- /help/articles/how-to-use-csv-from-the-wordpress-plugin-in-office-excel-or-google-sheets/
---
<p>Learn how you can use the downloaded CSV file of your results in Office Excel or Google Sheets.</p>

<h2 id="download">Download results to CSV</h2>
<p>First, download the results of your form to CSV using the download button(s) in the Results pane in Tripetto.</p>

<h2 id="editors">Open CSV file</h2>
<p>Now that you've got the CSV file, you can open this in a so called spreadsheet editor. The most common used programs are Microsoft Office Excel and Google Sheets.</p>
<p>We will explain for those two programs how to open the CSV export, so your data gets readable, understandable and usable for further analysis.</p>

<h3 id="office-excel">With Office Excel</h3>
<p>If your data isn't separated in Excel automatically, you can try the following:</p>
<ol>
  <li>Download the CSV, but don't open it yet;</li>
  <li>Open a new file in Excel;</li>
  <li>Click <code>Data</code><i class="fas fa-arrow-right"></i><code>From text/CSV</code>;</li>
  <li>Select the CSV file from your disk;</li>
  <li>You'll see an example of the data that gets imported. Make sure the separator is set to <code>Semicolon (;)</code>;</li>
  <li>Click <code>Load</code>.</li>
</ol>
<p>Your data should now be separated in columns matching your form's structure, so you can use it for further analysis in Excel.</p>

<h3 id="google-sheets">With Google Sheets</h3>
<p>If your data isn't separated in Google Sheets automatically, you can try the following:</p>
<ol>
  <li>Download the CSV, but don't open it yet;</li>
  <li>Open a new Sheets file in Google Docs;</li>
  <li>Click <code>File</code><i class="fas fa-arrow-right"></i><code>Import</code>;</li>
  <li>Select the CSV file from your disk;</li>
  <li>You'll see a Settings screen of the import. Make sure the Separator Type is set to <code>Custom</code> and fill out a <code>;</code> (<i>a semicolon sign</i>) in the field behind <code>Custom</code>;</li>
  <li>Click <code>Load</code>.</li>
</ol>
<p>Your data should now be separated in columns matching your form's structure, so you can use it for further analysis in Google Sheets.</p>

<h3 id="others">With other spreadsheet editors</h3>
<p>We cannot discuss all spreadsheet editors, but most of them will have comparable functionality to import and read CSV files.</p>
