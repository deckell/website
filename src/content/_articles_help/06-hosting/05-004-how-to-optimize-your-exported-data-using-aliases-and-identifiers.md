---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels
title: How to optimize your exported data using aliases, identifiers and labels - Tripetto Help Center
description: Learn how to use aliases, identifiers and labels to optimize your exported data for readability and usability.
article_title: How to optimize your exported data using aliases, identifiers and labels
article_folder: editor-alias-identifier
author: jurgen
time: 2
category_id: hosting
subcategory: hosting_data_control
areas: [studio, wordpress]
---
<p>Learn how to use aliases, identifiers and labels to optimize your exported data for readability and usability. These aliases, identifiers and labels will be used in the saved dataset, like CSV exports, email notifications and webhooks.</p>

<h2 id="concept">The concept</h2>
<p>Normally you just enter the question and after that for some question types you can add the available choices. By default, the texts you enter for the question and choices will also be used in your dataset.</p>
<p>But in some cases it's not really handy to see those full labels inside your dataset, or you even want to see other values in your dataset for a selected choice. And that's where the <code>Alias</code>, <code>Identifier</code> and <code>Labels</code> features come in. With those features you can determine what will be used in the dataset instead of the label of the question/choice.</p>

<h3 id="alias" data-anchor="Alias">Set alias for a question</h3>
<p>By enabling the <code>Alias</code> feature inside a question block, you can enter an alias label that will be used in the dataset. This has no effect on the text your respondents see in the form.</p>
<p>For example if you have a rather long question, you can enter a shorter alias to make your CSV export better readable and usable.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/alias.png" alt="Screenshot of alias in Tripetto" />
  <figcaption>Enable the Alias feature to enter an alias label for a question.</figcaption>
</figure>

<h3 id="identifier" data-anchor="Identifier">Set identifier for a choice</h3>
<p>By enabling the <code>Identifier</code> feature inside a choice, you can enter an identifier label that will be used in the dataset when that choice is selected by a respondent. This has no effect on the text your respondents see in the form.</p>
<p>You can use this to shorten long choices in your dataset, but also if you want to use a certain score instead of the label in your dataset.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/identifier.png" alt="Screenshot of identifier in Tripetto" />
  <figcaption>Enable the Identifier feature to enter an identifier label for a choice, in this case a certain value.</figcaption>
</figure>

<h3 id="labels" data-anchor="Labels">Set labels for choices</h3>
<p>By enabling the <code>Labels</code> feature inside a multiple choice questions or inside a specific choice, you can enter the labels that are used in the dataset for selected and not-selected options. This has no effect on the text your respondents see in the form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/labels.png" alt="Screenshot of labels in Tripetto" />
  <figcaption>Enable the Labels feature to overwrite the default selected and not-selected dataset labels.</figcaption>
</figure>

<h2 id="result">The result</h2>
<p>Those values will now be used in all places where the system uses the dataset, like the overview of your results, CSV exports, email notifications and webhook integrations.</p>
<p>For example if we have a look at a CSV export now, you'll see the CSV export uses the alias, identifiers and labels that we set in the above examples (see column C).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/result.png" alt="Screenshot of CSV export in Excel" />
  <figcaption>The labels and identifiers used in a CSV export (see column C-E).</figcaption>
</figure>
