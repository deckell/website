---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-determine-what-data-fields-get-saved/
title: How to determine what data fields get saved - Tripetto Help Center
description: Learn how you can determine what data fields from your form should be included or excluded from your exports, notifications and webhooks.
article_title: How to determine what data fields get saved
article_folder: editor-exportability
author: jurgen
time: 2
category_id: hosting
subcategory: hosting_data_control
areas: [studio, wordpress]
---
<p>Learn how you can determine what data fields from your form should be included or excluded from your exports, notifications and webhooks.</p>

<h2 id="concept">The concept</h2>
<p>By default, the data of each question block inside your form gets saved to the dataset at the moment a respondent completes the form (exceptions are the <a href="{{ page.base }}help/articles/how-to-use-the-password-block/" target="_blank">password block</a> and the <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank">send email block</a>). In most cases that will do the trick, as the main goal mostly is to collect data.</p>
<p>But we can imagine you also can have some use cases that you don't want to save a certain data field to your collected dataset. And that's where the <code>Exportability</code> feature comes in. With that feature you can determine if the collected data of each question block should be saved to the dataset (or not).</p>
<p>Please be aware that when you disable the saving of certain data fields, that data literally doesn't get saved. So it no longer can be used anywhere, for example not in exports to CSV, not in notifications to Slack/email and not in automations.</p>

<h2 id="exportability">Determine exportability</h2>
<p>By enabling the <code>Exportability</code> feature inside a question block, you can choose if the data of that block should be saved. Disable <code>Included in the dataset</code> to don't save the collected data of the selected question block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-settings.png" alt="Screenshot of exportability in Tripetto" />
  <figcaption>Enable the Exportability feature and don't include the data in the dataset.</figcaption>
</figure>
