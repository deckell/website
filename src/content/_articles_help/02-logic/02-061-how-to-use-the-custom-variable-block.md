---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-custom-variable-block/
title: How to use the custom variable block - Tripetto Help Center
description: Use the custom variable block for more advanced use cases where you want to store a certain value (text, number, date or boolean) in a variable and use that throughout your form.
article_title: How to use the custom variable block
article_id: action-blocks
article_folder: editor-block-custom-variable
author: mark
time: 5
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
---
<p>Use the custom variable block for more advanced use cases where you want to store a certain value (text, number, date or boolean) in a variable and use that throughout your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>The custom variable block is an advanced action block that's primarily designed for no-code purposes. You can use it to store different types of values and use those values inside your form.</p>
<p>In combination with other blocks (like the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">calculator block</a> and the <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/" target="_blank">set value</a> block) and all logic features in Tripetto it enables you to do all kinds of no-code actions, for example:</p>
<ul>
  <li>Store a string value and use that in several places in your form;</li>
  <li>Store a numeric value and increase the value of it when a certain answer is given;</li>
  <li>Store a date value to check if that date is in the future in several places in your form;</li>
  <li>Store a boolean value and determine the closing message based on the boolean value.</li>
</ul>

<h2 id="how-to-use">How to use</h2>
<p>The custom variable block is available as a question type. Add a new block to your form and then select the question type <code>Custom variable</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The custom variable block is available from the Question type menu.</figcaption>
</figure>

<h3 id="basic-features">Basic features</h3>
<p>You can now change the features of the block to get the desired behavior:</p>
<ul>
  <li>
    <h4>Name</h4>
    <p>The name of the variable in your form (not visible for respondents).</p>
  </li>
  <li>
    <h4>Type of variable</h4>
    <p>This determines the type of data that you want to store in the variable. The following types are available:</p>
    <ul>
      <li>A textual value;</li>
      <li>A numeric value;</li>
      <li>A date (and time) value;</li>
      <li>A boolean value (<code>true/false</code>).</li>
    </ul>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/settings.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a custom variable to store a numeric value.</figcaption>
</figure>

<h3 id="additional-features">Additional features</h3>
<p>Depending on the selected type of variable, additional features and settings can become activated to help you setup the variable. These features will popup on the left side of the pane. You can enable and configure these to your own needs.</p>

<h3 id="prefill-feature">Prefill feature</h3>
<p>One feature that all variable types have in common is the <code>Prefill</code> feature. By enabling this feature, you can set the value that the variable should start with.</p>
<hr/>

<h2 id="piping-logic">Show variables</h2>
<p>You can show the value of the variable to your respondents, by using our piping logic. This works the same way as you can use 'normal' question blocks as piping values, by typing the <code>@</code> sign at the desired position and select the custom variable (<a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">more information about piping logic</a>).</p>
<hr/>

<h2 id="branch-logic">Branch logic</h2>
<p>You can also use the value of the variable as branch conditions, so the form can take decisions based on the variable. This works the same way as you can add logic based on 'normal' question blocks, using branch logic (<a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">more information about branch logic</a>).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/logic.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Use a variable to execute logic.</figcaption>
</figure>

<p>Due to the flexibility of the custom variable block, the value of it can contain several types. It depends on the type of value that is saved inside the variable, which <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> you can create with it. Below you see the condition options per variable type.</p>
<h3 id="text-conditions">Text conditions</h3>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="number-conditions">Number conditions</h3>
<ul>
  <li>Number is equal to <code>your filter</code>;</li>
  <li>Number is not equal to <code>your filter</code>;</li>
  <li>Number is lower than<code>your filter</code>;</li>
  <li>Number is higher than <code>your filter</code>;</li>
  <li>Number is between <code>your filters</code>;</li>
  <li>Number is not between <code>your filters</code>;</li>
  <li>Number is empty;</li>
  <li>Number is not empty.</li>
</ul>
<h3 id="date-conditions">Date conditions</h3>
<ul>
  <li>Date is equal to <code>your filter</code>;</li>
  <li>Date is not equal to <code>your filter</code>;</li>
  <li>Date is before <code>your filter</code>;</li>
  <li>Date is after <code>your filter</code>;</li>
  <li>Date is between <code>your filters</code>;</li>
  <li>Date is not between <code>your filters</code>;</li>
  <li>Date is empty;</li>
  <li>Date is not empty.</li>
</ul>
<h3 id="boolean-conditions">Boolean conditions</h3>
<ul>
  <li>Value is true;</li>
  <li>Value is not false;</li>
  <li>Value is equal to <code>your filter</code>;</li>
  <li>Value is not equal to <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Fixed value - Compare with a fixed value that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />
{% include help-article-blocks.html %}
