---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-send-a-confirmation-mail-to-your-respondents/
title: How to send a confirmation mail to your respondents - Tripetto Help Center
description: You can use the send email block to send automatic email messages to your respondents, for example for a confirmation mail or a thank you mail.
article_title: How to send a confirmation mail to your respondents
article_folder: editor-block-send-email
author: martijn
time: 3
category_id: logic
subcategory: logic_email_actions
areas: [studio, wordpress]
---
<p>You can use the send email block to send automatic email messages to your respondents, for example for a confirmation mail or a thank you mail.</p>

<h2 id="when-to-use">When to use</h2>
<p>If you want to confirm the successful receipt and/or thank your respondents for their submission, you can send a confirmation mail to them. To do so you can use the email address that they entered inside the form as the recipient. And you can totally determine the subject and content of the email.</p>
<blockquote>
  <h4>Having email troubles in WordPress?</h4>
  <p>If you're using our WordPress plugin, the sending of emails is handled by your own WordPress installation. <a href="{{ page.base }}help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/" target="_blank">Click here for troubleshooting if your WordPress installation is not sending emails.</a></p>
</blockquote>

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to send a confirmation mail to your respondents. We use the send email block for that. For global instructions about the send email block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank">this article</a>.</p>

<h3 id="add-blocks">Add form blocks</h3>
<p>Start with adding a question block where your respondents can enter their email address, for example an <a href="{{ page.base }}help/articles/how-to-use-the-email-address-block/" target="_blank">email address block</a>. We will use the given answers to this question as the recepient for the confirmation mail. Make sure this block is entered <i>before</i> the send email block.</p>
<p>Continue with adding a <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank">send email block</a> in the right position in your form. You can also place send email blocks in branches, so they only will be sent when they meet your branch conditions. In this example we will only send a confirmation mail when the respondent opts in for that.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/setup.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our form setup to send a confirmation mail.</figcaption>
</figure>

<h3 id="recipient">Select recipient</h3>
<p>Now open the send email block. First thing you will see is the <code>Recipient</code> feature. That determines to whom the email message will be sent. Tripetto will list the question blocks that you can use as the recipient.</p>
<p>In this case we select the question from our form where we ask for the email address of the respondent. And that's it! The send email block will now use the email address that the respondent enters as the recipient.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/recipient.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Select the entered value as the recipient.</figcaption>
</figure>

<h3 id="message">Enter email message</h3>
<p>Now you can enter the rest of the email message:</p>
<ul>
  <li>Enter <code>Subject</code> - The subject line of the email;</li>
  <li>Enter <code>Message</code> - The actual content of the email message;</li>
  <li>Enable <code>Form data</code> (optionally) - Option to include all form data.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/message.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our configuration for the send email block.</figcaption>
</figure>
