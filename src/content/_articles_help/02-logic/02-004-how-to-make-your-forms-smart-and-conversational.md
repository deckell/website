---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-make-your-forms-smart-and-conversational/
title: How to make your forms smart and conversational - Tripetto Help Center
description: Learn how you can create smart forms that feel like conversations, using different kinds of logic.
article_title: How to make your forms smart and conversational
author: martijn
time: 3
category_id: logic
subcategory: logic_basics
areas: [studio, wordpress]
---
<p>We mention them all the time: <strong>smart forms</strong>. But what do we mean with smart forms and how can you create them? In this article we explain it and give some tips on how to achieve it.</p>

<h2 id="smart-forms" data-anchor="Smart forms">Why would I want a smart form?</h2>
<p>The problem with traditional forms is the fact that they always are the same for all respondents. Often it's just an endless list of boring questions. The risk of losing your respondents to boredom and annoyance is high, resulting in a low completion rate of your form.</p>

<h2 id="conversation" data-anchor="Logic">Let it feel like a conversation</h2>
<p>The overall goal of a smart form is making it more fun and effective to fill out. You can achieve this by letting the form feel like a real conversation. This means you have to make it personal and 'listen' and 'react' to the answers your respondents give.</p>

<h3 id="listen" data-anchor="Branch logic">Listen and react to your respondent</h3>
<p>While your respondent is filling out your form, you're getting smarter with every question. You can use this knowledge to make your forms react to the given answers and then only ask the right questions that apply to the case of each individual respondent. We call this <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank"><strong>branch logic</strong></a>.</p>
<p>For example, if you're having a market research on multiple products, you could just ask all questions for all products. But what if your respondent only used one of the products? In that case it's better to first ask which product(s) the respondent has used and then only ask the follow-up questions for the selected product(s).</p>
<p>That's a very easy example of logic, but Tripetto is built up from the ground to perform the best logic, so there are lots of possibilities to listen and react to your respondents.</p>

<h3 id="piping" data-anchor="Piping logic">Use given answers</h3>
<p>Another tool you can use to make a form more personal is the usage of so called <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank"><strong>piping logic</strong></a>. With piping logic you're able to use given answers in other areas of your form.</p>
<p>For example, you could ask for the first name of your respondent in the first question in an informal way. In the next question, you can now use the entered first name to address the respondent by his own name.</p>

<h3 id="endings" data-anchor="Closing messages">Personalize closing message</h3>
<p>By personalizing the closing message that respondents get to see, you again give them the feeling they have been heard. Tripetto helps you with this by being able to create unlimited amount of <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/" target="_blank"><strong>flexible closing messages</strong></a>, based on the given answers of each respondent.</p>
<hr />

<h2 id="actions" data-anchor="Action blocks">Take action</h2>
<p>Next to the logic features, Tripetto lets the form think for itself and perform actions. We offer multiple <strong>action blocks</strong> that help you to let your form work for you:</p>
<h3 id="calculations" data-anchor="Calculation actions">Perform calculations</h3>
<p>The <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank"><strong>calculator block</strong></a> can perform advanced calculations with given answers, scores, comparisons, functions, constants and much more.</p>
<h3 id="data" data-anchor="Data actions">Set and manipulate form data</h3>
<p>The <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/" target="_blank"><strong>hidden field block</strong></a> can help you to gather and save hidden data in your form.</p>
<p>The <a href="{{ page.base }}help/articles/how-to-use-the-custom-variable-block/" target="_blank"><strong>custom variable block</strong></a> lets you store a hidden value inside your form, for example a text, number, date or boolean value.</p>
<p>The <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/" target="_blank"><strong>set value block</strong></a> can change values and variables inside your form, for example to prefill values or to lock certain entered values.</p>
<h3 id="email" data-anchor="Email actions">Send emails to yourself or your respondents</h3>
<p>The <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank"><strong>send email block</strong></a> enables you to send emails right from your form, even directly to your respondent.</p>
<h3 id="stop" data-anchor="Stop actions">Prevent certain form submissions</h3>
<p>The <a href="{{ page.base }}help/articles/how-to-use-the-force-stop-block/" target="_blank"><strong>force stop block</strong></a> and <a href="{{ page.base }}help/articles/how-to-use-the-raise-error-block/" target="_blank"><strong>raise error block</strong></a> let you prevent a form to submit, so you only receive decent results.</p>
<hr />

<h2>More in-depth tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" alt="YouTube logo" />
    </div>
  </a>
</div>
<hr />

<h2>Also read our blog</h2>
<p>We also wrote some blog posts on this subject.</p>
<div>
  <a href="{{ page.base }}blog/forms-dont-need-to-suck/" target="_blank" class="blocklink">
    <div>
      <span class="title">Forms don’t need to suck - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">If you’re building a form or survey without proper logic, you’ll make it painful and risk losing your respondents to boredom and annoyance.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}blog/why-conversational-forms-still-matter/" target="_blank" class="blocklink">
    <div>
      <span class="title">Why Conversational Forms still matter - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">How to build conversational exchanges right.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}blog/five-simple-tips-to-improve-your-online-forms-and-surveys/" target="_blank" class="blocklink">
    <div>
      <span class="title">5 Simple tips to improve your online forms and surveys - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Find out how you can improve your online forms and surveys to boost your conversion or response.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
