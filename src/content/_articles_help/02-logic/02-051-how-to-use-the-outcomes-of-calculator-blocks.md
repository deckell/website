---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-outcomes-of-calculator-blocks/
title: How to use the outcomes of calculator blocks - Tripetto Help Center
description: The outcome of each calculator block can be used in several ways inside your form and your dataset.
article_title: How to use the outcomes of calculator blocks
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: jurgen
time: 3
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>The outcome of each calculator block can be used in several ways inside your form and your dataset.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">the calculator block</a> when you want to perform calculations inside your form. You can use the outcomes of calculator blocks in several ways, for example:</p>
<ul>
  <li>Show the outcome of a calculator block to your respondent;</li>
  <li>Use the outcome of a calculator block as a branch condition;</li>
  <li>Use the outcome of a calculator block as the input for another calculator block;</li>
  <li>Use the outcome of a calculator block in your dataset, like results, downloads, notifications and webhooks.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-quiz.gif" alt="Screenshot of a quiz in Tripetto" />
  <figcaption>Demonstration of showing the quiz score and a different text based on the score.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to use the outcomes of calculator blocks. For global instructions about the calculator block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">this article</a>.</p>

<h3 id="dataset" data-anchor="In dataset">Save outcome in dataset</h3>
<p>By default the outcome of the calculator block will be saved to your dataset. That way the outcome will be available in your results, exports, notifications and webhooks.</p>

<h3 id="piping" data-anchor="In piping logic">Show outcome in form (piping logic)</h3>
<p>You can show the outcome of a calculator block to your respondent in the form in realtime. All calculator blocks are available for piping logic, by typing the <code>@</code> sign at the position you want to show the outcome.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/piping.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Show the outcome of a quiz calculator.</figcaption>
</figure>
<p>Please have a look at <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">this article for more in depth information about piping logic</a>.</p>

<h3 id="calculator-input" data-anchor="As calculator input">Use outcome as input for other calculator block</h3>
<p>The outcome of each calculator block is also available in any calculator block that comes after. So you can use the outcome of the first calculator block as the input to a second calculator block.</p>

<h3 id="branch" data-anchor="In branch logic">Use outcome as logic condition (branch logic)</h3>
<p>You can use the outcome of a calculator block to check if a certain branch applies to your filter for that branch. The following filters are available for this:</p>
<ul>
  <li>When calculation is equal to <code>your filter</code>;</li>
  <li>When calculation is not equal to <code>your filter</code>;</li>
  <li>When calculation is lower than<code>your filter</code>;</li>
  <li>When calculation is higher than <code>your filter</code>;</li>
  <li>When calculation is between <code>your filters</code>;</li>
  <li>When calculation is not between <code>your filters</code>;</li>
  <li>When calculation is valid;</li>
  <li>When calculation is not valid.</li>
</ul>
<h4>Filters</h4>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value supplied in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Use the outcome of a quiz calculator as branch condition.</figcaption>
</figure>
<p>Please have a look at <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">this article for more in depth information about branch logic</a>.</p>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a> (current article).</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
