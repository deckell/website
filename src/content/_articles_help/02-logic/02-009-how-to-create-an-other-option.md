---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-create-an-other-option/
title: How to create an 'Other...' option - Tripetto Help Center
description: Learn how to create an 'Other...' option, for example to use in multiple choice questions.
article_title: How to create an 'Other...' option
article_id: logic-flow
article_folder: editor-branch-other
author: jurgen
time: 2
time_video: 6
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>Learn how to create an 'Other...' option, for example to use in multiple choice questions.</p>

<h2 id="when-to-use">When to use</h2>
<p>A common scenario inside forms is the case of an 'Other...' option as part of a list of options, giving the respondent the possibility to enter a missing option.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-other/demo.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Demonstration of an 'Other...' option.</figcaption>
</figure>
<blockquote>
  <h4>More logic possibilities</h4>
  <p>The logic described in this article is an example of what Tripetto can do to make your forms smart. <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">Discover the power of branches for your logic</a> to see all logic capabilities.</p>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The idea is to use a simple branch based on a match on the 'Other...' option. In case a respondent selects that option, a follow-up question is shown. In most cases this will be a text input field, so the respondent can enter its own answer.</p>

<h3 id="branch">Create branch</h3>
<p>To do so, we have already added a multiple choice question with several options, including one option named 'Other...'.</p>
<p>By clicking the <code><i class="fas fa-ellipsis-h"></i></code> button in the multiple choice block we can instantly create branches per selected option. In this case we select the <code>Other...</code> option. The branch will be created immediately.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-branch.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Creating a branch for an 'Other...' option.</figcaption>
</figure>

<h3 id="follow-up">Add follow-up</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-follow-up.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The follow-up if a respondent selected the 'Other...' option.</figcaption>
</figure>
<p>Now you can add the desired follow-up when a respondent selected the 'Other...' option. In this example we have just added one text field to let our respondent enter the name of their other favorite food.</p>
<p>But you can add anything you need as a follow-up. That includes unlimited questions, but also new branches and other logic types. The possibilities are endless; just how you need it.</p>

<h3 id="tutorial">Video tutorial</h3>
<p>We have made a video tutorial on how to add a branch for an 'Other...' option. It's a tutorial on simple logic, which shows what steps to take to get this done.</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/pb01iB9UhDQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on simple logic.</figcaption>
</figure>
