---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/
title: How to use subcalculations (multistep formulas) in your calculations - Tripetto Help Center
description: The calculator block is able to chain endless subcalculations to perform multistep formulas.
article_title: How to use subcalculations (multistep formulas) in your calculations
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: mark
time: 4
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>The calculator block is able to chain endless subcalculations to perform multistep formulas.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">the calculator block</a> when you want to perform calculations inside your form. If you want to perform advanced calculations that require subcalculations, you can also do this. For example:</p>
<ul>
  <li>First calculate the total price of selected products. And then use the result of that to do another operation;</li>
  <li>Perform a formula, for example to calculate the BMI: weight / (height*height).</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-bmi.gif" alt="Screenshot of a BMI wizard form in Tripetto" />
  <figcaption>Demonstration of a formula with a subcalculation.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to use subcalculations in your calculator blocks. For global instructions about the calculator block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">this article</a>.</p>

<h3 id="add-operation">Add operation</h3>
<p>You can use a subcalculation inside each operation. From the menu to add an operation, you see an item called <code>Subcalculation</code> (if you have an empty form, you will see these menus directly). This adds a subcalculation to your calculator.</p>

<h3 id="setup-function">Setup subcalculation</h3>
<p>Each subcalculation includes ALL features that the calculator block offers, including all operations, scores, comparisons, functions and constants. Even including subcalculations, so you can use infinite and unlimited instances of subcalculations.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/subcalculation.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we use a subcalculation to calculate the subtotal of product A (<code>$10.95/piece</code>).</figcaption>
</figure>

<h3 id="exportability">Exportability</h3>
<p>You can determine if you want the result of the subcalculation to be available for other blocks and in your dataset, for example to show the outcome of the subcalculation to your respondent. The following settings are available:</p>
<ul>
  <li><code>Make answer available for other blocks</code> - Enable this to make the outcome of the subcalculation available to use in other blocks and even in logic branches;</li>
  <li><code>Make answer exportable and include it in the dataset</code> - Enable this to save the outcome of the subcalculation to your dataset, so you can use it in results, exports, notifications and webhooks;</li>
  <li><code>Set an alias for this answer</code> - Set an alias that gets used in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels" target="_blank">more information about aliases over here</a>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/subcalculation-export.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>You can determine if the outcomes of subcalculations are available and exportable.</figcaption>
</figure>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
