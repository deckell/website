---
layout: help-article
base: ../../../
permalink: /help/articles/learn-about-different-types-of-branch-endings-for-your-logic/
title: Learn about different types of branch endings for your logic - Tripetto Help Center
description: You can determine how each branch should behave at the end of the branch. This will affect how the form continues after the branch, for example to skip to another point, the end, or show a custom closing message.
article_title: Learn about different types of branch endings for your logic
article_id: logic-details
author: jurgen
time: 3
time_video: 14
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>You can determine how each branch should behave at the end of the branch. This will affect how the form continues after the branch, for example to skip to another point, the end, or show a custom closing message.</p>

<h2 id="builder">Branches</h2>
<p>In Tripetto you use branches to add the desired logic to your form. In <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">this article we showed how to add branches to your form</a>.</p>
<p>Part of each branch is the <strong>branch ending</strong> that determines what happens at the end of the branch. Let's take a deeper look into that.</p>

<h2 id="types">Branch endings</h2>
<p>At the end of each branch it's possible to set what should happen after a respondent completes the branch.  To do so, you click the green bubble at the bottom of a branch. There are four options:</p>
<ul>
  <li>Continue with the next cluster or branch;</li>
  <li>Jump to a specific point;</li>
  <li>Jump to end;</li>
  <li>End with closing message.</li>
</ul>

<h3 id="next">Continue with the next cluster or branch</h3>
<p>This is the default branch end behavior. It lets the form determine the first logical continuation and show that to the respondent. Depending on your form's structure that could be the next question block, an action block, the end, etc.</p>

<h3 id="jump-point">Jump to a specific point</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-jump/00-jump.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a jump to a specific point.</figcaption>
</figure>
<p>This lets the form jump to a specific cluster inside your form that you select. In this way you can for example skip a few questions and jump to a later point in your form.</p>
<p>Keep in mind you can only jump to clusters that have a name, so before you make the jump, make sure you named the cluster you want to skip to.</p>

<h3 id="jump-end">Jump to end</h3>
<p>This lets the form jump to the very end immediately. This will show the <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/" target="_blank">common closing message</a> (including the <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/" target="_blank">possibility to redirect</a>) to your respondents.</p>

<h3 id="closing-message">End with closing message</h3>
<p>This lets the form end and show a <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/" target="_blank">custom closing message</a> (including the <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/" target="_blank">possibility to redirect)</a> to your respondents that end the form here.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-end/01-custom-end.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a custom closing message.</figcaption>
</figure>
<hr />

<h2 id="tutorials">Video tutorials</h2>
<p>We have made some video tutorials on how to use branches. It shows where to do this in the form builder and what options you have.</p>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/pb01iB9UhDQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on simple logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/DgyYh_xPWuo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on advanced logic.</figcaption>
</figure>
