---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-calculations-with-logic-branches/
title: How to use calculations with logic branches - Tripetto Help Center
description: You can use all calculations in combination with any logic branch in your form. This makes your calculations even more smart and powerful!
article_title: How to use calculations with logic branches
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: mark
time: 3
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>You can use all calculations in combination with any logic branch in your form. This makes your calculations even more smart and powerful!</p>

<h2 id="when-to-use">When to use</h2>
<p>Logic is an important part of forms in Tripetto. Of course the calculator block blends perfectly with the logic branches in your form. This enables you to do all kinds of smart actions with it:</p>
<ul>
  <li>Use calculators in logic branches, so you can only perform a certain calculation based on the matching branch conditions;</li>
  <li>Use calculators in iteraring logic branches, so you can perform one calculator block multiple times for each of the selected items in your branch condition;</li>
  <li>Use the outcome of a calculator as branch condition, so you can determine the follow-up based on the outcome of the calculator;</li>
  <li>Use the outcome of a calculator as branch condition, so you can customize the closing messages based on the outcome of the calculator.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-branch.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of a calculator in an iterating branch.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The calculator block actually works just like any other block in combination with logic, so you can use it in the following logic places:</p>
<ul>
  <li><strong>In logic branches</strong>, so you can perform certain calculator(s) only when your conditions match;</li>
  <li><strong>As branch condition</strong>, so you can determine the follow-up and customize the closing messages based on the outcome of calculator(s).</li>
</ul>

<h3 id="branches">In logic branches</h3>
<p>You can add calculator blocks in any position in your form, just like any other form block. This means you can also place a calculator block inside a branch, so the calculator only gets executed when the respondent enters that branch.</p>
<p>Calculator blocks even work with iterating branches, that can be executed multiple times based on multiple selected items. This way the calculator block will calculate an individual outcome for each time the branch gets executed.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/logic-iterating.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of an iterating branch with a calculator that calculates the subtotal for each of the selected products.</figcaption>
</figure>

<h3 id="condition">As branch condition</h3>
<p>You can use the outcome of each calculator block as input for a branch condition. This way you can take follow-up actions based on the outcome of a calculator.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/logic-condition.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we show different closing messages (red bubbles) for different outcomes of the score calculator.</figcaption>
</figure>
<h4>Possible branch conditions</h4>
<ul>
  <li>Calculation is equal to <code>your filter</code>;</li>
  <li>Calculation is not equal to <code>your filter</code>;</li>
  <li>Calculation is lower than<code>your filter</code>;</li>
  <li>Calculation is higher than <code>your filter</code>;</li>
  <li>Calculation is between <code>your filters</code>;</li>
  <li>Calculation is not between <code>your filters</code>;</li>
  <li>Calculation is valid;</li>
  <li>Calculation is not valid.</li>
</ul>
<h4>Filters</h4>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value supplied in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
