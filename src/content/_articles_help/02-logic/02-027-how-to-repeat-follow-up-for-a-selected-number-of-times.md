---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-repeat-follow-up-for-a-selected-number-of-times/
title: How to repeat follow-up for a selected number of times - Tripetto Help Center
description: Learn how to repeat follow-up questions for the number of times that your respondent has selected.
article_title: How to repeat follow-up for a selected number of times
article_folder: editor-branch-repeat-number
author: jurgen
time: 4
time_video: 8
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>Learn how to repeat follow-up questions for the number of times that your respondent has selected.</p>

<h2 id="when-to-use">When to use</h2>
<p>In some situations you want to repeat the same question(s) a couple of times. In Tripetto it is possible to automatically iterate through a set of follow-up questions for a number of times that your respondent has entered.</p>
<p>A simple example is when you want to ask for the names and dates of birth of someones children. You can ask how many children your respondent has and then ask the follow-up questions (name and date of birth) for the amount of children the respondent has.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of iterating questions for the selected number of times.</figcaption>
</figure>

<h2 id="how-to-use">How to use</h2>
<p>The idea is to first let the respondent enter the amount and then repeat the follow-up question(s) for the entered number of times.</p>
<h3 id="number-block">Add number block</h3>
<p>We start with adding a <a href="{{ page.base }}help/articles/how-to-use-the-number-block/" target="_blank">number block</a> and entering the question we want to ask.</p>
<p>We advice to enable the <code>Limit values</code> feature in the number block and set the maximum number of times you want to repeat the branch. This is important to remember when we create the branch conditions.</p>
<p>And in our example we have set an alias <code>KIDS</code> to the number block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/number-block.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Settings of the number block. In this example we have a maximum of 4.</figcaption>
</figure>

<blockquote>
  <h4>Use other question types</h4>
  <p>Instead of the number question type you can also use other question types to let your respondents enter the amount, for example a <a href="{{ page.base }}help/articles/how-to-use-the-dropdown-block/" target="_blank">dropdown</a>, <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/" target="_blank">multiple choice buttons</a> or <a href="{{ page.base }}help/articles/how-to-use-the-radio-buttons-block/" target="_blank">radio buttons</a>.</p>
  <p>We will explain how to set that up at the end of this article (<a href="#question-types" class="anchor">click here</a>).</p>
</blockquote>

<h3 id="branch">Setup branch</h3>
<p>Now, instead of creating the same follow-up questions multiple times, we will create a branch that we will be repeating. To do so, we add an empty branch by clicking the <code><i class="fas fa-plus"></i></code> diamond at the right side of the cluster.</p>
<p>In the newly created branch, click the branch bubble at the top of it and then select the <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/" target="_blank">branch behavior</a> <code>For each condition match (iteration)</code>. This will make your branch repeatable for each condition that gets matched.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch-behavior.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Add an empty branch and set the branch behavior to <code>For each condition match (iteration)</code>.</figcaption>
</figure>
<p>Next step is to add the <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a>. To do so, we click the <code><i class="fas fa-plus"></i></code> button at the bottom of the branch <i class="fas fa-arrow-right"></i> Select our <code>KIDS</code> question <i class="fas fa-arrow-right"></i> Select the condition <code>Number is higher than</code>.</p>
<p>In our example we add the following branch conditions (corresponding the maximum amount we entered in the number block):</p>
<ul>
  <li><code>KIDS</code> is higher than <code>0</code>;</li>
  <li><code>KIDS</code> is higher than <code>1</code>;</li>
  <li><code>KIDS</code> is higher than <code>2</code>;</li>
  <li><code>KIDS</code> is higher than <code>3</code>;</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch-conditions.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Add the <code>Higher than</code> branch conditions.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The result of the branch.</figcaption>
</figure>

<h3 id="follow-up">Add follow-up</h3>
<p>Now you can add the desired follow-up questions underneath the branch. You can add as many blocks as you'd like and they all will be repeated for the selected number of times.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/result.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The end result of the branch with the follow-up.</figcaption>
</figure>
<blockquote>
  <h4>More logic possibilities</h4>
  <p>The logic described in this article is an example of what Tripetto can do to make your forms smart. <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">Discover the power of branches for your logic</a> to see all logic capabilities.</p>
</blockquote>
<hr/>

<h3 id="tutorials">Video tutorials</h3>
<p>We have made several video tutorials on how to use logic. These tutorials show you how to create a repeating branch.</p>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/DgyYh_xPWuo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on advanced logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>
<hr/>

<h2 id="question-types" data-anchor="Other question types">Use other question types</h2>
<p>In the article above we used the question type 'Number' to let the respondents enter the amount, but you can also use other question types to let your respondents enter that amount, for example a <a href="{{ page.base }}help/articles/how-to-use-the-dropdown-block/" target="_blank">dropdown</a>, <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/" target="_blank">multiple choice buttons</a> or <a href="{{ page.base }}help/articles/how-to-use-the-radio-buttons-block/" target="_blank">radio buttons</a>.</p>
<h3>Add choices and scores</h3>
<p>To do so, add the desired amounts as choices in your 'Amount' question and enable the <code>Score</code> feature. Now you can add a score for each choice, corresponding the amount that each choice represents.</p>
<h3>Use scores in branches</h3>
<p>Now you can use those scores to setup the branch conditions that we described in the above article.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/dropdown-block.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The choices and scores of a dropdown question.</figcaption>
</figure>
