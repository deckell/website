---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-match-a-combination-of-multiple-conditions/
title: How to match a combination of multiple conditions - Tripetto Help Center
description: Learn how you can combine multiple conditions to create perfect matches for your branches.
article_title: How to match a combination of multiple conditions
article_folder: editor-branch-all
author: jurgen
time: 3
time_video: 14
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>Learn how you can combine multiple conditions to create perfect matches for your branches.</p>

<h2 id="when-to-use">When to use</h2>
<p>We've seen a simple example of a follow up that was in a branch with a single condition in <a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">this article</a>. But instead of one condition per branch it is possible to add as many conditions as you need to make the perfect match for your branch.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-all/demo.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of multiple matching conditions.</figcaption>
</figure>

<h2 id="how-to-use">How to use</h2>
<p>For this example we already added a couple of question blocks and created an empty branch.</p>

<h3 id="branch-conditions">Add branch conditions</h3>
<p>In each branch you can add an unlimited amount of branch conditions, by clicking the <code><i class="fas fa-plus"></i></code> button at the bottom of a branch. You can always use and combine <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">all different types of branch conditions</a>.</p>

<h3 id="branch-behavior">Set branch behavior</h3>
<p>Each branch can behave in three different ways (<a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/" target="_blank">more information</a>):
<ul>
  <li>For the <i>first</i> condition match (<code>OR</code> match);</li>
  <li>When <i>all</i> conditions match (<code>AND</code> match);</li>
  <li>For <i>each</i> condition match (iteration) (<code>FOR</code> match).</li>
</ul>
<p>By default a branch gets executed on the <i>first</i> condition match, so if one of the conditions match. In this article we want to check if <i>all</i> conditions match. To do so, we click the branch bubble at the top and set the branch behavior to <code>When all conditions match</code>. Now the branch will only be followed if the respondent matches all conditions.</p>
<p>In the example below we created a branch with three conditions. All three conditions have to match to go into the branch. In this example that branch approves the appointment and sends a confirmation message.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-behavior.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Set the behavior to match all branch conditions.</figcaption>
</figure>
<hr />

<h3 id="tutorials">Video tutorial</h3>
<p>We have made several video tutorials on how to use logic. The video below will show how to use branch conditions.</p>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>
