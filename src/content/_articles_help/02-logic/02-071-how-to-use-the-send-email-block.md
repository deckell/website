---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-send-email-block/
title: How to use the send email block - Tripetto Help Center
description: Use the send email block to send mails from within your forms, even with form data in the mail body.
article_title: How to use the send email block
article_id: action-blocks
article_folder: editor-block-send-email
author: mark
time: 5
category_id: logic
subcategory: logic_email_actions
areas: [studio, wordpress]
---
<p>You can add mailer blocks inside your forms to trigger mailings to yourself or your respondents. And even use form data inside those emails.</p>

<h2 id="when-to-use">When to use</h2>
<p>The send email block enables you to send emails from within your forms. You can use them for 'simple' notifications and confirmations, but mailer blocks are completely flexible, in terms of who receives them, what's the content and when they are sent.</p>

<h3>When is the mail sent?</h3>
<p>Keep in mind the email only gets sent at the moment the respondent has passed the mailer block while filling out the form. So if you place the mailer block inside a certain branch, the email block only gets triggered when a respondent enters that specific branch. Your respondent doesn't see when a mailer block gets triggered.</p>
<p>It's also good to know the email only gets sent <i>after</i> completion of the form. So it's not sent immediately when someone passes a mailer block, but only at the end of the form.</p>
<blockquote>
  <h4>Having email troubles in WordPress?</h4>
  <p>If you're using our WordPress plugin, the sending of emails is handled by your own WordPress installation. <a href="{{ page.base }}help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/" target="_blank">Click here for troubleshooting if your WordPress installation is not sending emails.</a></p>
</blockquote>

<h2 id="how-to-use">How to use</h2>
<p>The send email block is available as a question type. Add a new block to your form and then select the question type <code>Send email</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Send email blocks are available from the Question type menu.</figcaption>
</figure>

<h3 id="recipient" data-anchor="Recipient">Determine recipient address</h3>
<p>You can email yourself, or the respondent (if you know the email address of the respondent):</p>
<ul>
  <li>Select <code>Fixed recipient address</code> - You will use this setting to mail to the same person each time a form is completed. For example to get notified when someone completes the form. Just enter an email address you'd like to receive the mail on;</li>
  <li>Select from <code>Available email addresses</code> - You can also email to an email address that your respondent enters inside your form. For example to send a confirmation mail or thank you mail. <a href="{{ page.base }}help/articles/how-to-send-a-confirmation-mail-to-your-respondents/" target="_blank">Click here for more information on how to set that up.</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-recipient.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of using an answer as a recipient.</figcaption>
</figure>

<h3 id="message" data-anchor="Email message">Determine email message</h3>
<p>Next step you can enter the email message:</p>
<ul>
  <li>Enter <code>Subject</code> - The subject line of the email;</li>
  <li>Enter <code>Message</code> - The actual content of the email message.</li>
</ul>

<h4>Use form data in your email</h4>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-message.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of selecting a piping value in an email message.</figcaption>
</figure>
<p>If you want to include a copy of all given answers, you can activate the feature <code>Form data</code> (see below).</p>
<p>If you only want to use certain given answers from each entry inside your mails you can use piping logic. These piping values are available in both the subject and the message of an email. In that way you can for example address your recipient personally in the salutation of the email if you asked for the name in your form.</p>
<p>To do so, you can use piping values by typing the <code>@</code> sign and then select the desired question you want to show the answer of. More information can be found in <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">this article about piping logic</a>.</p>

<h3 id="settings">Settings</h3>
<p>The following advanced settings are available for the mailer block:</p>
<ul>
  <li>
    <h4>Sender address</h4>
    <p>You can enter a <code>Sender address</code> that will be used as a <strong>reply-to address</strong> in your email message (all mails will always be sent by no-reply@tripetto.com).</p>
    <p>Useful for example when you send an email from within the form to the respondent and the respondent wants to reply to that email. The reply message will then be addressed to the sender address you entered.</p>
    <p>To do so enable the <code>Sender</code> feature and then select the sender address:</p>
    <ul>
      <li>Select <code>Fixed sender address</code> - You will use this setting to set a sender address that's the same for every mail sent. For example the support email address of your company so respondents can mail the support department afterwards;</li>
      <li>Select from <code>Available email addresses</code> - You can also email on behalf of an email address that your respondent enters inside your form. To use this feature, please provide a question block that can contain an email address. In Tripetto this can be a block with question type 'Email address', or a '<a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/" target="_blank">Hidden field</a>' block. Make sure this block is entered <i>before</i> the mailer block.<br/>After you've set up a block that contains the email address, you can select this particular block inside the mailer block as a sender address. The form will now use the given email address as the sender address (reply-to).<br/></li>
    </ul>
  </li>
  <li>
    <h4>Form data</h4>
    <p>You can choose to <code>Append all form data</code> to the message. This will append all exportable data in the dataset of the form to the message. Useful, for example, if you want to send a copy of the form data to the respondent.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-settings.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Advanced settings of the mailer block.</figcaption>
</figure>

<h3 id="export">Exportability</h3>
<p>Unlike <a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" target="_blank">other question blocks</a> the data of the mailer block (recipient address, subject, message and optionally sender address) doesn't get saved to the dataset of the form entries by default.</p>
<p>If you wish to include any of this mailer data in your dataset, you can overrule the default setting by enabling the <code>Exportability</code> feature and activate the desired include option(s):</p>
<ul>
  <li><code>Include recipient address in the dataset</code>;</li>
  <li><code>Include subject in the dataset</code>;</li>
  <li><code>Include message in the dataset</code>;</li>
  <li><code>Include sender address in the dataset</code> (only available when the sender address is enabled).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-exportability.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Exportability settings of the mailer block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The send email block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Mail will be send;</li>
  <li>No mail will be send.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<p>Evaluate conditions can work separately for the following elements of the send email block: <code>Recipient</code>, <code>Subject</code> and <code>Message</code>:</p>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

<h2>Also read our blog</h2>
<p>We also wrote a blog post with some more background information, use cases and working examples of mailer blocks.</p>
<div>
  <a href="{{ page.base }}blog/your-form-is-now-a-mail-agent/" target="_blank" class="blocklink">
    <div>
      <span class="title">Your form is now a mail agent - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Introducing a new action block to send mails right from your form. Opening new possibilities for even smarter forms and surveys.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<hr />

{% include help-article-blocks.html %}

