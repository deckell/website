---
layout: help-article
base: ../../../
permalink: /help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/
title: Learn about different types of branch behavior for your logic - Tripetto Help Center
description: You can determine how each branch should behave when condition(s) match. This will affect the way your form responds to the branch.
article_title: Learn about different types of branch behavior for your logic
article_id: logic-details
article_folder: editor-branch-behavior
author: jurgen
time: 4
time_video: 14
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>You can determine how each branch should behave when condition(s) match. This will affect the way your form responds to the branch.</p>

<h2 id="builder">Branches</h2>
<p>In Tripetto you use branches to add the desired logic to your form. In <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">this article we showed how to add branches to your form</a>.</p>
<p>Part of each branch is the <strong>branch behavior</strong> that determines how to check the branch conditions. Let's take a deeper look into that.</p>

<h2 id="types">Branch behaviors</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-add.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Switching the branch behavior.</figcaption>
</figure>
<p>To determine how a branch should behave when one or more conditions match, you can select the right <strong>branch behavior</strong>. To do so, you click the green square at the top of a branch. Now you can choose from different branch behaviors, which we'll describe in detail below:</p>
<ul>
  <li>For the <i>first</i> condition match;</li>
  <li>When <i>all</i> conditions match;</li>
  <li>For <i>each</i> condition match (iteration).</li>
</ul>

<h3 id="first">For the <i>first</i> condition match</h3>
<p>The default behavior of branches is to follow the branch if at least one of the conditions matches.</p>
<blockquote>
  <p>Professionals would call this a <code>logical OR</code> statement.</p>
</blockquote>
<p>For example:</p>
<ul>
  <li>If the respondent selects the <i>'Other...'</i> option at the dropdown question <i>'Question 1'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-create-an-other-option/" target="_blank">also see this help article</a>);</li>
  <li>If the respondent selects <i>'Option A'</i> <strong>or</strong> <i>'Option B'</i> at the multiple choice question <i>'Question 2'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">also see this help article</a>).</li>
  <li>If the respondent selects <i>'Option C'</i> at the dropdown question <i>'Question 3'</i> <strong>or</strong> gives a <i>'lower than 3 star rating'</i> at the rating question <i>'Question 4'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">also see this help article</a>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-first.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a branch with a first condition match.</figcaption>
</figure>

<h3 id="all">When <i>all</i> conditions match</h3>
<p>This behavior follows the branch if all the conditions match.</p>
<blockquote>
  <p>Professionals would call this a <code>logical AND</code> statement.</p>
</blockquote>
<p>For example:</p>
<ul>
  <li>If the respondent selects <i>'Option A'</i> <strong>and</strong> <i>'Option B'</i> at the multiple choice question <i>'Question 2'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-match-a-combination-of-multiple-conditions/" target="_blank">also see this help article</a>);</li>
  <li>If the respondent selects <i>'Option A'</i> at the multiple choice question <i>'Question 2'</i> <strong>and</strong> has entered a valid email address at the email address question <i>'Question 5'</i>, then show this branch (<a href="{{ page.base }}help/articles/how-to-match-a-combination-of-multiple-conditions/" target="_blank">also see this help article</a>);</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-all.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a branch with an all condition match.</figcaption>
</figure>

<h3 id="each">For <i>each</i> condition match (iteration)</h3>
<p>This behavior follows the branch for each condition match. This will create an iteration of the branch.
<blockquote>
  <p>Professionals would call this a <code>FOR</code> statement.</p>
</blockquote>
<p>For example:</p>
<ul>
  <li>If the respondent selects one or multiple options at the checkboxes question <i>'Question 5'</i>, then show this branch <strong>for each</strong> of the selected options (<a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/" target="_blank">also see this help article</a>);</li>
  <li>If the respondent selects one or multiple options at the multiple choice question <i>'Question 6'</i>, then show this branch <strong>for each</strong> of the selected options (<a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/" target="_blank">also see this help article</a>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-each.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a branch with an each condition match.</figcaption>
</figure>
<hr />

<h2 id="tutorials">Video tutorials</h2>
<p>We have made some video tutorials on how to use branches. It shows where to do this in the form builder and what options you have.</p>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/pb01iB9UhDQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on simple logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/DgyYh_xPWuo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on advanced logic.</figcaption>
</figure>
