---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-repeat-follow-up-for-multiple-selected-options/
title: How to repeat follow-up for multiple selected options - Tripetto Help Center
description: Learn how to repeat follow-up questions for each of the selected options from a list.
article_title: How to repeat follow-up for multiple selected options
article_id: logic-flow
author: jurgen
time: 3
time_video: 8
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>Learn how to repeat follow-up questions for each of the selected options from a list.</p>

<h2 id="when-to-use">When to use</h2>
<p>A good example of smartness inside a form is when you can repeat follow-up for each of the selected options from a multiple choice list.</p>
<p>Let's say you've got a multiple choice list of five product options and your respondents can select multiple of those products. For each selected product you want to ask how many the respondent wants to order and calculate the subtotal per product.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-repeat/demo.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of iterating questions for selected products.</figcaption>
</figure>

<h2 id="how-to-use">How to use</h2>
<p>The idea is to make a question that respondents can select multiple options from and then ask a follow-up question for each of the selected items.</p>
<h3 id="question-types">Question types</h3>
<p>You can use several question types to let your respondents select multiple options from a list:</p>
<ul>
  <li>Checkboxes;</li>
  <li>Multiple choice buttons;</li>
  <li>Picture choice buttons.</li>
</ul>
<h3 id="branch">Create branch</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-branch-repeat/00-repeat.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Select the branch behavior.</figcaption>
</figure>
<p>Now, instead of creating separate branches for each option (like you would do in other form builders), you can just create one single branch and let the form repeat the question blocks inside that branch for <i>each</i> selected option.</p>
<p>To do so, click the branch bubble at the top of the branch and then select <code>For each condition match (iteration)</code>.</p>
<h3 id="follow-up">Add follow-up</h3>
<p>In the example we created a branch with one question. That question will be asked for each of the options the respondent selects.</p>
<h3 id="piping">Use piping values</h3>
<p>Inside the iterating branch the same question blocks will be used multiple times, namely for each of the items your respondent selects. To show your respondent about which selected option each question is, you can use <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">piping logic</a>. Inside an iterating branch there is a special piping value available, called <code>Automatic text value</code>. By using that piping value, the form will automatically show the name of the selected option that the iterating question is about.</p>

<blockquote>
  <h4>More logic possibilities</h4>
  <p>The logic described in this article is an example of what Tripetto can do to make your forms smart. <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">Discover the power of branches for your logic</a> to see all logic capabilities.</p>
</blockquote>
<hr />

<h3 id="tutorials">Video tutorials</h3>
<p>We have made several video tutorials on how to use logic. These tutorials show you how to create a repeating branch.</p>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/DgyYh_xPWuo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on advanced logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>
