---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-set-a-follow-up-based-on-respondents-answers/
title: How to set a follow-up based on respondents' answers - Tripetto Help Center
description: Learn how to use various kinds of logic to show a certain follow-up based on the respondents' answers.
article_title: How to set a follow-up based on respondents' answers
article_id: logic-flow
article_folder: editor-branches
author: jurgen
time: 4
time_video: 14
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>Learn how to use various kinds of logic to show a certain follow-up based on the respondents' answers.</p>

<h2 id="when-to-use">When to use</h2>
<p>As we stated in <a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/" target="_blank">our article about smart forms</a>, making your forms listen and react to your respondents will improve the completion rate. For each question you ask, there are several ways to listen and react to the givens answers by your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branches/demo.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of an 'Other...' option.</figcaption>
</figure>

<p>The filter options differ per question type. Discover our form blocks from the menu below to get an idea of what's possible in each question type.</p>
{% include help-article-blocks.html %}
<h3>Examples</h3>
<ul>
  <li>Based on a single selection from a list of options (i.e. single choice buttons, radio buttons, or dropdown);</li>
  <li>Based on a multiple selection from a list of options (i.e. multiple choice picture buttons, or checkboxes);</li>
  <li>Based on a (un)checked checkbox;</li>
  <li>Based on a yes-no decision;</li>
  <li>Based on a rating match;</li>
  <li>Based on a date match;</li>
  <li>Based on a text match (exact or contains);</li>
  <li>Based on a number verification (equal, lower, higher);</li>
  <li>Based on a calculator outcome (equal, lower, higher).</li>
</ul>
<blockquote>
  <h4>More logic possibilities</h4>
  <p>The logic described in this article is an example of what Tripetto can do to make your forms smart. <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" target="_blank">Discover the power of branches for your logic</a> to see all logic capabilities.</p>
</blockquote>

<hr />

<h2 id="how-to-use">How to use</h2>
<p>For each condition you want to make your form smart, you can create a branch. And as soon as you're going to use branches, the visual form builder comes in handy, because this will help you to visualize the flows and decisions in your forms. Branches are plotted in the horizontal direction in the form builder.</p>
<p>For this example, we have already added a multiple choice question with several options.</p>

<h3 id="branch">Create branches</h3>
<p>The easiest way to create a branch is by clicking the <code><i class="fas fa-ellipsis-h"></i></code> button in a question block. In that menu the possible branch filters will be shown and you can select on of them right away. In our example we can click one of the options in the multiple choice block. A branch will then be created automatically with the selected option as the branch condition.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-branches.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Creating two separate branches for selected options.</figcaption>
</figure>

<p>Another option to create branches is by first creating an empty branch and then select the desired branch conditions for that branch. To do that, click the <code><i class="fas fa-plus"></i></code> button on the right side of the desired cluster. An empty branch will be added. After that click the <code><i class="fas fa-plus"></i></code> button in the branch to add the desired branch condition(s).</p>
<h3 id="follow-up">Add follow-ups</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-follow-up.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Two branches for different user selections.</figcaption>
</figure>
<p>Now you can add the desired follow-ups for each branch. In our example we just added one question, but you can add anything you need as a follow-up. That includes unlimited questions, but also new branches and other logic types. The possibilities are endless; just how you need it.</p>

<h3 id="tutorials">Video tutorials</h3>
<p>We have made several video tutorials on how to use logic. These tutorials show multiple applications of logic, from simple logic, to pretty advanced (but still understandable) logic.</p>
<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/pb01iB9UhDQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on simple logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/RfD9bXK9Sos" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on branch logic.</figcaption>
</figure>

<figure>
  <div class="video-embed"><iframe src="https://www.youtube.com/embed/DgyYh_xPWuo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  <figcaption>Video tutorial on advanced logic.</figcaption>
</figure>

