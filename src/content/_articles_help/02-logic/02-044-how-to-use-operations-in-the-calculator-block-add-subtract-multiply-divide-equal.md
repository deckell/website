---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/
title: How to use operations in the calculator block (add, subtract, multiply, divide, equal) - Tripetto Help Center
description: The calculator block supports all needed operations, like adding, subtracting, multiplying, dividing and equaling.
article_title: How to use operations in the calculator block (add, subtract, multiply, divide, equal)
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: mark
time: 4
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>The calculator block supports all needed operations, like adding, subtracting, multiplying, dividing and equaling.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">the calculator block</a> when you want to perform calculations inside your form. You will always need one or more operations to perform the needed calculations. For example:</p>
<ul>
  <li>Add up the scores of correct given answers in a quiz;</li>
  <li>Subtract a discount in a quote form;</li>
  <li>Multiply a price of a product with the entered amount;</li>
  <li>Divide a value in a formula.</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-operations.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of several operations (divide, add, subtract).</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to manage operations in your calculator blocks. For global instructions about the calculator block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">this article</a>.</p>
<p>Each calculation block starts with an empty list of <code>Operations</code>. These operations are the core of the calculator and determine what this block will calculate. Each operation is a step in your calculation. You can add as many operations as you need.</p>

<h3 id="initial">Initial value</h3>
<p>To start with, you add the <code>Initial value</code> to the list of operations by clicking the <code><i class="fas fa-plus"></i></code> button at the bottom of the operations list. That's the starting point of your calculation.</p>
<p>There are several options to determine the initial value, for example static numbers, <a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/" target="_blank">blocks</a>, <a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/" target="_blank">scores</a>, <a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/" target="_blank">comparisons</a>, <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" target="_blank">functions/constants</a> and <a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/" target="_blank">subcalculations</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/initial-value-block.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we start with the answered value of a respondent to a question block.</figcaption>
</figure>

<h3 id="operations">Operations</h3>
<p>After you added the starting variable, you can perform all kinds of calculation operations to that. You can add unlimited operations by clicking the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Operations</code> list. The following operations are available:</p>
<ul>
  <li><code><i class="fas fa-plus fa-fw"></i> Add</code> - Add up a value to the outcome of the previous operation;</li>
  <li><code><i class="fas fa-minus fa-fw"></i> Subtract</code> - Subtract a value from the outcome of the previous operation;</li>
  <li><code><i class="fas fa-times fa-fw"></i> Multiply</code> - Multiply the outcome of the previous operation with a value;</li>
  <li><code><i class="fas fa-divide fa-fw"></i> Divide</code> - Divide the outcome of the previous operation with a value;</li>
  <li><code><i class="fas fa-equals fa-fw"></i> Equal</code> - Equal the calculator with a value.</li>
</ul>

<h4>Operation properties</h4>
<p>The properties of operations are so flexible, you can set them up exactly how you need them. It depends on what you want to do in the operation, what settings are available for that particular operation. Some operations don't need any additional settings, but others might have far-reaching options.</p>
<p>There are several options to use in the properties of your operation, for example static numbers, <a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/" target="_blank">blocks (given answers)</a>, <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" target="_blank">functions/constants</a> and <a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/" target="_blank">subcalculations</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/operations.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we multiply the entered amount with the price (<code>$4.95/piece</code>) and add the shipping costs (<code>$5.99</code>).</figcaption>
</figure>
<h4>Flexible values</h4>
<p>The mindblowing thing with calculator operations is everything is totally flexible. Even if an operation needs some steps to get to the right value, all options are available in each step. For example a comparison has to take some steps to get to the right outcome. And in each step all possible options are usable:</p>
<ul>
  <li><code>Last outcome (ANS)</code>;</li>
  <li><code>Number</code><i class="fas fa-arrow-right"></i>Enter a static number;</li>
  <li><code>Value (given answer)</code><i class="fas fa-arrow-right"></i>Select a question from your form to use the answered value;</li>
  <li><code>Constant</code><i class="fas fa-arrow-right"></i>Select a built-in constant value.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/operation-advanced.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we add the transport costs if the value of the order (<code>Last outcome</code>) is <code>lower than $50</code>.</figcaption>
</figure>

<h3 id="management">Management</h3>
<p>You can use the following actions to manage your operations:</p>
<ul>
  <li>You can click each operation to edit the properties. By clicking the <code><i class="fas fa-bars"></i></code> button at the right side of each operation, you can alter the operator and delete a single operation;</li>
  <li>You can arrange the order of your operations by dragging and dropping a specific operation row. The operations get executed in the order that you set them in the operations list. That order of course can have a big effect on the outcome of the calculation;</li>
  <li>By clicking the <code><i class="fas fa-bars"></i></code> button at the top of the operations list, you can reset the calculator by deleting the operations list.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/management.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Demo of the management options for operations.</figcaption>
</figure>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
