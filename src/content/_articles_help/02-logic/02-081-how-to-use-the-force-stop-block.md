---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-force-stop-block/
title: How to use the force stop block - Tripetto Help Center
description: Use the force stop block if you want to prevent a form from submitting, for example when a certain condition is (not) matched.
article_title: How to use the force stop block
article_id: action-blocks
article_folder: editor-block-force-stop
author: mark
time: 2
category_id: logic
subcategory: logic_stop_actions
areas: [studio, wordpress]
---
<p>Use the force stop block if you want to prevent a form from submitting, for example when a certain condition is (not) matched.</p>

<h2 id="when-to-use">When to use</h2>
<p>The force stop blocks helps you to keep your results clean. It shows a message in your form without the possibility for your respondent to proceed and submit the form. This can be handy when a respondent for example does not meet the requirements that you have set for your target audience. Then you can show a message explaining the respondent does not belong to your target audience.</p>
<blockquote>
  <h4>Tip</h4>
  <p>Of course make sure you only add a force stop block on the right places in your form's structure, as it prevents a respondent from finishing your form.</p>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The force stop block is available as a question type. Add a new block to your form and then select the question type <code>Force stop</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Force stop blocks are available from the Question type menu.</figcaption>
</figure>

<h3 id="basic-features">Basic features</h3>
<p>You can now add the message you'd like to show to your respondents. The message will be shown as a common block inside the form, but without any interaction visible, preventing the respondent to finish the form.</p>
<p>The following fields are available for the message:</p>
<ul>
  <li>
    <h4>Text</h4>
    <p>The main title of the message.</p>
  </li>
  <li>
    <h4>Description</h4>
    <p>An additional description in the message.</p>
  </li>
  <li>
    <h4>Image</h4>
    <p>An additional image in the message.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/message.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a force stop block, placed inside a branch if the respondent does not belong to your target audience.</figcaption>
</figure>
<hr />

{% include help-article-blocks.html %}
