---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-set-value-block/
title: How to use the set value block - Tripetto Help Center
description: Use the set value block to set, change, clear and lock/unlock values in your form. It lets you prefill values in your question blocks, but is also able to perform advanced actions with the values in your form.
article_title: How to use the set value block
article_id: action-blocks
article_folder: editor-block-set-value
author: mark
time: 6
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
---
<p>Use the set value block to set, change, clear and lock/unlock values in your form. It le's you prefill values in your question blocks, but is also able to perform advanced actions with the values in your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can use the set value block if you want to perform actions with the values inside question blocks in your form. The most common use case for that is to <a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-the-right-values/" target="_blank">prefill question blocks</a> with personalized values, but you can also think of actions like updating and/or locking values. Some examples:</p>
<ul>
  <li>Prefill the 'Name' question in your form with the name of your respondent;</li>
  <li>Take over the entered value of the 'Confirmation recipient' question into the 'Invoice recipient' question;</li>
  <li>Lock given answers in a quiz to prevent cheating: the given answers remain visible, but are not editable;</li>
  <li>Unlock the 'Email address' question in your form when the respondent indicates the prefilled email address is incorrect.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.png" alt="Screenshot of a quiz in Tripetto" />
  <figcaption>Demonstration of prefilling a question block from the URL (<code>?name=Julie</code>).</figcaption>
</figure>
<blockquote>
  <h4>Tip</h4>
  <p>You just want to prefill question blocks? We made a dedicated article about that use case, as that's the most common usage of the set value block: <a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-the-right-values/"><strong>How to prefill question blocks with the right values</strong></a>.</p>
</blockquote>

<h2 id="how-to-use">How to use</h2>
<p>The set value block is available as a question type. Add a new block to your form and then select the question type <code>Set value</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The set value block is available from the Question type menu.</figcaption>
</figure>
<h4>Positioning the set value block</h4>
<p>Please be aware that the position where you place the set value block in your form can influence how it will behave. Two examples to demonstrate:</p>
<ul>
  <li>If you place a set value block at the beginning of your form, it will immediately set the values that you define;</li>
  <li>But if you place a set value block for example inside a branch, then the defined values/actions will only be executed if the respondent enters that branch.</li>
</ul>

<h3 id="purpose">Purpose</h3>
<p>The set value block is a rather advanced block, being capable of many actions regarding the values of your blocks. But the most common purpose of it is to prefill values, so we help you to make that supereasy.</p>
<p>In the <code>Purpose</code> feature you can choose what's your goal with this set value block. Depening on your purpose the set value block hides/shows certain settings:</p>
<ul>
  <li><code>Prefilling</code> - This configures all settings automatically so you can easily prefill values, without thinking of any advanced settings;</li>
  <li><code>Advanced</code> - This lets you configure all settings yourself so you can perform advanced set value actions.</li>
</ul>

<h3 id="values">Values</h3>
<p>Next, you will see an empty list of <code>Values</code>. You can fill that list with the desired value actions you want to perform in this block. In one set value block, you can add an unlimited amount of value actions.</p>
<p>By clicking the <code><i class="fas fa-plus"></i></code> button at the bottom of the Values list you add a value action. From the menu, select the block in your form that you want to set a value for.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/value-add.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Add a value action to the set value block.</figcaption>
</figure>
<p>Now you can configure the value settings for that block. It depends on the purpose you have selected (prefilling or advanced) which settings are available.</p>

<h3 id="value-settings-prefilling">Values - Prefilling</h3>
<p>If you have selected the purpose 'prefilling', you only have to determine what will be the value for the selected block. These are the options:</p>
<ul>
  <li><code>Fixed value</code> - Set the value of the block to a fixed value, so all respondents will see the same value prefilled. It depends on the question type of the selected block how you enter the fixed value;</li>
  <li><code>From other variable</code> - Set the value of the block similar to a value from another block/variable in your form. You can select the block you want to use the value of;</li>
  <li><code>From query string</code> - Set the value of the block to a value that's captured from the query string in the URL (<a href="https://en.wikipedia.org/wiki/Query_string" target="_blank">what's a query string?</a>). You can enter the query string parameter that you want to get the value from;</li>
  <li>Depending on the question type of the block, other value options can become available. For example for a number block you can also use <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">all calculator features</a> instantly.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/value-prefill.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Settings for a prefilling set value block.</figcaption>
</figure>

<h3 id="value-settings-advanced">Values - Advanced</h3>
<p>If you have selected the purpose 'advanced', you have more settings available to get the right behavior. Now you can control 3 settings:</p>
<ul>
  <li>
    <h4>Value</h4>
    <p>This determines the value that you want to give to the selected block. These are the options:</p>
    <ul>
      <li><code>Do not change</code> - Leave the value of the block unchanged;</li>
      <li><code>Fixed value</code> - Set the value of the block to a fixed value, so all respondents will see the same value prefilled. It depends on the question type of the selected block how you enter the fixed value;</li>
      <li><code>From other variable</code> - Set the value of the block similar to a value from another block/variable in your form. You can select the block you want to use the value of;</li>
      <li><code>From query string</code> - Set the value of the block to a value that's captured from the query string in the URL. You can enter the query string parameter that you want to get the value from;</li>
      <li>Depending on the question type of the block, other value options can become available. For example for a number block you can also use <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">all calculator features</a> instantly;</li>
      <li><code>Clear</code> - Empty the value of the block.</li>
    </ul>
  </li>
  <li>
    <h4>Mode</h4>
    <p>This determines how the set value action should behave regarding the current value at that time. It depends on the question type of the selected block which options are available, but in general these are the options:</p>
    <ul>
      <li><code>Overwrite</code> - Regardless of the value of the block, the configured value will always overwrite the current value of the block;</li>
      <li><code>Prefill</code> - Only set the configured value if the current value of the block is empty;</li>
      <li>Advanced options are available for certain question types, for example for text values and boolean values.</li>
    </ul>
  </li>
  <li>
    <h4>Locking</h4>
    <p>This determines the lock state that you want to give to the selected block. This way you can enable/disable the ability for your respondents to edit the value of the selected block. These are the options:</p>
    <ul>
      <li><code>Do not change</code> - Leave the lock state unchanged;</li>
      <li><code>Lock</code> - Lock the value of the block, so the value cannot be changed anymore;</li>
      <li><code>Unlock</code> - Unlock the value of the block, so the value can be changed afterwards.</li>
    </ul>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/value-advanced.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Settings for an advanced set value block.</figcaption>
</figure>

<h3 id="description">Description</h3>
<p>You can add a <code>Description</code> to your set value block, so you can recognize what that block does in your form. This will not be visible for your respondents.</p>

<hr/>
<h2 id="value-usage">Action block values</h2>
<p>One of the options to set the value is taking over the value of another block in your form. You might be thinking of other question blocks, but you can also use the values of other <strong>action blocks</strong>, for example:</p>
<ul>
  <li>From a <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/" target="_blank">hidden field block</a> - The hidden field block can contain all sorts of values, like values from query strings, values from JavaScripts variables and browser information. You can use these directly inside the set value block;</li>
  <li>From a <a href="{{ page.base }}help/articles/how-to-use-the-custom-variable-block/" target="_blank">custom variable block</a> - The custom variable block can contain any value that you address to it. You can use that value directly inside the set value block;</li>
  <li>From a <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">calculator block</a> - The calculator block outputs the outcome of a certain calculation. You can use that outcome directly inside the set value block.</li>
</ul>
<hr/>

{% include help-article-blocks.html %}
