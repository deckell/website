---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-scores-in-your-calculations/
title: How to use scores in your calculations - Tripetto Help Center
description: You can grant scores to options in your form to use in your calculations, for example to score given answers in a quiz.
article_title: How to use scores in your calculations
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: jurgen
time: 4
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>You can grant scores to options in your form to use in your calculations, for example to score given answers in a quiz.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">the calculator block</a> when you want to perform calculations inside your form. One of the options of the calculator block is to calculate the score of a respondent based in the given answers. For example:</p>
<ul>
  <li>Calculate the score of a trivia quiz;</li>
  <li>Calculate the grade for an online assessment/exam.</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-quiz.gif" alt="Screenshot of a quiz in Tripetto" />
  <figcaption>Demonstration of scoring quiz questions.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to use scores in your calculator blocks. For global instructions about the calculator block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">this article</a>.</p>
<p>Scoring helps you to quickly calculate certain values, based on the given answers. This lets you score all options of question blocks where the respondent can choose from a list of options, for example a dropdown block, radio buttons block or a picture choice block (single and multiple selection).</p>

<blockquote>
  <h4>Instant scoring inside question blocks</h4>
  <p>In the article you're reading now we describe how to add scores inside a calculator block. But in most cases you can also use the instant scoring feature inside a question block itself, instead of adding a separate calculator block.</p>
  <p>We wrote a separate article about that: <a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/"><strong>How to add instant scores and calculations inside question blocks</strong></a>.</p>
</blockquote>

<h3 id="add-operation">Add operation</h3>
<p>You can use scores inside each operation. From the menu to add an operation, you will see a menu item <code>Score</code> (if you have an empty form, you will see these menus directly). This lists all blocks in your form that support scores. From that list, select your question block that you want to attach scores to.</p>

<h3 id="enter-scores">Setup scores</h3>
<p>Now you can enter the score for each of the possible options of the selected question. Just enter your scores and the operation will use those scores in your calculation if the option is selected by the respondent.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/score.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we add up the score of the given answers. A correct answer adds value <code>2</code>, an incorrect answer adds value <code>0</code> to the calculator.</figcaption>
</figure>
<hr />

<h2 id="example">Example: score a quiz</h2>
<p>A good example to use scores is when you create a quiz with multiple choice questions. Let's see how to set this up if each question has one correct answers. And that answer is worth 2 points.</p>
<h4>1. Create quiz</h4>
<p>First we have added our multiple choice questions, in this case five of them. We have used multiple choice blocks and picture choice blocks for that.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-quiz-1.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our quiz form.</figcaption>
</figure>
<h4>2. Add calculator block</h4>
<p>Now, at the end of the form we want to calculate the score of the quizzer. To do so, we add a <code>Calculator</code> block and we give that block a name, for example <code>Quiz Result</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-quiz-2.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our basic calculator block.</figcaption>
</figure>
<h4>3. Score first question</h4>
<p>Next, we start with scoring the first question. To do that, we add our first operation with type <code>Score</code><i class="fas fa-arrow-right"></i>Click our first question. The score operation will be added to your calculator.</p>
<p>In this case, the correct answer to this question is <code>Option B</code>, so for that option we enter a <code>score of 2 points</code>. The other options keep their <code>scores of 0 points</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-quiz-3.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our scoring for the first question.</figcaption>
</figure>
<h4>4. Score other questions</h4>
<p>The same trick can be done for the other questions. And we want to add those scores to the calculator. So we add another row to the calculator with an <code>Add</code> operation<i class="fas fa-arrow-right"></i>Select the <code>Score</code><i class="fas fa-arrow-right"></i>Select the next question<i class="fas fa-arrow-right"></i>Score the right answer with <code>2 points</code>.</p>
<p>After we have scored all questions, the calculator will calculate the quiz result.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-quiz-4.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our scoring for all questions.</figcaption>
</figure>
<h4>5. Show result</h4>
<p>Now we want to show the result to our respondents in the closing message. To do so, we open up the closing message. By typing the <code> @ </code> sign you can select our calculator block that calculates the quiz result. Now the result will be shown in that position.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-quiz-5.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Show the quiz result in the closing message.</figcaption>
</figure>
<hr />

<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
