---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-verify-passwords-inside-your-form/
title: How to verify passwords inside your form - Tripetto Help Center
description: The password block can be used to verify a password inside your form and you can use the outcome of that verification in your logic.
article_title: How to verify passwords inside your form
article_folder: editor-branch-password
author: mark
time: 2
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>The password block can be used to verify a password inside your form and you can use the outcome of that verification in your logic.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can use the password check if your form is (partly) for members only. If you make sure your members know the password, only they can access your form.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/password.gif" alt="Screenshot of a password block in Tripetto" />
  <figcaption>Demonstration of a password block.</figcaption>
</figure>

<h2 id="how-to-use">How to use</h2>
<p>For this example we already added a password block to our form that our respondents can use to enter the password in the form.</p>
<h3 id="branch">Create branch</h3>
<p>Now we create a branch with a <code>Password match</code> condition and we open that branch condition to set the desired password that our respondents have to match.</p>
<p>We enter the password twice, so we're sure it's the right password. The password indicator at the top will transform to green and will say the password is set. Because of Tripetto's password protection method (see below) you won't see your password anymore, but as long as the password indicator is green, the password is set correctly.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-password.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Adding a branch with a password match.</figcaption>
</figure>
<h3 id="follow-up">Add follow-up</h3>
<p>Now we have setup the password match branch, we can add the desired question blocks of our form that only will be visible after our respondent entered the right password we set.</p>

<h2 id="security">Password data security</h2>
<p>Tripetto never stores the password you set as branch match. Instead we use asymmetric cryptography to verify passwords without knowing them.</p>
<p>By default the passwords entered inside the form by your respondents are not saved for security reasons. We advise to don't save those password, but you <a href="{{ page.base }}help/articles/how-to-use-the-password-block/" target="_blank">can overrule that</a> and save the entered passwords.</p>
<blockquote>
  <h4>Warning</h4>
  <p>Please make sure you are always careful with saved passwords!</p>
</blockquote>
