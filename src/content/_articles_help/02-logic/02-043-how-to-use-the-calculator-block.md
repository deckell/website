---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-calculator-block/
title: How to use the calculator block - Tripetto Help Center
description: Perform realtime calculations inside your forms based on given answers. This article describes the basic usage of the calculator block.
article_title: How to use the calculator block
article_id: action-blocks-calculator
article_folder: editor-block-calculator
author: jurgen
time: 6
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>Perform realtime calculations inside your forms based on given answers. This article describes the basic usage of the calculator block.</p>

<h2 id="when-to-use">When to use</h2>
<p><a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">The calculator block</a> really pushes the smartness of your form to the next level. Calculator blocks can perform all kinds of calculations inside your form.</p>
<p>You can use calculations for many use cases, for example:</p>
<ul>
  <li>Count the scores for a quiz;</li>
  <li>Calculate the prices of selected products for a quote;</li>
  <li>Apply discounts with coupon codes;</li>
  <li>Perform formulas for medical purposes (like a BMI calculator);</li>
  <li>Calculate the age of a respondent for age checks;</li>
  <li>Perform advanced formulas in a wizard.</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-quiz.gif" alt="Screenshot of a quiz in Tripetto" />
  <figcaption>Demonstration of calculating a quiz score.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The calculator block is available as a question type. Add a new block to your form and then select the question type <code>Calculator</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The calculator block is available from the Question type menu.</figcaption>
</figure>
<p>You can add unlimited calculator blocks in any position in your form, just how you need them. Inside each calculator block you can perform the needed calculations with all possibilities we sum up in the article below.</p>
<blockquote>
  <h4>Instant calculations inside question blocks</h4>
  <p>In the article you're reading now, we describe the possibilities of the full calculator block. But you don't always need to add a separate calculator block to your form for each calculation.</p>
  <p>We made it easier to use the most common calculator features directly inside question blocks, for example to quickly score the options of a multiple choice question in a quiz.</p>
  <p>We wrote a separate article about that: <a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/"><strong>How to add instant scores and calculations inside question blocks</strong></a>.</p>
</blockquote>

<h3 id="basic-features">Basic features</h3>
<p>To start with, you can give each calculator block a <code>Description</code>. We advise to give it a good description, so you can easily see what that calculator does in your form.</p>

<h3 id="operations">Operations</h3>
<p>Each calculation block starts with an empty list of <code>Operations</code>. These operations are the core of the calculator and determine what this block will calculate. Each operation is a step in your calculation. You can add as many operations as you need.</p>

<h4>Initial value</h4>
<p>To start with, you add the <code>Initial value</code> to the list of operations by clicking the <code><i class="fas fa-plus"></i></code> button at the bottom of the Operations list. That's the starting point of your calculation. There are several options to determine the initial value:</p>
<ul>
  <li><code>Number</code> - The most simple initial value is a static number that you enter yourself;</li>
  <li><code>Block (given answer)</code> - You can use given answers of your respondents as the initial value;</li>
  <li><code>Score</code> - You can instantly score selected answers of your respondents;</li>
  <li><code>Comparison</code> - A comparison can check a certain value and determine the initial value;</li>
  <li><code>Function</code> - There are several mathematical functions you can use to get an initial value;</li>
  <li><code>Constant</code> - There are several constants that you can use as an initial value, including dates and times;</li>
  <li><code>Subcalculation</code> - You can begin with a subcalculation. That adds a subcalculation (with full calculation features) to your initial calculation and the outcome will be used as the initial value.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/initial-value-number.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we start with a fixed number <code>10</code> as the initial value.</figcaption>
</figure>

<h4>Operations</h4>
<p>Next, you supply the operations you want to perform after the initial value. For each operation click the <code><i class="fas fa-plus"></i></code> button at the bottom of the Operations list. You can add as many operations as you need to get to the right outcome. The following operations help you with that:</p>
<ul>
  <li><code>Add</code> - Add up a value to the outcome of the previous operation;</li>
  <li><code>Subtract</code> - Subtract a value from the outcome of the previous operation;</li>
  <li><code>Multiply</code> - Multiply the outcome of the previous operation with a value;</li>
  <li><code>Divide</code> - Divide the outcome of the previous operation with a value;</li>
  <li><code>Equal</code> - Equal the calculator with a value.</li>
</ul>
<p>Please have a look at <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/" target="_blank">this article for more in depth information about operations</a>.</p>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/score.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we add up the score of the given answers. A correct answer adds value <code>1</code>, an incorrect answer adds value <code>0</code> to the calculator.</figcaption>
</figure>

<h4>Operation properties</h4>
<p>For each operational step there are lots of options to perform the right calculation. The following options help you to use the right values in your calculations:</p>
<ul>
  <li><code>Number</code> - The most simple value is a number that you enter yourself;</li>
  <li><code>Block (given answer)</code> - You can use given answers of your respondents as the value;</li>
  <li><code>Score</code> - You can instantly score selected answers of your respondents;</li>
  <li><code>Comparison</code> - A comparison can check a certain value and perform an action based on the outcome;</li>
  <li><code>Function</code> - There are several mathematical functions you can use;</li>
  <li><code>Constant</code> - There are several constants that you can use, including dates and times;</li>
  <li><code>Subcalculation</code> - You can perform a subcalculation. That adds a subcalculation (with full calculation features) to your operations list that will be executed and the outcome will be used in the parent calculator.</li>
</ul>
<p>These values make the calculator block a very powerful tool that you can use to calculate anything you need.</p>
<p>Please have a look at these articles for more information about using <a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/" target="_blank">blocks</a>, <a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/" target="_blank">scores</a>, <a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/" target="_blank">comparisons</a>, <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" target="_blank">functions/constants</a> and <a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/" target="_blank">subcalculations</a> inside calculators.</p>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/pricing.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we calculate the price (<code>$9.99/piece</code>) based on the amount that the respondent has entered and subtract a <code>10%</code> discount.</figcaption>
</figure>

<h3 id="additional-features">Additional features</h3>
<p>On top of the basic features and all operation options, the calculator block also has the following advanced options to help you with the outcome:</p>
<ul>
  <li>
    <h4>Format</h4>
    <p>By enabling the <code>Format</code> feature, you can determine how many decimal places the outcome of the calculator may contain.</p>
  </li>
  <li>
    <h4>Signs</h4>
    <p>By enabling the <code>Signs</code> feature, you can set what signs to use as decimal sign and thousands separator. Please note that in your forms these signs can be overwritten by the respondent's locale settings. In your dataset the selected signs always will be used.</p>
  </li>
  <li>
    <h4>Limit values</h4>
    <p>By enabling the <code>Limit values</code> feature, you can enter a minimum and maximum value that the outcome of the calculator can become.</p>
  </li>
  <li>
    <h4>Prefix</h4>
    <p>By enabling the <code>Prefix</code> feature, you can enter a label that's shown in front of the outcome of the calculator. Optionally you can specify a different prefix for plural values.</p>
  </li>
  <li>
    <h4>Suffix</h4>
    <p>By enabling the <code>Suffix</code> feature, you can enter a label that's shown at the end of the outcome of the calculator. Optionally you can specify a different suffix for plural values.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/features.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Additional features of the calculator block.</figcaption>
</figure>

<h3 id="logic-calculations">Calculations in logic branches</h3>
<p>Special features are built in for calculations inside logic branches. For example to perform calculations in repeating branches.</p>
<p>Please have a look at <a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/" target="_blank">this article for more in depth information about using calculators inside logic branches</a>.</p>
<hr />

<h2 id="outcomes">Outcomes</h2>
<p>The outcome of each calculator block is usable in different ways in the rest of your form. You can show it to your respondents, but also use it as the input for other blocks, or perform logic with it. And of course the outcomes are available in your dataset for results, notifications and webhook connections.</p>
<p>Please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/" target="_blank">this article for more in depth information about using calculation outcomes</a>.</p>
<hr />

<h2 id="logic">Logic</h2>
<p>Logic is important to make your forms smart and conversational. The outcome of the calculator block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Calculation is equal to <code>your filter</code>;</li>
  <li>Calculation is not equal to <code>your filter</code>;</li>
  <li>Calculation is lower than<code>your filter</code>;</li>
  <li>Calculation is higher than <code>your filter</code>;</li>
  <li>Calculation is between <code>your filters</code>;</li>
  <li>Calculation is not between <code>your filters</code>;</li>
  <li>Calculation is valid;</li>
  <li>Calculation is not valid.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers-inside-your-form/" target="_blank">more info</a>).</li>
</ul>
<hr />

<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<hr />

{% include help-article-blocks.html %}
