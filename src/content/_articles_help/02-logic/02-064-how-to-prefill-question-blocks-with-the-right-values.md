---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-prefill-question-blocks-with-the-right-values/
title: How to prefill question blocks with the right values - Tripetto Help Center
description: You can enter initial values to your question blocks, so you can help your respondents to fill out your form quickly.
article_title: How to prefill question blocks with the right values
article_folder: editor-block-set-value
author: jurgen
time: 4
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
---
<p>You can enter initial values to your question blocks, so you can help your respondents to fill out your form quickly.</p>

<h2 id="when-to-use">When to use</h2>
<p>Normally all forms start with all blank input fields. But in some cases you already know some answers of your respondents, so why not prefill them? By doing so your respondents can complete your form easier and faster, resulting in higher completion rates. Some examples:</p>
<ul>
  <li>Most of your respondents live in New York City, so in the 'City' question in your form, you prefill 'New York City'. Most of your respondents can now simply accept the answer and continue your form;</li>
  <li>You have sent personalized invitation links to your respondents, so you already know the name of each respondent. The name is part of the form URL that you have sent them. You can now use that value from the URL to prefill in the 'Name' question;</li>
  <li>You have two separate email address blocks in your order form: one for the 'Confirmation recipient' and one for the 'Invoice recipient'. Your respondent first fills out the confirmation recipient. Now you can use that entered email address as the prefilled value for the invoice recipient.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.png" alt="Screenshot of a quiz in Tripetto" />
  <figcaption>Demonstration of prefilling a question block from the URL (<code>?name=Julie</code>).</figcaption>
</figure>

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to prefill values in your question blocks. We use the set value block for that. For global instructions about the set value block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/" target="_blank">this article</a>.</p>

<h3 id="add-blocks">Step 1 - Add form blocks</h3>
<p>First build your form with the question blocks you'd like to prefill.</p>
<p>Continue with adding a <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/" target="_blank">set value block</a>. In this case we add that block at the start of our form, so all our blocks will become prefilled when the respondent opens the form.</p>

<h3 id="purpose">Step 2 - Select purpose</h3>
<p>Now open the set value block. You will first see the <code>Purpose</code> feature. Set that to <code>Prefilling</code> and the builder will make sure all settings are configured correctly for you.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/prefill-setup.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our form setup with an empty set value block at the start of the form.</figcaption>
</figure>

<h3 id="values">Step 3 - Add values</h3>
<p>All we have to do now is add the desired question blocks and their values. We will show 3 different scenarios:</p>
<ul>
  <li>Prefill with a static value;</li>
  <li>Prefill with a value from the query string;</li>
  <li>Prefill with a value from another question block/variable in your form.</li>
</ul>

<h4 id="static">Step 3.1 - Prefill with static value</h4>
<p>In this example we want to prefill our 'City' question with the fixed value 'New York City', as we know most of our respondents live there. Follow these steps:</p>
<ol>
  <li>In the set value block click the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Values</code> list;</li>
  <li>Next select the <code>City</code> question from the form. In our example form this is a dropdown question type;</li>
  <li>Now select <code>Fixed value</code> as the desired value;</li>
  <li>Now select the right value from the dropdown options, in this case we select <code>New York City</code>.</li>
</ol>
<p>That's it! Now the form will always select 'New York City' as the default selected option.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/prefill-static.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The setup to prefill with a static value.</figcaption>
</figure>

<h4 id="querystring">Step 3.2 - Prefill with query string value</h4>
<p>In this example we want to prefill our 'Name' question with a value that we collect from the query string of the form URL (<a href="https://en.wikipedia.org/wiki/Query_string" target="_blank">what's a query string?</a>). Our form URL for example looks something like this: <code>https://tripetto.app/your-form/<strong>?name=Julie</strong></code>. Follow these steps:</p>
<ol>
  <li>We can use the same set value block to prefill multiple question blocks. Just click the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Values</code> list again;</li>
  <li>Next select the <code>Name</code> question from the form. In our example form this is a text (single line) question type;</li>
  <li>Now select <code>From query string</code> as the desired value;</li>
  <li>Now enter the query string parameter that you want to use the value of. In this case we want to use the value of parameter <code>name</code>.</li>
</ol>
<p>That's it! Now the form will use the value that's entered in the URL as the default entered text value.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/prefill-querystring.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The setup to prefill with a value from the query string.</figcaption>
</figure>

<h4 id="variable">Step 3.3 - Prefill with block/variable value</h4>
<p>In this example we want to take over the entered value of the 'Confirmation recipient' question into the 'Invoice recipient' question. Follow these steps:</p>
<ol>
  <li>To take over other variables, the set value block has to be entered <u>after</u> the block that you want to take the value from. So for this example we add a new set value block at the end of the form;</li>
  <li>In the new set value block click the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Values</code> list;</li>
  <li>Next select the <code>Invoice recipient</code> question from the form. In our example form this is an email address question type;</li>
  <li>Now select <code>From other variable</code> as the desired value;</li>
  <li>Now select the question block you want to get the value from. In this case that is our <code>Confirmation recipient</code> question.</li>
</ol>
<p>That's it! Now the form will update the default value of the 'Invoice recipient' question based on the entered value in 'Confirmation recipient' question.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/prefill-variable.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The setup to prefill with a value from another question block/variable in your form.</figcaption>
</figure>
<hr/>

{% include help-article-blocks.html %}
