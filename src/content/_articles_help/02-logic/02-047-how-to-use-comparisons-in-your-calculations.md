---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-comparisons-in-your-calculations/
title: How to use comparisons in your calculations - Tripetto Help Center
description: You can use comparisons inside each calculator block to perform operations based on the outcome of the comparison, for example to check a discount code.
article_title: How to use comparisons in your calculations
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: jurgen
time: 5
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>You can use comparisons inside each calculation block to perform operations based on the outcome of the comparison, for example to check a discount code.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">the calculator block</a> when you want to perform calculations inside your form. You can use comparisons to check certain values and based on the outcome of the comparison, perform a certain operator in your calculation. For example:</p>
<ul>
  <li>Compare an entered discount code and perform the discount if the discount code is correct;</li>
  <li>Compare the total price of a cart and add up the shipment costs if the price is below a certain amount.</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-coupon.gif" alt="Screenshot of a coupon code in Tripetto" />
  <figcaption>Demonstration of a comparing a coupon code.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to use comparisons in calculator blocks. For global instructions about the calculator block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">this article</a>.</p>

<h3 id="add-operation">Add operation</h3>
<p>You can use comparisons inside each operation. From the menu to add an operation, you will see a menu item <code>Comparison</code> (if you have an empty form, you will see these menus directly). This lists all blocks in your form that support comparisons. From that list, select your question block that you want to compare.</p>

<h3 id="setup-comparison">Setup comparison</h3>
<p>You can compare basically everything with each other and use those comparisons to calculate the right values. Each comparison contains of a few steps, namely:</p>
<ol>
  <li>Input;</li>
  <li>Compare mode;</li>
  <li>Compare filter;</li>
  <li>Outcome.</li>
</ol>

<h4>Input</h4>
<p>First step is to determine what has to be compared. The following options are available to use as input for comparison:</p>
<ul>
  <li><code>Last outcome (ANS)</code> - Compare the last outcome of the calculator with another value;</li>
  <li><code>Number</code> - Compare a number with another value;</li>
  <li><code>Date/time</code> - Compare the current date (and time) with another moment;</li>
  <li><code>Value (given answer)</code> - Compare a given answer of your respondents to a certain question with another value.</li>
</ul>
<h4>Compare mode</h4>
<p>Second step is to determine how you want to compare. It depends on what you are comparing, but there are always several compare modes. For example if you're comparing with numbers:</p>
<ul>
  <li><code>Equal to ...</code>;</li>
  <li><code>Lower than ...</code>;</li>
  <li><code>Higher than ...</code>;</li>
  <li><code>Between ... and ...</code>.</li>
</ul>
<p>Specific compare modes are built in for other types of comparisons, for example for text comparisons and date comparisons.</p>
<h4>Compare filter</h4>
<p>Third step is to determine to what you want to compare to. You can compare the selected input with all different kinds of values:</p>
<ul>
  <li><code>Last outcome (ANS)</code>;</li>
  <li><code>Number</code><i class="fas fa-arrow-right"></i>Enter a static number;</li>
  <li><code>Value (given answer)</code><i class="fas fa-arrow-right"></i>Select a question from your form;</li>
  <li><code>Constant</code><i class="fas fa-arrow-right"></i>Select a built-in constant value.</li>
</ul>
<h4>Outcome</h4>
<p>Last step, you determine the desired outcomes of the comparison. There are two possible outcomes:</p>
<ul>
  <li>A value when the comparison is true;</li>
  <li>A value when the comparison is false.</li>
</ul>
<p>And for each outcome, you again can use all different kinds of values:</p>
<ul>
  <li><code>Last outcome (ANS)</code>;</li>
  <li><code>Number</code><i class="fas fa-arrow-right"></i>Enter a static number;</li>
  <li><code>Value (given answer)</code><i class="fas fa-arrow-right"></i>Select a question from your form;</li>
  <li><code>Constant</code><i class="fas fa-arrow-right"></i>Select a built-in constant value.</li>
</ul>
<p>Those outcomes will be used as the value for the operation.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/comparison.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we compare a supplied text with value <code>DISCOUNT</code>. If the comparison is true, a discount of <code>$4.99</code> is subtracted. Else no discount is subtracted.</figcaption>
</figure>
<p>Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see all comparisons you can use in the calculator block.</p>
<hr />


<h2 id="example">Example: subtract a discount</h2>
<p>A good example to use a comparison is to check if your customer has entered a valid coupon code, in this case coupon code <code>DISCOUNT</code>. And if that code is entered subtract a discount to the shopping cart. Let's see how we can set this up.</p>
<h4>1. Create shopping form</h4>
<p>First we have created our shopping form. It's a pretty easy form where you can enter the number of products you'd like to order. At the end there is a text input field where customers can enter a discount code.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-coupon-1.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our shopping form.</figcaption>
</figure>

<h4>2. Add calculator block</h4>
<p>Now, at the end of the form we want to calculate the total price and optionally subtract a discount. To do so, we add a <code>Calculator</code> block and we give that block a name, for example <code>Price</code>.</p>
<p>For this example we have already setup the pricing calculations, so we move on to the comparison.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-coupon-2.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our basic calculator block.</figcaption>
</figure>

<h4>3. Compare discount code</h4>
<p>We now want to compare the supplied discount code. If the discount code matches a valid code, then we subtract a discount of € 4.99. To do that, we create a <code>Subtract</code> operation with type <code>Comparison</code><i class="fas fa-arrow-right"></i>Select <code>Blocks</code><i class="fas fa-arrow-right"></i>Select our discount input question.</p>
<p>Now we setup the comparison: if the entered text <code>matches 'DISCOUNT'</code>, then <code>output a fixed number of 4.99</code>, else <code>output a fixed number of 0</code>.</p>
<p>Now that we have subtracted the possible discount, the calculator block contains the total price.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-coupon-3.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Our comparison for the discount.</figcaption>
</figure>

<h4>4. Show total price</h4>
<p>Now we want to show the total price to our respondents in the closing message. To do so, we open up the closing message. By typing the <code> @ </code> sign you can select our calculator block that calculates the total price. Now the price will be shown in that position.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-coupon-4.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Show the total price in the closing message.</figcaption>
</figure>

<hr />

<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
