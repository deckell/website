---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-raise-error-block/
title: How to use the raise error block - Tripetto Help Center
description: Use the raise error block if you want to show an error message to the respondent and prevent a form from submitting.
article_title: How to use the raise error block
article_id: action-blocks
article_folder: editor-block-raise-error
author: mark
time: 2
category_id: logic
subcategory: logic_stop_actions
areas: [studio, wordpress]
---
<p>Use the raise error block if you want to show an error message to the respondent and prevent a form from submitting.</p>

<h2 id="when-to-use">When to use</h2>
<p>The raise error blocks shows an error message in your form without the possibility for your respondent to proceed and submit the form. This can be handy when a respondent for example entered an email address, but that email address does not meet your requirements for that address. Then you can show an error message explaining why the email address is not correct. And asking them to correct the given answer.</p>
<blockquote>
  <h4>Tip</h4>
  <p>Of course make sure you only add a raise error block on the right places in your form's structure, as it prevents a respondent from finishing your form.</p>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The raise error block is available as a question type. Add a new block to your form and then select the question type <code>Raise error</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Raise error blocks are available from the Question type menu.</figcaption>
</figure>

<h3 id="basic-features">Basic features</h3>
<p>You can now add the error message you'd like to show to your respondents. The error message will be shown as a common block inside the form, but without any interaction visible, preventing the respondent to finish the form. The error message will be styled in the color that's set as the <a href="{{ page.base }}help/articles/how-to-style-your-forms/" target="_blank">error/required color in the Style screen</a>.</p>
<p>The following fields are available for the error:</p>
<ul>
  <li>
    <h4>Error message</h4>
    <p>The main title of the error message.</p>
  </li>
  <li>
    <h4>Description</h4>
    <p>An additional description in the error message.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/error.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a raise error block, placed inside a branch if the respondent entered an invalid email address.</figcaption>
</figure>
<hr />

{% include help-article-blocks.html %}
