---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/
title: How to use given answers from respondents in your calculations - Tripetto Help Center
description: You can use given answers from your respondents as values for your calculation operations, making the outcomes of your calculations personalized.
article_title: How to use given answers from respondents in your calculations
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: mark
time: 5
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>You can use given answers from your respondents as values for your calculation operations, making the outcomes of your calculations personalized.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">the calculator block</a> when you want to perform calculations inside your form. If you want to do calculations with the answers of your respondents, you can use those in realtime. For example:</p>
<ul>
  <li>Calculate a quiz score of a respondent, based on the given answer in a picture choice question;</li>
  <li>Calculate a total price, based on the selected products and the entered amount of each product;</li>
  <li>Calculate a discount, based on the entered coupon code;</li>
  <li>Calculate an age of a respondent, based on the entered date of birth;</li>
  <li>Calculate a BMI of a respondent, based on the entered length and weight.</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-shopping.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of using given answers to perform calculations.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to use given answers of your respondents in your calculator blocks. For global instructions about the calculator block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">this article</a>.</p>
<p>There are several options to determine the value of each operation in your calculation. One of those options is to use the given answers of your respondents to questions in your form. This enables you to perform realtime calculations based on the input of your respondent and calculate personalized outcomes.</p>
<p>From the menu to add an operation, you will see a list of questions in your form that you can use to calculate with. It depends on the question type what you exactly can do with it. In general these are the options you have to use given answers:</p>
<ul>
  <li>Recall value;</li>
  <li>Score selected option(s);</li>
  <li>Check selected option(s);</li>
  <li>Count selected option(s);</li>
  <li>Compare value;</li>
  <li>Age date.</li>
</ul>
<p>We will explain each of these options below.</p>

<h3 id="value" data-anchor="Value">Recall value</h3>
<p>This lets you use the supplied value from your respondent inside the operation, for example from a number block, rating block or scale block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-value.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we use the given answers of two number blocks.</figcaption>
</figure>

<h3 id="score" data-anchor="Score">Score selected option(s)</h3>
<p>This lets you score all options of question blocks where the respondent can choose from a list of options, for example a dropdown block, radio buttons block or a picture choice block (single and multiple selection). The calculator will show all the options that are available in that question block and you can enter the score that each option represents. The score of each selected item will be used as the value for the operation.</p>
<p>More information about <a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/" target="_blank">the score feature can be found over here</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-score.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we add up the score of the given answers. A correct answer adds value <code>1</code>, an incorrect answer adds value <code>0</code> to the calculator.</figcaption>
</figure>

<h3 id="check" data-anchor="Check">Check selected option</h3>
<p>This lets you check if a certain option is checked/selected by your respondent, for example from a multiple choice block (multiple selection) or a picture choice block (multiple selection). Based on the selection you can determine the desired values to use in the operation:</p>
<ul>
  <li>A value when the option is selected;</li>
  <li>A value when the option is not selected.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-selected.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we add up the values we give for each individual selected option: a value when the option is selected and a value when the option is not selected.</figcaption>
</figure>

<h3 id="count" data-anchor="Count">Count selected options</h3>
<p>This counts the amount of selected options by your respondent, for example from a checkboxes block, a multiple choice block (multiple selection), or a picture choice block (multiple selection).</p>
<p>The amount of selected items will be the value of the operation.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-count.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we already added up some selected values. After that we want to calculate the average of those values by dividing the last outcome with the <code>count</code> of the selected items.</figcaption>
</figure>

<h3 id="compare" data-anchor="Compare">Compare value</h3>
<p>This lets you compare the supplied value from your respondent to a condition that you compose. It depends on the question type which compare modes are available. Based on the comparison you can determine the desired outcome to use in the operation. There are two possible outcomes of a comparison:</p>
<ul>
  <li>A value when the comparison is true;</li>
  <li>A value when the comparison is false.</li>
</ul>
<p>More information about <a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/" target="_blank">the comparison feature can be found over here</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-compare.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we compare a supplied text with value <code>DISCOUNT</code>. If the comparison is true, a discount of <code>$4.99</code> is subtracted. Else no discount is subtracted.</figcaption>
</figure>

<h3 id="age" data-anchor="Age">Age date</h3>
<p>This lets you calculate the age based on a date input by your respondent, for example a date of birth that your respondent supplies. You can use several dates to calculate the age from, for example the current date, a fixed date, or another supplied date by your respondent. And you can determine how the age should be outputted: in years, months or days.</p>
<p>The age will be used as the value for the operation.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-age.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we get the age of the respondent based on the supplied date of birth.</figcaption>
</figure>

<h3 id="flexible">Other usages</h3>
<p>The above examples all show how to use given answers from your respondents as inputs to determine what should be calculated. But you can also use the given answers in other steps of calculator operations, for example inside comparisons or as output values of an operation. This makes the usage of given answers extremely powerful and flexible.</p>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
