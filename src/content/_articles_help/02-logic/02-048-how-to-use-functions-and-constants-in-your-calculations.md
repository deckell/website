---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-functions-and-constants-in-your-calculations/
title: How to use functions and constants in your calculations - Tripetto Help Center
description: You can use mathematical functions and constants (including dates and times) inside each calculation block.
article_title: How to use functions and constants in your calculations
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: mark
time: 4
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>You can use mathematical functions and constants (including dates and times) inside each calculation block.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">the calculator block</a> when you want to perform calculations inside your form. If you want to perform calculations with advanced operations, that's possible with the built-in mathematical functions and constants (including dates and times) in the calculator block. For example:</p>
<ul>
  <li>Get the highest value of two given answers and use that highest value in a calculation;</li>
  <li>Perform a mathematical formula to determime the circumference of a circle.</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-formula.gif" alt="Screenshot of a math formula in Tripetto" />
  <figcaption>Demonstration of a mathematical formula with the constant π.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In the article you're reading now we describe how to use mathematical functions and constants in your calculator blocks. For global instructions about the calculator block, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" target="_blank">this article</a>.</p>

<h3 id="add-operation">Add operation</h3>
<p>You can use the advanced options inside each operation. From the menu to add an operation, you will see a menu item <code>Function</code> (if you have an empty form, you will see these menus directly). This includes all functions and constants, grouped by the type of function.</p>

<h3 id="setup-function">Setup function/constant</h3>
<p>You can use all kinds of mathematical functions to help you perform the right calculations. The available functions are divided into the following categories:</p>
<ul>
  <li><code>Limiting</code> - Contains functions to limit certain values, like <code>min</code>, <code>max</code> and <code>clamp</code>;</li>
  <li><code>Floating point</code> - Contains functions to work with floating point values, like <code>round</code>, <code>floor</code> and <code>ceil</code>;</li>
  <li><code>Exponentiation</code> - Contains functions to perfom exponentiation, like <code>powers</code>, <code>roots</code> and <code>exponents</code>;</li>
  <li><code>Trigonometry</code> - Contains functions to perform trigonometrial calculations, like <code>sin</code>, <code>cos</code> and <code>tan</code>;</li>
  <li><code>Factorial</code> - Contains functions to perform factorial calculations, like <code>n!</code> and <code>gamma</code>;</li>
  <li><code>Miscellaneous</code> - Contains miscellaneous functions, like <code>absolute</code>, <code>modulus</code> and <code>percentage</code>;</li>
  <li><code>Constants</code> - Contains constans, like <code>π ≈ 3.14159</code>, <code>e ≈ 2.71828</code> and <code>c = 299,792,458 m/s</code>, but also date and time constants, like <code>year</code>, <code>month</code>, <code>day (of month)</code> and <code>day (of week)</code>;</li>
</ul>
<p>After selecting the right function/constant, it depends on the selected function/constant what properties are available to configure the operation.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/function.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we use a given answer to calculate the sinus in radians.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/constant.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>In this example we calculate the circumference of a circle (<code>2*π*r</code>) by using the constant <code>π (pi)</code>.</figcaption>
</figure>
<p>Please have a look at our <a href="{{ page.base }}all-calculator-features/" target="_blank">calculator features overview</a> to see all functions and constants you can use in the calculator block.</p>
<hr />

<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a> (current article);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a>.</li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}all-calculator-features/" target="_blank" class="blocklink">
    <div>
      <span class="title">All calculator features<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
