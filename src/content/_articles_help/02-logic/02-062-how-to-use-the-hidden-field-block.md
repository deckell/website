---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-hidden-field-block/
title: How to use the hidden field block - Tripetto Help Center
description: Use hidden fields to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.
article_title: How to use the hidden field block
article_id: action-blocks
article_folder: editor-block-hidden-field
author: mark
time: 5
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
---
<p>Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.</p>

<h2 id="when-to-use">When to use</h2>
<p>The idea of a hidden field block is you're able to gather information from outside the form into your form's dataset. That information is then saved with the rest of the form data when the form is submitted. Some examples:</p>
<ul>
  <li>Get the name of a respondent from a query string parameter and;</li>
  <li>Save the URL of the page that your form is embedded into;</li>
  <li>Make a logic branch in your form based on the device size of the respondent.</li>
</ul>

<h2 id="how-to-use">How to use</h2>
<p>The hidden field block is available as a question type. Add a new block to your form and then select the question type <code>Hidden field</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Hidden fields are available from the Question type menu.</figcaption>
</figure>

<h3 id="basic-features">Basic features</h3>
<p>You can now change the features of the block to get the desired behavior:</p>
<ul>
  <li>
    <h4>Name</h4>
    <p>The name of the field in your form (not visible for respondents).</p>
  </li>
  <li>
    <h4>Type of field</h4>
    <p>This determines the type of information you want to get. Examples of field types are a query string, a cookie, a page URL, a timestamp and browser information (like the user language).</p>
  </li>
</ul>
<h3 id="additional-features">Additional features</h3>
<p>Depending on the selected type of field, additional features and settings can become activated to help you get the right value in your hidden field. These features will popup on the left side of the pane. You can enable and configure these to your own needs.</p>
<hr/>

<h2 id="logic">Logic</h2>
<p>After you retrieved a hidden field in your form, you can instantly use that value to execute logic based on the value of the hidden field.</p>

<h3 id="piping-logic">Piping logic</h3>
<p>You can show the value of the hidden field to your respondents, by using our piping logic. This works the same way as you can use 'normal' question blocks as piping values, like you can see in <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank">this article about piping logic</a>.</p>

<h3 id="branch-logic">Branch logic</h3>
<p>You can also use the value of the hidden field as branch conditions, so the form can take decisions based on the hidden field. This works the same way as you can add logic based on 'normal' question blocks, like you can see in <a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank">this article about branch logic</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-logic.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Use a hidden field value to execute logic.</figcaption>
</figure>

<p>Due to the flexibility of the hidden field block, the value that it generates can contain several types. It depends on the type of value that is saved inside the hidden field, which <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/" target="_blank">branch conditions</a> you can create with it.</p>
<p>Please see the following articles for the possible branch conditions:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/#logic" target="_blank">Branch conditions for <strong>textual values</strong></a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-number-block/#logic" target="_blank">Branch conditions for <strong>numeric values</strong></a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-date-and-time-block/#logic" target="_blank">Branch conditions for <strong>date (and time) values</strong></a>.</li>
</ul>
<hr />

<h2 id="examples">Examples</h2>
<p>Let us show you some examples of how to use the hidden field.</p>

<h3 id="example-page-url">Get page URL</h3>
<p>Let's say you have embedded a Tripetto form into various pages inside your website and you want to know from which page someone has submitted a form. The hidden field can simply gather the page URL of the page that the form is embedded into and save that data to the entry data.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-page-url.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a hidden field getting the page URL.</figcaption>
</figure>

<h3 id="example-querystring">Get value from query string</h3>
<p>The URL of a form/page can also contain certain values. Those are often stored in the <strong>query string</strong> of the URL (<a href="https://en.wikipedia.org/wiki/Query_string" target="_blank">what's a query string?</a>). The query string is at the end of the URL and can contain fully flexible <strong>name-value pairs</strong>.</p>
<blockquote>
  <h4>About query strings</h4>
  <p>A query string is a somewhat technical tool to send data in a URL. Some background information on how to supply a query string:</p>
  <ul>
    <li>You can add multiple parameters to a query string;</li>
    <li>Each parameter consists of a name and a value, separated by a <code> = </code> sign, for example <code>name=value</code>;</li>
    <li>The first parameter is preceded by a <code> ? </code> sign;</li>
    <li>Any following parameters are preceded by a <code> & </code> sign;</li>
  </ul>
  <p>A full example could be something like this: <code>https://yoursite.com/?name=abc&city=klm&country=xyz</code>.</p>
</blockquote>
<p>Let's say we have a form that we share via different channels, for example a mailing and Twitter. And you want to know how someone entered the form, so you can analyse which channel worked best. By adding a query string named <code>referral</code> to your URL's, you can add various values for that query string parameter that can be saved in a hidden field in the form.</p>
<p>So in the URL you share via a mailing the query string in the URL is something like <code>?referral=mailing</code>. And for the URL you share on Twitter something like <code>?referral=twitter</code>. Notice that all parameter names and values we use are just examples; those are totally flexible for your own needs.</p>
<p>Now, by adding a hidden field that reads the query string parameter <code>referral</code>, you can save where someone came from. Each entry will now contain where someone came from: the mailing, or Twitter.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-querystring.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a hidden field getting the query string parameter <code>referral</code>.</figcaption>
</figure>
<hr/>

<h2>Also read our blog</h2>
<p>We also wrote a blog post with some more background information, use cases and working examples of hidden fields.</p>
<div>
  <a href="{{ page.base }}blog/hidden-fields-in-tripetto/" target="_blank" class="blocklink">
    <div>
      <span class="title">Hidden fields in Tripetto - Tripetto Blog<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" alt="Tripetto logo" />
    </div>
  </a>
</div>
<hr />

{% include help-article-blocks.html %}
