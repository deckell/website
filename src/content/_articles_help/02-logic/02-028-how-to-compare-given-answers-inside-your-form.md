---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-compare-given-answers-inside-your-form/
title: How to compare given answers inside your form - Tripetto Help Center
description: Learn how you can compare two given answers from a respondent inside your form, for example for email address verification.
article_title: How to compare given answers inside your form
article_folder: editor-value-check
author: jurgen
time: 4
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-check-if-two-answers-match-for-example-for-email-address-verification/
---
<p>Learn how you can compare two given answers from a respondent inside your form, for example for email address verification.</p>

<h2 id="when-to-use">When to use</h2>
<p>In Tripetto you can compare every given answer with another given answer. Based on the outcome of the comparison, you can then take the right action, for example continue if the given answers mathc, or show an error if the comparison does not match.</p>
<p>The most common usage of comparing two given answers, is when you want to be sure your respondent entered the right email address. You can then let your respondent enter their email address twice and compare those inputs, so no typing mistakes have been made.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-value-check/demo.gif" alt="Screenshot of a form in Tripetto" />
  <figcaption>Demonstration of verifying two entered email addresses.</figcaption>
</figure>

<h2 id="how-to-use">How to use</h2>
<p>In Tripetto you can do this quite easily with branch logic. Depending on the question type, you have several options to check a branch condition. In general you can choose between:</p>
<ul>
  <li><code>Fixed value</code> - A fixed value that you enter in the form builder;</li>
  <li><code>Answered value</code> - A flexible value that your respondent has answered in another question in your form.</li>
</ul>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-values.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Options to compare a value.</figcaption>
</figure>
<p>The second option is what we need to compare two given answers of a respondent. Let's say we want to compare two entered email address questions, so we know for sure the right email address is entered. If they don't match, we want to show an error.</p>

<h3 id="branch">Add branch condition</h3>
<p>To do so, we have added two <a href="{{ page.base }}help/articles/how-to-use-the-email-address-block/" target="_blank">email address blocks</a> to our form. Now we can simply add a branch and add a condition for the second email address block. In this case we add a branch condition for <code>Email address mismatch</code>.</p>
<p>In the properties of that branch condition we can now determine what value we want to compare. To do so, we select <code>Value</code>. Then a list will be shown of all other question blocks in your form that you can compare the value with. In our case, we select the first email address block from that list.</p>
<p>And that's it! The form now checks if the entered values of those question blocks are mismatched, so we can react to that comparison.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-branch-condition.gif" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Add a branch that compares two given answers.</figcaption>
</figure>

<h3 id="follow-up">Add follow-up</h3>
<p>Now we can setup the follow-up for this branch. In this case we add a <a href="{{ page.base }}help/articles/how-to-use-the-raise-error-block/" target="_blank">raise error block</a> to prevent our respondents to proceed without a correct entered email address.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-branch.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Full setup of the email address check.</figcaption>
</figure>
