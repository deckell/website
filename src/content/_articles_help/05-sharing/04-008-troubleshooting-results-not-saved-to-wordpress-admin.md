---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-results-not-saved-to-wordpress-admin/
title: Troubleshooting - Results not saved to WordPress Admin - Tripetto Help Center
description: In some situations your form results might not be saved to your WordPress Admin. This article contains some troubleshooting for this.
article_title: Troubleshooting - Results not saved to WordPress Admin
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>Based on user feedback we're aware of some situations where your form results don't get saved to your WP Admin, resulting in missing data. In this article you can find the known issues with this.</p>

<h2 id="plugin">Check your plugin version</h2>
<p>First of all, check which version of the Tripetto plugin you're on. You can find which version you're on the Plugins section in your WP Admin. Check our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/" target="_blank">article about updating the plugin</a> if you're not on the latest release version.</p>

<h2 id="errors">Check form errors</h2>
<p>Next, check if your form gives an error message when you complete the form by clicking the submit button in the form.</p>
<p>In case the form does submit and shows the closing message, we know the form itselfs saves the entry. That's good news.</p>
<p>If the form does not submit and shows a red error message on top of the form, there is an issue with your form's ability to save the result. In this case, please <a href="{{ page.base }}contact/" target="_blank">contact us</a>, so we can assist you on this.</p>

<h2 id="database">Check databases</h2>
<p>Next, check if the required databases are inside your WP instance (it depends on your config how to access your database admin). Check if the following tables exist: <code>tripetto_forms</code>, <code>tripetto_entries</code>, <code>tripetto_attachments</code>.</p>
<p>If those tables exist, also check the following: the table column <code>entry</code> in the table <code>tripetto_entries</code> should be a type <code>LONGTEXT</code>. It might occur this column is set to type <code>TEXT</code>, but that's not sufficient to save large data entries.</p>
<p>If any of the above does not match your database structure, please <a href="{{ page.base }}contact/" target="_blank">contact us</a>, so we can help you with solving this.</p>

<h2 id="conflicts">Check conflicting plugins</h2>
<p>WordPress offers lots of plugins, so we can't prevent all conflicts with other plugins, as everybody uses a different set of plugins. Based on some user feedback we are aware of conflicts with the following (type of) plugins:</p>
<ul>
  <li><b>Wordfence</b><br/>This plugin might block the Tripetto plugin to submit data.<br/>To solve this, enable the <code>Learn mode</code> inside the Wordfence plugin and then complete a Tripetto form in your WP site. Wordfence then learns that Tripetto wants to send data. After that you can disable the <code>Learn mode</code> again.</li>
  <li><b>Optimize, minify and bundle plugins</b><br/>All of these type of plugins might mess up some scripts of the Tripetto plugin.<br/>To solve this, in most plugins you can exclude the Tripetto scripts from the plugin, preventing it to influence the Tripetto scripts. How to do this, depends on each particular plugin.</li>
</ul>
<hr />

<h2>Still not working?</h2>
<p>Don't hesitate to <a href="{{ page.base }}contact/" target="_blank">contact us</a> if you have any problems with the WordPress plugin. <a href="javascript:$crisp.push(['do', 'chat:open']);">Using the chat</a> will be the fastest and easiest way to do so. We're happy to help you!</p>
