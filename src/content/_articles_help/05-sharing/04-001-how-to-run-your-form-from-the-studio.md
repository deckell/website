---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-run-your-form-from-the-studio/
title: How to run your form from the studio - Tripetto Help Center
description: In the studio you can share a link to your form, or embed the form into your own website.
article_title: How to run your form from the studio
article_folder: studio-share
author: jurgen
time: 1
category_id: sharing
subcategory: sharing_methods
areas: [studio]
popular: true
---
<p>In the studio you can share a link to your form, or embed the form into your own website.</p>

<h2 id="methods">Run methods</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there you can choose between two methods to run your forms:</p>
<ul>
  <li><h3 id="share">Share a link</h3>Select <code>Share a link</code> to receive a shareable tripetto.app link to your form. You can just share that link with your audience and collect your data. Collected data will be stored at Tripetto. See <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-from-the-studio/" target="_blank">this article for more information on the shareable link</a>.</li>
  <li><h3 id="embed">Embed in a website or application</h3>Select <code>Embed in a website or application</code> to receive an embed code that you can use to show your form in a website or application. By default the collected data is stored at Tripetto, but you can take controle over your data. See <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/" target="_blank">this article for more information on embedding</a>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/studio-embed/01-embed.png" alt="Screenshot of sharing in Tripetto" />
  <figcaption>An example of the method setting.</figcaption>
</figure>

<h2 id="data-control" data-anchor="Data control">Take control over your data</h2>
<p>By using the embed code, you can also have full control over the data of your form. That way you can for example use the benefits of the Tripetto platform, but save the collected data only to your own back office, without it ever being saved to the Tripetto infrastructure. This can be very important with regards to <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/" target="_blank">privacy legislation, like GDPR</a>.</p>
<p>Please have a look at <a href="{{ page.base }}help/articles/how-to-take-control-over-your-data-from-the-studio/" target="_blank">this specific article for more information on full data control from your embedded forms</a>.</p>
