---
layout: help-article
base: ../../../
permalink: /help/articles/let-your-form-save-and-restore-uncompleted-entries/
title: Let your form save and restore uncompleted entries - Tripetto Help Center
description: Tripetto forms can automatically save and restore uncompleted forms, so your respondents can continue where they left off.
article_title: Let your form save and restore uncompleted entries
author: mark
time: 2
category_id: sharing
subcategory: sharing_uncompleted
areas: [studio, wordpress]
---
<p>Tripetto forms can automatically save and restore uncompleted forms, so your respondents can continue where they left off. For example handy when you have a support form that's on each page in your website.</p>

<h2 id="how-it-works">How it works</h2>
<p>If the save and restore functionality is enabled, the form will save a snapshot of the given answers inside the local storage of the respondent's browser. If the form is loaded again, the form will use that local storage snapshot to fill out the known answers and take the respondent back to the place where they left the form.</p>
<p>As we use the local storage of the browser, this will only work when the respondent uses the same browser when they revisit the form.</p>

<h2 id="shareable-links" data-anchor="In shareable links">Save and restore in shareable links</h2>
<p>If your respondents use the form from a shareable link, the save and restore functionality is not available.</p>

<h2 id="embed" data-anchor="In embedded forms">Save and restore in embedded forms</h2>
<p>If you choose to embed your form, you can manually activate the save and restore functionality inside the embed code.</p>
