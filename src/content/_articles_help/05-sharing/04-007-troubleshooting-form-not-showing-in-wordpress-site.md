---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-form-not-showing-in-wordpress-site/
title: Troubleshooting - Form not showing in WordPress site - Tripetto Help Center
description: In some situations your form might not show the right way in your WordPress site. This article contains some troubleshooting for this.
article_title: Troubleshooting - Form not showing in WordPress site
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>Based on the experiences of our users we're aware of some situations where your embedded form does not show the way you want it to. In this article you can find the known issues with this.</p>

<h2 id="plugin">Check your plugin version</h2>
<p>First of all, check which version of the Tripetto plugin you're on. You can find which version you're on the Plugins section in your WP Admin. Check our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/" target="_blank">article about updating the plugin</a> if you're not on the latest release version.</p>

<h2 id="shortcode">Check your shortcode</h2>
<p>The shortcode you use to embed the form in your WP site has some parameters you can use to influence the display of the form. You can find those parameters in <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site/" target="_blank">this article</a>. You can try to see if these parameters make any difference, for example by prodiving a <code>width</code> and/or <code>height</code> parameter to your shortcode.</p>

<h2 id="conflicts">Check conflicting plugins</h2>
<p>WordPress offers lots of plugins, so we can't prevent all conflicts with other plugins, as everybody uses a different set of plugins. Based on some user feedback we are aware of conflicts with the following (type of) plugins.</p>
<ul>
  <li><b>Optimize, minify and bundle plugins</b><br/>All of these type of plugins might mess up some scripts of the Tripetto plugin. An example of such a conflicting plugin is Autoptimizer.<br/>To solve this, in most plugins you can exclude the Tripetto scripts from the plugin, preventing it to influence the Tripetto scripts. How to do this, depends on each particular plugin.</li>
</ul>
<hr />

<h2>Still not working?</h2>
<p>Don't hesitate to <a href="{{ page.base }}contact/" target="_blank">contact us</a> if you have any problems with the WordPress plugin. <a href="javascript:$crisp.push(['do', 'chat:open']);">Using the chat</a> will be the fastest and easiest way to do so. We're happy to help you!</p>
