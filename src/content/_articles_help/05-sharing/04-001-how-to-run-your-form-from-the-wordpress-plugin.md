---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-run-your-form-from-the-wordpress-plugin/
title: How to run your form from the WordPress plugin - Tripetto Help Center
description: In the WordPress plugin you can share a link to your form, or embed the form into your own WordPress site with the shortcode.
article_title: How to run your form from the WordPress plugin
author: jurgen
time: 1
category_id: sharing
subcategory: sharing_methods
areas: [wordpress]
popular: true
---
<p>In the WordPress plugin you can share a link to your form, or embed the form into your own WordPress site with the shortcode.</p>

<h2 id="methods">Run methods</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there you see two methods to run your forms:</p>
<ul>
  <li><h3 id="share">Shareable link</h3>This is a direct link to your form within your own WordPress site. You can just share that link with your audience and collect your data. All data will be stored inside your own WordPress instance. See <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/" target="_blank">this article for more information on the shareable link</a>.</li>
  <li><h3 id="embed">WordPress shortcode</h3>This is the shortcode to easily embed your form inside your WordPress site. You can easily compose your shortcode with the settings you need. All data will be stored inside your own WordPress instance. See <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site/" target="_blank">this article for more information on embedding</a>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/wordpress-share/00-wp-share.png" alt="Screenshot of sharing in Tripetto" />
  <figcaption>The Share screen in the WordPress plugin.</figcaption>
</figure>

<h2 id="data" data-anchor="Data control">Data control in the WordPress plugin</h2>
<p>Both the form and collected data are hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
