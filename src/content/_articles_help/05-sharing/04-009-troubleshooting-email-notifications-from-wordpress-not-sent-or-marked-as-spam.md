---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/
title: Troubleshooting - Email notifications from WordPress not sent or marked as spam - Tripetto Help Center
description: If you use email notifications from your forms in WordPress, it can happen those are not sent properly, or marked as spam. This article contains some troubleshooting for this.
article_title: Troubleshooting - Email notifications from WordPress not sent or marked as spam
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>If you use email notifications from your forms in WordPress, it can happen those are not sent properly, or marked as spam. This article contains some troubleshooting for this.</p>

<h2 id="background">Emails from Tripetto</h2>
<p>Tripetto offers a few possibilities to send mails:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/" target="_blank">Automated email notification</a> of new entries;</li>
  <li>Emails from the form itself, created with the <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank">send email block</a>.</li>
</ul>
<p>In WordPress it can happen these emails get marked as spam or are not sent at all. Let us explain why that can happen.</p>

<h2 id="background">Known issues</h2>
<p>As every WordPress instance is different (from server setup to plugins and themes), the Tripetto plugin itself can not handle the email sending itself. That's why the plugin uses the <a href="https://developer.wordpress.org/reference/functions/wp_mail/" target="_blank">WordPress standards regarding email functionality</a>: the <code>wp_mail()</code> function.</p>
<p>By default, the <code>wp_mail()</code> function of WordPress uses the <a href="https://github.com/PHPMailer/PHPMailer" target="_blank">PHPMailer function</a> to create and send unauthenticated emails from the webserver.</p>
<p>Issues with the default WordPress mailer function can be caused by:</p>
<ul>
  <li>An incorrect or non-existing WordPress mailer configuration;</li>
  <li>Your hosting platform not allowing the use of PHP for sending emails;</li>
  <li>Incorrect or non-existing DNS records, resulting in your webserver not being allowed to send emails on behalf of your domain;</li>
  <li>The use of an out-of-date or misconfigured plugin that replaces the default WordPress mailer function.</li>
</ul>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>See the below recommendations to troubleshoot mailing issues in WordPress.</p>

<h3 id="configuration" data-anchor="WP mail configuration">Check your WP mail configuration</h3>
<p>First of all, make sure your WP installation is able to send mails using the default WordPress mailer function the correct way. Tripetto uses this email function to send your emails from WordPress. In fact, the Tripetto plugin itself does nothing to send emails; all is done by your own WP mail configuration. That's why it is important you know for sure your WP installation is able to send emails.</p>
<blockquote>
  <h4>Test your configuration</h4>
  <p>You can use other WP plugins to test if your WP installation is able to send mails, for example <a href="https://wordpress.org/plugins/check-email/" target="_blank">this plugin</a> (install and use on your own responsibility).</p>
</blockquote>
<p>If you're absolutely sure your WP installation is able to mail, but still don't receive the emails, please have a look at the below suggestions.</p>

<h3 id="spam" data-anchor="Spam">Check your spam folder</h3>
<p>Check your spam folder of your recipient email. Due to various reasons it can happen your email client marks the email as spam.</p>

<h3 id="version" data-anchor="Tripetto plugin version">Check your Tripetto plugin version</h3>
<p>Check which version of the Tripetto plugin you're on. You can find which version you're on the Plugins section in your WP Admin. Check our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/" target="_blank">article about updating the plugin</a> if you're not on the latest release version.</p>

<h3 id="other">Other recommendations</h3>
<p>Lastly, you can try the following to solve issues:</p>
<ul>
  <li>Check your email logs to see if you can find any reasons why the emails are not sent or not received;</li>
  <li>Install, activate, and configure a plugin that replaces or extends the default WordPress mailer function. Most plugins use secure SMTP with authentication. Some plugins offer OAuth2.0 for authentication;</li>
  <li>Or use the WordPress API and extend the <code>wp_mail()</code> function to configure secure SMTP with authentication.</li>
</ul>

<blockquote>
<h4>Footnotes</h4>
  <p>Keep in mind that some solutions will store your email account details to either wp-config or the database, making it a potential risk when your website/web server gets compromised.</p>
  <p>We don’t recommend any specific plugins on this matter and don't provide support or instructions on how to use them.</p>
</blockquote>
<hr />

<h2>Still not working?</h2>
<p>If you're sure your WP installation is able to mail and the above suggestions don't work out for you, please <a href="{{ page.base }}contact/" target="_blank">contact us</a>. <a href="javascript:$crisp.push(['do', 'chat:open']);">Using the chat</a> will be the fastest and easiest way to do so. We're happy to help you!</p>
