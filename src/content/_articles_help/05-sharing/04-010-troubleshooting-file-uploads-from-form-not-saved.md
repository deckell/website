---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-file-uploads-from-form-not-saved/
title: Troubleshooting - File uploads from form not saved - Tripetto Help Center
description: If you use a file upload block in your form, it can happen the files aren't saved properly. This article contains some troubleshooting for this.
article_title: Troubleshooting - File uploads from form not saved
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>If you use a file upload block in your form, it can happen the files aren't saved properly. This article contains some troubleshooting for this.</p>

<h2 id="plugin">Check your plugin version</h2>
<p>First of all, check which version of the Tripetto plugin you're on. You can find which version you're on the Plugins section in your WP Admin. Check our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/" target="_blank">article about updating the plugin</a> if you're not on the latest release version.</p>

<h2 id="file-upload-block">Check your file upload block</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-block-file-upload/00-file-upload.png" alt="Screenshot of file upload block in Tripetto" />
  <figcaption>Advanced options of the file upload block.</figcaption>
</figure>
<p>Next, check the settings of your file upload block. It offers multiple settings to let you control what people can upload. Maybe the maximum file size you entered is not sufficient, or a certain file type is not on your list of allowed file types.</p>
<p>For more information on these options, please have a look at <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/" target="_blank">this article about the options of the file upload block</a>.</p>

<h2 id="settings">Check your WordPress upload settings</h2>
<p>Your WordPress installation also has a few settings for uploading. Some of these can influence what's allowed to upload to your WordPress instance, for example the maximum upload size.</p>
<p>How to access and edit those settings differs per WordPress installation, so you'd have to see how you can do that for your case.</p>

<h2 id="conflicts">Check conflicting plugins</h2>
<p>Check in your list of plugins if you have activated a plugin that prevents uploading, like a firewall plugin. If so, try to see if it makes a difference if you disable that plugin. Possibly you can make an exception for Tripetto uploads, or you can edit the settings of the plugin to allow uploads.</p>
<hr />

<h2>Still not working?</h2>
<p>Don't hesitate to <a href="{{ page.base }}contact/" target="_blank">contact us</a> if you have any problems with the WordPress plugin. <a href="javascript:$crisp.push(['do', 'chat:open']);">Using the chat</a> will be the fastest and easiest way to do so. We're happy to help you!</p>
