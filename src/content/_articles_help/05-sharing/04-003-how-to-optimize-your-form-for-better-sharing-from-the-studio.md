---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-optimize-your-form-for-better-sharing/
title: How to optimize your form for better sharing - Tripetto Help Center
description: You can optimize your form for better sharing it across the web by adding a title, description and keywords to it.
article_title: How to optimize your form for better sharing
article_folder: editor-properties
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_methods
areas: [studio]
---
<p>You can optimize your form for better sharing it across the web by adding a title, description and keywords to it. That information will be used inside previews when you share the link to your form.</p>

<h2 id="properties">Open form properties</h2>
<p>To see the the form properties, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Properties</code>.</p>

<h2 id="meta-data">Add form information</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-properties.png" alt="Screenshot of form properties in Tripetto" />
  <figcaption>The form properties pane.</figcaption>
</figure>
<p>You can now add/edit the following form information:</p>
<ul>
  <li><strong>Name</strong> - Supply the name of your form. This gets handy for yourself to be able to find the form later on in Tripetto. But the name is also visible for respondents when they see a share preview, for example when you share the form's URL on Whatsapp, Twitter, Facebook, etc.;</li>
  <li><strong>Description</strong> - Supply the description of your form. This also gets used in the share preview of your form, for example when you share the form's URL on Whatsapp, Twitter, Facebook, etc.;</li>
  <li><strong>Keywords</strong> - Supply keywords that describe the content/subject of your form. You can add multiple keywords, separated with a comma (<code>,</code>) or semicolon (<code>;</code>). If your form is a public form search engines like Google can use these keywords to determine what your form is about;</li>
  <li><strong>Language</strong> - Select the language that you have used in the texts of your form. This also enables search engines like Google to understand the content of your form. This setting also has its effect on the translations of your form. For more information <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/" target="_blank">see this help article about translations</a>.</li>
</ul>

<h2 id="preview">Preview in Slack</h2>
<p>Now, if we share the link to the form in the screenshot above, you'll see your title and description will be used in the link preview, for example in Slack.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-preview.png" alt="Screenshot of Slack" />
  <figcaption>Example of a preview in Slack.</figcaption>
</figure>
