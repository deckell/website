---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-schedule-or-close-the-availability-of-your-form/
title: How to schedule or close the availability of your form - Tripetto Help Center
description: Does your form have a certain start and end date to collect responses? Or did you collect sufficient submissions? Then you can schedule or close the availability of your form.
article_title: How to schedule or close the availability of your form
article_folder: planning
author: martijn
time: 3
category_id: sharing
subcategory: sharing_planning
areas: [studio, wordpress]
---
<p>Does your form have a certain start and end date to collect responses? Or did you collect sufficient submissions? Then you can schedule or close the availability of your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>In some situations you want to prevent respondents to fill out your form. A few of those situations are:</p>
<ul>
  <li>Your form may only be entered after a certain date (start date);</li>
  <li>Your form may only be entered before a certain date (closing date);</li>
  <li>Your form may only be entered in between a certain date range (start and closing date);</li>
  <li>You have collected sufficient results and your form has to be closed.</li>
</ul>

<h2 id="how-to-use">How to use</h2>
<p>To prevent a form from being able to submit, we have two question types available:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-force-stop-block/" target="_blank">Force stop block</a> - Stops the form and optionally shows an explanation text;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-raise-error-block/" target="_blank">Raise error block</a> - Stops the form and optionally shows an error text.</li>
</ul>
<p>By adding one of these blocks to your form, the form will not be able to continue and thus preventing form submissions. You can use that behavior to schedule or close your form.</p>

<h3 id="close">Close availability</h3>
<p>The easiest use case is simply closing your form. To do so, you just add a force stop block or a raise error block at the start of your form. This will immediately stop your form and you can show a message to your visitors why the form is closed.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/closed.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a form that is closed.</figcaption>
</figure>

<h3 id="schedule">Schedule availability</h3>
<p>If your form is only available from a certain date or for a certain date range, you can use some smart logic to achieve that. To do so, we need a few steps:</p>
<ol>
  <li><strong>Get respondent's timestamp</strong> - To be able to verify your schedule we first need to let our form know the current date and time of a respondent. To do so, we add a <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/" target="_blank">hidden field block</a> to gather the current timestamp, by setting it up like this:
    <ul>
      <li>Name: <code>Respondent's timestamp</code> (or any other name you'd like);</li>
      <li>Type: <code>Timestamp</code>.</li>
    </ul>
  </li>
  <li><strong>Set form availability</strong> - Now create a logic branch and add the branch conditions to match when the form is NOT available. To do so you can compare the timestamp in your hidden field with your desired conditions, for example:
    <ul>
      <li><code>Respondent's timestamp</code> is before <code>your start date</code>;</li>
      <li><code>Respondent's timestamp</code> is after <code>your closing date</code>;</li>
      <li><code>Respondent's timestamp</code> is before <code>your start date</code> or <code>Respondent's timestamp</code> is after <code>your closing date</code>.</li>
    </ul>
  </li>
  <li><strong>Stop the form</strong> - Lastly add a force stop block or a raise error block inside the branch, so that will be executed when the branch is entered and your form stops. If the branch does not get entered (so your form is active), the form will just continue with your first question.</li>
</ol>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/schedule.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a form that is scheduled between April 1st and May 1st.</figcaption>
</figure>
