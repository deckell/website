---
base: ../
---

<section class="examples-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-lg-9 shape-before shape-after">
        <h1>Examples</h1>
        <p>The true power of Tripetto is best experienced when everything comes together in a beautiful, hypersmart form or survey. <strong>Click any example to run, or even edit it.</strong></p>
      </div>
    </div>
  </div>
</section>
