---
base: ../
---

<section class="examples-list">
  <div class="container">
    <div class="row">
      <div class="col">
        <ul>
          {% for article in site.articles_examples %}
          <li onclick="window.location='{{ article.url }}';">
              <div class="example-list-preview example-list-{{ article.example_id }}">
                <div class="example-list-play palette-{{ article.example_face }}"><i class="fas fa-play"></i></div>
              </div>
              <div class="example-list-info">
                <div>{% include icon-face.html face=article.example_face size='small' name=article.example_face_name template='background' %}</div>
                <div>
                <small>{{ article.example_face_name }} form</small>
                <a href="{{ article.url }}"><h2>{{ article.article_title }}</h2></a>
                </div>
              </div>
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col examples-list-footer">
        <a href="{{ page.base }}tutorials/" class="hyperlink"><span>See how to build forms and surveys like these</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
