---
base: ../../
---

{% assign faq_id = 0 %}
{% assign faq = site.data.help-faq | where_exp: "item", "item.areas contains 'wordpress'" %}

<section class="help-faq">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-xl-7">
        <h2>FAQ</h2>
        <div class="accordion" id="faq">
          {% for item in faq %}
          <div class="card">
            <div class="card-header" id="faq_heading{{ faq_id }}"><a href="#" class="collapsed" role="button" data-toggle="collapse" data-target="#faq_collapse{{ faq_id }}" aria-expanded="true" aria-controls="faq_collapse{{ faq_id }}"><h3>{{ item.question }}</h3><i class="fas fa-chevron-right"></i></a></div>
            <div id="faq_collapse{{ faq_id }}" class="collapse" aria-labelledby="faq_heading{{ faq_id }}" data-parent="#faq"><div class="card-body">{{ item.answer }}</div></div>
          </div>
          {% assign faq_id = faq_id | plus: 1 %}
          {% endfor %}
        </div>
      </div>
    </div>
  </div>
</section>
