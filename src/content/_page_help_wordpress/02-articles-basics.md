---
base: ../../
---

{% assign articles = site.articles_help | where_exp: "item", "item.areas contains 'wordpress' and item.category_id == 'basics'" %}

<section class="help-tiles">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2>WordPress Plugin Basics</h2>
        <ul class="tiles">
          {% for article in articles %}
            {% include tile.html url=article.url title=article.article_title palette-top='light' palette-bottom='light' palette-title=article.category_id time=article.time time_video=article.time_video description=article.description %}
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
