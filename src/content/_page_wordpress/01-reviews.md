---
base: ../
---

{% assign reviews = site.data.reviews | where_exp: "item", "item.wordpress_slider == true" %}

<section class="wordpress-reviews content reviews">
  <div class="container-fluid ticker-holder">
    <div class="row">
      <ul class="ticker-reviews reviews">
          {% for item in reviews %}
          <li>
            <div class="review-palette-{{ item.wordpress_color }}">
              <div class="review-rating">
              {% for i in (1..item.rating) %}
                <i class="fas fa-star"></i>
              {% endfor %}
              </div>
              <a href="{{ item.url }}" target="_blank"><h4>{{ item.title }}</h4></a>
              <p>{{ item.text }}</p>
              <div class="review-footer">
                <span>{{ item.author }}</span>
                <small>{{ item.date }}</small>
              </div>
            </div>
          </li>
          {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col shape-after">
        <a href="{{ page.base }}reviews/" class="hyperlink"><span>Read all reviews</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/ticker.js?v={{ site.cache_version }}"></script>
<script src="{{ page.base }}js/reviews.js?v={{ site.cache_version }}"></script>
