---
base: ../
---

<section class="wordpress-features content matrix">
  <div class="container">
    <div class="row">
      <div class="col-12 shape-after">
        <h2>Features Overview</h2>
      </div>
    </div>
    {% assign groups = site.data.features-matrix-groups | where_exp: "item", "item.show contains 'wordpress'" %}
    {% for group in groups %}
    {% assign features = site.data.features-matrix | where_exp: "item", "item.group == group.id" | where_exp: "item", "item.show contains 'wordpress'" %}
    {% if features.size > 0 %}
    <div class="row">
      <div class="col-12">
        <div class="matrix-title"><h3>{{ group.title }}</h3></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-body">
            <tbody>
            {% for feature in features %}
              <tr>
                <td class="matrix-body-row"><h4>{% if feature.icon %}{% assign icon_url = "icons/" | append: feature.icon %}{% include {{ icon_url }}.html %}{% endif %}{% if feature.image %}<span><img src="{{ page.base | append: feature.image }}" alt="{{ feature.title }}" /></span>{% endif %}<span>{{ feature.title }}</span></h4>{% if feature.subtitle %}<small>{{ feature.subtitle }}</small>{% endif %}</td>
                <td>
                {% if feature.wordpress == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes</span></div>
                {% elsif feature.wordpress == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.wordpress }}</div>
                {% endif %}
                {% if feature.wordpress_subtitle %}
                  <small>{{ feature.wordpress_subtitle }}</small>
                {% endif %}
                {% if feature.wordpress_premium == true %}
                  <small class="premium"><a href="{{ page.base }}pricing/wordpress/" target="_blank"><i class="fas fa-crown fa-fw"></i>Premium feature</a></small>
                {% endif %}
                </td>
              </tr>
            {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {% endif %}
    {% endfor %}
    <div class="row">
      <div class="col-12">
        <ul class="hyperlinks">
          <li><a href="{{ page.base }}pricing/wordpress/" class="hyperlink"><span>See plugin pricing</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="{{ page.base }}features/" class="hyperlink"><span>Compare to studio web app</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<script src="{{ page.base }}js/matrix.js?v={{ site.cache_version }}"></script>
