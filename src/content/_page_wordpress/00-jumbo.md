---
base: ../
---

<section class="wordpress-jumbo block-first jumbo">
  <div class="container jumbo-container">
    <div class="row">
      <div class="col jumbo-holder palette-wordpress shape-after">
        <div class="jumbo-content">
          <h1>Tripetto as WordPress plugin.</h1>
          <p>Entirely build, run and store customizable conversational forms and surveys inside your WordPress exclusively with the Tripetto plugin.</p>
          <div>
            <a href="{{ site.url_wordpress_plugin }}" class="button" target="_blank"><img src="{{ page.base }}images/tripetto-logo-white.svg" alt="Logo Tripetto"/>Get free plugin</a>
            <span class="button button-outline button-wordpress-purchase" role="button">Buy premium</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col jumbo-footer jumbo-footer-hyperlinks"><a href="{{ page.base }}studio/" class="hyperlink hyperlink-small"><span>Tripetto is also available as web app</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script src="{{ page.base }}js/wordpress-freemius.js?v={{ site.cache_version }}"></script>

