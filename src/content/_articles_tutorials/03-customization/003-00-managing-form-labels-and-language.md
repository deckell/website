---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/managing-form-labels-and-language/
title: Managing form labels and language - Tripetto Tutorials
description: A tutorial about editing and translating form messages and text labels.
article_title: Managing form labels and language
author: mark
time: 1
category_id: customization
common_content_core: true
---
<p>All labels inside Tripetto forms are fully editable and translatable, so the language exactly meets your users.</p>

<div class="article-content-core">
<h2 id="labels">Form labels</h2>
<p>Next to the blocks you add to your create your form, there are several labels inside the form. Think of buttons, placeholders and messages. All these labels are editable, so you can change a single label to a better suiting word for your form, or even translate all those labels into the desired language.</p>

<h2 id="translations">Translations</h2>
<p>Translations are even more flexible than just one simple translation per form. Tripetto forms can recognize automatically which language suits a respondent best and based on that knowledge use the right translated labels.</p>
<p>Of course you also can set a fixed language, so all your respondents will see the same labels.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find a more detailed help article about translating form labels:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'translations'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
