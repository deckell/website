---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/styling-forms-and-surveys/
title: Styling forms and surveys - Tripetto Tutorials
description: A tutorial about customizing fonts, colors and other style settings.
article_title: Styling forms and surveys
author: jurgen
time: 1
category_id: customization
common_content_core: true
---
<p>It's important you can style your forms and surveys exactly to your needs, so Tripetto helps you with this.</p>

<div class="article-content-core">
<h2 id="styling">Common styling</h2>
<p>Styling can help to give your respondents the right experience in your forms. Next to the fact it's fun to play with the styling, it can also be very important in case you're tied to a style guide or branding rules.</p>
<p>In general you can use the following styling options:</p>
<ul>
  <li>Colors;</li>
  <li>Fonts (including all Google Fonts);</li>
  <li>Background (color or image);</li>
  <li>Inputs;</li>
  <li>Buttons.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/styling/demo-whatsapp.gif" alt="Screenshot of styling in Tripetto" />
  <figcaption>Preview of styling a chat form face to a Whatsapp experience.</figcaption>
</figure>

<h2 id="styling">Form face styling</h2>
<p>The mentioned styling options will be used in all different form faces we offer. So you can always switch between form faces and your styling will be persistant.</p>
<p>Next to that, each form face has some additional settings/options to finetune the experience of that specific form face.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about styling your forms:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'styling' or item.article_id == 'form_face'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
