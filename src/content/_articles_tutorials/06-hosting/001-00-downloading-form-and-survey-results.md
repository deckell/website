---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/downloading-form-and-survey-results/
title: Downloading form and survey results - Tripetto Tutorials
description: Learn how to extract collected form and survey data.
article_title: Downloading form and survey results
author: martijn
time: 2
time_video: 1
category_id: hosting
common_content_core: true
---
<p>Of course you can extract the collected data from Tripetto so you can analyze it further in your favorite data tool.</p>

<div class="article-content-core">
<h2 id="export">Data export</h2>
<p>You can export and download all data of your entries instantly to a CSV file. CSV files can easily be opened in a spreadsheet editor like Office Excel and Google Sheets for basic analysis. Or analyze the data even deeper in your own favorite data analysis tool like SPSS or Tableau.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about the data export:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'download-studio'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top='studio' category=data_category.name palette-category=articles.category_id type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'download-wordpress'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top='wordpress' category=data_category.name palette-category=articles.category_id type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'download-csv'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
