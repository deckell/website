---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/performing-actions-inside-your-forms-and-surveys/
title: Performing actions inside your forms and surveys - Tripetto Tutorials
description: A tutorial about action blocks to let your forms and surveys perform certain actions.
article_title: Performing actions inside your forms and surveys
author: jurgen
time: 2
category_id: logic
common_content_core: true
---
<p>With action blocks you can let your form perform certain actions, like performing a calculation, using a variable, or sending an automated email.</p>

<div class="article-content-core">
<h2 id="concept">Concept</h2>
<p>Action blocks can be added like 'normal' question blocks. You can add them anywhere in your form and for an unlimited amount.</p>
<p>The difference is that an action block does not ask for a certain input from the respondent, but it performs a certain <strong>action</strong> in the background. This makes your forms and surveys even more powerful.</p>

<h2 id="action_blocks">Action blocks</h2>
<p>The following type of action blocks are available:</p>
<ul>
  <li>Calculation actions;</li>
  <li>Data actions;</li>
  <li>Email actions;</li>
  <li>Stop actions.</li>
</ul>

<h3 id="calculation">Calculation actions</h3>
<p>The <strong>calculator block</strong> is a very powerful action block that lets you perform calculations with given answers inside the form. Calculator blocks are fully flexible. You can use the outcomes anywhere in the form. Simply by showing the outcome to your respondent, but also to perform follow-up calculations.</p>

<h3 id="date">Data actions</h3>
<p>There are several data action blocks that let you gather and control data inside your form, ideal for no-code form solutions:</p>
<ul>
  <li>The <strong>custom variable block</strong> lets you store a hidden value inside your form, for example a text, number, date or boolean value;</li>
  <li>The <strong>hidden field block</strong> lets the form gather certain data that you can use inside the form, for example a variable from the query string in the URL;</li>
  <li>The <strong>set value block</strong> can change values and variables inside your form, for example to prefill values or to lock certain entered values.</li>
</ul>

<h3 id="email">Email actions</h3>
<p>The <strong>send email block</strong> sends an automated email to a fixed email address, or even to an email address that your respondent entered in the form. And you can use all entered form data directly in the email.</p>

<h3 id="stop">Stop actions</h3>
<p>There are two blocks that can prevent the form from being completed. Why would you want that? Well, imagine a respondent does not meet your requirements to fill out a certain form, based on the given answers. To keep your results clean, you can prevent them from completing the form.</p>
<ul>
  <li>The <strong>force stop block</strong> disables the form to continue and shows a normal message;</li>
  <li>The <strong>raise error block</strong> disables the form to continue and shows an error message.</li>
</ul>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about action blocks inside your forms:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'action-blocks-calculator' or item.article_id == 'action-blocks'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
