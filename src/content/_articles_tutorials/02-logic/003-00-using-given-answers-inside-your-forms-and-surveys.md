---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/using-given-answers-inside-your-forms-and-surveys/
title: Using given answers inside your forms and surveys - Tripetto Tutorials
description: A tutorial about piping logic to show respondent's answers.
article_title: Using given answers inside your forms and surveys
author: jurgen
time: 2
category_id: logic
common_content_core: true
---
<p>Make your forms and surveys feel personal by using the given answers in your form's content.</p>

<div class="article-content-core">
<h2 id="branch_logic">Pipe logic</h2>
<p>You can use given answers anywhere inside your forms and surveys. At Tripetto we call this <strong>pipe logic</strong>.</p>
<p>It enables you for example to show a summary of all given answers inside the form, the closing message, or even inside a confirmation mail. Or give your form a personal touch by asking for the respondent's name and then greet your respondent.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about pipe logic:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'logic-piping'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
