---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/showing-flexible-closing-messages-based-on-given-answers/
title: Showing flexible closing messages based on given answers - Tripetto Tutorials
description: A tutorial about the possibility to determine which closing message should be shown, based on the given answers of each respondent.
article_title: Showing flexible closing messages based on given answers
author: jurgen
time: 2
category_id: logic
common_content_core: true
---
<p>Tripetto lets you create unlimited flexible closing messages, that will be shown based on the given answers of each respondent.</p>

<div class="article-content-core">
<h2 id="closing_messages">Common closing message</h2>
<p>The closing message is shown right after a respondent completes the form. You can always show a common message that everyone will see after they completed the form.</p>

<h2 id="flexible_messages">Flexible closing messages</h2>
<p>But if you're using logic inside your form, it's also possible to differentiate the closing message, based on the given answers of each respondent. By doing so, you can show a certain closing message to one respondent, and another closing message to a respondent that answered your questions differently.</p>

<h2 id="redirects">Redirects</h2>
<p>It's also possible to automatically redirect to another URL after form completion. This is also fully flexible, so you can add flexible redirects based on the given answers.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about flexible closing messages:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'closing'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
