---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/the-logic-basics/
title: The logic basics - Tripetto Tutorials
description: A tutorial about making forms and surveys conversational with logic.
article_title: The logic basics
author: jurgen
time: 3
category_id: logic
common_content_core: true
---
<p>To make your forms and surveys feel like conversations, you need the right logic. Tripetto is built up from the base to help you with that!</p>

<div class="article-content-core">
<h2 id="storyboard">Storyboard</h2>
<p>The <strong>storyboard</strong> of Tripetto helps you to add and maintain logic flows in your forms and surveys. You can use all logic in all your forms without any limitations.</p>

<h2 id="types">Logic types</h2>
<p>We divide logic in 3 different types:</p>
<ul>
  <li><strong>Branch logic</strong>, to determine the follow-up, based on given answers;</li>
  <li><strong>Jump logic</strong>, to jump to other parts of the form, based on given answers;</li>
  <li><strong>Pipe logic</strong>, to show earlier given answers.</li>
</ul>

<h3 id="branch_logic">Branch logic</h3>
<p>Branch logic helps you to only ask the right questions to your respondents. Based on given answers your form can determine what's the right follow-up.</p>

<h3 id="jump_logic">Jump logic</h3>
<p>Jump logic helps you to jump over a certain part of a form if that's not applicable, based on the given answers.</p>

<h3 id="pipe_logic">Pipe logic</h3>
<p>Pipe logic lets you show given answers inside your forms and surveys. This helps to make your forms and surveys more personal.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about logic:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'logic-basics' or item.article_id == 'logic-piping'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
