---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/performing-calculations-inside-your-forms-and-surveys/
title: Performing calculations inside your forms and surveys - Tripetto Tutorials
description: A tutorial about the calculator block that lets you perform all kinds of smart calculations inside your forms and surveys.
article_title: Performing calculations inside your forms and surveys
author: jurgen
time: 3
category_id: logic
common_content_core: true
---
<p>Possibly the most powerful logic feature in Tripetto is the calculator for creating tests, quizzes, quotes, assessments, wizards and so much more.</p>

<div class="article-content-core">
<h2 id="concept">Concept</h2>
<p><a href="{{ page.base }}no-code-calculations-with-the-calculator-block/" target="_blank">The calculator</a> is one of our most advanced action blocks. It can perform all kinds of calculations using the answers that your respondent has given inside your form. This way you can create tests, quizzes, quotes, assessments, wizards, or anything else you can think of that needs calculations.</p>

<h2 id="calculations">Calculations</h2>
<p>Calculations are very powerful, flexible and intelligent. You are in full control of what each calculator block calculates, thanks to the flexible operations and powerful operation values.</p>

<h3 id="operations">Operations</h3>
<p>The calculator blocks enables you to do all possible calculation operations in your form:</p>
<ul>
  <li><code><i class="fas fa-plus fa-fw"></i> Add</code> - Add up a value;</li>
  <li><code><i class="fas fa-minus fa-fw"></i> Subtract</code> - Subtract a value;</li>
  <li><code><i class="fas fa-times fa-fw"></i> Multiply</code> - Multiply with a value;</li>
  <li><code><i class="fas fa-divide fa-fw"></i> Divide</code> - Divide by a value;</li>
  <li><code><i class="fas fa-equals fa-fw"></i> Equal</code> - Equal to a value.</li>
</ul>

<h3 id="values" data-anchor="Values">Operation values</h3>
<p>For each operation there are several methods available to use the right values in that operation:</p>
<ul>
  <li>Number;</li>
  <li>Blocks (given answers by your respondents);</li>
  <li>Scores;</li>
  <li>Comparisons;</li>
  <li>Functions;</li>
  <li>Constants;</li>
  <li>Subcalculations.</li>
</ul>

<h2 id="logic" data-anchor="Logic">Calculations with logic branches</h2>
<p>Tripetto is all about smart forms with the right logic. Of course the calculator block blends perfectly with the logic branches in your form, for example by being able to work with iterating branches and by using the calculator outcomes as branch conditions.</p>

<h2 id="outcomes">Outcomes</h2>
<p>The outcome of each calculator block is usable in different ways in your forms:</p>
<ul>
  <li>The outcome is available in all data exports, like the CSV export, email and Slack notifications and the webhook;</li>
  <li>It's also usable inside your form as the input for other calculator blocks;</li>
  <li>You can show the outcome in your form by using the piping value;</li>
  <li>You can use the outcome to determine branch conditions, so you can make logic desicions, based on the calculator outcome.</li>
</ul>

<hr/>
<h2>More information</h2>
<p>We made some overviews of the capabilities that the calculator block provides:</p>
</div>
<ul class="tiles tiles-two">
    {% assign landing_url = page.base | append: "no-code-calculations-with-the-calculator-block/" %}
    {% include tile.html url=landing_url target=true title='No-code calculations with the calculator block' description="Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block." palette-top='light' palette-bottom='light' type='Information' time=4 %}
    {% assign features_url = page.base | append: "all-calculator-features/" %}
    {% include tile.html url=features_url target=true title='All calculator features' description='A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.' palette-top='light' palette-bottom='light' type='Information' time=3 %}
</ul>
<div class="article-content-core">

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about the calculator block:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'action-blocks-calculator' or item.article_id == 'action-blocks-calculator-detail'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
