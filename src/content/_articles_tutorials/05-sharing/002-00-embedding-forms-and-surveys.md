---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/embedding-forms-and-surveys/
title: Embedding forms and surveys - Tripetto Tutorials
description: Learn how to embed forms and surveys into your own website or project.
article_title: Embedding forms and surveys
author: jurgen
time: 1
category_id: sharing
common_content_core: true
---
<p>All Tripetto forms are embeddable inside your website or projects with the embed code we provide.</p>

<div class="article-content-core">
<h2 id="embed">Embed code</h2>
<p>You can embed all your forms and surveys from Tripetto into your own website or projects. We provide you a ready-to-go embed code that you can simply copy and paste where you want to show the form.</p>

<h2 id="options">Embed options</h2>
<p>We've got some additiontal options for the embedding of your form, for example to determine if the form is pausable, if uncompleted forms may be stored locally and how you want to integrate the form into your website.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about embedding:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'embed-studio'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top='studio' category=data_category.name palette-category=articles.category_id type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'embed-wordpress'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top='wordpress' category=data_category.name palette-category=articles.category_id type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
