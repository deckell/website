---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/connecting-to-other-services-with-webhooks/
title: Connecting to other services with webhooks - Tripetto Tutorials
description: A tutorial about using webhooks to connect your data to other services.
article_title: Connecting to other services with webhooks
article_folder: studio-create-account
author: mark
time: 2
category_id: automations
common_content_core: true
---
<p>Webhooks can be very powerful to connect your received response data to other services. Tripetto lets you easily connect to webhooks.</p>

<div class="article-content-core">
<h2 id="webhooks">Webhooks</h2>
<p>Webhooks are the glue between the data that you collect in Tripetto and the automated actions you want to do with that data. This makes your Tripetto form even more valuable as you can integrate the data collection right into your processes.</p>
<p>A simple example of a webhook action would be to automatically add the form data to a Google Sheets row in a spreadsheet document. But you can also think of other actions, like database storage, automated email follow-ups, CRM integrations, marketing actions, etc.</p>

<h3 id="services">Automation tools</h3>
<p>To help you connect with other services, you need an automation tool. Basically that's the glue between Tripetto and other services. It receives your data via a webhook URL and sends that data to the third party service(s) that you want to connect with. Without a single line of code. Fully no-code!</p>
<p>There are lots of automation tools. Popular tools are <a href="https://integromat.com?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program" target="_blank"><strong>Integromat</strong></a>, <a href="https://pabbly.com/connect" target="_blank"><strong>Pabbly Connect</strong></a> and <a href="https://zapier.com" target="_blank"><strong>Zapier</strong></a>. Of course it depends on your needs/wishes/budget which tool fits you best.</p>

<h3 id="own">Custom webhook endpoint</h3>
<p>The webhook feature in Tripetto is a common webhook URL, so you can enter any webhook URL that supports incoming webhooks. In case you have your own custom webhook endpoint, you can just use that webhook URL.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find a more detailed help article about the webhook connection:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'webhook'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
