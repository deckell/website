---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/getting-automatic-email-notifications/
title: Getting automatic email notifications - Tripetto Tutorials
description: A tutorial about email alerts for form and survey completions.
article_title: Getting automatic email notifications
article_folder: studio-create-account
author: jurgen
time: 1
category_id: automations
common_content_core: true
---
<p>Tripetto can send you automatic email notifications for each new entry.</p>

<div class="article-content-core">
<h2 id="notification">Email notifications</h2>
<p>By enabling email notifications, Tripetto will send you an email for each newly completed form entry. This way you know what entries have come in, without logging into Tripetto.</p>
<h3 id="form_data">Form data</h3>
<p>To make this even more easy, you can also include all form data from the entry inside the email that you receive. That helps you to quickly get insights of the entry and determine if you have to take action.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find a more detailed help article about email notifications:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'email'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
