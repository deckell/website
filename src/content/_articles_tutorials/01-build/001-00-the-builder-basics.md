---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/the-builder-basics/
title: The builder basics - Tripetto Tutorials
description: A tutorial about how to smoothly control the builder’s assistive storyboard.
article_title: The builder basics
author: mark
time: 2
category_id: build
common_content_core: true
---
<p>The Tripetto form builder is a bit different from existing tools, but you'll know why and love it almost instantly when you spend a minute getting the hang of it.</p>

<div class="article-content-core">
<h2 id="builder">Form builder</h2>
<p>Our form builder is not just a list of questions. Our form builder helps you during the building process to let you create an amazing and smart form. To do so, we provide a <strong>storyboard</strong> and a <strong>live preview</strong>.</p>

<h2 id="storyboard">Storyboard</h2>
<p>As we want to make it easy to create smart forms with logic, we use a storyboard. This board gives a visual presentation of the flows inside your form, making it much better understandable what's happening in your form structure.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-conditions/04-device.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>The storyboard in action, showing 2 flows depending on the given answer.</figcaption>
</figure>

<h3 id="gestures">Gestures</h3>
<p>To navigate through the storyboard you can use several gestures:</p>
<ul>
  <li><strong>Arranging</strong> - Arrange blocks by dragging and dropping them;</li>
  <li><strong>Panning</strong> - Navigate through your form by holding and dragging the storyboard;</li>
  <li><strong>Zooming</strong> - Zoom in and out the storyboard.</li>
</ul>

<h2 id="preview">Live preview</h2>
<p>On the right side of the storyboard you see the live preview. This will always show a realtime preview of what you're building.</p>
<p>You can edit your form quickly by showing all questions underneath each other, or really test your form by executing the logic to your form immediately.</p>
<p>Of course all Tripetto forms are optimized for all modern devices. The preview helps you to see how your form looks on all those different screen sizes.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about our storyboard:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'builder' or item.article_id == 'preview'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
