---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/using-the-builder-to-create-forms-and-surveys/
title: Using the builder to create forms and surveys - Tripetto Tutorials
description: A tutorial about our unique form builder to create your forms.
article_title: Using the builder to create forms and surveys
author: jurgen
time: 3
category_id: build
common_content_core: true
---
<p>Inside the form builder you can manage everything to build your form's structure, using different kinds of form blocks.</p>

<div class="article-content-core">
<h2 id="blocks">Form blocks</h2>
<p>You build your form's content by adding form blocks. There are two kinds of form blocks, namely <strong>question blocks</strong> and <strong>action blocks</strong>.</p>
<h3 id="question_blocks">Question blocks</h3>
<p>Question blocks are blocks that contain an input control inside the form. Examples of question types are text (single and multiple line), multiple choice, checkboxes, radio buttons, date, matrix, file upload, etc.</p>
<h3 id="action_blocks">Action blocks</h3>
<p>Action blocks perform a certain action inside the form. Examples of actions are performing a calculation, sending an email or setting the value of a variable.</p>

<h2 id="logic">Logic</h2>
<p>Logic is an important part of the whole Tripetto platform. The key feature to use logic is the main reason our form builder works like this.</p>
<h3 id="branches">Branches</h3>
<p>To create the necessary logic for smart forms, you use so called <strong>branches</strong>. Branches are shown as separate tracks in the storyboard, giving you a clear overview of what paths a respondent can take.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-conditions/00-basic.png" alt="Screenshot of the form builder in Tripetto" />
  <figcaption>Example of a branch.</figcaption>
</figure>

<p>Each branch has 3 ways to determine what that branch should do.</p>
<h3 id="branch_conditions">Branch conditions</h3>
<p>Branch conditions let you determine when a branch meets your requirements. You can add endless branch conditions and combine those to get the perfect fit.</p>
<h3 id="branch_behavior">Branch behavior</h3>
<p>By setting the right branch behavior you determine how the branch should check if the branch conditions match. For example if only one of the conditions match, all conditions match, or each condition that matches.</p>
<h3 id="branch_endings">Branch endings</h3>
<p>At the end of each branch you can choose from different types of branch endings how the form should proceed after this branch.</p>

<h2 id="start_end">Start and end</h2>
<p>Next to the structure of your form that you build with blocks and logic, you can add a welcome and multiple closing messages. Those can help to introduce your form before it starts and giving your respondent personal closing messages, based on the answers they have given.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about our form builder:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'build'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url target=true title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
