---
base: ../
---

<section class="help-platforms">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2 class="help-platforms-title">Help Articles Per Platform</h2>
      </div>
    </div>
    <div class="row help-platforms-row">
      <div class="col-md-6 platform-holder">
        <div class="platform platform-clickable palette-studio" onclick="window.location='{{ page.base }}help/studio/';">
          <h2>Studio</h2>
          <p>Tripetto as web app</p>
          <div class="platform-image">
            <img src="{{ page.base }}images/platforms/studio.svg" alt="Illustration representing the help center for the studio">
            <a href="{{ page.base }}help/studio/" class="button button-full">Studio help center</a>
          </div>
        </div>
        <ul class="hyperlinks platform-hyperlinks">
          <li><a href="{{ page.base }}studio/" class="hyperlink hyperlink-small"><span>About the studio</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="{{ page.base }}pricing/studio/" class="hyperlink hyperlink-small"><span>Studio pricing</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
      <div class="col-md-6 platform-holder">
        <div class="platform platform-clickable palette-wordpress" onclick="window.location='{{ page.base }}help/wordpress/';">
          <h2>WP Plugin</h2>
          <p>Tripetto inside WordPress</p>
          <div class="platform-image">
            <img src="{{ page.base }}images/platforms/wordpress.svg" alt="Illustration representing the help center for the WordPress plugin">
            <a href="{{ page.base }}help/wordpress/" class="button button-full">Plugin help center</a>
          </div>
        </div>
        <ul class="hyperlinks platform-hyperlinks">
          <li><a href="{{ page.base }}wordpress/" class="hyperlink hyperlink-small"><span>About the WordPress plugin</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="{{ page.base }}pricing/wordpress/" class="hyperlink hyperlink-small"><span>Plugin pricing</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
