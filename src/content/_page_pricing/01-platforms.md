---
base: ../
---

<section class="pricing-platforms">
  <div class="container">
    <div class="row pricing-platforms-row">
      <div class="col-md-6 platform-holder">
        <div class="platform platform-clickable palette-studio" onclick="window.location='{{ page.base }}pricing/studio/';">
          <h2>Studio</h2>
          <p>Tripetto as web app</p>
          <div class="platform-image">
            <img src="{{ page.base }}images/platforms/studio.svg" alt="Illustration representing the pricing for the studio">
            <a href="{{ page.base }}pricing/studio/" class="button button-full">Studio pricing</a>
          </div>
        </div>
        <ul class="hyperlinks platform-hyperlinks">
          <li><a href="{{ page.base }}studio/" class="hyperlink hyperlink-small"><span>About the Tripetto studio</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
      <div class="col-md-6 platform-holder pricing-platform-hint">
        <div class="platform platform-clickable palette-wordpress" onclick="window.location='{{ page.base }}pricing/wordpress/';">
          <h2>WP Plugin</h2>
          <p>Tripetto inside WordPress</p>
          <div class="platform-image">
            <img src="{{ page.base }}images/platforms/wordpress.svg" alt="Illustration representing the pricing for the WordPress plugin">
            <a href="{{ page.base }}pricing/wordpress/" class="button button-full">Plugin pricing</a>
          </div>
        </div>
        <ul class="hyperlinks platform-hyperlinks">
          <li><a href="{{ page.base }}wordpress/" class="hyperlink hyperlink-small"><span>About the Tripetto WordPress plugin</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
