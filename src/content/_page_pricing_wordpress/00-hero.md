---
base: ../../
---

<section class="pricing-wordpress-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="breadcrumb-navigation">
          <a href="{{ page.base }}pricing/">Pricing</a>
          <i class="fas fa-chevron-right"></i>
          <div class="dropdown">
              <a role="button" class="dropdown-button active" id="pricingBreadcrumb" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">WordPress plugin<i class="fas fa-chevron-down"></i></a>
              <div class="dropdown-menu" aria-labelledby="pricingBreadcrumb">
                <a class="dropdown-item" href="{{ page.base }}pricing/studio/"><div>Tripetto Studio<small>at tripetto.app</small></div></a>
                <a class="dropdown-item" href="{{ page.base }}pricing/wordpress/"><div>Tripetto WP<small>WordPress plugin</small></div></a>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>WP Plugin</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8 shape-after">
        <p>The <strong>standard WordPress plugin is completely free</strong>. We offer premium upgrades for select features and removing Tripetto branding. There’s even an LTD 🚀!</p>
      </div>
    </div>
  </div>
</section>
