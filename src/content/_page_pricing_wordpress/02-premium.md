---
base: ../../
---

<section class="pricing-wordpress-premium" id="premium">
  <div class="container">
    <div class="row pricing-wordpress-intro">
      <div class="col-lg-10 col-xl-9">
      <h2>Premium upgrades</h2>
      <p>The free WordPress plugin is a fullblown form solution. On top of that, you can boost it with premium upgrades. <strong>Upgrades are per plugin and charged recurringly</strong>, except for the Lifetime Deal 🚀.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 premium-switch">
        <ul class="carousel-buttons">
          <li class="carousel-button-left active" data-target="#carouselPricingWordPress" data-slide-to="0">Single-site</li><li class="carousel-button-middle" data-target="#carouselPricingWordPress" data-slide-to="1" id="carouselPricingWordPressMulti">5-Sites</li><li class="carousel-button-right" data-target="#carouselPricingWordPress" data-slide-to="2" id="carouselPricingWordPressUnlimited">Unlimited</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-12 premium-plans">
        <div id="carouselPricingWordPress" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Monthly</h3>
                    <p>Most flexible, not cheapest.</p>
                    <div class="pricing-price palette-purple">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">9</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Per month.</span>
                        <span class="conditions-subtitle">Billed monthly.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Single-site installation</li>
                        <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Multisite installations<sup>1</sup></li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <span class="button button-full button-wordpress-purchase-single-monthly" role="button">Purchase monthly</span>
                    </div>
                    <small class="pricing-footer">1. Available as <span role="button" class="switch switch-multi">5-sites</span> or <span role="button" class="switch switch-unlimited">unlimited</span>.</small>
                  </div>
                </div>
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Annually</h3>
                    <p>Same. But 25% less than monthly.</p>
                    <div class="pricing-price palette-purple">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">7</div>
                        <div class="decimals">.49</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Per month.</span>
                        <span class="conditions-subtitle">Billed annually for $89.88.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Single-site installation</li>
                        <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Multisite installations<sup>1</sup></li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <span class="button button-full button-wordpress-purchase-single-yearly" role="button">Purchase annually</span>
                    </div>
                    <small class="pricing-footer">1. Available as <span role="button" class="switch switch-multi">5-sites</span> or <span role="button" class="switch switch-unlimited">unlimited</span>.</small>
                  </div>
                </div>
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Lifetime 🚀</h3>
                    <p>Hands down our best deal. Period.</p>
                    <div class="pricing-price palette-red">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">269</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Pay once.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Single-site installation</li>
                        <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Multisite installations<sup>1</sup></li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <img src="{{ page.base }}images/pricing/wordpress-lifetime.svg" alt="Illustration representing the lifetime deal for the WordPress plugin." />
                      <span class="button button-full button-red button-wordpress-purchase-single-lifetime" role="button">Purchase lifetime</span>
                    </div>
                    <small class="pricing-footer">1. Available as <span role="button" class="switch switch-multi">5-sites</span> or <span role="button" class="switch switch-unlimited">unlimited</span>.</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Monthly</h3>
                    <p>Most flexible, not cheapest.</p>
                    <div class="pricing-price palette-purple">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">24</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Per month.</span>
                        <span class="conditions-subtitle">Billed monthly.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>5-Sites installation</li>
                        <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Unlimited sites installation<sup>1</sup></li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <span class="button button-full button-wordpress-purchase-five-monthly" role="button">Purchase monthly</span>
                    </div>
                    <small class="pricing-footer">1. Also available as <span role="button" class="switch switch-unlimited">unlimited</span>.</small>
                  </div>
                </div>
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Annually</h3>
                    <p>Same. But 20% less than monthly.</p>
                    <div class="pricing-price palette-purple">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">19</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Per month.</span>
                        <span class="conditions-subtitle">Billed annually for $239.88.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>5-Sites installation</li>
                        <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Unlimited sites installation<sup>1</sup></li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <span class="button button-full button-wordpress-purchase-five-yearly" role="button">Purchase annually</span>
                    </div>
                    <small class="pricing-footer">1. Also available as <span role="button" class="switch switch-unlimited">unlimited</span>.</small>
                  </div>
                </div>
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Lifetime 🚀</h3>
                    <p>Hands down our best deal. Period.</p>
                    <div class="pricing-price palette-red">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">719</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Pay once.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>5-Sites installation</li>
                        <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Unlimited sites installation<sup>1</sup></li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <img src="{{ page.base }}images/pricing/wordpress-lifetime.svg" alt="Illustration representing the lifetime deal for the WordPress plugin." />
                      <span class="button button-full button-red button-wordpress-purchase-five-lifetime" role="button">Purchase lifetime</span>
                    </div>
                    <small class="pricing-footer">1. Also available as <span role="button" class="switch switch-unlimited">unlimited</span>.</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Monthly</h3>
                    <p>Most flexible, not cheapest.</p>
                    <div class="pricing-price palette-purple">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">66</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Per month.</span>
                        <span class="conditions-subtitle">Billed monthly.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited sites installation</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <span class="button button-full button-wordpress-purchase-unlimited-monthly" role="button">Purchase monthly</span>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Annually</h3>
                    <p>Same. But 20% less than monthly.</p>
                    <div class="pricing-price palette-purple">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">53</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Per month.</span>
                        <span class="conditions-subtitle">Billed annually for $647.88.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited sites installation</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <span class="button button-full button-wordpress-purchase-unlimited-yearly" role="button">Purchase annually</span>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 premium-plan">
                  <div>
                    <h3>Lifetime 🚀</h3>
                    <p>Hands down our best deal. Period.</p>
                    <div class="pricing-price palette-red">
                      <div class="pricing-price-prefix">
                        <div class="valuta">$</div>
                        <div class="amount">1949</div>
                        <div class="decimals">.99</div>
                      </div>
                      <div class="pricing-price-suffix">
                        <span class="conditions-title">Pay once.</span>
                      </div>
                    </div>
                    <div class="pricing-features-primary">
                      <ul class="pricing-features fa-ul">
                        <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}wordpress/" target="_blank">Everything in the free plan</a></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Slack notifications</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Webhooks to 1.000+ services</li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited sites installation</li>
                      </ul>
                    </div>
                    <div class="pricing-features-secondary">
                      <ul class="pricing-features fa-ul">
                        <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/wordpress/" target="_blank">help center</a><small>[24/7]</small></li>
                        <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
                      </ul>
                    </div>
                    <div class="purchase">
                      <img src="{{ page.base }}images/pricing/wordpress-lifetime.svg" alt="Illustration representing the lifetime deal for the WordPress plugin." />
                      <span class="button button-full button-red button-wordpress-purchase-unlimited-lifetime" role="button">Purchase lifetime</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/carousel-pricing-wordpress.js?v={{ site.cache_version }}"></script>
<script src="https://checkout.freemius.com/checkout.min.js"></script>
<script src="{{ page.base }}js/wordpress-freemius.js?v={{ site.cache_version }}"></script>


