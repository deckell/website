---
base: ../
---

<section class="reviews-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-9 shape-before">
        <h1>The truth is in the eye of the beholder</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-lg-7 col-xl-6 shape-after">
        <p>Read what users think about Tripetto and how they compare it to <strong>their own experiences</strong> with form and survey tools like Typeform, WP Forms and others.</p>
        <a href="{{ page.base }}product/" class="hyperlink"><span>Take the product tour</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
