---
base: ../
---

<section class="changelog-request" id="request-feature">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 col-xl-9">
        <h2>Request a feature</h2>
        <div id="TripettoRunner" class="runner-autoscroll-horizontal"></div>
      </div>
    </div>
  </div>
</section>
<script src="https://unpkg.com/tripetto-runner-foundation"></script>
<script src="https://unpkg.com/tripetto-runner-autoscroll"></script>
<script src="https://unpkg.com/tripetto-services"></script>
<script>
var tripetto = TripettoServices.init({ token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibFV6Q2ROWXVWYnEvcmZSRkxObERPQ25JcGtEV0VLbkRLUWlRUVNZSzRSMD0iLCJkZWZpbml0aW9uIjoicTVrSXNHaHVWaFFwZWxqYkVVK3lFZ0FlRVQrdjhsM2c3YUt1VktDcy9Bdz0iLCJ0eXBlIjoiY29sbGVjdCJ9.zCS32Q72vVUFAJG8SKirjw6PgiuvGWNeICD7gEp4mvQ" });
TripettoAutoscroll.run({
    element: document.getElementById("TripettoRunner"),
    definition: tripetto.definition,
    styles: tripetto.styles,
    l10n: tripetto.l10n,
    locale: tripetto.locale,
    translations: tripetto.translations,
    attachments: tripetto.attachments,
    onSubmit: tripetto.onSubmit
});
</script>
