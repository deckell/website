---
base: ../
---

<section class="changelog-intro roadmap block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-lg-9 col-xl-8 shape-before shape-after">
        <h1>Roadmap</h1>
        <p>Tripetto is a work in progress. Have a look at new features, improvements and bugfixes coming up. Or even <strong>put in a feature request</strong> if something’s missing.</p>
        <ul class="hyperlinks">
          <li><a href="#request-feature" class="hyperlink anchor"><span>Request a feature</span><i class="fas fa-arrow-down"></i></a></li>
          <li><a href="{{ page.base }}changelog/" class="hyperlink"><span>View changelog</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
