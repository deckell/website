---
base: ../../
---

<section class="help-articles-all">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h2>Articles for the studio</h2>
        {% assign categories = site.page_help_studio_categories %}
        {% for category in categories %}
        {% assign data_category = site.data.help-categories[category.category_id] %}
        <div class="category-title">
          <div>{% include icon-chapter.html chapter=category.category_id size='small' name=data_category.name %}</div>
          <h3>{{ data_category.name }}</h3>
        </div>
        <ul>
          {% assign articles = site.articles_help | where_exp: "item", "item.areas contains 'studio' and item.category_id == category.category_id" %}
          {% for article in articles %}
          <li>
            <div>
              <a href="{{ article.url }}"><h4>{{ article.article_title }}</h4></a>
              <small class="pills">
                {% if article.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ article.time }} Min.</span>{% endif %}{% if article.time_video > 0 %}<span><i class="fas fa-video"></i>{{ article.time_video }} Min.</span>{% endif %}
              </small>
            </div>
            <hr />
          </li>
          {% endfor %}
        </ul>
        {% endfor %}
      </div>
      <div class="col-lg-6">
        <h2>Articles for the WP plugin</h2>
        {% assign categories = site.page_help_wordpress_categories %}
        {% for category in categories %}
        {% assign data_category = site.data.help-categories[category.category_id] %}
        <div class="category-title">
          <div>{% include icon-chapter.html chapter=category.category_id size='small' name=data_category.name %}</div>
          <h3>{{ data_category.name }}</h3>
        </div>
        <ul>
          {% assign articles = site.articles_help | where_exp: "item", "item.areas contains 'wordpress' and item.category_id == category.category_id" %}
          {% for article in articles %}
          <li>
            <div>
              <a href="{{ article.url }}"><h4>{{ article.article_title }}</h4></a>
              <small class="pills">
                {% if article.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ article.time }} Min.</span>{% endif %}{% if article.time_video > 0 %}<span><i class="fas fa-video"></i>{{ article.time_video }} Min.</span>{% endif %}
              </small>
            </div>
            <hr />
            </li>
          {% endfor %}
        </ul>
        {% endfor %}
      </div>
    </div>
  </div>
</section>
