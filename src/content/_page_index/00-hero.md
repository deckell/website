---
base:
---

<section class="index-hero block-first showcase">
  <div class="container">
    <div class="row row-hero">
      <div class="col-md-7 col-hero">
        <h1>Give life to forms and surveys.</h1>
        <p>Tripetto is like SurveyMonkey, Typeform and Landbot united, but with a really fresh vision on <strong>conversational data collection</strong>.</p>
      </div>
      <div class="col-12 col-showcase-switcher">
        <div class="showcase-switcher">
          <div class="dropdown">
            <button class="dropdown-button" type="button" id="carouselSwitcher" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,8" data-flip="false"><span id="carouselSwitcherLabel">Choose your look</span><i class="fas fa-sync-alt"></i></button>
            <div class="dropdown-menu" aria-labelledby="carouselSwitcher">
              <h2>Give forms a face</h2>
              <p>Compare an oldschool form to one of the 3 distinct Tripetto experiences. We call them <strong>form faces</strong>.</p>
              <ul class="carousel-buttons">
                <li class="active" data-target="#carouselShowcase" data-slide-to="0" data-label="Oldschool vs. Tripetto autoscroll">
                  <div>{% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='background' %}</div>
                  <div>
                    <h3>Autoscroll Face</h3>
                    <p class="explanation">Fluently presents one question at a time.</p>
                  </div>
                </li>
                <li data-target="#carouselShowcase" data-slide-to="1" data-label="Oldschool vs. Tripetto chat">
                  <div>{% include icon-face.html face='chat' size='small' name='Chat Form Face' template='background' %}</div>
                  <div>
                    <h3>Chat Face</h3>
                    <p class="explanation">Presents questions and answers as speech bubbles.</p>
                  </div>
                </li>
                <li data-target="#carouselShowcase" data-slide-to="2" data-label="Oldschool vs. Tripetto classic">
                  <div>{% include icon-face.html face='classic' size='small' name='Classic Form Face' template='background' %}</div>
                  <div>
                    <h3>Classic Face</h3>
                    <p class="explanation">Presents question fields in a traditional format.</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row row-showcase">
      <div class="col">
        <div id="carouselShowcase" class="carousel slide carousel-fade carousel-hint">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="slider-container" id="showcaseAutoscroll">
                <div class="slider-before">
                  <div class="slider-before-inset">
                    <img src="{{ page.base }}images/scenes/autoscroll-fitness-registration.png" alt="Screenshots of a fitness registration form in Tripetto's autoscroll form face, shown on a tablet and a mobile phone." />
                    <div class="slider-before-position"></div>
                  </div>
                </div>
                <div class="slider-after">
                  <img src="{{ page.base }}images/scenes/oldschool-fitness-registration.png" alt="Screenshots of a fitness registration form in an oldschool boring non-Tripetto form, shown on a tablet and a mobile phone." />
                  <div class="slider-after-position"></div>
                </div>
                <div class="slider-handle">
                  <div><img src="{{ page.base }}images/tripetto-logo-white.svg" alt="Logo Tripetto in showcase slider" /></div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="slider-container" id="showcaseChat">
                <div class="slider-before">
                  <div class="slider-before-inset">
                    <img src="{{ page.base }}images/scenes/chat-fitness-registration.png" alt="Screenshots of a fitness registration form in Tripetto's chat form face, shown on a tablet and a mobile phone." />
                    <div class="slider-before-position"></div>
                  </div>
                </div>
                <div class="slider-after">
                  <img src="{{ page.base }}images/scenes/oldschool-fitness-registration.png" alt="Screenshots of a fitness registration form in an oldschool boring non-Tripetto form, shown on a tablet and a mobile phone." />
                  <div class="slider-after-position"></div>
                </div>
                <div class="slider-handle">
                  <div><img src="{{ page.base }}images/tripetto-logo-white.svg" alt="Logo Tripetto in showcase slider" /></div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="slider-container" id="showcaseClassic">
                <div class="slider-before">
                  <div class="slider-before-inset">
                    <img src="{{ page.base }}images/scenes/classic-fitness-registration.png" alt="Screenshots of a fitness registration form in Tripetto's classic form face, shown on a tablet and a mobile phone." />
                    <div class="slider-before-position"></div>
                  </div>
                </div>
                <div class="slider-after">
                  <img src="{{ page.base }}images/scenes/oldschool-fitness-registration.png" alt="Screenshots of a fitness registration form in an oldschool boring non-Tripetto form, shown on a tablet and a mobile phone." />
                  <div class="slider-after-position"></div>
                </div>
                <div class="slider-handle">
                  <div><img src="{{ page.base }}images/tripetto-logo-white.svg" alt="Logo Tripetto in showcase slider" /></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/carousel-showcase.js?v={{ site.cache_version }}"></script>
<script src="{{ page.base }}js/slider.js?v={{ site.cache_version }}"></script>
