---
base: ../
---

<section class="product-hosting content">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-xl-10 shape-before" id="hosting">
        {% include icon-chapter.html chapter='hosting' size='normal' name='Flexible Storage' %}
        <span class="caption caption-rotated">Take stock</span>
        <h2 class="palette-hosting">Where forms and data live is essential.<span>Storage freedom, too.</span></h2>
        <p><strong>You decide where your data is stored.</strong> Could be at Tripetto. But maybe you want to self-host forms or surveys and the data you collect elsewhere, bypassing Tripetto entirely. That’s cool, too!</p>
      </div>
    </div>
    <div class="row">
      <div class="col product-hosting-visual">
        <div>
          <div class="product-hosting-option product-hosting-cloud">
            <h3>We host</h3>
            <p>The forms and surveys you build and data you collect are all stored in your Tripetto studio account. You can always extract your work and results.</p>
            <img src="{{ page.base }}images/product/hosting-cloud.svg" alt="Human illustrating hosting at Tripetto" />
          </div>
        </div>
        <div>
          <div class="product-hosting-option product-hosting-self">
            <h3>Self-host</h3>
            <p>You decide what you want to store outside of Tripetto and where. Data you collect and choose to self-host never reaches Tripetto at all. <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/" target="_blank">Read our blog on GDPR!</a></p>
            <img src="{{ page.base }}images/product/hosting-self.svg" alt="Human illustrating self-hosting" />
          </div>
          <span>BTW, you can also do both!</span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}tutorials/#hosting" class="hyperlink palette-hosting"><span>How to manage form and survey data</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
