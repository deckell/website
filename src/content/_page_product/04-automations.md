---
base: ../
---

<section class="product-automations content">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 shape-before" id="automations">
        {% include icon-chapter.html chapter='automations' size='normal' name='Automations' %}
        <span class="caption caption-rotated">Hook up</span>
        <h2 class="palette-automations">Automate your data collection flows with <span>active integrations.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10">
        <p>What you do with data counts. Tripetto can notify you of form completions in Slack or email and lets you <strong>connect to 1.000+ services via Integromat, Zapier</strong> and others with webhooks.</p>
      </div>
    </div>
    <div class="row">
      <div class="col product-automations-visual">
        <a href="{{ page.base }}tutorials/getting-automatic-slack-notifications/" target="_blank" class="automation-bubble automation-slack"><img src="{{ page.base }}images/product/automation-slack.svg" alt="Logo of Slack"></a>
        <a href="{{ page.base }}tutorials/getting-automatic-email-notifications/" target="_blank" class="automation-bubble automation-email"><img src="{{ page.base }}images/product/automation-email.svg" alt="Icon representing email"></a>
        <a href="{{ page.base }}tutorials/connecting-to-other-services-with-webhooks/" target="_blank" class="automation-bubble automation-webhooks"><img src="{{ page.base }}images/product/automation-webhook.svg" alt="Icon representing webhooks"></a>
        <a href="{{ page.base }}tutorials/connecting-to-other-services-with-webhooks/" target="_blank" class="automation-bubble automation-zapier"><img src="{{ page.base }}images/product/automation-zapier.svg" alt="Logo of Zapier"></a>
        <a href="{{ page.base }}tutorials/connecting-to-other-services-with-webhooks/" target="_blank" class="automation-bubble automation-integromat"><img src="{{ page.base }}images/product/automation-integromat.svg" alt="Logo of Integromat"></a>
        <a href="{{ page.base }}tutorials/connecting-to-other-services-with-webhooks/" target="_blank" class="automation-bubble automation-pabbly-connect"><img src="{{ page.base }}images/product/automation-pabbly-connect.png" alt="Logo of Pabbly Connect"></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}tutorials/#automations" class="hyperlink palette-automations"><span>How to automate data collection flows</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
