---
base: ../
---

<section class="product-logic content">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-xl-9 shape-before" id="logic">
        {% include icon-chapter.html chapter='logic' size='normal' name='Advanced Logic' %}
        <span class="caption caption-rotated">Add brains</span>
        <h2 class="palette-logic shape-after">Get profoundly personable with <span>conversational logic.</span></h2>
        <p>Tripetto has all the advanced logic features you need to get on point and conversational with each respondent, and effectively <strong>increase completion rates and reduce drop-offs</strong>.</p>
      </div>
    </div>
  </div>
  <div class="container-fluid ticker-holder">
    <div class="row">
      <ul class="ticker-blocks ticker-blocks-logic-basic">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'basic'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-build">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
            {% if item.suffix %}<small>{{ item.suffix }}</small>{% endif %}
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-blocks-logic-action">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'advanced'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-logic">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
            {% if item.suffix %}<small>{{ item.suffix }}</small>{% endif %}
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-blocks-logic-advanced">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'action'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-customization">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
            {% if item.suffix %}<small>{{ item.suffix }}</small>{% endif %}
          </a>
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container container-bottom">
    <div class="row">
      <div class="col-md-4">
        <p class="explanation">The visual storyboard for building forms and surveys was specifically designed to <strong>spark and simplify using advanced logic</strong> features.</p>
        <a href="{{ page.base }}tutorials/#logic" class="hyperlink palette-logic"><span>How to use logic</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4">
        <p class="explanation">Besides the questions that require input from respondents, forms and surveys can <strong>perform powerful real time actions</strong> in the background.</p>
        <a href="{{ page.base }}tutorials/performing-actions-inside-your-forms-and-surveys/" class="hyperlink palette-logic"><span>How to set actions</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4">
        <p class="explanation">Possibly the most powerful logic feature in Tripetto is the <strong>calculator for creating tests, quizzes, quotes, assessments</strong> and so much more.</p>
        <a href="{{ page.base }}tutorials/performing-calculations-inside-your-forms-and-surveys/" class="hyperlink palette-logic"><span>How to calculate</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/ticker.js?v={{ site.cache_version }}"></script>
