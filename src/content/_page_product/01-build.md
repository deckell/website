---
base: ../
---

<section class="product-build content">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-xl-7" id="build">
        {% include icon-chapter.html chapter='build' size='normal' name='Visual Builder' %}
        <span class="caption caption-rotated">Give shape</span>
        <h2 class="palette-build">Visually build on the assistive <span>storyboard.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10 col-xl-10 shape-after">
        <p>The storyboard lets you <strong>create forms and surveys like flowcharts</strong> and actively organizes what’s on it. This makes building highly conversational flows clean, fast and fun. No-code, promised!</p>
      </div>
    </div>
    <div class="row row-visual">
      <div class="col-12 col-visual">
        <div class="product-build-block product-build-builder">
          <img src="{{ page.base }}images/product/build-storyboard.png" alt="Screenshot of the storyboard of the form builder." />
          <p class="explanation">The storyboard shows you what goes where and always <strong>actively aligns your arrangement</strong> while you move things around. The <strong>smart zoom</strong> lets you quickly go into and out of any area on the storyboard once your structure grows bigger than the screen. Things just can’t get messy.</p>
          <a href="{{ page.base }}tutorials/the-builder-basics/" class="hyperlink palette-build"><span>How to work the storyboard</span><i class="fas fa-arrow-right"></i></a>
        </div>
        <div class="product-build-block product-build-preview">
          <img src="{{ page.base }}images/product/build-preview.png" alt="Screenshot of the preview of the form builder." />
          <p class="explanation">The <strong>real-time preview</strong> next to the storyboard always instantly shows what you’re building and designing as <strong>a working form or survey</strong>.</p>
          <a href="{{ page.base }}tutorials/using-the-real-time-preview-in-the-builder/" class="hyperlink palette-build"><span>How to preview</span><i class="fas fa-arrow-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
