---
base: ../../
---

<section class="pricing-studio-premium" id="premium">
  <div class="container">
    <div class="row pricing-studio-intro">
      <div class="col-lg-10 col-xl-9">
      <h2>Premium upgrade</h2>
      <p>The free account includes all Tripetto features. Plain and simple. On top of that, you can boost forms with optional upgrades.<br/><strong>Upgrades are per form and pay-once.</strong> Nothing recurring.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-11">
        <div class="premium-feature">
          <div class="row premium-feature-content">
            <div class="col-md-6 col-lg-7 premium-feature-content-left">
              <span>Premium upgrade</span>
              <h3>Unbrand 1</h3>
              <p>Remove Tripetto branding in one designated form. This upgrade is <strong>pay-once and per form</strong>. You can <a href="{{ page.base }}help/articles/how-to-remove-branding-in-studio-forms/" target="_blank">purchase</a> it directly from the studio.</p>
            </div>
            <div class="col-md-6 col-lg-5 premium-feature-content-right">
            <div class="pricing-price palette-purple">
              <div class="pricing-price-prefix">
                <div class="valuta">$</div>
                <div class="amount">89</div>
                <div class="decimals">.00</div>
              </div>
              <div class="pricing-price-suffix">
                <span class="conditions-title">Per form.</span>
                <span class="conditions-subtitle">Pay-once.</span>
              </div>
            </div>
              <ul class="pricing-features fa-ul">
                <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}studio/" target="_blank">Everything in the free plan</a></li>
                <li class="included"><span class="fa-li"><i class="fas fa-plus"></i></span>Removable Tripetto branding</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col premium-feature-footer">
      <ul class="hyperlinks">
        <li><a href="{{ page.base }}help/articles/how-to-remove-branding-in-studio-forms/" class="hyperlink hyperlink-small" target="_blank"><span>See how to activate upgrades</span><i class="fas fa-arrow-right"></i></a></li>
      </ul>
      </div>
    </div>
  </div>
</section>
