---
base: ../../
---

<section class="pricing-studio-free pricing-jumbo">
  <div class="container jumbo-container">
    <div class="row jumbo-row">
      <div class="col jumbo-col palette-studio">
        <div class="row jumbo-content">
          <div class="col-md-6 col-lg-7 jumbo-content-left">
            <span>Standard account</span>
            <h2>Free Plan</h2>
            <p>Tripetto studio accounts are free and include pretty much everything the studio has to offer. One catch, though. All forms have Tripetto branding. Removing it is an optional <a href="#premium" class="anchor">premium upgrade</a> per form.</p>
          </div>
          <div class="col-md-6 col-lg-5 jumbo-content-right">
            <div class="pricing-price palette-yellow">
              <div class="pricing-price-prefix">
                <div class="valuta">$</div>
                <div class="amount">0</div>
                <div class="decimals">.00</div>
              </div>
              <div class="pricing-price-suffix">
                <span class="conditions-title">Per account.</span>
                <span class="conditions-subtitle">Forever.</span>
              </div>
            </div>
            <div class="pricing-features-primary">
              <ul class="pricing-features fa-ul">
                <li class="featured included"><span class="fa-li"><i class="fas fa-check"></i></span><a href="{{ page.base }}studio/" target="_blank">All Tripetto features</a></li>
                <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited forms</li>
                <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited logic</li>
                <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited questions</li>
                <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited responses</li>
                <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Removable Tripetto branding<sup>1</sup></li>
              </ul>
            </div>
            <div class="pricing-features-secondary">
              <ul class="pricing-features fa-ul">
                <li class="included"><span class="fa-li"><i class="fas fa-check"></i></span>Access to <a href="{{ page.base }}help/studio/" target="_blank">help center</a><small>[24/7]</small></li>
                <li class="excluded"><span class="fa-li"><i class="fas fa-times"></i></span>Live chat support<small>[Mon-Fri, 9-17 CET]</small></li>
              </ul>
            </div>
            <a href="{{ site.url_app }}" class="button button-full" target="_blank">Start now</a>
            <small class="pricing-footer">1. Available as <a href="#premium" class="anchor">premium upgrade per form</a>.</small>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col jumbo-footer jumbo-footer-hyperlinks">
      <ul class="hyperlinks">
        <li><a href="#premium" class="hyperlink hyperlink-small anchor"><span>See premium upgrades</span><i class="fas fa-arrow-down"></i></a></li>
        <li><a href="{{ page.base }}studio/" class="hyperlink hyperlink-small"><span>About the Tripetto studio</span><i class="fas fa-arrow-right"></i></a></li>
      </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>

