---
base: ../../
---

<section class="pricing-studio-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="breadcrumb-navigation">
          <a href="{{ page.base }}pricing/">Pricing</a>
          <i class="fas fa-chevron-right"></i>
          <div class="dropdown">
              <a role="button" class="dropdown-button active" id="pricingBreadcrumb" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">Studio<i class="fas fa-chevron-down"></i></a>
              <div class="dropdown-menu" aria-labelledby="pricingBreadcrumb">
                <a class="dropdown-item" href="{{ page.base }}pricing/studio/"><div>Tripetto Studio<small>at tripetto.app</small></div></a>
                <a class="dropdown-item" href="{{ page.base }}pricing/wordpress/"><div>Tripetto WP<small>WordPress plugin</small></div></a>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>Studio</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10 col-xl-9 shape-after">
        <p><strong>The studio is free.</strong> We offer an optional upgrade per form for removing Tripetto branding, because we need visibility or sponsorships to have a chance of shaking the big guys 🙏🏼.</p>
      </div>
    </div>
  </div>
</section>
