---
base: ../
---

<section class="launch-intro block-first intro">
  <div class="container">
    <div class="row launch-row-intro">
      <div class="col-md-10 col-lg-8 shape-before shape-after">
        <h1>Get Started</h1>
        <p><strong>Tripetto comes in two versions.</strong> Both are separate, full-blown Tripetto platforms with practically identical feature sets. For different users.</p>
      </div>
    </div>
  </div>
</section>
