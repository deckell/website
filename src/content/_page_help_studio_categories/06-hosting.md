---
layout: help-category
base: ../../../
permalink: /help/studio/managing-data-and-results/
title: Managing data and results in the studio - Tripetto Help Center
description: Find out how you can use your response data and get notified of new results.
category_area: studio
category_id: hosting
redirect_from:
- /help/studio/get-results/
---
