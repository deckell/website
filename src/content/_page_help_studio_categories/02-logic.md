---
layout: help-category
base: ../../../
permalink: /help/studio/using-logic-features/
title: Using logic features in the studio - Tripetto Help Center
description: Learn how to make your forms smart with advanced logic features.
category_area: studio
category_id: logic
---
