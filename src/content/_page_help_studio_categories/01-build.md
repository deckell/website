---
layout: help-category
base: ../../../
permalink: /help/studio/building-forms-and-surveys/
title: Building forms and surveys in the studio - Tripetto Help Center
description: Get to know how the form builder works, from the simple basics to advanced tutorials.
category_area: studio
category_id: build
redirect_from:
- /help/studio/build-forms/
---
