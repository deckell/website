---
layout: help-category
base: ../../../
permalink: /help/studio/automating-things/
title: Automating things in the studio - Tripetto Help Center
description: Learn how to automate actions with your data.
category_area: studio
category_id: automations
---
