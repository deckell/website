---
layout: help-category
base: ../../../
permalink: /help/studio/styling-and-customization/
title: Styling and customization in the studio - Tripetto Help Center
description: Learn how to customize your form experience, styling and translations.
category_area: studio
category_id: customization
redirect_from:
- /help/studio/customize-forms/
---
