---
base: ../
---
<section class="contact-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-11 shape-before">
        <h1>We are Tripetto from Amsterdam</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10 shape-after">
        <p>Nice to meet you! We’re a small team, but we intend to do great things. We like to think of it as <strong>quality over quantity</strong> 😉.</p>
      </div>
    </div>
  </div>
</section>
