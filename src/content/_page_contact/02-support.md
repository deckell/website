---
base: ../
---

<section class="contact-support">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>At your service</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-xl-3 contact-support-list">
        <h3>Get in touch</h3>
        <ul class="palette-purple">
          <li><a href="javascript:$crisp.push(['do', 'chat:open']);"><i class="fas fa-comments fa-fw"></i><span>Chat with us</span></a></li>
          <li><a href="mailto:support@tripetto.com"><i class="fas fa-paper-plane fa-fw"></i><span>Mail the team</span></a></li>
          <li><a href="{{ page.base }}roadmap/#request-feature"><i class="fas fa-star fa-fw"></i><span>Request a feature</span></a></li>
          <li><a href="{{ page.base }}partner-with-us/"><i class="fas fa-handshake fa-fw"></i><span>Partner with us</span></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-xl-3 contact-support-list">
        <h3>Find help</h3>
        <ul class="palette-red">
          <li><a href="{{ page.base }}help/"><i class="fas fa-life-ring fa-fw"></i><span>Browse the help center</span></a></li>
          <li><a href="{{ page.base }}examples/"><i class="fas fa-eye fa-fw"></i><span>Get inspired by examples</span></a></li>
          <li><a href="{{ page.base }}tutorials/"><i class="fas fa-graduation-cap fa-fw"></i><span>View the tutorials</span></a></li>
          <li><a href="javascript:$crisp.push(['do', 'chat:open']);"><i class="fas fa-comments fa-fw"></i><span>Start a support chat</span></a></li>
        </ul>
      </div>
      <div class="col-md-4 col-xl-3 contact-support-list">
        <h3>Stay tuned</h3>
        <ul class="palette-green">
          <li><a href="https://twitter.com/tripetto" target="_blank"><i class="fab fa-twitter fa-fw"></i><span>Follow us on Twitter</span></a></li>
          <li><a href="{{ page.base }}subscribe/"><i class="fas fa-bell fa-fw"></i><span>Subscribe to updates</span></a></li>
          <li><a href="{{ page.base }}blog/"><i class="fas fa-feather-alt fa-fw"></i><span>Read our blog</span></a></li>
          <li><a href="{{ page.base }}changelog/"><i class="fas fa-clipboard-list fa-fw"></i><span>Check the changelog</span></a></li>
          <li><a href="{{ page.base }}roadmap/"><i class="fas fa-road fa-fw"></i><span>View our roadmap</span></a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
