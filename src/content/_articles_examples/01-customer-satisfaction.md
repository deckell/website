---
layout: examples-article
base: ../../
permalink: /examples/customer-satisfaction-with-net-promoter-score-nps/
title: Customer satisfaction with NPS - Tripetto Examples
description: Use Tripetto to create a customer satisfaction survey including a Net Promoter Score (NPS) question that can be used to measure the loyalty of your customer relationships.
article_title: Customer Satisfaction
article_breadcrumb: Customer Satisfaction
article_description: Use Tripetto to create a customer satisfaction survey including a Net Promoter Score (NPS) question that can be used to measure the loyalty of your customer relationships. This example uses the <strong>autoscroll form face</strong> in a vertical scroll direction (top-down).
example_id: customer-satisfaction
example_face: autoscroll
example_face_name: Autoscroll
example_questions: [matrix, scale, paragraph, text-multiple]
example_logic: [branch, piping, closing]
url_runner: https://tripetto.app/run/AINYGRABMR
url_template: https://tripetto.app/template/GI4XIBCQC6
---
