---
layout: examples-article
base: ../../
permalink: /examples/body-mass-index-bmi-wizard/
title: Body Mass Index (BMI) Wizard - Tripetto Examples
description: Use Tripetto to create a wizard that can calculate your BMI based on your height and weight data. And after that present the right advice based on the outcome.
article_title: Body Mass Index (BMI) Wizard
article_breadcrumb: BMI Wizard
article_description: Use Tripetto to create a wizard that can calculate your BMI based on your height and weight data. And after that present the right advice based on the outcome. This example uses the <strong>chat form face</strong>.
example_id: bmi-wizard
example_purpose: calculator
example_face: chat
example_face_name: Chat
example_questions: [paragraph, number]
example_logic: [branch, piping, endings, calculator]
url_runner: https://tripetto.app/run/S7S6Z5DEE9
url_template: https://tripetto.app/template/XXCMDH91I5
---
