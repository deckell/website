---
layout: examples-article
base: ../../
permalink: /examples/product-evaluation/
title: Product evaluation - Tripetto Examples
description: Use Tripetto to create a product evaluation survey to get valuable feedback from your customers to improve your products.
article_title: Product Evaluation
article_breadcrumb: Product Evaluation
article_description: Use Tripetto to create a product evaluation survey to get valuable feedback from your customers to improve your products. This example uses the <strong>chat form face</strong>.
example_id: product-evaluation
example_face: chat
example_face_name: Chat
example_questions: [picture-choice, rating, matrix, yes-no, text-single, paragraph]
example_logic: [branch, iteration, piping]
url_runner: https://tripetto.app/run/Z4FOVID1MV
url_template: https://tripetto.app/template/2R7KXKZHFQ
---
