---
layout: examples-article
base: ../../
permalink: /examples/trivia-quiz/
title: Trivia quiz with score - Tripetto Examples
description: Use Tripetto to create a quiz with various question types and calculate the end score based on the given answers. Also usable for assessments and exams of course.
article_title: Trivia Quiz
article_breadcrumb: Trivia Quiz
article_description: Use Tripetto to create a quiz with various question types and calculate the end score based on the given answers. Also usable for assessments and exams of course. This example uses the <strong>autoscroll form face</strong> in a horizontal scroll direction (left-right).
example_id: trivia-quiz
example_purpose: calculator
example_face: autoscroll
example_face_name: Autoscroll
example_questions: [paragraph, text-single, picture-choice, multiple-choice, number, rating]
example_logic: [branch, piping, closing, calculator]
url_runner: https://tripetto.app/run/LDGW34WFH7
url_template: https://tripetto.app/template/U82YILRWH4
---
