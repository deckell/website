---
layout: examples-article
base: ../../
permalink: /examples/coronavirus-covid-19-screening/
title: Corona (COVID-19) screening - Tripetto Examples
description: Use Tripetto to create a screening tool on how to react to symptoms of the coronavirus (COVID-19).
article_title: Corona Screening
article_breadcrumb: Corona Screening
article_description: Use Tripetto to create a screening tool on how to react to symptoms of the coronavirus (COVID-19). This example uses the <strong>autoscroll form face</strong> in a horizontal scroll direction (left-right).
example_id: corona-screening
example_face: autoscroll
example_face_name: Autoscroll
example_questions: [multiple-choice]
example_logic: [branch, piping, raise-error, endings]
url_runner: https://tripetto.app/run/WRDC05RGPJ
url_template: https://tripetto.app/template/TZK9WO6F76
---
