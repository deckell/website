---
layout: examples-article
base: ../../
permalink: /examples/restaurant-reservation/
title: Restaurant Reservation - Tripetto Examples
description: Use Tripetto to create a reservation form for your restaurant, including a personalized confirmation mail that will be sent to your guest.
article_title: Restaurant Reservation
article_breadcrumb: Restaurant Reservation
article_description: Use Tripetto to create a reservation form for your restaurant, including a personalized confirmation mail that will be sent to your guest. This example uses the <strong>classic form face</strong>.
example_id: restaurant-reservation
example_face: classic
example_face_name: Classic
example_questions: [checkbox, date, phone-number, rating, text-single, text-multiple, email-address, yes-no, paragraph]
example_logic: [branch, piping, send-email]
url_runner: https://tripetto.app/run/Y1DN01BVHZ
url_template: https://tripetto.app/template/A6YB4WH79P
---
