---
base: ../../
---

{% assign categories = site.page_help_studio_categories %}

<section class="help-chapters">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2>All Things Tripetto</h2>
        <ul>
        {% for category in categories %}
        {% assign data_category = site.data.help-categories[category.category_id] %}
          <li onclick="window.location='{{ category.url }}';" class="palette-{{ category.category_id }}">
            <a href="{{ category.url }}"><h3>{{ data_category.name }}</h3></a>
          </li>
        {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
