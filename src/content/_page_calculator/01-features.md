---
base: ../
---

<section class="calculator-features">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 calculator-features-left">
        <div class="calculator-feature-primary palette-dark">
          <h2>Plain calculations</h2>
          <p>A calculator can be inserted anywhere inside a form and <strong>supports straightforward operations for pretty much any purpose</strong> from counting inputs to scoring a test, for instance.</p>
          <ul><li><img src="{{ page.base }}images/calculator/add.svg" alt="Icon representing an addition" /><h3>Add</h3></li><li><img src="{{ page.base }}images/calculator/subtract.svg" alt="Icon representing a subtraction" /><h3>Subtract</h3></li><li><img src="{{ page.base }}images/calculator/multiply.svg" alt="Icon representing a multiplication" /><h3>Multiply</h3></li><li><img src="{{ page.base }}images/calculator/divide.svg" alt="Icon representing a division" /><h3>Divide</h3></li><li><img src="{{ page.base }}images/calculator/equal.svg" alt="Icon representing an equation" /><h3>Equal</h3></li></ul>
        </div>
        <div class="calculator-feature-secondary calculator-feature-secondary-visual size-medium">
          <img src="{{ page.base }}images/calculator/multiple-calculators.svg" alt="Icon representing multiple calculators" class="multiple-calculators" />
          <h2>Multiple calculators</h2>
          <p>Forms and surveys can handle as many calculations as you need, anywhere you need them. In fact, you can <strong>set up one calculator to use the outcome of another.</strong></p>
        </div>
        <div class="calculator-feature-primary palette-red">
          <h2>Subcalculations</h2>
          <p>Now here’s the magic part... <strong>The calculator can also handle calculations within calculations.</strong> Like formulas. Handy for calculating a shopping cart’s subtotal and VAT in a single calculator.</p>
          <ul class="visual-steps"><li><img src="{{ page.base }}images/calculator/divide.svg" /></li><li><i class="fas fa-arrow-right"></i></li><li><img src="{{ page.base }}images/calculator/angles.svg" /></li><li><i class="fas fa-arrow-right"></i></li><li><img src="{{ page.base }}images/calculator/placeholder.svg" /></li><li><i class="fas fa-arrow-right"></i></li></ul>
          <small>Every single calculation and subcalculation outcome is available as a reusable value throughout the form.</small>
        </div>
        <div class="calculator-feature-payoff">Imagine your powers when you put Tripetto’s mind to it ;)</div>
      </div>
      <div class="col-lg-6 calculator-features-right">
        <div class="calculator-feature-secondary calculator-feature-secondary-visual size-small">
          <img src="{{ page.base }}images/calculator/respondent-driven.svg" alt="Icon representing respondent driven" />
          <h2>Respondent driven</h2>
          <p>It almost goes without saying that you can <strong>select any respondent input for calculations.</strong> Actually, you may include as many inputs as you like in a single calculation.</p>
        </div>
        <div class="calculator-feature-primary palette-yellow">
          <h2>Mathematics</h2>
          <p>Beyond operations like adding, subtracting, multiplying and dividing, the calculator also supports advanced operations for <strong>more mathematically challenging use cases.</strong></p>
          <ul><li><img src="{{ page.base }}images/calculator/roots.svg" alt="Icon representing mathematical roots" /><h3>Roots</h3></li><li><img src="{{ page.base }}images/calculator/powers.svg" alt="Icon representing mathematical powers" /><h3>Powers</h3></li><li><img src="{{ page.base }}images/calculator/functions.svg" alt="Icon representing mathematical functions" /><h3>Functions</h3></li><li><img src="{{ page.base }}images/calculator/constants.svg" alt="Icon representing mathematical constants" /><h3>Constants</h3></li><li><img src="{{ page.base }}images/calculator/more.svg" alt="Icon representing more mathematical features" /><h3>More</h3></li></ul>
          <small>And much more for logarithms, advanced comparisons etc. to take your calculations into another dimension.</small>
        </div>
        <div class="calculator-feature-secondary size-medium">
          <h2>Logic just loves calculations</h2>
          <p><strong>Mindblowing things happen when logic and calculators come together.</strong></p>
          <p>Funnel respondents into any desired flow based on age, score, probability, height, BMI etc. or any combination of factors. The possibilities are endless.</p>
        </div>
      </div>
    </div>
  </div>
</section>
