---
base: ../
---

<section class="calculator-examples examples-list">
  <div class="container">
    <div class="row calculator-examples-intro">
      <div class="col-md-11 col-lg-10 col-xl-9">
        <h2>Calculators in action</h2>
        <p>The true power and capabilities of the Tripetto calculator are best demonstrated with some practical examples. <strong>Click any example below to run it live</strong> and learn about its calculator implementation.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <ul>
          {% assign examples = site.articles_examples | where_exp: "item", "item.example_purpose == 'calculator'" %}
          {% for article in examples %}
          <li onclick="window.location='{{ article.url }}';">
              <div class="example-list-preview example-list-{{ article.example_id }}">
                <div class="example-list-play palette-{{ article.example_face }}"><i class="fas fa-play"></i></div>
              </div>
              <div class="example-list-info">
                <div>{% include icon-face.html face=article.example_face size='small' name=article.example_face_name template='background' %}</div>
                <div>
                <small>{{ article.example_face_name }} form</small>
                <a href="{{ article.url }}"><h2>{{ article.article_title }}</h2></a>
                </div>
              </div>
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
