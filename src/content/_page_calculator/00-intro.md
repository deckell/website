---
base: ../
---

<section class="calculator-intro block-first">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-xl-9 shape-before">
        <h1>Make quizzes, order forms, exams and more with no-code calculations. Fast.</h1>
      </div>
      <div class="col-md-11 col-lg-8 shape-after">
        <p>The calculator can add, subtract, multiply and divide, but also supports formulas and advanced mathematical functions. <strong>All without any coding.</strong> Plus, forms can handle multiple calculators.</p>
        <ul class="hyperlinks">
          <li><a href="{{ page.base }}launch/" class="hyperlink"><span>Start building right away</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
