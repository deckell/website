---
base: ../
---

<section class="calculator-ticker">
  <div class="container-fluid ticker-holder">
    <div class="row">
      <ul class="ticker-blocks ticker-calculator-operations">
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/" class="palette-dark">
            <img src="{{ page.base }}images/calculator/add.svg" alt="Icon representing an addition" />
            <span>Addition</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/" class="palette-dark">
            <img src="{{ page.base }}images/calculator/subtract.svg" alt="Icon representing a subtraction" />
            <span>Subtraction</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/" class="palette-dark">
            <img src="{{ page.base }}images/calculator/multiply.svg" alt="Icon representing a multiplication" />
            <span>Multiplication</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/" class="palette-dark">
            <img src="{{ page.base }}images/calculator/divide.svg" alt="Icon representing a division" />
            <span>Division</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/" class="palette-dark">
            <img src="{{ page.base }}images/calculator/equal.svg" alt="Icon representing an equation " />
            <span>Equation</span>
          </a>
        </li>
      </ul>
      <ul class="ticker-blocks ticker-calculator-values">
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block-add-subtract-multiply-divide-equal/" class="palette-red">
            {% include icons/numbers.html %}
            <span>Numbers</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/" class="palette-red">
            {% include icons/scores.html %}
            <span>Scores</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/" class="palette-red">
            {% include icons/comparisons.html %}
            <span>Comparisons</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" class="palette-red">
            {% include icons/functions.html %}
            <span>Functions</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" class="palette-red">
            {% include icons/constants.html %}
            <span>Constants</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/" class="palette-red">
            {% include icons/subcalculation.html %}
            <span>Subcalculation</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/" class="palette-red">
            {% include icons/blocks.html %}
            <span>Blocks</span>
          </a>
        </li>
      </ul>
      <ul class="ticker-blocks ticker-calculator-mathematics">
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" class="palette-yellow">
            <img src="{{ page.base }}images/calculator/roots.svg" alt="Icon representing mathematical roots" />
            <span>Roots</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" class="palette-yellow">
            <img src="{{ page.base }}images/calculator/powers.svg" alt="Icon representing mathematical powers" />
            <span>Powers</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" class="palette-yellow">
            <img src="{{ page.base }}images/calculator/functions.svg" alt="Icon representing mathematical functions" />
            <span>Functions</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" class="palette-yellow">
            <img src="{{ page.base }}images/calculator/constants.svg" alt="Icon representing mathematical constants" />
            <span>Constants</span>
          </a>
        </li>
        <li>
          <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/" class="palette-yellow">
            <img src="{{ page.base }}images/calculator/angles.svg" alt="Icon representing mathematical radians" />
            <span>Radians</span>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col ticker-hyperlink">
        <a href="{{ page.base }}all-calculator-features/" class="hyperlink palette-logic"><span>See all calculator features</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/ticker.js?v={{ site.cache_version }}"></script>
