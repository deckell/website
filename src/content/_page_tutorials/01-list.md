---
base: ../
---

<section class="tutorials-list">
  {% for category in site.data.tutorials-categories %}
    {% assign tutorials = site.articles_tutorials | where_exp: "item", "item.level == 1" | where_exp: "item", "item.category_id == category.id" %}
    {% if tutorials.size > 0 %}
    <div class="container" id="{{ category.id }}">
      <div class="row">
        <div class="col">
          <div class="tutorial-category">
            <div>
              {% include icon-chapter.html chapter=category.id size='normal' name=category.name %}
            </div>
            <div>
              <span class="caption caption-small">{{ category.caption }}</span>
              <h2>{{ category.title }}</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <ul class="tiles">
            {% for article in tutorials %}
              {% include tile.html url=article.url title=article.article_title palette-top=category.id time=article.time time_video=article.time_video type='Tutorial' %}
            {% endfor %}
          </ul>
        </div>
      </div>
    </div>
    {% endif %}
  {% endfor %}
</section>
