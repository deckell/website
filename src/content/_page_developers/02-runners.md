---
base: ../
---

<section class="developers-runners content">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-10 col-xl-9" id="runners">
        <span class="caption">Embedding Tripetto runners</span>
        <h2 class="palette-yellow">Run Tripetto forms inside your project <span>independently.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-11">
        <p>The runner is for <strong>deploying forms in websites and applications</strong>. It turns a form definition into an executable program; a finite state machine that handles the complex logic and response collection during form execution. Choose any of our default runners for your project.</p>
      </div>
    </div>
  </div>
  <div class="container container-runners">
    <div class="row">
      <div class="col-md-4 runner">
        <div>
          {% include icon-face.html face='autoscroll' size='medium' name='Autoscroll Form Face' template='developers' %}
          <h3>Autoscroll<br/>runner</h3>
        </div>
        <p class="explanation">The autoscroll runner presents <strong>one question at a time</strong> and is akin to Typeform’s well known conversational format.</p>
        <ul class="hyperlinks-list">
          <li><a href="{{ page.base }}examples/" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Autoscroll runner examples</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://www.npmjs.com/package/tripetto-runner-autoscroll" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Embed the autoscroll runner</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://gitlab.com/tripetto/runners/autoscroll" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Autoscroll repo on GitLab</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 runner">
        <div>
          {% include icon-face.html face='chat' size='medium' name='Chat Form Face' template='developers' %}
          <h3>Chat<br/>runner</h3>
        </div>
        <p class="explanation">The chat runner presents questions and answers as <strong>speech bubbles</strong> and is partly inspired by Landbot.</p>
        <ul class="hyperlinks-list">
          <li><a href="{{ page.base }}examples/" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Chat runner examples</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://www.npmjs.com/package/tripetto-runner-chat" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Embed the chat runner</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://gitlab.com/tripetto/runners/chat" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Chat repo on GitLab</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4 runner">
        <div>
          {% include icon-face.html face='classic' size='medium' name='Classic Form Face' template='developers' %}
          <h3>Classic<br/>runner</h3>
        </div>
        <p class="explanation">The classic runner presents question fields in a <strong>traditional format</strong> as regularly seen in SurveyMonkey and the likes.</p>
        <ul class="hyperlinks-list">
          <li><a href="{{ page.base }}examples/" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Classic runner examples</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://www.npmjs.com/package/tripetto-runner-classic" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Embed the classic runner</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://gitlab.com/tripetto/runners/classic" target="_blank" class="hyperlink hyperlink-small palette-yellow"><span>Classic repo on GitLab</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container container-carousel">
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselDevelopersRunners" class="carousel slide carousel-fade">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/autoscroll-secure-bond-007.png" alt="Screenshots of a secure, password protected form in the autoscroll form face, shown on a tablet and a mobile phone." />
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/chat-product-evaluation.png" alt="Screenshots of a product evaluation survey in the chat form face, shown on a tablet and a mobile phone." />
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/classic-restaurant-reservation.png" alt="Screenshots of a restaurant reservation form in the classic form face, shown on a tablet and a mobile phone." />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 runners-switch">
        <ul class="carousel-buttons">
          <li class="carousel-button-left active" data-target="#carouselDevelopersRunners" data-slide-to="0">Autoscroll</li><li class="carousel-button-middle" data-target="#carouselDevelopersRunners" data-slide-to="1">Chat</li><li class="carousel-button-right" data-target="#carouselDevelopersRunners" data-slide-to="2">Classic</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-12 runners-footer">
        <small>All runners, building blocks and developer resources are free no matter what.</small>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}js/carousel-developers-runners.js?v={{ site.cache_version }}"></script>
