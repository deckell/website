---
base: ../
---

<section class="developers-jumbo block-first jumbo">
  <div class="container jumbo-container">
    <div class="row">
      <div class="col jumbo-holder palette-sdk">
        <div class="jumbo-content">
          <h1>Tripetto in pieces for developers.</h1>
          <p>The Tripetto SDK contains fully customizable components for equipping websites and apps with a comprehensive solution for building and running conversational forms. All up in minutes.</p>
          <div>
            <a href="#components" class="button anchor">Learn more</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col jumbo-footer jumbo-footer-text"><small>Use of the entire development kit is free for non-commercial personal or academic use and evaluation purposes. A paid license is required for select builder implementations in business applications.</small></div>
    </div>
  </div>
</section>

