---
base: ../
---

<section class="developers-customization content">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-9 col-xl-8" id="customization">
        <span class="caption">Customizing things</span>
        <h2 class="palette-red">The development kit is thoroughly <span>customizable.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10">
        <p>The development kit also comes with a rich set of <strong>customizable, open source question types</strong> for developers to use out of the box or enhance for use in the Tripetto builder and runners. Also, developers can create original runners on our runner engine.</p>
      </div>
    </div>
  </div>
  <div class="container container-customization">
    <div class="row">
      <div class="col-md-5 col-lg-4 customization">
        <h3>Question blocks</h3>
        <p class="explanation">Question blocks make up the forms and surveys created in the builder and executed by the runner. Customize our default blocks or <strong>extend with your custom blocks to unlock API's and other amazing things</strong>.</p>
        <ul class="hyperlinks-list">
          <li><a href="https://docs.tripetto.com/guide/blocks/" target="_blank" class="hyperlink hyperlink-small palette-red"><span>Blocks documentation</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://www.npmjs.com/search?q=tripetto-block" target="_blank" class="hyperlink hyperlink-small palette-red"><span>Default question blocks</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://gitlab.com/tripetto/blocks" target="_blank" class="hyperlink hyperlink-small palette-red"><span>Open source repos on GitLab</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://gitlab.com/tripetto/blocks/boilerplate" target="_blank" class="hyperlink hyperlink-small palette-red"><span>Block boilerplate</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
      <div class="col-md-5 col-lg-4 customization">
        <h3>Custom runners</h3>
        <p class="explanation">The <strong>runner engine is headless</strong> and just begging for A-Team devs to wield its blistering powers in radical ways. There's a learning curve. But hey, no pain no gain!</p>
        <ul class="hyperlinks-list">
          <li><a href="https://docs.tripetto.com/guide/runner/" target="_blank" class="hyperlink hyperlink-small palette-red"><span>Runner documentation</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://docs.tripetto.com/examples/" target="_blank" class="hyperlink hyperlink-small palette-red"><span>Custom runner examples</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="https://gitlab.com/tripetto/examples" target="_blank" class="hyperlink hyperlink-small palette-red"><span>Runner repos on GitLab</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
