---
base: ../
---

<section class="developers-stairs stairs content">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-10 col-xl-9" id="components">
        <span class="caption">Tripetto for developers</span>
        <h2 class="palette-main"><span>Every part of Tripetto</span> neatly packed into your application.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9">
        <p>The SDK's parts for form creation and deployment are designed for quick embedding in straightforward cases as well as <strong>deep integration into advanced applications</strong>.</p>
      </div>
    </div>
  </div>
  <div class="container container-stairs">
    <div class="row">
      <div class="col-12 stair-steps">
        <div class="stair-step stair-step-one">
          <img src="{{ page.base }}images/developers/drive.svg" alt="Illustration representing running forms in projects.">
          <div>
            <h2>Drive</h2>
            <p>The all-in-one <strong>form runner executes interactions</strong> created with the builder and handles UI rendering, UX, logic and response delivery.</p>
            <a href="#runners" class="button button-full anchor">Run forms in projects</a>
          </div>
        </div>
        <div class="stair-step stair-step-two">
          <img src="{{ page.base }}images/developers/create.svg" alt="Illustration representing building forms in projects.">
          <div>
            <h2>Create</h2>
            <p>The intelligent storyboard is for <strong>building forms like flowcharts</strong> and actively organizes their structures and logic for the optimal flow.</p>
            <a href="#builder" class="button button-full anchor">Build forms in projects</a>
          </div>
        </div>
        <div class="stair-step stair-step-three">
          <img src="{{ page.base }}images/developers/tailor.svg" alt="Illustration representing customizing things.">
          <div>
            <h2>Tailor</h2>
            <p>Deeply <strong>customize the toolset</strong> to specific project needs for optimal appearance, performance, security and response management.</p>
            <a href="#customization" class="button button-full anchor">Customize things</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
