---
base: ../
---

<section class="changelog-list">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 col-xl-9">
        <ul>
          {% for item in site.data.changelog %}
          <li>
            <div>
              <h2>{{ item.name }}</h2>
              {% if item.description %}<p>{{ item.description }}</p>{% endif %}
              <div>Released: {{ item.date | date_to_string }}</div>
              <small class="pills">
                <span class="palette-{{ item.type_class }} pill-splitter pill-splitter-after">{{ item.type }}</span>{% if item.areas contains 'studio' %}<a href="{{ page.base }}studio/" target="_blank" class="palette-studio">Studio</a>{% endif %}{% if item.areas contains 'wordpress' %}<a href="{{ page.base }}wordpress/" target="_blank" class="palette-wordpress">WordPress plugin</a>{% endif %}{% if item.areas contains 'sdk' %}<a href="{{ page.base }}developers/" target="_blank" class="palette-sdk">SDK</a>{% endif %}
              </small>
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
