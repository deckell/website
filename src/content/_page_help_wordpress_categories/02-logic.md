---
layout: help-category
base: ../../../
permalink: /help/wordpress/using-logic-features/
title: Using logic features in the WordPress plugin - Tripetto Help Center
description: Learn how to make your forms smart with advanced logic features.
category_area: wordpress
category_id: logic
---
