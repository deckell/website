---
layout: help-category
base: ../../../
permalink: /help/wordpress/managing-data-and-results/
title: Managing data and results in the WordPress plugin - Tripetto Help Center
description: Find out how you can use your response data and get notified of new results.
category_area: wordpress
category_id: hosting
redirect_from:
- /help/wordpress/get-results/
---
