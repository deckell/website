---
layout: help-category
base: ../../../
permalink: /help/wordpress/sharing-forms-and-surveys/
title: Sharing forms and surveys in the WordPress plugin - Tripetto Help Center
description: See how you can distribute your forms from the studio to your audience.
category_area: wordpress
category_id: sharing
redirect_from:
- /help/wordpress/run-forms/
---
