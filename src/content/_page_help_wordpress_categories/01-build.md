---
layout: help-category
base: ../../../
permalink: /help/wordpress/building-forms-and-surveys/
title: Building forms and surveys in the WordPress plugin - Tripetto Help Center
description: Get to know how the form builder works, from the simple basics to advanced tutorials.
category_area: wordpress
category_id: build
redirect_from:
- /help/wordpress/build-forms/
---
