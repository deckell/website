---
layout: help-category
base: ../../../
permalink: /help/wordpress/automating-things/
title: Automating things in the WordPress plugin - Tripetto Help Center
description: Learn how to automate actions with your data.
category_area: wordpress
category_id: automations
---
