---
base: ../
---

<section class="studio-mosaic content mosaic">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-10 col-xl-9">
        <h2><span>Enter Tripetto.</span> Wake-up call for form tool dinosaurs.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p>We merged the best of SurveyMonkey, Typeform and Landbot into a single conversational solution to let you build beautiful forms and surveys to <strong>boost completion rates for better insights</strong>.</p>
        <a href="{{ page.base }}product/" class="hyperlink"><span>Take the product tour</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
  <div class="container container-mosaic">
    <div class="row mosaic-row-top shape-before">
      <a href="{{ page.base }}product/#build" class="mosaic-block mosaic-block-horizontal palette-build">
        <div>
          <h3>Assistive Storyboard</h3>
          <p>Build forms and surveys like flowcharts.</p>
        </div>
      </a>
      <a href="{{ page.base }}product/#hosting" class="mosaic-block mosaic-block-vertical palette-hosting">
        <div>
          <h3>Data Storage Freedom</h3>
          <p>Store things at Tripetto or self-host whatever you like. Hello GDPR!</p>
        </div>
        <img src="{{ page.base }}images/chapters/hosting.svg" alt="Illustration representing data storage freedom" />
      </a>
      <a href="{{ page.base }}product/#customization" class="mosaic-block mosaic-block-horizontal palette-customization">
        <div>
          <h3>Convertible Form Faces</h3>
          <p>Present as a classic form, chat or Typeform-like interaction.</p>
        </div>
      </a>
    </div>
    <div class="row mosaic-row-middle">
      <a href="{{ page.base }}product/#sharing" class="mosaic-block mosaic-block-vertical palette-sharing">
        <div>
          <h3>Flexible Publication</h3>
          <p>Share a simple link or embed deeply.</p>
        </div>
        <img src="{{ page.base }}images/chapters/sharing.svg" alt="Illustration representing flexible publication" />
      </a>
      <a href="{{ page.base }}product/#logic" class="mosaic-block mosaic-block-horizontal palette-logic">
        <div>
          <h3>Conversational Logic Types</h3>
          <p>Create seriously conversational flows with logic, actions and calculations. No code, guaranteed.</p>
        </div>
      </a>
    </div>
    <div class="row mosaic-row-bottom shape-before shape-after">
      <a href="{{ page.base }}product/#automations" class="mosaic-block mosaic-block-vertical palette-automations">
        <div>
          <h3>Active Integrations</h3>
          <p>Automate things smartly.</p>
        </div>
      </a>
      <a href="{{ page.base }}product/" class="mosaic-more">And more...</a>
    </div>
  </div>
</section>
