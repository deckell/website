---
base: ../
---

<section class="studio-jumbo block-first jumbo">
  <div class="container jumbo-container">
    <div class="row">
      <div class="col jumbo-holder palette-studio shape-after">
        <div class="jumbo-content">
          <h1>Tripetto studio on the web.</h1>
          <p>Build and run fully customizable conversational forms and surveys from the Tripetto studio and choose where you want to store collected data.</p>
          <div>
            <a href="{{ site.url_app }}" class="button" target="_blank"><img src="{{ page.base }}images/tripetto-logo-white.svg" alt="Logo Tripetto"/>Start free, without account</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col jumbo-footer jumbo-footer-hyperlinks"><a href="{{ page.base }}wordpress/" class="hyperlink hyperlink-small"><span>Tripetto is also available for WordPress</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>

