---
base: ../
---

<section class="studio-stairs stairs">
  <div class="container">
    <div class="row">
      <div class="col-12 stair-steps">
        <div class="stair-step stair-step-one">
          <img src="{{ page.base }}images/studio/taste.svg" alt="Illustration representing examples of Tripetto">
          <div>
            <h2>Taste</h2>
            <p>See <strong>examples</strong> of what forms and surveys you can build for your projects in the Tripetto studio.</p>
            <a href="{{ page.base }}examples/" class="button button-full">Get inspired</a>
          </div>
        </div>
        <div class="stair-step stair-step-two">
          <img src="{{ page.base }}images/studio/begin.svg" alt="Illustration representing tutorials of Tripetto">
          <div>
            <h2>Begin</h2>
            <p>Quickly get a grasp of what’s possible with the Tripetto studio and start building fast with <strong>tutorials</strong>.</p>
            <a href="{{ page.base }}tutorials/" class="button button-full">Start easy</a>
          </div>
        </div>
        <div class="stair-step stair-step-three">
          <img src="{{ page.base }}images/studio/refine.svg" class="refine" alt="Illustration representing the help center of Tripetto">
          <div>
            <h2>Refine</h2>
            <p>Want pro tips? Something not working as expected? The <strong>help center</strong> takes you to the next level.</p>
            <a href="{{ page.base }}help/studio/" class="button button-full">Tips & tricks</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
