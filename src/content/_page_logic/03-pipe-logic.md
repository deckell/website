---
base: ../
---

<section class="logic-block logic-pipe" id="pipe_logic">
    <div class="container">
        <div class="row block-intro">
            <div class="col-md-11 col-xl-9">
                <h2>Pipe logic</h2>
                <p>Pipe logic enables the use of any previously given answer for display in other questions to personalize the form for respondents (and boost completion rates). This works even when you're looping through the same question for multiple previously selected answers.</p>
                <small>Others might call it <i>piping</i>, <i>question piping</i>, <i>answer piping</i>, <i>recall information</i> and/or <i>merge codes</i>.</small>
            </div>
        </div>
        <div class="row block-resources">
            <div class="col-lg-7">
                <h3 class="resources-example">Example</h3>
                <p>Imagine you're on IT support for your company. Employees can ask for support with a form that's focused on personal help. The form could start with gathering some personal information, so we know who’s asking for support. And we could use that input to give a short summary inside the form on the go. Next, the applicant can select multiple problems and for each problem point out what's wrong. We use the name of each selected problem in the follow-up.</p>
                <ul class="fa-ul resources-example">
                    <li><a href="{{ site.url_app }}/collect/6U6G2HXTZ1" target="_blank"><span class="fa-li"><i class="fas fa-running"></i></span>Run this example form</a></li>
                    <li><a href="{{ site.url_app }}/template/3HS5LJJ3T4" target="_blank"><span class="fa-li"><i class="fas fa-pencil-ruler"></i></span>View this template in the builder</a></li>
                    <li><a href="{{ site.url_app }}" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Start from scratch</a></li>
                </ul>
            </div>
            <div class="col-lg-5 offset-xl-1 col-xl-4">
                <h3>Help Center</h3>
                <ul class="fa-ul resources-help-center">
                    <li><a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" target="_blank"><span class="fa-li"><i class="fas fa-life-ring"></i></span>How to show respondents' answers using piping logic</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/" target="_blank"><span class="fa-li"><i class="fas fa-life-ring"></i></span>How to repeat follow-up for multiple selected options</a></li>
                    <li><a href="{{ page.base }}help/" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Visit our Help Center</a></li>
                </ul>
                <h3>Video Tutorials</h3>
                <ul class="fa-ul resources-tutorials">
                    <li><a data-toggle="modal" data-target="#videoModal" data-video="DgyYh_xPWuo"><span class="fa-li"><i class="fab fa-youtube"></i></span>Watch tutorial about advanced logic</a></li>
                    <li><a data-toggle="modal" data-target="#videoModal" data-video="S4VJ4qu3SIY"><span class="fa-li"><i class="fab fa-youtube"></i></span>Watch tutorial about piping logic</a></li>
                    <li><a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Visit our YouTube channel</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
