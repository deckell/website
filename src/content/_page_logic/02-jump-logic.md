---
base: ../
---

<section class="logic-block logic-jump" id="jump_logic">
    <div class="container">
        <div class="row block-intro">
            <div class="col-md-11 col-xl-9">
                <h2>Jump logic</h2>
                <p>Jump logic enables you to have respondents skip certain parts of your form and jump to selected other points in it to bypass unnecessary parts. Such a jump might even be straight to the end of the form at once.</p>
                <small>Others might call it <i>logic</i>, <i>logic jumps</i> and/or <i>routing</i>.</small>
            </div>
        </div>
        <div class="row block-resources">
            <div class="col-lg-7">
                <h3 class="resources-example">Example</h3>
                <p>In this use case we have a population screening regarding healthcare. We only want quality data, so we start with filtering respondents that didn't get any healthcare during the past year. We let that group jump to the end of the form immediately. And for all other respondents we include the desired questions, but also create some jumps to the right locations in the form; in this case based on the type of healthcare.</p>
                <ul class="fa-ul resources-example">
                    <li><a href="{{ site.url_app }}/collect/CECNCUJNQ5" target="_blank"><span class="fa-li"><i class="fas fa-running"></i></span>Run this example form</a></li>
                    <li><a href="{{ site.url_app }}/template/T9WFCOD2OE" target="_blank"><span class="fa-li"><i class="fas fa-pencil-ruler"></i></span>View this template in the builder</a></li>
                    <li><a href="{{ site.url_app }}" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Start from scratch</a></li>
                </ul>
            </div>
            <div class="col-lg-5 offset-xl-1 col-xl-4">
                <h3>Help Center</h3>
                <ul class="fa-ul resources-help-center">
                    <li><a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-respondents-answers/" target="_blank"><span class="fa-li"><i class="fas fa-life-ring"></i></span>How to set a follow-up based on respondents' answers</a></li>
                    <li><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/" target="_blank"><span class="fa-li"><i class="fas fa-life-ring"></i></span>How to jump to another point or the end</a></li>
                    <li><a href="{{ page.base }}help/" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Visit our Help Center</a></li>
                </ul>
                <h3>Video Tutorials</h3>
                <ul class="fa-ul resources-tutorials">
                    <li><a data-toggle="modal" data-target="#videoModal" data-video="pb01iB9UhDQ"><span class="fa-li"><i class="fab fa-youtube"></i></span>Watch tutorial about simple logic</a></li>
                    <li><a data-toggle="modal" data-target="#videoModal" data-video="DgyYh_xPWuo"><span class="fa-li"><i class="fab fa-youtube"></i></span>Watch tutorial about advanced logic</a></li>
                    <li><a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank"><span class="fa-li"><i class="fas fa-arrow-right"></i></span>Visit our YouTube channel</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
