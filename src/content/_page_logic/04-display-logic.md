---
base: ../
---

<section class="logic-block logic-display" id="display_logic">
  <div class="container">
    <div class="row block-intro">
      <div class="col-md-11 col-xl-9">
        <h2>Display logic</h2>
        <p>Display logic lets you determine to show or hide upcoming questions, depending on previously given answers. This way, you can funnel respondents even more precisely.</p>
      </div>
    </div>
  </div>
</section>
