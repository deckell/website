$(document).ready(function() {
    var help_area = window.localStorage.getItem("tripetto_help_area");

    if (help_area == "studio" || help_area == "wordpress" || help_area == "sdk") {
        var sArea = "";

        switch (help_area) {
            case "studio":
                sArea = "Studio";
                break;
            case "wordpress":
                sArea = "WordPress plugin";
                break;
            case "sdk":
                sArea = "SDK";
                break;
        }

        $("#help_breadcrumb_areas").removeClass("d-none");
        $("#help_breadcrumb_areas_name").text(sArea);
    }
});
