$(document).ready(function() {
    var articleSide = $("div.article-side");

    if (articleSide.length) {
        var positionTop = articleSide.offset().top;

        var fnScroll = function () {
            requestAnimationFrame(function () {
                if ($(window).scrollTop() > (positionTop-$("#header").height())) {
                    articleSide.addClass("article-side-sticky");
                } else {
                    articleSide.removeClass("article-side-sticky");
                }
            });
        };

        $(document).scroll(fnScroll);
        $(document).ready(fnScroll);
    }
});
