var handler = FS.Checkout.configure({
    plugin_id: "3825",
    public_key: "pk_bbe3e39e20ddf86c6ff4721c5e30e",
    plan_id: "6319",
    image: "https://tripetto.com/images/pricing/freemius_checkout.png"
});

$(".button-wordpress-purchase").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-single-monthly").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: 1,
        billing_cycle: "monthly"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-single-yearly").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: 1,
        billing_cycle: "annual"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-single-lifetime").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: 1,
        billing_cycle: "lifetime"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-five-monthly").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: 5,
        billing_cycle: "monthly"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-five-yearly").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: 5,
        billing_cycle: "annual"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-five-lifetime").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: 5,
        billing_cycle: "lifetime"
    });
    e.preventDefault();
});


$(".button-wordpress-purchase-unlimited-monthly").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: "unlimited",
        billing_cycle: "monthly"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-unlimited-yearly").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: "unlimited",
        billing_cycle: "annual"
    });
    e.preventDefault();
});

$(".button-wordpress-purchase-unlimited-lifetime").on("click", function(e) {
    handler.open({
        name: "Tripetto WordPress plugin - Premium",
        licenses: "unlimited",
        billing_cycle: "lifetime"
    });
    e.preventDefault();
});
