$(document).ready(function() {
    $(".matrix-nav-right").on("click", function() {
        $(".table-responsive").animate({
            scrollLeft: 290
        }, 500);
    });

    $(".matrix-nav-left").on("click", function() {
        $(".table-responsive").animate({
            scrollLeft: 0
        }, 500);
    });

    $(".table-responsive").on("scroll", function() {
        $(".table-responsive").scrollLeft($(this).scrollLeft() );
    });
});
