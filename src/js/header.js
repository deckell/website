$(document).ready(function () {
    var header = $("#header");

    if (header.length) {
        var fnScroll = function () {
            requestAnimationFrame(function () {
                if ($(this).scrollTop() > 40) {
                    header.addClass("background");
                } else {
                    header.removeClass("background");
                }
            });
        };

        $(document).scroll(fnScroll);
        $(document).ready(fnScroll);
    }

    $(".hamburger").on("click", function() {
        $(this).toggleClass('opened');
        $("#header").toggleClass('opened');
        $("#navOverlay").toggleClass('opened');
    });
});
