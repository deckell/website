var countdownStart = new Date("Nov 27, 2020 00:00:00").getTime();
var countdownEnd = new Date("Dec 4, 2020 23:59:59").getTime();
var staticNow = new Date().getTime();

var x = setInterval(function() {
    var now = new Date().getTime();
    var distanceToStart = countdownStart - now;
    var distanceToEnd = countdownEnd - now;
    var labelTitle= ""
    var labelDays = "";
    var labelHours = "";
    var labelMinutes = "";
    var labelSeconds = "";
    var labelButton = "";

    if (distanceToStart <= 0 && distanceToEnd <= 0) {
        clearInterval(x);

        labelTitle = "You're too late... Better luck next time!";
        labelDays = "--";
        labelHours = "--";
        labelMinutes = "--";
        labelSeconds = "--";
        labelButton = "You're too late";
    } else {
        var distance = distanceToStart > 0 ? distanceToStart : distanceToEnd;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        labelTitle = distanceToStart > 0 ? "Our <u>50% off</u> Black Friday sale goes live in:" : "Our <u>50% off</u> Black Friday sale is limited available for:";
        labelDays = (days < 10 ? "0" : "") + days;
        labelHours = (hours < 10 ? "0" : "") + hours;
        labelMinutes = (minutes < 10 ? "0" : "") + minutes;
        labelSeconds = (seconds < 10 ? "0" : "") + seconds;
        labelButton = distanceToStart > 0 ? "Come back Nov 27<sup>th</sup>" : "Get lifetime with 50% off";
    }

    document.getElementById("countdown_title").innerHTML = labelTitle;
    document.getElementById("countdown_d").innerHTML = labelDays;
    document.getElementById("countdown_h").innerHTML = labelHours;
    document.getElementById("countdown_m").innerHTML = labelMinutes;
    document.getElementById("countdown_s").innerHTML = labelSeconds;
    document.getElementById("button_single").innerHTML = labelButton;
    document.getElementById("button_five").innerHTML = labelButton;
}, 1000);

if (countdownStart <= staticNow && countdownEnd >= staticNow) {
    var handler = FS.Checkout.configure({
        plugin_id: "3825",
        public_key: "pk_bbe3e39e20ddf86c6ff4721c5e30e",
        plan_id: "6319",
        image: "https://tripetto.com/images/pricing/freemius_checkout.png"
    });

    $(".button-wordpress-purchase-single-lifetime").on("click", function(e) {
            handler.open({
                name: "Tripetto WordPress plugin - Premium",
                licenses: 1,
                billing_cycle: "lifetime",
                coupon: "FSBFCM2020"
            });
            e.preventDefault();
    });

    $(".button-wordpress-purchase-five-lifetime").on("click", function(e) {
            handler.open({
                name: "Tripetto WordPress plugin - Premium",
                licenses: 5,
                billing_cycle: "lifetime",
                coupon: "FSBFCM2020"
            });
            e.preventDefault();
    });
}
