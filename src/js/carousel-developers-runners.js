$(document).ready(function () {
    $("#carouselDevelopersRunners").carousel({
        interval: false,
        touch: false
    });

    $("#carouselDevelopersRunners").on("slid.bs.carousel", function () {
        $(".carousel-buttons li.active").removeClass("active");
        $(".carousel-buttons li[data-slide-to=" + $("div.carousel-item.active").index() + "]").addClass("active");
    });

    $(".carousel-buttons li").on("click", function () {
        $("#carouselDevelopersRunners").carousel(parseInt($(this).data("slide-to")));
        $(".carousel-buttons li.active").removeClass("active");
        $(this).addClass("active");
    });
});
