$(document).ready(function() {
    $("#videoModal").on("show.bs.modal", function(event) {
        var button = $(event.relatedTarget);
        var video = button.data("video");
        var modal = $(this);
        modal.find("#youtube").attr("src","https://www.youtube.com/embed/" + video + "?modestbranding=1&color=white&autoplay=1&loop=1&playlist=" + video);
    }).on("hide.bs.modal", function(event) {
        var modal = $(this);
        modal.find("#youtube").attr("src", "");
    });
});
